package com.bave.be.entity.serialz;

import java.util.Map;

import org.apache.kafka.common.serialization.Deserializer;

import com.bave.backend.persistence.entity.NotificationOneSignal;
import com.fasterxml.jackson.databind.ObjectMapper;

public class NotificationOneSignalDeserializer implements Deserializer {

	@Override
	public void close() {
		// TODO Auto-generated method stub
	}

	@Override
	public void configure(Map arg0, boolean arg1) {
		// TODO Auto-generated method stub
	}

	@Override
	public Object deserialize(String arg0, byte[] arg1) {
		ObjectMapper mapper = new ObjectMapper();
		NotificationOneSignal notification = null;
		try {
			notification = mapper.readValue(arg1, NotificationOneSignal.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return notification;
	}

}
