//package com.bave.backend;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Properties;
//import java.util.concurrent.ExecutionException;
//
//import org.apache.kafka.clients.producer.KafkaProducer;
//import org.apache.kafka.clients.producer.Producer;
//import org.apache.kafka.clients.producer.ProducerConfig;
//import org.apache.kafka.clients.producer.ProducerRecord;
//import org.apache.kafka.clients.producer.RecordMetadata;
//
//import com.bave.be.entity.CommonLog;
//import com.bave.be.entity.CommonLogSerializer;
//import com.bave.be.entity.Notification;
//import com.bave.be.entity.NotificationSerializer;
//import com.bave.be.entity.User;
//
//public class testProducer {
//	private final static String BOOTSTRAP_SERVERS = "localhost:9092";
//	private final static String TOPIC = "erpNotification";
//
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		// for (int i = 1; i <= 20; i++) {
//		// User user = new User();
//		// user.setName("Tung test");
//		// user.setChannel("app");
//		// user.setCallbackUrl(
//		// "http://test_mobile.bave.io:8888/update-message-to-mobile-state/QtFxcBKzysfG/1787/u/26");
//		// List lstAdr = new ArrayList();
//		// lstAdr.add("51a27de5-e089-4fb2-bb0a-53286c01820c");
//		// user.setListAddress(lstAdr);
//		//
//		// List lstUser = new ArrayList();
//		// lstUser.add(user);
//		// Notification noti = new Notification();
//		// noti.setListUser(lstUser);
//		// noti.setDataObject("fleet.workorder");
//		// noti.setDomain("test_mobile");
//		// noti.setAction("write");
//		// noti.setId(7L);
//		// noti.setMessage("ms: " + i);
//		// List lstDataRefId = new ArrayList();
//		// lstDataRefId.add("420");
//		// noti.setListDataRefId(lstDataRefId);
//		//
//		// sendMessage(noti);
//		// }
//
//		// for (int i = 0; i <= 5; i++) {
//		// CommonLog log1 = new CommonLog();
//		// log1.setGaraName("test log " + i);
//		// log1.setContent("test content log 123");
//		// log1.setLogType("gara" + i);
//		//
//		// sendLog(log1);
//		// }
//
//		System.out.println(convertPhone("321567654"));
//
//	}
//
//	public static String convertPhone(String phone) {
//		String result = "";
//		String firstLetter = String.valueOf(phone.charAt(0));
//		String secondLetter = String.valueOf(phone.charAt(1));
//
//		if ("8".equals(firstLetter) && "4".equals(secondLetter)) {
//			result = phone.substring(2);
//		} else if ("0".equals(firstLetter)) {
//			result = phone.substring(1);
//		} else {
//			result = phone;
//		}
//
//		return result;
//	}
//
//	private static void sendMessage(com.bave.be.entity.Notification notification) {
//		final Producer<String, com.bave.be.entity.Notification> producer = createProducer();
//		long time = System.currentTimeMillis();
//
//		try {
//			final ProducerRecord<String, com.bave.be.entity.Notification> record = new ProducerRecord<String, com.bave.be.entity.Notification>(
//					TOPIC, notification);
//
//			RecordMetadata metadata = producer.send(record).get();
//
//			long elapsedTime = System.currentTimeMillis() - time;
//			System.out.printf("sent record(key=%s value=%s) " + "meta(partition=%d, offset=%d) time=%d\n", record.key(),
//					record.value(), metadata.partition(), metadata.offset(), elapsedTime);
//
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (ExecutionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} finally {
//			producer.flush();
//			producer.close();
//		}
//	}
//
//	private static Producer<String, com.bave.be.entity.Notification> createProducer() {
//		Properties props = new Properties();
//		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
//		props.put(ProducerConfig.CLIENT_ID_CONFIG, "KafkaNotifiProducer2");
//		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, NotificationSerializer.class.getName());
//		// props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
//		// NotificationSerializer.class.getName());
//		props.put("value.serializer", "com.bave.be.entity.NotificationSerializer");
//
//		return new KafkaProducer<>(props);
//	}
//
//	private static void sendLog(CommonLog commonLog) {
//		final Producer<String, CommonLog> producer = createProducer2();
//		long time = System.currentTimeMillis();
//
//		try {
//			final ProducerRecord<String, CommonLog> record = new ProducerRecord<String, CommonLog>(TOPIC, commonLog);
//
//			RecordMetadata metadata = producer.send(record).get();
//
//			long elapsedTime = System.currentTimeMillis() - time;
//			System.out.printf("sent record(key=%s value=%s) " + "meta(partition=%d, offset=%d) time=%d\n", record.key(),
//					record.value(), metadata.partition(), metadata.offset(), elapsedTime);
//
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (ExecutionException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} finally {
//			producer.flush();
//			producer.close();
//		}
//	}
//
//	private static Producer<String, CommonLog> createProducer2() {
//		Properties props = new Properties();
//		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
//		props.put(ProducerConfig.CLIENT_ID_CONFIG, "CommonLogProducer");
//		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, CommonLogSerializer.class.getName());
//		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, CommonLogSerializer.class.getName());
//
//		return new KafkaProducer<>(props);
//	}
//}
