/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bave.backend.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author haitx
 */
@Entity
@Table(name = "notification")
@Getter
@Setter
@NoArgsConstructor
public class Notification implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notification_seq")
	@SequenceGenerator(name = "notification_seq", sequenceName = "notification_seq", allocationSize = 1, initialValue = 1)
	@JsonProperty("id")
	private Long id;
	@Column(name = "member_id")
	@JsonProperty("member_id")
	private Long memberId;
	@Column(name = "not_notify_id")
	@JsonProperty("not_notify_id")
	private Long notNotifyId;
	@Column(name = "domain")
	@JsonProperty("domain")
	private String domain;
	@Column(name = "address")
	@JsonProperty("address")
	private String address;
	@Column(name = "type")
	@JsonProperty("type")
	private String type;
	@Column(name = "action")
	@JsonProperty("action")
	private String action;
	@Column(name = "status")
	@JsonProperty("status")
	private String status;
	@Column(name = "create_date")
	@JsonProperty("create_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	@Column(name = "send_date")
	@JsonProperty("send_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date sendDate;
	@Column(name = "source_object")
	@JsonProperty("source_object")
	private String sourceObject;
	@Column(name = "source_ref_id")
	@JsonProperty("source_ref_id")
	private Long sourceRefId;
	@Column(name = "source_name")
	@JsonProperty("source_name")
	private String sourceName;
	@Column(name = "source_avatar")
	@JsonProperty("source_avatar")
	private String sourceAvatar;
	@Column(name = "destination_object")
	@JsonProperty("destination_object")
	private String destinationObject;
	@Column(name = "destination_ref_id")
	@JsonProperty("destination_ref_id")
	private Long destinationRefId;
	@Column(name = "dest_name")
	@JsonProperty("dest_name")
	private String destName;
	@Column(name = "dest_avatar")
	@JsonProperty("dest_avatar")
	private String destAvatar;
	@Column(name = "dest_shortcontent")
	@JsonProperty("dest_shortcontent")
	private String destShortcontent;
	@Column(name = "data_object")
	@JsonProperty("data_object")
	private String dataObject;
	@Column(name = "data_ref_id")
	@JsonProperty("data_ref_id")
	private String dataRefId;
	@Column(name = "data_name")
	@JsonProperty("data_name")
	private String dataName;
	@Column(name = "addition_data")
	@JsonProperty("addition_data")
	private String additionData;
	@Column(name = "datatype")
	@JsonProperty("datatype")
	private String datatype;
	@Column(name = "message")
	@JsonProperty("message")
	private String message;
	@Column(name = "title")
	@JsonProperty("title")
	private String title;
	@Column(name = "content")
	@JsonProperty("content")
	private String content;
	@Column(name = "template_id")
	@JsonProperty("template_id")
	private Integer templateId;
	@Column(name = "sequence")
	@JsonProperty("sequence")
	private String sequence;
	@Column(name = "callback_url")
	@JsonProperty("callback_url")
	private String callbackUrl;
	@Column(name = "app")
	@JsonProperty("app")
	private String app;
	@Column(name = "notification_id")
	@JsonProperty("notification_id")
	private Long notificationId;
	@Column(name = "sender_status")
	@JsonProperty("sender_status")
	private String senderStatus;
	@Column(name = "fail_reason")
	@JsonProperty("fail_reason")
	private String failReason;
	@Column(name = "seen")
	@JsonProperty("seen")
	private Boolean seen;
	@Column(name = "channel")
	@JsonProperty("channel")
	private String channel;
	@Transient
	@JsonProperty("lst_address")
	private List<String> lstAddress;

	@JsonProperty("data_ref_ids")
	@Transient
	private List<String> listDataRefId = new ArrayList<String>();

	@JsonProperty("users")
	@Transient
	private List<User> listUser = new ArrayList<User>();

	@JsonProperty("partners")
	@Transient
	private List<User> listPartner = new ArrayList<User>();

	@Transient
	@JsonProperty("mobile")
	private String mobile;

	@Transient
	@JsonProperty("phone")
	private String phone;

	@Transient
	@JsonProperty("root")
	private boolean root;

	@Column(name = "icon")
	@JsonProperty("icon")
	private String icon;

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		hash += (app != null ? app.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Notification)) {
			return false;
		}
		Notification other = (Notification) object;
		if (((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)))
				&& ((this.app == null && other.app != null) || (this.app != null && !this.app.equals(other.app)))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Notification[ seq=" + sequence + ", app=" + app + ", id=" + id + ", add: " + address + " ]";
	}

}
