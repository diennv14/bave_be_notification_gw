package com.bave.backend.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "erp_user_online")
public class ErpUser implements Serializable {

	@JsonProperty("id")
	@Id
	@Basic(optional = false)
	@Column(name = "id", columnDefinition = "NUMERIC")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "erp_user_online_seq")
	@SequenceGenerator(name = "erp_user_online_seq", sequenceName = "erp_user_online_seq", allocationSize = 1, initialValue = 1)
	private Integer id;
	@JsonProperty("domain")
	@Column(name = "domain")
	private String domain;
	@JsonProperty("erp_user_id")
	@Column(name = "erp_user_id", columnDefinition = "NUMERIC")
	private Integer erpUserId;
	@JsonProperty("player_id")
	@Column(name = "player_id")
	private String playerId;
	@JsonProperty("status")
	@Column(name = "status", columnDefinition = "NUMERIC")
	private String status;
	@JsonProperty("username")
	@Column(name = "erp_user_name")
	private String username;

	@JsonProperty("create_date")
	@Column(name = "create_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@JsonProperty("device_id")
	@Column(name = "device_id")
	private String deviceId;

	public ErpUser() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public Integer getErpUserId() {
		return erpUserId;
	}

	public void setErpUserId(Integer erpUserId) {
		this.erpUserId = erpUserId;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

}
