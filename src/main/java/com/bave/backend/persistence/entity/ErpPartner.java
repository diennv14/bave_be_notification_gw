package com.bave.backend.persistence.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the erp_partner database table.
 * 
 */
@Entity
@Table(name="erp_partner")
@NamedQuery(name="ErpPartner.findAll", query="SELECT e FROM ErpPartner e")
public class ErpPartner implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="ERP_PARTNER_ID_GENERATOR", sequenceName="ERP_PARTNER_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ERP_PARTNER_ID_GENERATOR")
	private Integer id;

	@Column(name="create_date")
	private Timestamp createDate;

	private Boolean customer;

	private String domain;

	private String email;

	@Column(name="invite_code_encode")
	private String inviteCodeEncode;

	@Column(name="member_id")
	private Integer memberId;

	private String mobile;

	private String name;

	@Column(name="partner_id")
	private Integer partnerId;

	private String phone;

	private String type;

	@Column(name="write_date")
	private Timestamp writeDate;

	public ErpPartner() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Boolean getCustomer() {
		return this.customer;
	}

	public void setCustomer(Boolean customer) {
		this.customer = customer;
	}

	public String getDomain() {
		return this.domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getInviteCodeEncode() {
		return this.inviteCodeEncode;
	}

	public void setInviteCodeEncode(String inviteCodeEncode) {
		this.inviteCodeEncode = inviteCodeEncode;
	}

	public Integer getMemberId() {
		return this.memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPartnerId() {
		return this.partnerId;
	}

	public void setPartnerId(Integer partnerId) {
		this.partnerId = partnerId;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Timestamp getWriteDate() {
		return this.writeDate;
	}

	public void setWriteDate(Timestamp writeDate) {
		this.writeDate = writeDate;
	}

}