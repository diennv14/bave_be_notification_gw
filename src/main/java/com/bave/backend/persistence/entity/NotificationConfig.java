package com.bave.backend.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "notification_config")
@Getter
@Setter
@NoArgsConstructor
public class NotificationConfig {
	@Id
	@Column(name = "id")
	@JsonProperty("id")
	private int id;
	@Column(name = "environment")
	@JsonProperty("environment")
	private String environment;
	@Column(name = "action")
	@JsonProperty("action")
	private String action;
	@Column(name = "channel")
	@JsonProperty("channel")
	private String channel;
	@Column(name = "app")
	@JsonProperty("app")
	private String app;
	@Column(name = "api_key")
	@JsonProperty("api_key")
	private String apiKey;
	@Column(name = "app_key")
	@JsonProperty("app_key")
	private String appKey;
	@Column(name = "active")
	@JsonProperty("active")
	private boolean active;

}
