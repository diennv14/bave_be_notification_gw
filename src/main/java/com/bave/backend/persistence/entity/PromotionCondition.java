
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bave.backend.persistence.entity;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Luu Lam Thanh Tung
 */
@Entity
@Table(name = "promotion_condition")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "PromotionCondition.findAll", query = "SELECT p FROM PromotionCondition p"),
		@NamedQuery(name = "PromotionCondition.findById", query = "SELECT p FROM PromotionCondition p WHERE p.id = :id"),
		@NamedQuery(name = "PromotionCondition.findByPmNewId", query = "SELECT p FROM PromotionCondition p WHERE p.pmNewId = :pmNewId"),
		@NamedQuery(name = "PromotionCondition.findByStatus", query = "SELECT p FROM PromotionCondition p WHERE p.status = :status"),
		@NamedQuery(name = "PromotionCondition.findByBookingTimeLimit", query = "SELECT p FROM PromotionCondition p WHERE p.bookingTimeLimit = :bookingTimeLimit"),
		@NamedQuery(name = "PromotionCondition.findByMemberType", query = "SELECT p FROM PromotionCondition p WHERE p.memberType = :memberType"),
		@NamedQuery(name = "PromotionCondition.findByMemberValue", query = "SELECT p FROM PromotionCondition p WHERE p.memberValue = :memberValue"),
		@NamedQuery(name = "PromotionCondition.findByArea", query = "SELECT p FROM PromotionCondition p WHERE p.area = :area"),
		@NamedQuery(name = "PromotionCondition.findByVehicleBrandCode", query = "SELECT p FROM PromotionCondition p WHERE p.vehicleBrandCode = :vehicleBrandCode"),
		@NamedQuery(name = "PromotionCondition.findByVehicleModelCode", query = "SELECT p FROM PromotionCondition p WHERE p.vehicleModelCode = :vehicleModelCode"),
		@NamedQuery(name = "PromotionCondition.findByVehicleGenCode", query = "SELECT p FROM PromotionCondition p WHERE p.vehicleGenCode = :vehicleGenCode"),
		@NamedQuery(name = "PromotionCondition.findByPackageName", query = "SELECT p FROM PromotionCondition p WHERE p.packageName = :packageName") })
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PromotionCondition implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	private Integer id;
	@Column(name = "pm_new_id")
	@JsonProperty("pm_new_id")
	private Integer pmNewId;
	@Size(max = 255)
	private String status;
	@Column(name = "booking_time_limit")
	@JsonProperty("booking_time_limit")
	private Integer bookingTimeLimit;
	@Size(max = 255)
	@Column(name = "member_type")
	@JsonProperty("member_type")
	private String memberType;
	@Size(max = 255)
	@Column(name = "member_value")
	@JsonProperty("member_value")
	private String memberValue;
	@Size(max = 255)
	private String area;
	@Size(max = 255)
	@Column(name = "vehicle_brand_code")
	@JsonProperty("vehicle_brand_code")
	private String vehicleBrandCode;
	@Size(max = 255)
	@Column(name = "vehicle_model_code")
	@JsonProperty("vehicle_model_code")
	private String vehicleModelCode;
	@Size(max = 255)
	@Column(name = "vehicle_gen_code")
	@JsonProperty("vehicle_gen_code")
	private String vehicleGenCode;
	@Size(max = 255)
	@Column(name = "package_name")
	@JsonProperty("package_name")
	private String packageName;
	@Column(name = "brand_id")
	@JsonProperty("brand_id")
	private Integer brandId;
	@Column(name = "model_id")
	@JsonProperty("model_id")
	private Integer modelId;
	@Column(name = "ve_gen_id")
	@JsonProperty("ve_gen_id")
	private Integer veGenId;

	public PromotionCondition() {
	}

	public Integer getBrandId() {
		return brandId;
	}

	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}

	public Integer getModelId() {
		return modelId;
	}

	public void setModelId(Integer modelId) {
		this.modelId = modelId;
	}

	public Integer getVeGenId() {
		return veGenId;
	}

	public void setVeGenId(Integer veGenId) {
		this.veGenId = veGenId;
	}

	public PromotionCondition(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPmNewId() {
		return pmNewId;
	}

	public void setPmNewId(Integer pmNewId) {
		this.pmNewId = pmNewId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getBookingTimeLimit() {
		return bookingTimeLimit;
	}

	public void setBookingTimeLimit(Integer bookingTimeLimit) {
		this.bookingTimeLimit = bookingTimeLimit;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getMemberValue() {
		return memberValue;
	}

	public void setMemberValue(String memberValue) {
		this.memberValue = memberValue;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getVehicleBrandCode() {
		return vehicleBrandCode;
	}

	public void setVehicleBrandCode(String vehicleBrandCode) {
		this.vehicleBrandCode = vehicleBrandCode;
	}

	public String getVehicleModelCode() {
		return vehicleModelCode;
	}

	public void setVehicleModelCode(String vehicleModelCode) {
		this.vehicleModelCode = vehicleModelCode;
	}

	public String getVehicleGenCode() {
		return vehicleGenCode;
	}

	public void setVehicleGenCode(String vehicleGenCode) {
		this.vehicleGenCode = vehicleGenCode;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof PromotionCondition)) {
			return false;
		}
		PromotionCondition other = (PromotionCondition) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.bave.backend.persistence.entity.PromotionCondition[ id=" + id + " ]";
	}

}