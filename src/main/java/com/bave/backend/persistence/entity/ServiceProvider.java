/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bave.backend.persistence.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import com.bave.backend.util.RpcXmlConfig;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author haitx
 */
@Entity
@Table(name = "service_provider")
@JsonInclude(Include.NON_NULL)
public class ServiceProvider implements Serializable {

	public static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "id", columnDefinition = "NUMERIC")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "service_provider_id_seq")
	@SequenceGenerator(name = "service_provider_id_seq", sequenceName = "service_provider_id_seq", allocationSize = 1)
	private Integer id;
	@JsonIgnore
	@Column(name = "spo_id", columnDefinition = "NUMERIC")
	private Long spoId;
	@Column(name = "name")
	private String name;
	@Column(name = "type")
	private String type;
	@Column(name = "code")
	private String code;
	@Column(name = "description")
	private String description;
	@Column(name = "address")
	private String address;
	// @Size(max = 200)
	// @Column(name = "avatar")
	@Transient
	private String avatar;
	@Transient
	private String banner;

	@Transient
	@JsonProperty("map_image")
	private String mapImage;

	// @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$",
	// message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the
	// field contains phone or fax number consider using this annotation to
	// enforce field validation
	@Column(name = "phone")
	private String phone;
	@Column(name = "website")
	private String website;
	@Column(name = "introduction")
	private String introduction;
	@Column(name = "likecount", columnDefinition = "NUMERIC")
	private Long likecount;
	@Column(name = "viewcount", columnDefinition = "NUMERIC")
	private Long viewcount;
	@Column(name = "sharecount", columnDefinition = "NUMERIC")
	private Long sharecount;
	@Column(name = "commentcount", columnDefinition = "NUMERIC")
	private Long commentcount;
	@JsonProperty("ratecount")
	@Column(name = "ratecount", columnDefinition = "NUMERIC")
	private Long rateCount;
	@Column(name = "ranking", columnDefinition = "NUMERIC")
	private Float ranking;
	@Column(name = "opentime")
	private String opentime;
	@Column(name = "closetime")
	private String closetime;
	// @Max(value=?) @Min(value=?)//if you know range of your decimal fields
	// consider using these annotations to enforce field validation
	@Column(name = "longitude")
	private Double longitude;
	@Column(name = "latitude")
	private Double latitude;
	@JsonIgnore
	@Column(name = "notify")
	private String notify;
	@JsonIgnore
	@Column(name = "notify_type")
	private String notifyType;
	@JsonIgnore
	@Column(name = "notify_mobile")
	private String notifyMobile;
	@JsonIgnore
	@Column(name = "notify_email")
	private String notifyEmail;
	@JsonIgnore
	@Column(name = "notify_time")
	private String notifyTime;
	@JsonIgnore
	@Column(name = "g_place_id")
	private String gPlaceId;
	@JsonIgnore
	@Column(name = "country_id", columnDefinition = "NUMERIC")
	private Long countryId;
	@JsonIgnore
	@Column(name = "province_id", columnDefinition = "NUMERIC")
	private Long provinceId;

	@Transient
	@JsonProperty("lst_brand_id")
	private List<Integer> lstBrandId;

	@Transient
	@JsonProperty("lst_rate")
	private Map<String, Float> lst_rate = new HashMap<>();

	@Transient
	@JsonProperty("member_bookmarked")
	private Boolean memberBookmarked;

	@Transient
	@JsonProperty("member_liked")
	private Boolean memberLiked = false;

	@Column(name = "district_id", columnDefinition = "NUMERIC")
	private Integer districtId;

	@Column(name = "state_id", columnDefinition = "NUMERIC")
	private Integer stateId;

	@Column(name = "active")
	private Boolean active;

	@Column(name = "facebook")
	private String facebook;

	@Column(name = "email")
	private String email;

	@Column(name = "unread_notify")
	private Integer unreadNotify;

	@Column(name = "create_date")
	private Date createDate;

	@Column(name = "write_date")
	private Date writeDate;

	// @Column(name = "bookmark")
	// @Transient
	// private String bookmark;

	@Transient
	private Double distance;

	// @Column(name = "service_provider_categ_id")
	// @JsonProperty("sp_categ_id")
	// private Integer serviceProviderCategId;
	// @Column(name = "service_provider_categ_name")
	// @JsonProperty("sp_categ_id")
	// private Integer serviceProviderCategName;

	@Transient
	@JsonProperty("member_id")
	private Long memberId;

	@Transient
	@JsonProperty("repair_last_date")
	private Date repairLastDate;

	public ServiceProvider() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getSpoId() {
		return spoId;
	}

	public void setSpoId(Long spoId) {
		this.spoId = spoId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAvatar() {
		if (avatar == null) {
			String model = this.getClass().getAnnotation(Table.class).name();
			avatar = String.format(RpcXmlConfig.AVATAR_MEDIUM_URL, model.replace("_", "."), getId());
		}
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getBanner() {
		if (banner == null) {
			String model = this.getClass().getAnnotation(Table.class).name();
			banner = String.format(RpcXmlConfig.BANNER_MEDIUM_URL, model.replace("_", "."), getId());
		}
		return banner;
	}

	public void setBanner(String banner) {
		this.banner = banner;
	}

	public String getMapImage() {
		if (mapImage == null) {
			String model = this.getClass().getAnnotation(Table.class).name();
			mapImage = String.format(RpcXmlConfig.MAP_URL, model.replace("_", "."), getId());
		}
		return mapImage;
	}

	public void setMapImage(String mapImage) {
		this.mapImage = mapImage;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public Long getLikecount() {
		return likecount;
	}

	public void setLikecount(Long likecount) {
		this.likecount = likecount;
	}

	public Long getViewcount() {
		return viewcount;
	}

	public void setViewcount(Long viewcount) {
		this.viewcount = viewcount;
	}

	public Long getSharecount() {
		return sharecount;
	}

	public void setSharecount(Long sharecount) {
		this.sharecount = sharecount;
	}

	public Long getCommentcount() {
		return commentcount;
	}

	public void setCommentcount(Long commentcount) {
		this.commentcount = commentcount;
	}

	public Float getRanking() {
		return this.ranking;
	}

	public void setRanking(Float ranking) {
		this.ranking = ranking;
	}

	public String getOpentime() {
		return opentime;
	}

	public void setOpentime(String opentime) {
		this.opentime = opentime;
	}

	public String getClosetime() {
		return closetime;
	}

	public void setClosetime(String closetime) {
		this.closetime = closetime;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public String getNotify() {
		return notify;
	}

	public void setNotify(String notify) {
		this.notify = notify;
	}

	public String getNotifyType() {
		return notifyType;
	}

	public void setNotifyType(String notifyType) {
		this.notifyType = notifyType;
	}

	public String getNotifyMobile() {
		return notifyMobile;
	}

	public void setNotifyMobile(String notifyMobile) {
		this.notifyMobile = notifyMobile;
	}

	public String getNotifyEmail() {
		return notifyEmail;
	}

	public void setNotifyEmail(String notifyEmail) {
		this.notifyEmail = notifyEmail;
	}

	public String getNotifyTime() {
		return notifyTime;
	}

	public void setNotifyTime(String notifyTime) {
		this.notifyTime = notifyTime;
	}

	public String getGPlaceId() {
		return gPlaceId;
	}

	public void setGPlaceId(String gPlaceId) {
		this.gPlaceId = gPlaceId;
	}

	public Long getCountryId() {
		return countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	public Long getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}

	public String getgPlaceId() {
		return gPlaceId;
	}

	public void setgPlaceId(String gPlaceId) {
		this.gPlaceId = gPlaceId;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public Long getRateCount() {
		return rateCount;
	}

	public void setRateCount(Long rateCount) {
		this.rateCount = rateCount;
	}

	// public Integer getServiceProviderCategId() {
	// return serviceProviderCategId;
	// }
	//
	// public void setServiceProviderCategId(Integer serviceProviderCategId) {
	// this.serviceProviderCategId = serviceProviderCategId;
	// }

	public List<Integer> getLstBrandId() {
		return lstBrandId;
	}

	public void setLstBrandId(List<Integer> lstBrandId) {
		this.lstBrandId = lstBrandId;
	}

	@Transient
	public Map<String, Float> getLst_rate() {
		return lst_rate;
	}

	public void setLst_rate(Map<String, Float> lst_rate) {
		this.lst_rate = lst_rate;
	}

	// public String getBookmark() {
	// return bookmark;
	// }
	//
	// public void setBookmark(String bookmark) {
	// this.bookmark = bookmark;
	// }

	public Boolean getMemberBookmarked() {
		return memberBookmarked;
	}

	public void setMemberBookmarked(Boolean memberBookmarked) {
		this.memberBookmarked = memberBookmarked;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getUnreadNotify() {
		return unreadNotify;
	}

	public void setUnreadNotify(Integer unreadNotify) {
		this.unreadNotify = unreadNotify;
	}

	public Boolean getMemberLiked() {
		return memberLiked;
	}

	public void setMemberLiked(Boolean memberLiked) {
		this.memberLiked = memberLiked;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getWriteDate() {
		return writeDate;
	}

	public void setWriteDate(Date writeDate) {
		this.writeDate = writeDate;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Date getRepairLastDate() {
		return repairLastDate;
	}

	public void setRepairLastDate(Date repairLastDate) {
		this.repairLastDate = repairLastDate;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof ServiceProvider)) {
			return false;
		}
		ServiceProvider other = (ServiceProvider) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.bave.backend.persistence.entity.ServiceProvider[ id=" + id + " ]";
	}

}
