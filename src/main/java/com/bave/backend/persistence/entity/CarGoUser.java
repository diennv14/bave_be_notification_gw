/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bave.backend.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author haitx
 */
//@Entity
@Table(name = "member_device")
@XmlRootElement
public class CarGoUser implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "member_id", columnDefinition = "NUMERIC")
	private Long memberId;
	@Id
	@Column(name = "device_id")
	private String deviceId;
	@Column(name = "online")
	private String online;
	@Column(name = "active")
	@Id
	private String active;

	@Column(name = "player_id")
	private String playerId;

	@JsonProperty("username")
	@Column(name = "username")
	private String username;

	public CarGoUser() {
	}

	public CarGoUser(Long memberId, String deviceId, String online, String active) {
		super();
		this.memberId = memberId;
		this.deviceId = deviceId;
		this.online = online;
		this.active = active;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getOnline() {
		return online;
	}

	public void setOnline(String online) {
		this.online = online;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (memberId != null ? memberId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof CarGoUser)) {
			return false;
		}
		CarGoUser other = (CarGoUser) object;
		if ((this.memberId == null && other.memberId != null)
				|| (this.memberId != null && !this.memberId.equals(other.memberId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.bave.backend.persistence.entity.MemberDevice[ memberId=" + memberId + " ]";
	}

}
