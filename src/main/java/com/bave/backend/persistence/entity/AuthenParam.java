package com.bave.backend.persistence.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthenParam {
	public String db;
	public String username;
	public String password;
	public String uid;
	public String tz;
	public String lang;
	public String playerId;

	public AuthenParam() {
		super();
	}

	public AuthenParam(String db, String username, String password) {
		super();
		this.db = db;
		this.username = username;
		this.password = password;
	}

	public AuthenParam(String db, String username, String password, String uid) {
		super();
		this.db = db;
		this.username = username;
		this.password = password;
		this.uid = uid;
	}

	public AuthenParam(String db, String username, String password, String uid, String tz) {
		super();
		this.db = db;
		this.username = username;
		this.password = password;
		this.uid = uid;
		this.tz = tz;
	}

	public AuthenParam(String db, String username, String password, String uid, String tz, String lang,
			String playerId) {
		super();
		this.db = db;
		this.username = username;
		this.password = password;
		this.uid = uid;
		this.tz = tz;
		this.lang = lang;
		this.playerId = playerId;
	}

	public String getDb() {
		return db;
	}

	public void setDb(String db) {
		this.db = db;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getTz() {
		return tz;
	}

	public void setTz(String tz) {
		this.tz = tz;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

}
