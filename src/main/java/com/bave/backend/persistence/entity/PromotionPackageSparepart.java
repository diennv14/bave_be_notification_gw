///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.bave.backend.persistence.entity;
//
//import java.io.Serializable;
//import javax.persistence.Basic;
//import javax.persistence.Column;
//import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
//import javax.persistence.Id;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
//import javax.persistence.Table;
//import javax.validation.constraints.Size;
//import javax.xml.bind.annotation.XmlRootElement;
//
//import com.fasterxml.jackson.annotation.JsonProperty;
//
///**
// *
// * @author Luu Lam Thanh Tung
// */
//@Entity
//@Table(name = "promotion_package")
//@XmlRootElement
//@NamedQueries({ @NamedQuery(name = "PromotionPackage.findAll", query = "SELECT p FROM PromotionPackage p"),
//		@NamedQuery(name = "PromotionPackage.findById", query = "SELECT p FROM PromotionPackage p WHERE p.id = :id"),
//		@NamedQuery(name = "PromotionPackage.findByPmNewId", query = "SELECT p FROM PromotionPackage p WHERE p.pmNewId = :pmNewId"),
//		@NamedQuery(name = "PromotionPackage.findByPackageId", query = "SELECT p FROM PromotionPackage p WHERE p.packageId = :packageId"),
//		@NamedQuery(name = "PromotionPackage.findByPackagePriceId", query = "SELECT p FROM PromotionPackage p WHERE p.packagePriceId = :packagePriceId"),
//		@NamedQuery(name = "PromotionPackage.findByPackageCode", query = "SELECT p FROM PromotionPackage p WHERE p.packageCode = :packageCode"),
//		@NamedQuery(name = "PromotionPackage.findByDomain", query = "SELECT p FROM PromotionPackage p WHERE p.domain = :domain") })
//public class PromotionPackage implements Serializable {
//
//	private static final long serialVersionUID = 1L;
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Basic(optional = false)
//	@JsonProperty("id")
//	private Integer id;
//	@Column(name = "pm_new_id")
//	@JsonProperty("pm_new_id")
//	private Integer pmNewId;
//	@Column(name = "package_id")
//	@JsonProperty("package_id")
//	private Integer packageId;
//	@Column(name = "package_price_id")
//	@JsonProperty("package_price_id")
//	private Integer packagePriceId;
//	@Size(max = 255)
//	@Column(name = "package_code")
//	@JsonProperty("package_code")
//	private String packageCode;
//	@Size(max = 255)
//	@Column(name = "domain")
//	@JsonProperty("domain")
//	private String domain;
//	@Column(name = "status")
//	@JsonProperty("status")
//	private String status;
//	@Column(name = "booking_time_limit")
//	@JsonProperty("booking_time_limit")
//	private Integer bookingTimeLimit;
//
//	public PromotionPackage() {
//	}
//
//	public PromotionPackage(Integer id) {
//		this.id = id;
//	}
//
//	public Integer getId() {
//		return id;
//	}
//
//	public String getStatus() {
//		return status;
//	}
//
//	public void setStatus(String status) {
//		this.status = status;
//	}
//
//	public Integer getBookingTimeLimit() {
//		return bookingTimeLimit;
//	}
//
//	public void setBookingTimeLimit(Integer bookingTimeLimit) {
//		this.bookingTimeLimit = bookingTimeLimit;
//	}
//
//	public void setId(Integer id) {
//		this.id = id;
//	}
//
//	public Integer getPmNewId() {
//		return pmNewId;
//	}
//
//	public void setPmNewId(Integer pmNewId) {
//		this.pmNewId = pmNewId;
//	}
//
//	public Integer getPackageId() {
//		return packageId;
//	}
//
//	public void setPackageId(Integer packageId) {
//		this.packageId = packageId;
//	}
//
//	public Integer getPackagePriceId() {
//		return packagePriceId;
//	}
//
//	public void setPackagePriceId(Integer packagePriceId) {
//		this.packagePriceId = packagePriceId;
//	}
//
//	public String getPackageCode() {
//		return packageCode;
//	}
//
//	public void setPackageCode(String packageCode) {
//		this.packageCode = packageCode;
//	}
//
//	public String getDomain() {
//		return domain;
//	}
//
//	public void setDomain(String domain) {
//		this.domain = domain;
//	}
//
//	@Override
//	public int hashCode() {
//		int hash = 0;
//		hash += (id != null ? id.hashCode() : 0);
//		return hash;
//	}
//
//	@Override
//	public boolean equals(Object object) {
//		// TODO: Warning - this method won't work in the case the id fields are
//		// not set
//		if (!(object instanceof PromotionPackage)) {
//			return false;
//		}
//		PromotionPackage other = (PromotionPackage) object;
//		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
//			return false;
//		}
//		return true;
//	}
//
//	@Override
//	public String toString() {
//		return "com.bave.backend.api.object.PromotionPackage[ id=" + id + " ]";
//	}
//
//}
