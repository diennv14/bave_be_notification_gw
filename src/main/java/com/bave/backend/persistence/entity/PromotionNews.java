
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bave.backend.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "promotion_news")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "PromotionNews.findAll", query = "SELECT p FROM PromotionNews p"),
		@NamedQuery(name = "PromotionNews.findByPmNewId", query = "SELECT p FROM PromotionNews p WHERE p.pmNewId = :pmNewId"),
		@NamedQuery(name = "PromotionNews.findByPmId", query = "SELECT p FROM PromotionNews p WHERE p.pmId = :pmId"),
		@NamedQuery(name = "PromotionNews.findByObjectTypeDest", query = "SELECT p FROM PromotionNews p WHERE p.objectTypeDest = :objectTypeDest"),
		@NamedQuery(name = "PromotionNews.findByObjectValueDest", query = "SELECT p FROM PromotionNews p WHERE p.objectValueDest = :objectValueDest"),
		@NamedQuery(name = "PromotionNews.findByNotificationId", query = "SELECT p FROM PromotionNews p WHERE p.notificationId = :notificationId"),
		@NamedQuery(name = "PromotionNews.findByCreaterUserType", query = "SELECT p FROM PromotionNews p WHERE p.createrUserType = :createrUserType"),
		@NamedQuery(name = "PromotionNews.findByCreaterUserDo", query = "SELECT p FROM PromotionNews p WHERE p.createrUserDo = :createrUserDo"),
		@NamedQuery(name = "PromotionNews.findByCreateDatetime", query = "SELECT p FROM PromotionNews p WHERE p.createDatetime = :createDatetime"),
		@NamedQuery(name = "PromotionNews.findByApprovedUserType", query = "SELECT p FROM PromotionNews p WHERE p.approvedUserType = :approvedUserType"),
		@NamedQuery(name = "PromotionNews.findByApprovedMethod", query = "SELECT p FROM PromotionNews p WHERE p.approvedMethod = :approvedMethod"),
		@NamedQuery(name = "PromotionNews.findByApprovedUserDo", query = "SELECT p FROM PromotionNews p WHERE p.approvedUserDo = :approvedUserDo"),
		@NamedQuery(name = "PromotionNews.findByApprovedDatetime", query = "SELECT p FROM PromotionNews p WHERE p.approvedDatetime = :approvedDatetime"),
		@NamedQuery(name = "PromotionNews.findByWebImageRepresent", query = "SELECT p FROM PromotionNews p WHERE p.webImageRepresent = :webImageRepresent"),
		@NamedQuery(name = "PromotionNews.findByWebTitle", query = "SELECT p FROM PromotionNews p WHERE p.webTitle = :webTitle"),
		@NamedQuery(name = "PromotionNews.findByWebShortContent", query = "SELECT p FROM PromotionNews p WHERE p.webShortContent = :webShortContent"),
		@NamedQuery(name = "PromotionNews.findByWebContent", query = "SELECT p FROM PromotionNews p WHERE p.webContent = :webContent"),
		@NamedQuery(name = "PromotionNews.findByWebUrlDetail", query = "SELECT p FROM PromotionNews p WHERE p.webUrlDetail = :webUrlDetail"),
		@NamedQuery(name = "PromotionNews.findByMobileImageRepresent", query = "SELECT p FROM PromotionNews p WHERE p.mobileImageRepresent = :mobileImageRepresent"),
		@NamedQuery(name = "PromotionNews.findByMobileTitle", query = "SELECT p FROM PromotionNews p WHERE p.mobileTitle = :mobileTitle"),
		@NamedQuery(name = "PromotionNews.findByMobileShortContent", query = "SELECT p FROM PromotionNews p WHERE p.mobileShortContent = :mobileShortContent"),
		@NamedQuery(name = "PromotionNews.findByMobileContent", query = "SELECT p FROM PromotionNews p WHERE p.mobileContent = :mobileContent"),
		@NamedQuery(name = "PromotionNews.findByMobileUrlDetail", query = "SELECT p FROM PromotionNews p WHERE p.mobileUrlDetail = :mobileUrlDetail"),
		@NamedQuery(name = "PromotionNews.findByApproveLevel", query = "SELECT p FROM PromotionNews p WHERE p.approveLevel = :approveLevel"),
		@NamedQuery(name = "PromotionNews.findByApproveStatus", query = "SELECT p FROM PromotionNews p WHERE p.approveStatus = :approveStatus"),
		@NamedQuery(name = "PromotionNews.findByMerchantApproveStatus", query = "SELECT p FROM PromotionNews p WHERE p.merchantApproveStatus = :merchantApproveStatus"),
		@NamedQuery(name = "PromotionNews.findByGoType", query = "SELECT p FROM PromotionNews p WHERE p.goType = :goType"),
		@NamedQuery(name = "PromotionNews.findByGoValue", query = "SELECT p FROM PromotionNews p WHERE p.goValue = :goValue"),
		@NamedQuery(name = "PromotionNews.findByStartDatetime", query = "SELECT p FROM PromotionNews p WHERE p.startDatetime = :startDatetime"),
		@NamedQuery(name = "PromotionNews.findByExpectFinishDatetime", query = "SELECT p FROM PromotionNews p WHERE p.expectFinishDatetime = :expectFinishDatetime"),
		@NamedQuery(name = "PromotionNews.findByRealFinishDatetime", query = "SELECT p FROM PromotionNews p WHERE p.realFinishDatetime = :realFinishDatetime"),
		@NamedQuery(name = "PromotionNews.findByStatus", query = "SELECT p FROM PromotionNews p WHERE p.status = :status") })
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PromotionNews implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "pm_new_id")
	@JsonProperty("pm_new_id")
	private Long pmNewId;
	@Column(name = "pm_id")
	@JsonProperty("pm_id")
	private Integer pmId;
	@Size(max = 2147483647)
	@Column(name = "object_type_dest")
	@JsonProperty("object_type_dest")
	private String objectTypeDest;
	@Size(max = 2147483647)
	@Column(name = "object_value_dest")
	@JsonProperty("object_value_dest")
	private String objectValueDest;
	@Column(name = "notification_id")
	@JsonProperty("notification_id")
	private Long notificationId;
	@Size(max = 2147483647)
	@Column(name = "creater_user_type")
	@JsonProperty("creater_user_type")
	private String createrUserType;
	@Size(max = 2147483647)
	@Column(name = "creater_user_do")
	@JsonProperty("creater_user_do")
	private String createrUserDo;
	@Column(name = "create_datetime")
	@JsonProperty("create_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDatetime;
	@Size(max = 2147483647)
	@Column(name = "approved_user_type")
	@JsonProperty("approved_user_type")
	private String approvedUserType;
	@Size(max = 2147483647)
	@Column(name = "approved_method")
	@JsonProperty("approved_method")
	private String approvedMethod;
	@Size(max = 2147483647)
	@Column(name = "approved_user_do")
	@JsonProperty("approved_user_do")
	private String approvedUserDo;
	@Column(name = "approved_datetime")
	@JsonProperty("approved_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date approvedDatetime;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 2147483647)
	@Column(name = "web_image_represent")
	@JsonProperty("web_image_represent")
	private String webImageRepresent;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 2147483647)
	@Column(name = "web_title")
	@JsonProperty("web_title")
	private String webTitle;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 2147483647)
	@Column(name = "web_short_content")
	@JsonProperty("web_short_content")
	private String webShortContent;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 2147483647)
	@Column(name = "web_content")
	@JsonProperty("web_content")
	private String webContent;
	@Size(max = 2147483647)
	@Column(name = "web_url_detail")
	@JsonProperty("web_url_detail")
	private String webUrlDetail;
	@Size(max = 2147483647)
	@Column(name = "mobile_image_represent")
	@JsonProperty("mobile_image_represent")
	private String mobileImageRepresent;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 2147483647)
	@Column(name = "mobile_title")
	@JsonProperty("mobile_title")
	private String mobileTitle;
	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 2147483647)
	@Column(name = "mobile_short_content")
	@JsonProperty("mobile_short_content")
	private String mobileShortContent;
	@Size(max = 2147483647)
	@Column(name = "mobile_content")
	@JsonProperty("mobile_content")
	private String mobileContent;
	@Size(max = 2147483647)
	@Column(name = "mobile_url_detail")
	@JsonProperty("mobile_url_detail")
	private String mobileUrlDetail;
	@Column(name = "approve_level")
	@JsonProperty("approve_level")
	private Integer approveLevel;
	@Size(max = 2147483647)
	@Column(name = "approve_status")
	@JsonProperty("approve_status")
	private String approveStatus;
	@Size(max = 2147483647)
	@Column(name = "merchant_approve_status")
	@JsonProperty("merchant_approve_status")
	private String merchantApproveStatus;
	@Column(name = "go_type")
	@JsonProperty("go_type")
	private Integer goType;
	@Size(max = 2147483647)
	@Column(name = "go_value")
	@JsonProperty("go_value")
	private String goValue;
	@Column(name = "start_datetime")
	@JsonProperty("start_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDatetime;
	@Column(name = "expect_finish_datetime")
	@JsonProperty("expect_finish_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date expectFinishDatetime;
	@Column(name = "real_finish_datetime")
	@JsonProperty("real_finish_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date realFinishDatetime;
	private String status;
//	@Transient
//	@JsonProperty("lst_package_info")
//	private List<ServicePackage> lstPackageInfo = new ArrayList<ServicePackage>();
	@Column(name = "package_id")
	@JsonProperty("package_id")
	private Integer packageId;
	@Column(name = "package_price_id")
	@JsonProperty("package_price_id")
	private Integer packagePriceId;
	@Column(name = "domain")
	@JsonProperty("domain")
	private String domain;
	@Column(name = "start_date")
	@JsonProperty("start_date")
	private Date startDate;
	@Column(name = "end_date")
	@JsonProperty("end_date")
	private Date endDate;

	public PromotionNews() {
	}

	public PromotionNews(Long pmNewId) {
		this.pmNewId = pmNewId;
	}

	public PromotionNews(Long pmNewId, String webImageRepresent, String webTitle, String webShortContent,
			String webContent, String mobileTitle, String mobileShortContent) {
		this.pmNewId = pmNewId;
		this.webImageRepresent = webImageRepresent;
		this.webTitle = webTitle;
		this.webShortContent = webShortContent;
		this.webContent = webContent;
		this.mobileTitle = mobileTitle;
		this.mobileShortContent = mobileShortContent;
	}

	public Long getPmNewId() {
		return pmNewId;
	}

	public void setPmNewId(Long pmNewId) {
		this.pmNewId = pmNewId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getPmId() {
		return pmId;
	}

	public void setPmId(Integer pmId) {
		this.pmId = pmId;
	}

//	public List<ServicePackage> getLstPackageInfo() {
//		return lstPackageInfo;
//	}
//
//	public void setLstPackageInfo(List<ServicePackage> lstPackageInfo) {
//		this.lstPackageInfo = lstPackageInfo;
//	}

	public Integer getPackageId() {
		return packageId;
	}

	public void setPackageId(Integer packageId) {
		this.packageId = packageId;
	}

	public Integer getPackagePriceId() {
		return packagePriceId;
	}

	public void setPackagePriceId(Integer packagePriceId) {
		this.packagePriceId = packagePriceId;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public String getObjectTypeDest() {
		return objectTypeDest;
	}

	public void setObjectTypeDest(String objectTypeDest) {
		this.objectTypeDest = objectTypeDest;
	}

	public String getObjectValueDest() {
		return objectValueDest;
	}

	public void setObjectValueDest(String objectValueDest) {
		this.objectValueDest = objectValueDest;
	}

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public String getCreaterUserType() {
		return createrUserType;
	}

	public void setCreaterUserType(String createrUserType) {
		this.createrUserType = createrUserType;
	}

	public String getCreaterUserDo() {
		return createrUserDo;
	}

	public void setCreaterUserDo(String createrUserDo) {
		this.createrUserDo = createrUserDo;
	}

	public Date getCreateDatetime() {
		return createDatetime;
	}

	public void setCreateDatetime(Date createDatetime) {
		this.createDatetime = createDatetime;
	}

	public String getApprovedUserType() {
		return approvedUserType;
	}

	public void setApprovedUserType(String approvedUserType) {
		this.approvedUserType = approvedUserType;
	}

	public String getApprovedMethod() {
		return approvedMethod;
	}

	public void setApprovedMethod(String approvedMethod) {
		this.approvedMethod = approvedMethod;
	}

	public String getApprovedUserDo() {
		return approvedUserDo;
	}

	public void setApprovedUserDo(String approvedUserDo) {
		this.approvedUserDo = approvedUserDo;
	}

	public Date getApprovedDatetime() {
		return approvedDatetime;
	}

	public void setApprovedDatetime(Date approvedDatetime) {
		this.approvedDatetime = approvedDatetime;
	}

	public String getWebImageRepresent() {
		return webImageRepresent;
	}

	public void setWebImageRepresent(String webImageRepresent) {
		this.webImageRepresent = webImageRepresent;
	}

	public String getWebTitle() {
		return webTitle;
	}

	public void setWebTitle(String webTitle) {
		this.webTitle = webTitle;
	}

	public String getWebShortContent() {
		return webShortContent;
	}

	public void setWebShortContent(String webShortContent) {
		this.webShortContent = webShortContent;
	}

	public String getWebContent() {
		return webContent;
	}

	public void setWebContent(String webContent) {
		this.webContent = webContent;
	}

	public String getWebUrlDetail() {
		return webUrlDetail;
	}

	public void setWebUrlDetail(String webUrlDetail) {
		this.webUrlDetail = webUrlDetail;
	}

	public String getMobileImageRepresent() {
		return mobileImageRepresent;
	}

	public void setMobileImageRepresent(String mobileImageRepresent) {
		this.mobileImageRepresent = mobileImageRepresent;
	}

	public String getMobileTitle() {
		return mobileTitle;
	}

	public void setMobileTitle(String mobileTitle) {
		this.mobileTitle = mobileTitle;
	}

	public String getMobileShortContent() {
		return mobileShortContent;
	}

	public void setMobileShortContent(String mobileShortContent) {
		this.mobileShortContent = mobileShortContent;
	}

	public String getMobileContent() {
		return mobileContent;
	}

	public void setMobileContent(String mobileContent) {
		this.mobileContent = mobileContent;
	}

	public String getMobileUrlDetail() {
		return mobileUrlDetail;
	}

	public void setMobileUrlDetail(String mobileUrlDetail) {
		this.mobileUrlDetail = mobileUrlDetail;
	}

	public Integer getApproveLevel() {
		return approveLevel;
	}

	public void setApproveLevel(Integer approveLevel) {
		this.approveLevel = approveLevel;
	}

	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	public String getMerchantApproveStatus() {
		return merchantApproveStatus;
	}

	public void setMerchantApproveStatus(String merchantApproveStatus) {
		this.merchantApproveStatus = merchantApproveStatus;
	}

	public Integer getGoType() {
		return goType;
	}

	public void setGoType(Integer goType) {
		this.goType = goType;
	}

	public String getGoValue() {
		return goValue;
	}

	public void setGoValue(String goValue) {
		this.goValue = goValue;
	}

	public Date getStartDatetime() {
		return startDatetime;
	}

	public void setStartDatetime(Date startDatetime) {
		this.startDatetime = startDatetime;
	}

	public Date getExpectFinishDatetime() {
		return expectFinishDatetime;
	}

	public void setExpectFinishDatetime(Date expectFinishDatetime) {
		this.expectFinishDatetime = expectFinishDatetime;
	}

	public Date getRealFinishDatetime() {
		return realFinishDatetime;
	}

	public void setRealFinishDatetime(Date realFinishDatetime) {
		this.realFinishDatetime = realFinishDatetime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (pmNewId != null ? pmNewId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof PromotionNews)) {
			return false;
		}
		PromotionNews other = (PromotionNews) object;
		if ((this.pmNewId == null && other.pmNewId != null)
				|| (this.pmNewId != null && !this.pmNewId.equals(other.pmNewId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.bave.backend.persistence.entity.PromotionNews[ pmNewId=" + pmNewId + " ]";
	}

}