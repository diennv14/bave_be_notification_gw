
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bave.backend.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Luu Lam Thanh Tung
 */
@Entity
@Table(name = "promotion_package")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "PromotionPackage.findAll", query = "SELECT p FROM PromotionPackage p"),
		@NamedQuery(name = "PromotionPackage.findById", query = "SELECT p FROM PromotionPackage p WHERE p.id = :id"),
		@NamedQuery(name = "PromotionPackage.findByPmNewId", query = "SELECT p FROM PromotionPackage p WHERE p.pmNewId = :pmNewId"),
		@NamedQuery(name = "PromotionPackage.findByType", query = "SELECT p FROM PromotionPackage p WHERE p.type = :type"),
		@NamedQuery(name = "PromotionPackage.findByValue", query = "SELECT p FROM PromotionPackage p WHERE p.value = :value") })
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PromotionPackage implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	private Integer id;
	@Column(name = "pm_new_id")
	@JsonProperty("pm_new_id")
	private Integer pmNewId;
	@Size(max = 255)
	private String type;
	@Size(max = 255)
	private String value;
	@Transient
	private String domain;
	@Transient
	private Double discountValue;
	@Transient
	private String discountType;
	@Transient
	private Integer bookingTimeLimit;
	@Transient
	private Integer bookingTime;
//	@Transient
//	private List<PromotionLine> lstPromotionLine = new ArrayList<>();
	@Transient
	private Date lastBookingTime;
	@Transient
	private String timeLimitUnit;
	@Transient
	private Date startDate;
	@Transient
	private Date endDate;

	public PromotionPackage() {
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public PromotionPackage(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBookingTime() {
		return bookingTime;
	}

	public void setBookingTime(Integer bookingTime) {
		this.bookingTime = bookingTime;
	}

	public Integer getPmNewId() {
		return pmNewId;
	}

	public void setPmNewId(Integer pmNewId) {
		this.pmNewId = pmNewId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Double getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(Double discountValue) {
		this.discountValue = discountValue;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public Integer getBookingTimeLimit() {
		return bookingTimeLimit;
	}

	public void setBookingTimeLimit(Integer bookingTimeLimit) {
		this.bookingTimeLimit = bookingTimeLimit;
	}

//	public List<PromotionLine> getLstPromotionLine() {
//		return lstPromotionLine;
//	}
//
//	public void setLstPromotionLine(List<PromotionLine> lstPromotionLine) {
//		this.lstPromotionLine = lstPromotionLine;
//	}

	public Date getLastBookingTime() {
		return lastBookingTime;
	}

	public void setLastBookingTime(Date lastBookingTime) {
		this.lastBookingTime = lastBookingTime;
	}

	public String getTimeLimitUnit() {
		return timeLimitUnit;
	}

	public void setTimeLimitUnit(String timeLimitUnit) {
		this.timeLimitUnit = timeLimitUnit;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof PromotionPackage)) {
			return false;
		}
		PromotionPackage other = (PromotionPackage) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.bave.backend.persistence.entity.PromotionPackage[ id=" + id + " ]";
	}

}