
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bave.backend.persistence.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author admin
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Entity
public class CarserviceMaintainReminder implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@JsonProperty("id")
	@Column(name = "id")
	private Integer id;
	@JsonProperty("note")
	@Column(name = "note")
	private String note;
	@JsonProperty("create_date")
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	private Date createDate;
	@Column(name = "name")
	@JsonProperty("name")
	private String name;
	@Column(name = "time_reminder")
	@JsonProperty("time_reminder")
	private Date timeReminder;
	@JsonProperty("priority")
	@Column(name = "priority")
	private String priority;
	@JsonProperty("license_plate")
	@Column(name = "license_plate")
	private String licensePlate;
	@Column(name = "car_name")
	@JsonProperty("car_name")
	private String carName;
	@Column(name = "status")
	@JsonProperty("status")
	private String status;
	@Column(name = "seen")
	@JsonProperty("seen")
	private boolean seen;
	@Column(name = "state")
	@JsonProperty("state")
	private String state;
	@JsonProperty("partner_id")
	private Integer partnerId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getTimeReminder() {
		return timeReminder;
	}

	public void setTimeReminder(Date timeReminder) {
		this.timeReminder = timeReminder;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isSeen() {
		return seen;
	}

	public void setSeen(boolean seen) {
		this.seen = seen;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Integer getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Integer partnerId) {
		this.partnerId = partnerId;
	}

}