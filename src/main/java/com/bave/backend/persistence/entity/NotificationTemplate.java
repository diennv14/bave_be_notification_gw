package com.bave.backend.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "notification_template")
@Getter
@Setter
@NoArgsConstructor
public class NotificationTemplate {
	@Id
	@Column(name = "id")
	@JsonProperty("id")
	private int id;
	@Column(name = "name")
	@JsonProperty("name")
	private String name;
	@Column(name = "description")
	@JsonProperty("description")
	private String description;
	@Column(name = "message")
	@JsonProperty("message")
	private String message;
	@Column(name = "active")
	@JsonProperty("active")
	private boolean active;
}
