
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bave.backend.persistence.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author admin
 */
@Entity
@Table(name = "user_notification_sticky")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "UserNotificationSticky.findAll", query = "SELECT n FROM UserNotificationSticky n"),
		@NamedQuery(name = "UserNotificationSticky.findById", query = "SELECT n FROM UserNotificationSticky n WHERE n.id = :id"),
		@NamedQuery(name = "UserNotificationSticky.findByMemberId", query = "SELECT n FROM UserNotificationSticky n WHERE n.memberId = :memberId"),
		@NamedQuery(name = "UserNotificationSticky.findByNotNotifyId", query = "SELECT n FROM UserNotificationSticky n WHERE n.notNotifyId = :notNotifyId"),
		@NamedQuery(name = "UserNotificationSticky.findByDomain", query = "SELECT n FROM UserNotificationSticky n WHERE n.domain = :domain"),
		// @NamedQuery(name = "UserNotificationSticky.findByAddress", query =
		// "SELECT n FROM UserNotificationSticky n WHERE n.address = :address"),
		@NamedQuery(name = "UserNotificationSticky.findByType", query = "SELECT n FROM UserNotificationSticky n WHERE n.type = :type"),
		@NamedQuery(name = "UserNotificationSticky.findByAction", query = "SELECT n FROM UserNotificationSticky n WHERE n.action = :action"),
		@NamedQuery(name = "UserNotificationSticky.findByStatus", query = "SELECT n FROM UserNotificationSticky n WHERE n.status = :status"),
		@NamedQuery(name = "UserNotificationSticky.findByCreateDate", query = "SELECT n FROM UserNotificationSticky n WHERE n.createDate = :createDate"),
		@NamedQuery(name = "UserNotificationSticky.findBySourceObject", query = "SELECT n FROM UserNotificationSticky n WHERE n.sourceObject = :sourceObject"),
		@NamedQuery(name = "UserNotificationSticky.findBySourceRefId", query = "SELECT n FROM UserNotificationSticky n WHERE n.sourceRefId = :sourceRefId"),
		@NamedQuery(name = "UserNotificationSticky.findBySourceName", query = "SELECT n FROM UserNotificationSticky n WHERE n.sourceName = :sourceName"),
		@NamedQuery(name = "UserNotificationSticky.findBySourceAvatar", query = "SELECT n FROM UserNotificationSticky n WHERE n.sourceAvatar = :sourceAvatar"),
		@NamedQuery(name = "UserNotificationSticky.findByDestinationObject", query = "SELECT n FROM UserNotificationSticky n WHERE n.destinationObject = :destinationObject"),
		@NamedQuery(name = "UserNotificationSticky.findByDestinationRefId", query = "SELECT n FROM UserNotificationSticky n WHERE n.destinationRefId = :destinationRefId"),
		@NamedQuery(name = "UserNotificationSticky.findByDestName", query = "SELECT n FROM UserNotificationSticky n WHERE n.destName = :destName"),
		@NamedQuery(name = "UserNotificationSticky.findByDestAvatar", query = "SELECT n FROM UserNotificationSticky n WHERE n.destAvatar = :destAvatar"),
		@NamedQuery(name = "UserNotificationSticky.findByShortContent", query = "SELECT n FROM UserNotificationSticky n WHERE n.shortContent = :shortContent"),
		@NamedQuery(name = "UserNotificationSticky.findByDataObject", query = "SELECT n FROM UserNotificationSticky n WHERE n.dataObject = :dataObject"),
		@NamedQuery(name = "UserNotificationSticky.findByDataRefId", query = "SELECT n FROM UserNotificationSticky n WHERE n.dataRefId = :dataRefId"),
		@NamedQuery(name = "UserNotificationSticky.findByDataName", query = "SELECT n FROM UserNotificationSticky n WHERE n.dataName = :dataName"),
		@NamedQuery(name = "UserNotificationSticky.findByAdditionData", query = "SELECT n FROM UserNotificationSticky n WHERE n.additionData = :additionData"),
		@NamedQuery(name = "UserNotificationSticky.findByDatatype", query = "SELECT n FROM UserNotificationSticky n WHERE n.datatype = :datatype"),
		@NamedQuery(name = "UserNotificationSticky.findByMessage", query = "SELECT n FROM UserNotificationSticky n WHERE n.message = :message"),
		@NamedQuery(name = "UserNotificationSticky.findByTitle", query = "SELECT n FROM UserNotificationSticky n WHERE n.title = :title"),
		@NamedQuery(name = "UserNotificationSticky.findByContent", query = "SELECT n FROM UserNotificationSticky n WHERE n.content = :content"),
		@NamedQuery(name = "UserNotificationSticky.findByTemplateId", query = "SELECT n FROM UserNotificationSticky n WHERE n.templateId = :templateId"),
		@NamedQuery(name = "UserNotificationSticky.findBySequence", query = "SELECT n FROM UserNotificationSticky n WHERE n.sequence = :sequence"),
		@NamedQuery(name = "UserNotificationSticky.findByCallbackUrl", query = "SELECT n FROM UserNotificationSticky n WHERE n.callbackUrl = :callbackUrl"),
		@NamedQuery(name = "UserNotificationSticky.findByApp", query = "SELECT n FROM UserNotificationSticky n WHERE n.app = :app"),
		@NamedQuery(name = "UserNotificationSticky.findByNotificationId", query = "SELECT n FROM UserNotificationSticky n WHERE n.notificationId = :notificationId"),
		@NamedQuery(name = "UserNotificationSticky.findBySeen", query = "SELECT n FROM UserNotificationSticky n WHERE n.seen = :seen"),
		@NamedQuery(name = "UserNotificationSticky.findBySendDate", query = "SELECT n FROM UserNotificationSticky n WHERE n.sendDate = :sendDate"),
		@NamedQuery(name = "UserNotificationSticky.findBySenderStatus", query = "SELECT n FROM UserNotificationSticky n WHERE n.senderStatus = :senderStatus"),
		@NamedQuery(name = "UserNotificationSticky.findByFailReason", query = "SELECT n FROM UserNotificationSticky n WHERE n.failReason = :failReason"),
		@NamedQuery(name = "UserNotificationSticky.findByChannel", query = "SELECT n FROM UserNotificationSticky n WHERE n.channel = :channel"),
		@NamedQuery(name = "UserNotificationSticky.findByIcon", query = "SELECT n FROM UserNotificationSticky n WHERE n.icon = :icon"),
		@NamedQuery(name = "UserNotificationSticky.findBySourceType", query = "SELECT n FROM UserNotificationSticky n WHERE n.sourceType = :sourceType"),
		@NamedQuery(name = "UserNotificationSticky.findByCreaterUserType", query = "SELECT n FROM UserNotificationSticky n WHERE n.createrUserType = :createrUserType"),
		@NamedQuery(name = "UserNotificationSticky.findByCreaterUserDo", query = "SELECT n FROM UserNotificationSticky n WHERE n.createrUserDo = :createrUserDo"),
		@NamedQuery(name = "UserNotificationSticky.findByApprovedUserType", query = "SELECT n FROM UserNotificationSticky n WHERE n.approvedUserType = :approvedUserType"),
		@NamedQuery(name = "UserNotificationSticky.findByApprovedMethod", query = "SELECT n FROM UserNotificationSticky n WHERE n.approvedMethod = :approvedMethod"),
		@NamedQuery(name = "UserNotificationSticky.findByApprovedUserDo", query = "SELECT n FROM UserNotificationSticky n WHERE n.approvedUserDo = :approvedUserDo"),
		@NamedQuery(name = "UserNotificationSticky.findByApprovedDatetime", query = "SELECT n FROM UserNotificationSticky n WHERE n.approvedDatetime = :approvedDatetime"),
		@NamedQuery(name = "UserNotificationSticky.findByStartDatetime", query = "SELECT n FROM UserNotificationSticky n WHERE n.startDatetime = :startDatetime"),
		@NamedQuery(name = "UserNotificationSticky.findByRealTimeToLive", query = "SELECT n FROM UserNotificationSticky n WHERE n.realTimeToLive = :realTimeToLive"),
		@NamedQuery(name = "UserNotificationSticky.findByExpectFinishDatetime", query = "SELECT n FROM UserNotificationSticky n WHERE n.expectFinishDatetime = :expectFinishDatetime"),
		@NamedQuery(name = "UserNotificationSticky.findByRealFinishDatetime", query = "SELECT n FROM UserNotificationSticky n WHERE n.realFinishDatetime = :realFinishDatetime"),
		@NamedQuery(name = "UserNotificationSticky.findByPriority", query = "SELECT n FROM UserNotificationSticky n WHERE n.priority = :priority") })
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserNotificationSticky implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@NotNull
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_user_notification_sticky")
	@SequenceGenerator(name = "seq_user_notification_sticky", sequenceName = "seq_user_notification_sticky", allocationSize = 1)
	@Column(name = "id")
	@JsonProperty("id")
	private Long id;
	@Column(name = "member_id")
	@JsonProperty("member_id")
	private Long memberId;
	@Column(name = "not_notify_id")
	@JsonProperty("not_notify_id")
	private Long notNotifyId;
	@Size(max = 255)
	@Column(name = "domain")
	@JsonProperty("domain")
	private String domain;
	// @Size(max = 255)
	// @Column(name = "address")
	@JsonInclude()
	@JsonProperty("address")
	private String address;
	@Size(max = 2147483647)
	@Column(name = "type")
	@JsonProperty("type")
	private String type;
	@Size(max = 2147483647)
	@Column(name = "action")
	@JsonProperty("action")
	private String action;
	@Size(max = 2147483647)
	@Column(name = "status")
	@JsonProperty("status")
	private String status;
	@Column(name = "create_date")
	@JsonProperty("create_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	@Size(max = 2147483647)
	@Column(name = "source_object")
	@JsonProperty("source_object")
	private String sourceObject;
	@Column(name = "source_ref_id")
	@JsonProperty("source_ref_id")
	private BigInteger sourceRefId;
	@Size(max = 2147483647)
	@Column(name = "source_name")
	@JsonProperty("source_name")
	private String sourceName;
	@Size(max = 2147483647)
	@Column(name = "source_avatar")
	@JsonProperty("source_avatar")
	private String sourceAvatar;
	@Size(max = 2147483647)
	@Column(name = "destination_object")
	@JsonProperty("destination_object")
	private String destinationObject;
	@Column(name = "destination_ref_id")
	@JsonProperty("destination_ref_id")
	private Long destinationRefId;
	@Size(max = 2147483647)
	@Column(name = "dest_name")
	@JsonProperty("dest_name")
	private String destName;
	@Size(max = 2147483647)
	@Column(name = "dest_avatar")
	@JsonProperty("dest_avatar")
	private String destAvatar;
	@Size(max = 2147483647)
	@Column(name = "short_content")
	@JsonProperty("short_content")
	private String shortContent;
	@Size(max = 255)
	@Column(name = "data_object")
	@JsonProperty("data_object")
	private String dataObject;
	@Size(max = 400)
	@Column(name = "data_ref_id")
	@JsonProperty("data_ref_id")
	private String dataRefId;
	@Size(max = 255)
	@Column(name = "data_name")
	@JsonProperty("data_name")
	private String dataName;
	@Size(max = 300)
	@Column(name = "addition_data")
	@JsonProperty("addition_data")
	private String additionData;
	@Size(max = 255)
	@Column(name = "datatype")
	@JsonProperty("datatype")
	private String datatype;
	@Size(max = 2147483647)
	@Column(name = "message")
	@JsonProperty("message")
	private String message;
	@Size(max = 400)
	@Column(name = "title")
	@JsonProperty("title")
	private String title;
	@Size(max = 2000)
	@Column(name = "content")
	@JsonProperty("content")
	private String content;
	@Column(name = "template_id")
	@JsonProperty("template_id")
	private Integer templateId;
	@Size(max = 255)
	@Column(name = "sequence")
	@JsonProperty("sequence")
	private String sequence;
	@Size(max = 400)
	@Column(name = "callback_url")
	@JsonProperty("callback_url")
	private String callbackUrl;
	@Size(max = 20)
	@Column(name = "app")
	@JsonProperty("app")
	private String app;
	@Column(name = "notification_id")
	@JsonProperty("notification_id")
	private Long notificationId;
	@Column(name = "seen")
	@JsonProperty("seen")
	private Boolean seen;
	@Column(name = "send_date")
	@JsonProperty("send_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date sendDate;
	@Size(max = 20)
	@Column(name = "sender_status")
	@JsonProperty("sender_status")
	private String senderStatus;
	@Size(max = 255)
	@Column(name = "fail_reason")
	@JsonProperty("fail_reason")
	private String failReason;
	@Size(max = 255)
	@Column(name = "channel")
	@JsonProperty("channel")
	private String channel;
	@Size(max = 255)
	@Column(name = "icon")
	@JsonProperty("icon")
	private String icon;
	@Size(max = 255)
	@Column(name = "source_type")
	@JsonProperty("source_type")
	private String sourceType;
	@Size(max = 2147483647)
	@Column(name = "creater_user_type")
	@JsonProperty("creater_user_type")
	private String createrUserType;
	@Size(max = 255)
	@Column(name = "creater_user_do")
	@JsonProperty("creater_user_do")
	private String createrUserDo;
	@Size(max = 255)
	@Column(name = "approved_user_type")
	@JsonProperty("approved_user_type")
	private String approvedUserType;
	@Size(max = 255)
	@Column(name = "approved_method")
	@JsonProperty("approved_method")
	private String approvedMethod;
	@Size(max = 255)
	@Column(name = "approved_user_do")
	@JsonProperty("approved_user_do")
	private String approvedUserDo;
	@Column(name = "approved_datetime")
	@JsonProperty("approved_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date approvedDatetime;
	@Column(name = "start_datetime")
	@JsonProperty("start_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDatetime;
	@Column(name = "real_time_to_live")
	@JsonProperty("real_time_to_live")
	@Temporal(TemporalType.TIMESTAMP)
	private Date realTimeToLive;
	@Column(name = "expect_finish_datetime")
	@JsonProperty("expect_finish_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date expectFinishDatetime;
	@Column(name = "real_finish_datetime")
	@JsonProperty("real_finish_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date realFinishDatetime;
	@Column(name = "priority")
	@JsonProperty("priority")
	private BigInteger priority;

	@JsonProperty("lst_address")
	@Transient
	private List<String> lstAddress = new ArrayList<String>();

	@Column(name = "notify_code")
	@JsonProperty("notify_code")
	private String notifyCode;
	
	@Column(name = "ownServiceCode")
	@JsonProperty("ownServiceCode")
	private String ownServiceCode;

	public UserNotificationSticky() {
	}

	public UserNotificationSticky(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public String getNotifyCode() {
		return notifyCode;
	}

	public void setNotifyCode(String notifyCode) {
		this.notifyCode = notifyCode;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Transient
	public List<String> getLstAddress() {
		return lstAddress;
	}

	public void setLstAddress(List<String> lstAddress) {
		this.lstAddress = lstAddress;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public Long getNotNotifyId() {
		return notNotifyId;
	}

	public void setNotNotifyId(Long notNotifyId) {
		this.notNotifyId = notNotifyId;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	@Transient
	public String getAddress() {
		return address;
	}

	@Transient
	public void setAddress(String address) {
		this.address = address;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getSourceObject() {
		return sourceObject;
	}

	public void setSourceObject(String sourceObject) {
		this.sourceObject = sourceObject;
	}

	public BigInteger getSourceRefId() {
		return sourceRefId;
	}

	public void setSourceRefId(BigInteger sourceRefId) {
		this.sourceRefId = sourceRefId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getSourceAvatar() {
		return sourceAvatar;
	}

	public void setSourceAvatar(String sourceAvatar) {
		this.sourceAvatar = sourceAvatar;
	}

	public String getDestinationObject() {
		return destinationObject;
	}

	public void setDestinationObject(String destinationObject) {
		this.destinationObject = destinationObject;
	}

	public Long getDestinationRefId() {
		return destinationRefId;
	}

	public void setDestinationRefId(Long destinationRefId) {
		this.destinationRefId = destinationRefId;
	}

	public String getDestName() {
		return destName;
	}

	public void setDestName(String destName) {
		this.destName = destName;
	}

	public String getDestAvatar() {
		return destAvatar;
	}

	public void setDestAvatar(String destAvatar) {
		this.destAvatar = destAvatar;
	}

	public String getShortContent() {
		return shortContent;
	}

	public void setShortContent(String shortContent) {
		this.shortContent = shortContent;
	}

	public String getDataObject() {
		return dataObject;
	}

	public void setDataObject(String dataObject) {
		this.dataObject = dataObject;
	}

	public String getDataRefId() {
		return dataRefId;
	}

	public void setDataRefId(String dataRefId) {
		this.dataRefId = dataRefId;
	}

	public String getDataName() {
		return dataName;
	}

	public void setDataName(String dataName) {
		this.dataName = dataName;
	}

	public String getAdditionData() {
		return additionData;
	}

	public void setAdditionData(String additionData) {
		this.additionData = additionData;
	}

	public String getDatatype() {
		return datatype;
	}

	public void setDatatype(String datatype) {
		this.datatype = datatype;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getTemplateId() {
		return templateId;
	}

	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}

	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	public String getCallbackUrl() {
		return callbackUrl;
	}

	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}

	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		this.app = app;
	}

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public Boolean getSeen() {
		if(this.seen == null) {
			this.seen = false;
		}
		return seen;
	}

	public void setSeen(Boolean seen) {
		this.seen = seen;
	}

	public Date getSendDate() {
		return sendDate;
	}

	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}

	public String getSenderStatus() {
		return senderStatus;
	}

	public void setSenderStatus(String senderStatus) {
		this.senderStatus = senderStatus;
	}

	public String getFailReason() {
		return failReason;
	}

	public void setFailReason(String failReason) {
		this.failReason = failReason;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getCreaterUserType() {
		return createrUserType;
	}

	public void setCreaterUserType(String createrUserType) {
		this.createrUserType = createrUserType;
	}

	public String getCreaterUserDo() {
		return createrUserDo;
	}

	public void setCreaterUserDo(String createrUserDo) {
		this.createrUserDo = createrUserDo;
	}

	public String getApprovedUserType() {
		return approvedUserType;
	}

	public void setApprovedUserType(String approvedUserType) {
		this.approvedUserType = approvedUserType;
	}

	public String getApprovedMethod() {
		return approvedMethod;
	}

	public void setApprovedMethod(String approvedMethod) {
		this.approvedMethod = approvedMethod;
	}

	public String getApprovedUserDo() {
		return approvedUserDo;
	}

	public void setApprovedUserDo(String approvedUserDo) {
		this.approvedUserDo = approvedUserDo;
	}

	public Date getApprovedDatetime() {
		return approvedDatetime;
	}

	public void setApprovedDatetime(Date approvedDatetime) {
		this.approvedDatetime = approvedDatetime;
	}

	public Date getStartDatetime() {
		return startDatetime;
	}

	public void setStartDatetime(Date startDatetime) {
		this.startDatetime = startDatetime;
	}

	public Date getRealTimeToLive() {
		return realTimeToLive;
	}

	public void setRealTimeToLive(Date realTimeToLive) {
		this.realTimeToLive = realTimeToLive;
	}

	public Date getExpectFinishDatetime() {
		return expectFinishDatetime;
	}

	public void setExpectFinishDatetime(Date expectFinishDatetime) {
		this.expectFinishDatetime = expectFinishDatetime;
	}

	public Date getRealFinishDatetime() {
		return realFinishDatetime;
	}

	public void setRealFinishDatetime(Date realFinishDatetime) {
		this.realFinishDatetime = realFinishDatetime;
	}

	public BigInteger getPriority() {
		return priority;
	}

	public void setPriority(BigInteger priority) {
		this.priority = priority;
	}
	
	/**
	 * @return the ownServiceCode
	 */
	public String getOwnServiceCode() {
		return ownServiceCode;
	}

	/**
	 * @param ownServiceCode the ownServiceCode to set
	 */
	public void setOwnServiceCode(String ownServiceCode) {
		this.ownServiceCode = ownServiceCode;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof UserNotificationSticky)) {
			return false;
		}
		UserNotificationSticky other = (UserNotificationSticky) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.bave.backend.persistence.entity.UserNotificationSticky[ id=" + id + " ]";
	}

}