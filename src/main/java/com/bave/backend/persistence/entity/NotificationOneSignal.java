/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bave.backend.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.bave.backend.util.EntityAnnotation;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author haitx
 */
@Entity
@Table(name = "notification_onesignal")
@EntityAnnotation(table_name = "notification_onesignal", pk_name = "id", seq_name = "notification_onesignal_seq", fields_insert = {
		"id", "member_id", "not_notify_id", "domain", "address", "type", "action", "status", "create_date",
		"source_object", "source_ref_id", "source_name", "source_avatar", "destination_object", "destination_ref_id",
		"dest_name", "dest_avatar", "short_content", "data_object", "data_ref_id", "data_name", "addition_data",
		"datatype", "message", "title", "content", "template_id", "sequence", "callback_url", "url", "notification_id",
		"onesignal_status", "seen" })
@Getter
@Setter
@NoArgsConstructor
public class NotificationOneSignal implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notification_onesignal_seq")
	@SequenceGenerator(name = "notification_onesignal_seq", sequenceName = "notification_onesignal_seq", allocationSize = 1, initialValue = 1)
	private Long id;
	@Column(name = "member_id")
	private Long memberId;
	@Column(name = "not_notify_id")
	private Long notNotifyId;
	@Column(name = "domain")
	private String domain;
	@Column(name = "address")
	private String address;
	@Column(name = "type")
	private String type;
	@Column(name = "action")
	private String action;
	@Column(name = "status")
	private String status;
	@Column(name = "create_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;
	@Column(name = "send_date")
	@JsonProperty("send_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date sendDate;
	@Column(name = "source_object")
	private String sourceObject;
	@Column(name = "source_ref_id")
	private Long sourceRefId;
	@Column(name = "source_name")
	private String sourceName;
	@Column(name = "source_avatar")
	private String sourceAvatar;
	@Column(name = "destination_object")
	private String destinationObject;
	@Column(name = "destination_ref_id")
	private Long destinationRefId;
	@Column(name = "dest_name")
	private String destName;
	@Column(name = "dest_avatar")
	private String destAvatar;
	@Column(name = "short_content")
	private String shortContent;
	@Column(name = "data_object")
	private String dataObject;
	@Column(name = "data_ref_id")
	private String dataRefId;
	@Column(name = "data_name")
	private String dataName;
	@Column(name = "addition_data")
	private String additionData;
	@Column(name = "datatype")
	private String datatype;
	@Column(name = "message")
	private String message;
	@Column(name = "title")
	private String title;
	@Column(name = "content")
	private String content;
	@Column(name = "template_id")
	private Integer templateId;
	@Column(name = "sequence")
	private String sequence;
	@Column(name = "callback_url")
	private String callbackUrl;
	@Column(name = "app")
	private String app;
	@Column(name = "notification_id")
	private Long notificationId;
	@Column(name = "sender_status")
	@JsonProperty("sender_status")
	private String senderStatus;
	@Column(name = "fail_reason")
	private String failReason;
	@Column(name = "seen")
	private Boolean seen;
	@Column(name = "channel")
	@JsonProperty("channel")
	private String channel;

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof NotificationOneSignal)) {
			return false;
		}
		NotificationOneSignal other = (NotificationOneSignal) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "notification_onesignal[ seq=" + sequence + ", add: " + address + " ]";
	}

}
