package com.bave.backend.persistence.entity;

import java.beans.Transient;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author haitx
 */
@Entity
@Table(name = "configuration")
@XmlRootElement
public class Configuration implements Serializable {

	@Id
	@Basic(optional = false)
	 
	@Column(name = "id", columnDefinition = "NUMERIC")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "configuration_seq")
	@SequenceGenerator(name = "configuration_seq", sequenceName = "configuration_seq")
	private Long id;
	@Column(name = "group_id", columnDefinition = "NUMERIC")
	private Long groupId;
	@Basic(optional = false)
	 
	@Column(name = "type")
	private String type;
	@Basic(optional = false)
	 
	@Column(name = "code")
	private String code;
	@Column(name = "description")
	private String description;
	@Basic(optional = false)
	 
	@Column(name = "value")
	private String value;
	@Basic(optional = false)
	 
	@Column(name = "status")
	private String status;
	@Column(name = "start_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDatetime;
	@Column(name = "end_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date endDatetime;

	public Configuration() {
	}

	public Configuration(String type, String code) {
		super();
		this.type = type;
		this.code = code;
	}

	public Configuration(String type, String code, String status) {
		super();
		this.type = type;
		this.code = code;
		this.status = status;
	}

	public Configuration(Long id) {
		this.id = id;
	}

	public Configuration(Long id, String type, String code, String value, String status) {
		this.id = id;
		this.type = type;
		this.code = code;
		this.value = value;
		this.status = status;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getStartDatetime() {
		return startDatetime;
	}

	public void setStartDatetime(Date startDatetime) {
		this.startDatetime = startDatetime;
	}

	public Date getEndDatetime() {
		return endDatetime;
	}

	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}

	@Transient
	public Integer getIntValue() {
		return Integer.valueOf(this.value);
	}

	@Transient
	public Long getLongValue() {
		return Long.valueOf(this.value);
	}

	@Transient
	public Double getDoubleValue() {
		return Double.valueOf(this.value);
	}

	@Transient
	public Float getFloatValue() {
		return Float.valueOf(this.value);
	}

	@Transient
	public Boolean getBooleanValue() {
		if ("1".equals(this.value) || "true".equals(this.value.toLowerCase()))
			return true;
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Configuration)) {
			return false;
		}
		Configuration other = (Configuration) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.bave.backend.persistence.entity.Configuration[ id=" + id + " ]";
	}
}
