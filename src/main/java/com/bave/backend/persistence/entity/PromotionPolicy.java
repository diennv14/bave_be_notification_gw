
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bave.backend.persistence.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Luu Lam Thanh Tung
 */
@Entity
@Table(name = "promotion_policy")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "PromotionPolicy.findAll", query = "SELECT p FROM PromotionPolicy p"),
		@NamedQuery(name = "PromotionPolicy.findById", query = "SELECT p FROM PromotionPolicy p WHERE p.id = :id"),
		@NamedQuery(name = "PromotionPolicy.findByName", query = "SELECT p FROM PromotionPolicy p WHERE p.name = :name"),
		@NamedQuery(name = "PromotionPolicy.findByPmNewId", query = "SELECT p FROM PromotionPolicy p WHERE p.pmNewId = :pmNewId"),
		@NamedQuery(name = "PromotionPolicy.findByDiscountType", query = "SELECT p FROM PromotionPolicy p WHERE p.discountType = :discountType"),
		@NamedQuery(name = "PromotionPolicy.findByDiscountValue", query = "SELECT p FROM PromotionPolicy p WHERE p.discountValue = :discountValue"),
		@NamedQuery(name = "PromotionPolicy.findByDescription", query = "SELECT p FROM PromotionPolicy p WHERE p.description = :description"),
		@NamedQuery(name = "PromotionPolicy.findByStatus", query = "SELECT p FROM PromotionPolicy p WHERE p.status = :status"),
		@NamedQuery(name = "PromotionPolicy.findByCode", query = "SELECT p FROM PromotionPolicy p WHERE p.code = :code") })
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PromotionPolicy implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	private Integer id;
	@Size(max = 255)
	private String name;
	@Column(name = "pm_new_id")
	@JsonProperty("pm_new_id")
	private Integer pmNewId;
	@Size(max = 255)
	@Column(name = "discount_type")
	@JsonProperty("discount_type")
	private String discountType;
	@Size(max = 255)
	@Column(name = "discount_value")
	@JsonProperty("discount_value")
	private String discountValue;
	@Size(max = 255)
	private String description;
	@Size(max = 255)
	private String status;
	@Size(max = 255)
	private String code;

	public PromotionPolicy() {
	}

	public PromotionPolicy(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPmNewId() {
		return pmNewId;
	}

	public void setPmNewId(Integer pmNewId) {
		this.pmNewId = pmNewId;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public String getDiscountValue() {
		return discountValue;
	}

	public void setDiscountValue(String discountValue) {
		this.discountValue = discountValue;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof PromotionPolicy)) {
			return false;
		}
		PromotionPolicy other = (PromotionPolicy) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "com.bave.backend.persistence.entity.PromotionPolicy[ id=" + id + " ]";
	}

}