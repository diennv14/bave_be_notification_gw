package com.bave.backend.generic.dao;

/**
 * Generated 12/06/2017 by HaiTX Computer
 */
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.bave.backend.generic.configuration.Condition;

/**
 * @author haitx
 *
 * @param <T>
 * @param <PK>
 */
public interface GenericDao<T, PK> {

	Transaction getTransaction() throws Exception;

	public Session getSession() throws Exception;

	PK persist(T objT) throws Exception;

	// Class<T> persistEntity(Class<T> clazz, Object entity) throws Exception;
	// void persistByProperty(HashMap<String, Object> mapProperty) throws
	// Exception;

	void persistBatch(List<T> objT) throws Exception;

	void update(T objT) throws Exception;

	/**
	 * @param pk
	 * @param mapProperty Map<String, Object> mapProperty > ex:
	 *                    mapProperty.set(property_name1,value1);
	 * @throws Exception
	 */
	void updateByProperty(PK pk, Map<String, Object> mapProperty) throws Exception;

	void updateByProperty(PK pk, String propertyName, Object value) throws Exception;

	/**
	 * @param arrSet   new Object[] { new Object[] { "column_name1", value },new
	 *                 Object[] { "column_name2", value }}
	 * @param arrWhere new Object[] { new Object[] { "column_name1", value },new
	 *                 Object[] { "column_name2", value }}
	 * @throws Exception
	 */
	void updateByColumnArr(Object[] arrSet, Object[] arrWhere) throws Exception;

	/**
	 * @param mapUpdateProperty > ex: mapProperty.set(property_name1,value1);
	 * @param mapWhereProperty  > ex: mapProperty.set(property_name1,value1);
	 * @throws Exception
	 */
	void updateByColumn(Map<String, Object> mapUpdateProperty, Map<String, Object> mapWhereProperty) throws Exception;

	void saveOrUpdate(T objT);

	void delete(T objT) throws Exception;

	void deleteBatch(List<T> objT) throws Exception;

	/**
	 * @param arrWhere new Object[] { new Object[] { "column_name1", value },new
	 *                 Object[] { "column_name2", value }}
	 * @throws Exception
	 */
	void deleteByColumnArr(Object[] arrWhere) throws Exception;

	T findById(PK pk) throws Exception;

	T findByIdLock(PK pk, boolean paramBoolean) throws Exception;

	T findByIdUnique(PK pk) throws Exception;

	List<T> findAll() throws Exception;

	void flush() throws Exception;

	void clear() throws Exception;

	Date getSystemDate() throws Exception;

	T findSingleByObjT(T objT) throws Exception;

	List<T> findByObjT(T objT) throws Exception;

	List<T> findByObjT(T objT, String orderByColumn, Boolean ascendingOrder) throws Exception;

	List<T> findAll(T objT, String[] arrOrderByColumn, Boolean blnAscOrder, int intFirstItemIndex, int intMaxItems,
			boolean blnExactMatch) throws Exception;

	List<T> findAllByIds(List<PK> lstPK) throws Exception;

	PK countAll(T objT, boolean blnExactMatch) throws Exception;

	T findSingleByProperty(String propertyName, Object value) throws Exception;

	T findSingleByProperty(Map<String, Object> mapParams) throws Exception;

	/**
	 * @param arrWhere          > ex: new Object[] { new Object[] {
	 *                          "column_name1","=", value }}
	 * @param arrOrderByColumn  > ex: new String[] { "name","id" }
	 * @param blnAscOrder       > ex: true = asc, false = desc
	 * @param intFirstItemIndex
	 * @param intMaxItems
	 * @param blnExactMatch
	 * @return
	 * @throws Exception
	 */
	public List<T> findByProperty(Object[] arrWhere, String[] arrOrderByColumn, Boolean blnAscOrder,
			int intFirstItemIndex, int intMaxItems, boolean blnExactMatch) throws Exception;

	public List<T> findByProperty(Object[] arrWhere) throws Exception;

	List<T> findByProperty(String propertyName, Object value, String[] orderByColumn, Boolean blnAscOrder)
			throws Exception;

	List<T> findByProperty(String propertyName, Object value) throws Exception;

	List<T> findByProperty(String propertyName, Object value, String[] orderByColumn, Boolean blnAscOrder,
			int intFirstItemIndex, int intMaxItems, boolean blnExactMatch) throws Exception;

	List<T> findByProperty(String propertyName, Object[] value) throws Exception;

	List<T> findByProperty(String propertyName, Object[] value, String[] arrOrderByColumn, Boolean blnAscOrder)
			throws Exception;

	List<T> findByProperty(String propertyName, Object[] value, String[] arrOrderByColumn, Boolean blnAscOrder,
			int intFirstItemIndex, int intMaxItems, boolean blnExactMatch) throws Exception;

	PK countByProperty(String propertyName, Object value) throws Exception;

	PK countByProperty(String propertyName, Object[] value) throws Exception;

	PK countByColumn(Map<String, Object> mapParams) throws Exception;

	PK countByColumn(Object[] value) throws Exception;

	long countByColumnToLong(Object[] params) throws Exception;

	List<T> findAllByParams(Condition condition) throws Exception;

	List<T> findAllByParams(Condition condition, String[] fields) throws Exception;

	PK countAllByParams(Condition condition) throws Exception;

	public boolean exeSql(String sql) throws Exception;
}