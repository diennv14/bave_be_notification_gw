package com.bave.backend.generic.dao.impl;

/**
 * Generated 12/06/2017 by HaiTX Computer
 */
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Table;

import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.TimestampType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bave.backend.generic.api.AliasToBeanNestedResultTransformer;
import com.bave.backend.generic.configuration.Condition;
import com.bave.backend.generic.dao.GenericDao;

/**
 * Basic DAO operations dependent with Hibernate's specific classes
 *
 * @see SessionFactory
 */
@SuppressWarnings("unchecked")
@Repository
public abstract class GenericDaoImpl<T, PK extends Serializable> implements GenericDao<T, PK> {

	@Autowired
	private SessionFactory sessionFactory;

	protected Class<? extends T> daoType;

	/**
	 * By defining this class as abstract, we prevent Spring from creating instance
	 * of this class If not defined abstract getClass().getGenericSuperClass() would
	 * return Object. There would be exception because Object class does not hava
	 * constructor with parameters.
	 */
	public GenericDaoImpl() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		daoType = (Class) pt.getActualTypeArguments()[0];
	}

	@Override
	public Transaction getTransaction() {
		return sessionFactory.getCurrentSession().getTransaction();
	}

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	protected Class<?> getPersistenceType() {
		return this.daoType;
	}

	protected String getTableName() {
		Table table = getPersistenceType().getAnnotation(Table.class);
		return table.name();
	}

	public Query setResultTransformer(Query query, Class resultClass) {
		query.setResultTransformer(new AliasToBeanNestedResultTransformer(resultClass));
		return query;
	}

	protected Criteria criteriaAddParams(Criteria criteria, Map<String, Object> mapParams) {
		if (mapParams != null && mapParams.size() > 0) {
			for (String pName : mapParams.keySet()) {
				Object val = mapParams.get(pName);
				if (val instanceof String[]) {
					criteria.add(Restrictions.in(pName, (String[]) mapParams.get(pName)));
				} else if (val instanceof Integer[]) {
					criteria.add(Restrictions.in(pName, (Integer[]) mapParams.get(pName)));
				} else if (val instanceof Long[]) {
					criteria.add(Restrictions.in(pName, (Long[]) mapParams.get(pName)));
				} else if (val instanceof Float[]) {
					criteria.add(Restrictions.in(pName, (Float[]) mapParams.get(pName)));
				} else if (val instanceof Double[]) {
					criteria.add(Restrictions.in(pName, (Double[]) mapParams.get(pName)));
				} else if (val instanceof ArrayList<?>) {
					criteria.add(Restrictions.in(pName, (ArrayList<?>) mapParams.get(pName)));
				} else {
					criteria.add(Restrictions.eq(pName, mapParams.get(pName)));
				}
			}
		}
		return criteria;
	}

	protected String getPKColumn() {
		return sessionFactory.getClassMetadata(getPersistenceType()).getIdentifierPropertyName();
	}

	protected org.hibernate.type.Type getPKType() {
		String propertyName = sessionFactory.getClassMetadata(getPersistenceType()).getIdentifierPropertyName();
		return sessionFactory.getClassMetadata(getPersistenceType()).getPropertyType(propertyName);
	}

	protected String[] getProperties() {
		AbstractEntityPersister classMetadata = (AbstractEntityPersister) sessionFactory
				.getClassMetadata(getPersistenceType());
		String[] propertyNames = classMetadata.getPropertyNames();
		// List<String> columns = new ArrayList<>();

		// for (String property : propertyNames) {
		// String[] column = classMetadata.getPropertyColumnNames(property);
		// if (column != null && column.length > 0)
		// columns.add(column[0]);
		// }
		return propertyNames;
	}

	protected ProjectionList getColumnForQuery(String[] properties) {
		if (properties != null) {
			ProjectionList projectionList = Projections.projectionList();
			String pkName = getPKColumn();
			projectionList.add(Projections.property(pkName), pkName);
			if ("-".equals(properties[0])) {
				List<String> lstProperties = Arrays.asList(properties);
				for (String column : getProperties()) {
					if (!lstProperties.contains(column)) {
						projectionList.add(Projections.property(column), column);
					}
				}
			} else {
				for (String field : properties) {
					projectionList.add(Projections.property(field), field);
				}
			}
			return projectionList;
		} else {
			return null;
		}
	}

	public void delete(T persistentObject) {
		getSession().delete(persistentObject);
	}

	public List<T> findAll() {
		return getSession().createCriteria(this.daoType.getName()).list();
	}

	@Override
	public void deleteBatch(List<T> objT) {
		System.out.println("deleteBatch: Begin");
		Session session = getSession();
		try {
			int count = 0;
			for (T e : objT) {
				session.delete(e);
				count++;
				if ((count % 500) == 0) {
					System.out.println(objT.size() + " deleteBatch: " + count);
					count++;
				}
			}
			System.out.println("deleteBatch: End");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
		}
	}

	public void deleteByColumnArr(Object[] arrWhere) throws Exception {
		if (arrWhere != null && arrWhere.length > 0) {
			String table = getTableName();
			HashMap<String, Object> mapParams = newMapParameters();

			Session session = getSession();
			String sql = " delete from " + table + " ";
			sql += " where ";
			String whereColumn = null;
			for (Object where : arrWhere) {
				Object[] arr = (Object[]) where;
				if (whereColumn == null) {
					whereColumn = arr[0].toString() + "=:w_" + arr[0].toString();
				} else {
					whereColumn += " and " + arr[0].toString() + "=:w_" + arr[0].toString();
				}
				mapParams.put("w_" + arr[0].toString(), arr[1]);
			}
			sql += whereColumn;

			SQLQuery sqlQuery = session.createSQLQuery(sql);
			sqlQuery = setParametersByMap(sqlQuery, mapParams);

			sqlQuery.executeUpdate();
		}
	}

	public T findById(PK id) {
		return (T) getSession().get(this.daoType, id);
	}

	public T findByIdUnique(PK id) {
		return (T) getSession().get(this.daoType, id);
	}

	public T findByIdLock(PK id, boolean lock) {
		if (lock) {
			return (T) getSession().get(this.daoType, id, LockMode.UPGRADE);
		}
		return (T) getSession().get(this.daoType, id);
	}

	public void flush() {
		getSession().flush();
	}

	// public Class<T> persistEntity(Class<T> clazz, Object entity) {
	// try {
	// T t = (T) entity;
	// HashMap<String, Object> mapParams = new HashMap<>();
	// Table table = clazz.getAnnotation(Table.class);
	// String tableName = table.name();
	// String columnInsert = null;
	// for (Field field : clazz.getDeclaredFields()) {
	// boolean sccessible = field.isAccessible();
	// field.setAccessible(true);
	// Column column = field.getAnnotation(Column.class);
	// if (column != null) {
	// if (columnInsert == null) {
	// columnInsert = column.name();
	// } else {
	// columnInsert += "," + column.name();
	// }
	// mapParams.put(column.name(), field.get(t));
	// }
	// field.setAccessible(sccessible);
	// }
	// persist(t);
	// return (Class<T>) t;
	// } catch (Exception e) {
	// e.printStackTrace();
	// } finally {
	// }
	// return null;
	// }

	public PK persist(T newInstance) {
		Session session = getSession();
		return (PK) session.save(newInstance);
	}

	@Override
	public void persistBatch(List<T> objT) {
		System.out.println("insertBatch: Begin");
		Session session = getSession();
		Transaction tx = session.getTransaction();
		try {
			int count = 0;
			if (!tx.isActive()) {
				tx.begin();
			}
			for (T e : objT) {
				session.save(e);
				count++;
				if ((count % 500) == 0) {
					System.out.println(objT.size() + " insertBatch: " + count);
					tx.commit();
					count++;
					tx = session.beginTransaction();
				}
			}
			tx.commit();
			System.out.println("insertBatch: End");
		} catch (Exception e) {
			e.printStackTrace();
			tx.rollback();
			throw e;
		} finally {
			session = getSession();
		}
	}

	public void update(T transientObject) {
		getSession().update(transientObject);
	}

	// public void updateByProperty(String[] whereColumns, Object[] whereValues,
	// String[] propertyNames, Object[] values)
	// throws Exception {
	// T t = findSingleByProperty(whereColumns, whereValues);
	// int i = 0;
	// for (String propertyName : propertyNames) {
	// Field variableName = t.getClass().getDeclaredField(propertyName);
	// variableName.setAccessible(true);
	// variableName.set(t, values[i]);
	// i++;
	// }
	// update(t);
	// }

	public void updateByProperty(PK pk, Map<String, Object> mapProperty) throws Exception {
		T t = findById(pk);
		for (String propertyName : mapProperty.keySet()) {
			Field variableName = t.getClass().getDeclaredField(propertyName);
			variableName.setAccessible(true);
			variableName.set(t, mapProperty.get(propertyName));
		}
		update(t);
	}

	public void updateByColumn(Map<String, Object> mapUpdateProperty, Map<String, Object> mapWhereProperty)
			throws Exception {
		String table = getTableName();
		HashMap<String, Object> mapParams = newMapParameters();

		Session session = getSession();
		String sql = " update " + table + " ";
		String setColumn = null;
		for (String column : mapUpdateProperty.keySet()) {
			if (setColumn == null) {
				setColumn = " set " + column + "=:" + column;
			} else {
				setColumn += ", " + column + "=:" + column;
			}
			mapParams.put(column, mapUpdateProperty.get(column));
		}
		sql += setColumn + " where ";
		String whereColumn = null;
		for (String column : mapWhereProperty.keySet()) {
			if (whereColumn == null) {
				whereColumn = column + "=:w_" + column;
			} else {
				whereColumn += " and " + column + "=:w_" + column;
			}
			mapParams.put("w_" + column, mapWhereProperty.get(column));
		}
		sql += whereColumn;

		SQLQuery sqlQuery = session.createSQLQuery(sql);
		sqlQuery = setParametersByMap(sqlQuery, mapParams);

		sqlQuery.executeUpdate();
	}

	public void updateByProperty(PK pk, String propertyName, Object value) throws Exception {
		T t = findById(pk);
		if (t != null) {
			Field variableName = t.getClass().getDeclaredField(propertyName);
			variableName.setAccessible(true);
			variableName.set(t, value);
			update(t);
		}
	}

	public void updateByColumnArr(Object[] arrSet, Object[] arrWhere) throws Exception {
		String table = getTableName();
		HashMap<String, Object> mapParams = newMapParameters();

		Session session = getSession();
		String sql = " update " + table + " ";
		String setColumn = null;
		for (Object set : arrSet) {
			Object[] arr = (Object[]) set;
			if (setColumn == null) {
				setColumn = " set " + arr[0].toString() + "=:" + arr[0].toString();
			} else {
				setColumn += ", " + arr[0].toString() + "=:" + arr[0].toString();
			}
			mapParams.put(arr[0].toString(), arr[1]);
		}
		sql += setColumn + " where ";
		String whereColumn = null;
		for (Object where : arrWhere) {
			Object[] arr = (Object[]) where;
			if (whereColumn == null) {
				whereColumn = arr[0].toString() + "=:w_" + arr[0].toString();
			} else {
				whereColumn += " and " + arr[0].toString() + "=:w_" + arr[0].toString();
			}
			mapParams.put("w_" + arr[0].toString(), arr[1]);
		}
		sql += whereColumn;

		SQLQuery sqlQuery = session.createSQLQuery(sql);
		sqlQuery = setParametersByMap(sqlQuery, mapParams);

		sqlQuery.executeUpdate();
	}

	public void saveOrUpdate(T transientObject) {
		getSession().saveOrUpdate(transientObject);
	}

	public Date getSystemDate() throws Exception {
		// String strDateFormat = "dd-MM-yyyy-HH:mm:ss";
		// SimpleDateFormat formatter = new SimpleDateFormat(strDateFormat);
		String strColumn = "systemdate";

		String strSqlSysDate = " select now() as " + strColumn + " ";

		SQLQuery sqlQuery = getSession().createSQLQuery(strSqlSysDate).addScalar(strColumn, TimestampType.INSTANCE);

		Timestamp strDate = (Timestamp) sqlQuery.uniqueResult();
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(strDate.getTime());
		return calendar.getTime();
	}

	public void clear() {
		getSession().clear();
	}

	public void deleteAll() {

	}

	public T findSingleByObjT(T objT) throws Exception {
		Criteria criteria = createCriteria(objT, null, true, 0, 0, true);
		Object rs = criteria.uniqueResult();
		return (rs != null) ? (T) rs : null;
	}

	public List<T> findByObjT(T objT) throws Exception {
		Criteria criteria = createCriteria(objT, null, true, 0, 0, true);
		List<T> rs = criteria.list();
		return (rs != null && rs.size() > 0) ? rs : new ArrayList<T>();
	}

	public List<T> findByObjT(T objT, String orderByColumn, Boolean ascendingOrder) throws Exception {
		String[] arrOrderByColumn = new String[] { orderByColumn };
		Criteria criteria = createCriteria(objT, arrOrderByColumn, ascendingOrder, 0, 0, true);
		List<T> rs = criteria.list();
		return (rs != null && rs.size() > 0) ? rs : new ArrayList<T>();
	}

	private Criteria createCriteria(T objT, String[] arrOrderByColumn, Boolean blnAscOrder, int intFirstItemIndex,
			int intMaxItems, boolean blnExactMatch) throws Exception {

		Class<? extends Object> objectClass = objT.getClass();
		Field[] fields = objectClass.getDeclaredFields();
		PropertyDescriptor propertyDescriptor;
		Method getter;
		Object result;

		Criteria criteria = getSession().createCriteria(objectClass.getName());

		for (Field currentField : fields) {

			propertyDescriptor = new PropertyDescriptor(currentField.getName(), objT.getClass());
			getter = propertyDescriptor.getReadMethod();

			if (getter != null) {
				result = getter.invoke(objT, null);
				if (result != null) {
					if (blnExactMatch) {
						criteria.add(Restrictions.eq(currentField.getName(), result));
					} else {
						criteria.add(Restrictions.like(currentField.getName(), result.toString(), MatchMode.ANYWHERE));
					}
				}

			}
		}

		if (arrOrderByColumn != null) {
			if (blnAscOrder) {
				for (String propertyName : arrOrderByColumn) {
					criteria.addOrder(Order.asc(propertyName));
				}
			} else
				for (String propertyName : arrOrderByColumn) {
					criteria.addOrder(Order.desc(propertyName));
				}
		}

		return criteria;
	}

	private Criteria createCriteria(T objT) throws Exception {
		Class<? extends Object> objectClass = objT.getClass();
		Field[] fields = objectClass.getDeclaredFields();
		PropertyDescriptor propertyDescriptor;
		Method getter;
		Object result;

		Criteria criteria = getSession().createCriteria(objectClass.getName());

		for (Field currentField : fields) {
			propertyDescriptor = new PropertyDescriptor(currentField.getName(), objT.getClass());
			getter = propertyDescriptor.getReadMethod();

			if (getter != null) {
				result = getter.invoke(objT, null);
				if (result != null) {
					criteria.add(Restrictions.eq(currentField.getName(), result));
				}

			}
		}
		return criteria;
	}

	public List<T> findAll(T objT, String[] arrOrderByColumn, Boolean blnAscOrder, int intFirstItemIndex,
			int intMaxItems, boolean blnExactMatch) throws Exception {
		Criteria criteria = createCriteria(objT, arrOrderByColumn, blnAscOrder, intFirstItemIndex, intMaxItems,
				blnExactMatch);

		criteria = makePaging(criteria, intFirstItemIndex, intMaxItems);

		List<T> rs = criteria.list();
		return (rs != null && rs.size() > 0) ? rs : new ArrayList<T>();
	}

	public List<T> findAllByIds(List<PK> lstPKs) throws Exception {
		Criteria criteria = getSession().createCriteria(getPersistenceType());
		if (lstPKs != null && lstPKs.size() > 0) {
			criteria.add(Restrictions.in(getPKColumn(), lstPKs));
		} else {
			return new ArrayList<T>();
		}
		List<T> rs = criteria.list();
		return (rs != null && rs.size() > 0) ? rs : new ArrayList<T>();
	}

	public PK countAll(T objT, boolean blnExactMatch) throws Exception {
		Criteria criteria = createCriteria(objT, null, true, 0, 0, blnExactMatch);
		criteria.setProjection(Projections.rowCount());
		return (PK) criteria.uniqueResult();
	}

	public T findSingleByProperty(String propertyName, Object value) {
		Criteria crit = getSession().createCriteria(getPersistenceType());
		crit.add(Restrictions.eq(propertyName, value));
		List<T> lstRs = crit.list();
		return (lstRs.size() > 0) ? (T) lstRs.get(0) : null;
	}

	public T findSingleByProperty(Map<String, Object> mapParams) {
		Criteria criteria = getSession().createCriteria(getPersistenceType());
		if (mapParams != null && mapParams.size() > 0) {
			for (String pName : mapParams.keySet()) {
				criteria.add(Restrictions.eq(pName, mapParams.get(pName)));
			}
		}
		List<T> lstRs = criteria.list();
		return (lstRs.size() > 0) ? (T) lstRs.get(0) : null;
	}

	public List<T> findByProperty(String propertyName, Object value, String[] orderByColumn, Boolean blnAscOrder)
			throws Exception {
		return findByProperty(propertyName, value, orderByColumn, blnAscOrder, 0, 0, true);
	}

	public List<T> findByProperty(String propertyName, Object value) throws Exception {
		return findByProperty(propertyName, value, null, true, 0, 0, true);
	}

	public List<T> findByProperty(String propertyName, Object[] value) throws Exception {
		return findByProperty(propertyName, value, null, true, 0, 0, true);
	}

	public List<T> findByProperty(String propertyName, Object[] value, String[] arrOrderByColumn, Boolean blnAscOrder)
			throws Exception {
		return findByProperty(propertyName, value, arrOrderByColumn, blnAscOrder, 0, 0, true);
	}

	public List<T> findByProperty(String propertyName, Object value, String[] arrOrderByColumn, Boolean blnAscOrder,
			int intFirstItemIndex, int intMaxItems, boolean blnExactMatch) {
		Criteria criteria = getSession().createCriteria(getPersistenceType());

		Class<? extends Object> objectClass = getPersistenceType();
		Field[] fields = objectClass.getDeclaredFields();
		for (Field currentField : fields) {
			if (propertyName.equals(currentField.getName())) {
				value = convertValue(value, currentField);
			}
		}
		criteria.add(Restrictions.eq(propertyName, value));
		criteria = makeOrderBy(criteria, arrOrderByColumn, blnAscOrder);
		criteria = makePaging(criteria, intFirstItemIndex, intMaxItems);
		List<T> rs = criteria.list();
		return (rs != null && rs.size() > 0) ? rs : new ArrayList<T>();
	}

	public List<T> findByProperty(Object[] arrWhere) throws Exception {
		return findByProperty(arrWhere, null, true, 0, 0, true);
	}

	public List<T> findByProperty(Object[] arrWhere, String[] arrOrderByColumn, Boolean blnAscOrder,
			int intFirstItemIndex, int intMaxItems, boolean blnExactMatch) throws Exception {
		Criteria criteria = getSession().createCriteria(getPersistenceType());

		if (arrWhere != null) {
			for (Object cond : arrWhere) {
				Object[] arr = (Object[]) cond;
				String key = arr[0].toString();
				Object value = arr[2];
				String operator = arr[1].toString();

				if (!"like".equals(operator)) {
					if (operator != null) {
						criteria = buildCriteriaOperator(criteria, key, value, operator);
					} else {
						criteria.add(Restrictions.eq(key, value));
					}
				} else {
					criteria.add(Restrictions.like(key, String.valueOf(value), MatchMode.ANYWHERE).ignoreCase());
				}
			}
		}

		criteria = makeOrderBy(criteria, arrOrderByColumn, blnAscOrder);
		criteria = makePaging(criteria, intFirstItemIndex, intMaxItems);

		List<T> rs = criteria.list();
		return (rs != null && rs.size() > 0) ? rs : new ArrayList<T>();
	}

	public PK countByProperty(String propertyName, Object value) {
		Criteria criteria = getSession().createCriteria(getPersistenceType());
		criteria.add(Restrictions.eq(propertyName, value));
		criteria.setProjection(Projections.rowCount());
		return (PK) criteria.uniqueResult();
	}

	public List<T> findByProperty(String propertyName, Object[] value, String[] arrOrderByColumn, Boolean blnAscOrder,
			int intFirstItemIndex, int intMaxItems, boolean blnExactMatch) {
		Criteria criteria = getSession().createCriteria(getPersistenceType());

		Class<? extends Object> objectClass = getPersistenceType();
		Field[] fields = objectClass.getDeclaredFields();

		Object[] newValue = new Object[value.length];
		for (Field currentField : fields) {
			if (propertyName.equals(currentField.getName())) {
				for (int i = 0; i < value.length; i++) {
					newValue[i] = convertValue(newValue[i], currentField);
				}

			}
		}
		criteria.add(Restrictions.in(propertyName, newValue));
		criteria = makeOrderBy(criteria, arrOrderByColumn, blnAscOrder);
		criteria = makePaging(criteria, intFirstItemIndex, intMaxItems);
		List<T> rs = criteria.list();
		return (rs != null && rs.size() > 0) ? rs : new ArrayList<T>();
	}

	public PK countByProperty(String propertyName, Object[] value) {
		Criteria criteria = getSession().createCriteria(getPersistenceType());

		Class<? extends Object> objectClass = getPersistenceType();
		Field[] fields = objectClass.getDeclaredFields();

		Object[] newValue = new Object[value.length];
		for (Field currentField : fields) {
			if (propertyName.equals(currentField.getName())) {
				for (int i = 0; i < value.length; i++) {
					newValue[i] = convertValue(newValue[i], currentField);
				}

			}
		}

		criteria.add(Restrictions.in(propertyName, newValue));
		criteria.setProjection(Projections.rowCount());
		if ("integer".equals(getPKType().getName())) {
			Object val = criteria.uniqueResult();
			return (PK) Integer.valueOf(val.toString());
		} else {
			return (PK) criteria.uniqueResult();
		}
	}

	public PK countByColumn(Map<String, Object> mapParams) {
		Session session = getSession();
		String strWhere = null;
		for (String key : mapParams.keySet()) {
			String cond = "=";
			if (mapParams.get(key) instanceof Arrays || mapParams.get(key) instanceof String[]) {
				cond = "in";
			}
			if (strWhere == null) {
				strWhere = key + " " + cond + ":" + key;
			} else {
				strWhere += " and " + key + " " + cond + ":" + key;
			}

		}
		SQLQuery sqlQuery = session
				.createSQLQuery("select count(*) as total from " + getTableName() + " where " + strWhere);
		sqlQuery = setParametersByMap(sqlQuery, new HashMap<>(mapParams));

		if ("integer".equals(getPKType().getName())) {
			Object val = sqlQuery.uniqueResult();
			return (PK) Integer.valueOf(val.toString());
		} else if ("long".equals(getPKType().getName())) {
			Object val = sqlQuery.uniqueResult();
			return (PK) Long.valueOf(val.toString());
		} else {
			return (PK) sqlQuery.uniqueResult();
		}
	}

	public PK countByColumn(Object[] params) {
		Session session = getSession();
		String strWhere = null;
		Map<String, Object> mapParams = new HashMap<>();
		for (Object item : params) {
			Object[] arr = (Object[]) item;
			String key = arr[0].toString();
			Object value = arr[2];
			String cond = arr[1].toString();

			if (strWhere == null) {
				strWhere = key + " " + cond + ":" + key;
				mapParams.put(key, value);
			} else {
				strWhere += " and " + key + " " + cond + ":" + key;
				mapParams.put(key, value);
			}

		}
		Query sqlQuery = session
				.createSQLQuery("select count(*) as total from " + getTableName() + " where " + strWhere);
		sqlQuery = setParametersByMap(sqlQuery, new HashMap<>(mapParams));

		if ("integer".equals(getPKType().getName())) {
			Object val = sqlQuery.uniqueResult();
			return (PK) Integer.valueOf(val.toString());
		} else if ("long".equals(getPKType().getName())) {
			Object val = sqlQuery.uniqueResult();
			return (PK) Long.valueOf(val.toString());
		} else {
			return (PK) sqlQuery.uniqueResult();
		}
	}

	public long countByColumnToLong(Object[] params) {
		Session session = getSession();
		String strWhere = null;
		Map<String, Object> mapParams = new HashMap<>();
		for (Object item : params) {
			Object[] arr = (Object[]) item;
			String key = arr[0].toString();
			Object value = arr[2];
			String cond = arr[1].toString();

			if (strWhere == null) {
				strWhere = key + " " + cond + ":" + key;
				mapParams.put(key, value);
			} else {
				strWhere += " and " + key + " " + cond + ":" + key;
				mapParams.put(key, value);
			}

		}
		Query sqlQuery = session
				.createSQLQuery("select count(*) as total from " + getTableName() + " where " + strWhere);
		sqlQuery = setParametersByMap(sqlQuery, new HashMap<>(mapParams));

		String val = sqlQuery.uniqueResult().toString();
		return Long.valueOf(val);
	}

	@Override
	public List<T> findAllByParams(Condition condition) throws Exception {
		Criteria criteria = getSession().createCriteria(getPersistenceType());
		if (condition != null) {

			if (condition != null) {
				Class<? extends Object> objectClass = getPersistenceType();
				for (String key : condition.getParams().keySet()) {
					Object value = condition.getParams().get(key);
					String operator = condition.getOperator(key);
					Field currentField = objectClass.getDeclaredField(key);
					if (currentField != null) {
						value = convertValue(value, currentField);

						if (condition.isExactMatch()) {
							if (operator != null) {
								criteria = buildCriteriaOperator(criteria, key, value, operator);
							} else {
								criteria.add(Restrictions.eq(key, value));
							}
						} else if (currentField.getType() == String.class) {
							criteria.add(
									Restrictions.like(currentField.getName(), String.valueOf(value), MatchMode.ANYWHERE)
											.ignoreCase());

						}
					}
				}
			}
		}

		criteria = makeOrderBy(criteria, condition.getOrderByColumns(), condition.isAscOrder());
		criteria = makePaging(criteria, condition.getFirstItemIndex(), condition.getMaxItems());

		List<T> rs = criteria.list();
		return (rs != null && rs.size() > 0) ? rs : new ArrayList<T>();

	}

	@Override
	public List<T> findAllByParams(Condition condition, String[] dbFields) throws Exception {
		Criteria criteria = getSession().createCriteria(getPersistenceType());

		ProjectionList projectionList = getColumnForQuery(dbFields);
		if (projectionList != null) {
			criteria.setProjection(projectionList);
			criteria.setResultTransformer(Transformers.aliasToBean(getPersistenceType()));
		}

		if (condition != null) {
			Class<? extends Object> objectClass = getPersistenceType();
			for (String key : condition.getParams().keySet()) {
				Object value = condition.getParams().get(key);
				String operator = condition.getOperator(key);
				Field currentField = objectClass.getDeclaredField(key);
				if (currentField != null) {
					value = convertValue(value, currentField);

					if (condition.isExactMatch()) {
						if (operator != null) {
							criteria = buildCriteriaOperator(criteria, key, value, operator);
						} else {
							criteria.add(Restrictions.eq(key, value));
						}
					} else if (currentField.getType() == String.class) {
						criteria.add(Restrictions.like(key, String.valueOf(value), MatchMode.ANYWHERE).ignoreCase());
					}
				}
			}
		}

		criteria = makeOrderBy(criteria, condition.getOrderByColumns(), condition.isAscOrder());
		criteria = makePaging(criteria, condition.getFirstItemIndex(), condition.getMaxItems());

		List<T> rs = criteria.list();
		return (rs != null && rs.size() > 0) ? rs : new ArrayList<T>();
	}

	@Override
	public PK countAllByParams(Condition condition) throws Exception {
		Criteria criteria = getSession().createCriteria(getPersistenceType());

		if (condition != null) {
			Class<? extends Object> objectClass = getPersistenceType();
			for (String key : condition.getParams().keySet()) {
				Object value = condition.getParams().get(key);
				String operator = condition.getOperator(key);
				Field currentField = objectClass.getDeclaredField(key);
				if (currentField != null) {
					value = convertValue(value, currentField);
					if (condition.isExactMatch()) {
						if (operator != null) {
							criteria = buildCriteriaOperator(criteria, key, value, operator);
						} else {
							criteria.add(Restrictions.eq(key, value));
						}
					} else if (currentField.getType() == String.class) {
						criteria.add(Restrictions.like(key, String.valueOf(value), MatchMode.ANYWHERE).ignoreCase());
					}
				}
			}

			criteria.setProjection(Projections.rowCount());
			return (PK) criteria.uniqueResult();
		}
		return null;
	}

	public Query pagingQuery(Query query, Integer firstItem, Integer maxItem) {
		if (firstItem != null && maxItem != null && maxItem > 0 && firstItem >= 0) {
			query.setMaxResults(maxItem);
			query.setFirstResult(firstItem);
		}
		return query;
	}

	public SQLQuery pagingQuery(SQLQuery query, Integer firstItem, Integer maxItem) {
		if (firstItem != null && maxItem != null && maxItem > 0 && firstItem >= 0) {
			query.setMaxResults(maxItem);
			query.setFirstResult(firstItem);
		}
		return query;
	}

	public Query setParametersByMap(Query query, HashMap<String, Object> mapParams) {
		for (String param : mapParams.keySet()) {
			Object value = mapParams.get(param);
			if (value instanceof Collection<?>) {
				query.setParameterList(param, (Collection<?>) value);
				continue;
			} else if (value instanceof String[]) {
				query.setParameterList(param, (Object[]) value);
				continue;
			} else {
				query.setParameter(param, value);
				continue;
			}
		}
		return query;
	}

	public SQLQuery setParametersByMap(SQLQuery query, HashMap<String, Object> mapParams) {
		for (String param : mapParams.keySet()) {
			Object value = mapParams.get(param);
			if (value instanceof Collection<?>) {
				query.setParameterList(param, (Collection<?>) value);
				continue;
			} else if (value instanceof String[]) {
				query.setParameterList(param, (Object[]) value);
				continue;
			} else {
				query.setParameter(param, value);
				continue;
			}
		}
		return query;
	}

	public HashMap<String, Object> newMapParameters() {
		return new HashMap<String, Object>();
	}

	public String createParameter(String condition, String sql, HashMap<String, Object> mapParams, String key,
			Object value) {
		if (value != null) {
			sql += condition;
			mapParams.put(key, value);
		}
		return sql;
	}

	public Object convertValue(Object value, Field field) {
		if (field.getType() == Long.class) {
			value = Long.valueOf(value.toString());
		} else if (field.getType() == Integer.class) {
			value = Integer.valueOf(value.toString());
		} else if (field.getType() == Double.class) {
			value = Double.valueOf(value.toString());
		} else if (field.getType() == Float.class) {
			value = Float.valueOf(value.toString());
		}
		return value;
	}

	public Criteria buildCriteriaOperator(Criteria criteria, String key, Object value, String operator) {
		if ("=".equals(operator)) {
			criteria.add(Restrictions.eq(key, value));
		} else if ("<".equals(operator)) {
			criteria.add(Restrictions.lt(key, value));
		} else if (">".equals(operator)) {
			criteria.add(Restrictions.gt(key, value));
		} else if ("like".equals(operator)) {
			criteria.add(Restrictions.like(key, String.valueOf(value), MatchMode.ANYWHERE).ignoreCase());
		}
		return criteria;
	}

	public Criteria makeOrderBy(Criteria criteria, String[] orderByColumns, Boolean ascOrder) {
		if (orderByColumns != null) {
			String[] arr = orderByColumns;
			if (ascOrder == null) {
				for (String propertyName : arr) {
					propertyName = propertyName.trim();
					String[] arrColumn = propertyName.split(" ");
					if ("asc".equals(arrColumn[1])) {
						criteria.addOrder(Order.asc(arrColumn[0]));
					} else {
						criteria.addOrder(Order.desc(arrColumn[0]));
					}
				}
			} else {
				if (ascOrder) {
					for (String propertyName : arr) {
						criteria.addOrder(Order.asc(propertyName));
					}
				} else {
					for (String propertyName : arr) {
						criteria.addOrder(Order.desc(propertyName));
					}
				}
			}
		}
		return criteria;
	}

	public Criteria makeOrderBy(Criteria criteria, String orderByColumns, Boolean ascOrder) {
		if (orderByColumns != null) {
			String[] arr = orderByColumns.split(",");
			return makeOrderBy(criteria, arr, ascOrder);
		}
		return criteria;
	}

	public Criteria makePaging(Criteria criteria, Integer firstItemIndex, Integer maxItems) {
		if (firstItemIndex != null && maxItems != null && firstItemIndex >= 0 && maxItems > 0) {
			criteria.setFirstResult(firstItemIndex);
			criteria.setMaxResults(maxItems);
		}
		return criteria;
	}

	public Criteria makePaging(Criteria criteria, Condition condition) {
		if (condition.getFirstItemIndex() >= 0 && condition.getMaxItems() > 0) {
			criteria.setFirstResult(condition.getFirstItemIndex());
			criteria.setMaxResults(condition.getMaxItems());
		}
		return criteria;
	}

	public boolean exeSql(String sql) throws Exception {
		Session session = getSession();
		HashMap<String, Object> mapParams = newMapParameters();
		Query query = session.createNativeQuery(sql);
		query.executeUpdate();
		return true;
	}

}