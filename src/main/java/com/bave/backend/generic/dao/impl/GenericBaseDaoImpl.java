package com.bave.backend.generic.dao.impl;

import java.util.Collection;
import java.util.HashMap;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Basic DAO operations dependent with Hibernate's specific classes
 *
 * @see SessionFactory
 */
@SuppressWarnings("unchecked")
@Repository
public abstract class GenericBaseDaoImpl {

	@Autowired
	private SessionFactory sessionFactory;

	public GenericBaseDaoImpl() {
	}

	public Transaction getTransaction() {
		return sessionFactory.getCurrentSession().getTransaction();
	}

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public SQLQuery pagingQuery(SQLQuery query, Integer firstItem, Integer maxItem) {
		if (firstItem != null && maxItem != null && maxItem > 0 && firstItem >= 0) {
			query.setMaxResults(maxItem);
			query.setFirstResult(firstItem);
		}
		return query;
	}

	public SQLQuery setParametersByMap(SQLQuery query, HashMap<String, Object> mapParams) {
		for (String param : mapParams.keySet()) {
			Object value = mapParams.get(param);
			if (value instanceof Collection<?>) {
				query.setParameterList(param, (Collection<?>) value);
				continue;
			} else if (value instanceof String[]) {
				query.setParameterList(param, (Object[]) value);
				continue;
			} else {
				query.setParameter(param, value);
				continue;
			}
		}
		return query;
	}

	public Query pagingQuery(Query query, Integer firstItem, Integer maxItem) {
		if (firstItem != null && maxItem != null && maxItem > 0 && firstItem >= 0) {
			query.setMaxResults(maxItem);
			query.setFirstResult(firstItem);
		}
		return query;
	}

	public Query setParametersByMap(Query query, HashMap<String, Object> mapParams) {
		for (String param : mapParams.keySet()) {
			Object value = mapParams.get(param);
			if (value instanceof Collection<?>) {
				query.setParameterList(param, (Collection<?>) value);
				continue;
			} else if (value instanceof String[]) {
				query.setParameterList(param, (Object[]) value);
				continue;
			} else {
				query.setParameter(param, value);
				continue;
			}
		}
		return query;
	}

	public HashMap<String, Object> newMapParameters() {
		return new HashMap<String, Object>();
	}

	public String createParameter(String condition, String sql, HashMap<String, Object> mapParams, String key,
			Object value) {
		if (value != null) {
			sql += condition;
			mapParams.put(key, value);
		}
		return sql;
	}

	////////////////////////////////////////////////// 5/12/2017

	////////////////////////////////////////////////// 5/12/2017
}