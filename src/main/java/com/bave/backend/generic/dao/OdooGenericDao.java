package com.bave.backend.generic.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bave.backend.util.FieldsInfo;

public interface OdooGenericDao<T, PK> {

	HashMap<String, FieldsInfo> getFieldsInfo();

	PK persist(T objT, String[] fields) throws Exception;

	PK persist(T objT) throws Exception;

	void update(T objT) throws Exception;

	void update(T objT, String[] fields) throws Exception;

	PK persist(Object[] fields) throws Exception;

	void update(PK pk, Object[] fields) throws Exception;

	void updateByProperty(PK pk, Object[] conditions) throws Exception;

	void delete(PK pk) throws Exception;

	void deleteBatch(List<PK> objT) throws Exception;

	T findById(PK pk) throws Exception;

	T findById(PK pk, String[] fields) throws Exception;

	List<T> findByIds(PK[] pk, String[] fields) throws Exception;

	public Map<PK, T> findMapByIds(PK[] pk, String[] fields) throws Exception;

	List<T> findAll() throws Exception;

	List<T> findAll(Object[] conditions, String[] arrOrderByColumn, boolean blnAscOrder, Integer intFirstItemIndex,
			Integer intMaxItems, boolean blnExactMatch) throws Exception;

	List<T> findAll(Object[] conditions, String[] arrOrderByColumn, boolean blnAscOrder) throws Exception;

	List<T> findAll(Object[] conditions, String[] fields, String[] arrOrderByColumn, boolean blnAscOrder,
			Integer intFirstItemIndex, Integer intMaxItems, boolean blnExactMatch) throws Exception;

	List<T> findAll(Object[] conditions, String[] fields, String[] arrOrderByColumn, boolean blnAscOrder)
			throws Exception;

	List<T> findAll(Object[] conditions) throws Exception;

	List<T> findAll(Object[] conditions, String[] fields) throws Exception;

	T findSingleByProperty(Object[] conditions, String[] fields) throws Exception;

	PK countByProperty(Object[] conditions) throws Exception;

}