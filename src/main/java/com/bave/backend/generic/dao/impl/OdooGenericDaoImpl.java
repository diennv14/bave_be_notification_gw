package com.bave.backend.generic.dao.impl;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.xmlrpc.XmlRpcException;
import org.hibernate.SessionFactory;

import com.bave.backend.generic.dao.OdooGenericDao;
import com.bave.backend.service.CommonERPService;
import com.bave.backend.util.FieldsInfo;
import com.bave.backend.util.RpcXmlUtil;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Basic DAO operations dependent with Hibernate's specific classes
 *
 * @see SessionFactory
 */
@SuppressWarnings("unchecked")
public abstract class OdooGenericDaoImpl<T, PK extends Serializable> extends CommonERPService
		implements OdooGenericDao<T, PK>, Serializable {

	protected HttpServletRequest request;

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	protected Class<? extends T> daoType;

	public OdooGenericDaoImpl() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		daoType = (Class) pt.getActualTypeArguments()[0];
	}

	protected Class<?> getPersistenceType() {
		return this.daoType;
	}

	public String getModelName() {
		return daoType.getAnnotation(OdooEntityAnnotation.class).model();
	}

	public String getModelName(Class clazz) {
		return ((OdooEntityAnnotation) clazz.getAnnotation(OdooEntityAnnotation.class)).model();
	}

	public String[] getFilterFields(Class moedlClazz) {
		String[] arr = ((OdooEntityAnnotation) moedlClazz.getAnnotation(OdooEntityAnnotation.class)).fields();
		if (arr != null && arr.length > 0) {
			if (arr[0].length() > 0) {
				return arr;
			}
		}
		return null;
	}

	public String[] getFilterShortFields(Class moedlClazz) {
		String[] arr = ((OdooEntityAnnotation) moedlClazz.getAnnotation(OdooEntityAnnotation.class)).short_fields();
		if (arr != null && arr.length > 0) {
			if (arr[0].length() > 0) {
				return arr;
			}
		}
		return null;
	}

	public String[] getFilterFields() {
		String[] arr = daoType.getAnnotation(OdooEntityAnnotation.class).fields();
		if (arr != null && arr.length > 0) {
			if (arr[0].length() > 0) {
				return arr;
			}
		}
		return null;
	}

	public String[] getFilterShortFields() {
		String[] arr = daoType.getAnnotation(OdooEntityAnnotation.class).short_fields();
		if (arr != null && arr.length > 0) {
			if (arr[0].length() > 0) {
				return arr;
			}
		}
		return null;
	}

	public HashMap<String, FieldsInfo> getFieldsInfo() {
		return super.getFieldsInfo(getModelName());
	}

	public PK persist(Object[] fields) throws Exception {
		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "create"));
		lstParams.add(Arrays.asList(new HashMap<String, Object>() {
			{
				for (Object item : fields) {
					Object[] arr = (Object[]) item;
					setParamIVN(this, arr[0].toString(), arr[1]);
				}
			}
		}));

		Object executeRs = executeToObject(lstParams.toArray());
		return (PK) executeRs;
	}

	public void update(PK pk, Object[] fields) throws Exception {
		// List<Object> lstParams =
		// initClient(initAuthenParam(request));List<List<Object>> lstCond = new
		// ArrayList<>();
		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "write"));
		lstParams.add(Arrays.asList(Arrays.asList(pk), new HashMap<String, Object>() {
			{
				for (Object item : fields) {
					Object[] arr = (Object[]) item;
					setParamIVN(this, arr[0].toString(), arr[1]);
				}
			}
		}));
		executeToObject(lstParams.toArray());
	}

	public PK persist(T objT) throws Exception {

		String modelName = getModelName(objT.getClass());
		HashMap<String, FieldsInfo> mapOrginalField = getFieldsInfo(modelName);

		Map<String, Object> mapField = new HashMap<>();
		Field[] arrfields = getPersistenceType().getDeclaredFields();
		for (Field field : arrfields) {
			if (field.isAnnotationPresent(JsonProperty.class)) {
				String annotationValue = field.getAnnotation(JsonProperty.class).value();
				field.setAccessible(true);
				Object value = field.get(objT);
				if (!"id".equals(annotationValue) && mapOrginalField.containsKey(annotationValue)) {
					mapField.put(annotationValue, value);
				}
			}
		}

		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "create"));
		lstParams.add(Arrays.asList(new HashMap<String, Object>() {
			{
				for (String key : mapField.keySet()) {
					setParamIVN(this, key, mapField.get(key));
				}
			}
		}));

		Object executeRs = executeToObject(lstParams.toArray());
		return (PK) executeRs;
	}

	public PK persist(T objT, String[] fields) throws Exception {

		Map<String, Object> mapField = new HashMap<>();
		Field[] arrfields = getPersistenceType().getDeclaredFields();
		for (Field field : arrfields) {
			if (field.isAnnotationPresent(JsonProperty.class)) {
				String annotationValue = field.getAnnotation(JsonProperty.class).value();
				field.setAccessible(true);
				Object value = field.get(objT);
				if (!"id".equals(annotationValue)) {
					mapField.put(annotationValue, value);
				}
			}
		}

		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "create"));
		lstParams.add(Arrays.asList(new HashMap<String, Object>() {
			{
				for (String key : fields) {
					makeValueByCustomKey(mapField, key);
					if (mapField.containsKey(key)) {
						setParamIVN(this, key, mapField.get(key));
					}
				}
			}
		}));

		Object executeRs = executeToObject(lstParams.toArray());
		return (PK) executeRs;
	}

	public void update(T objT) throws Exception {

		String modelName = getModelName(objT.getClass());
		HashMap<String, FieldsInfo> mapOrginalField = getFieldsInfo(modelName);

		PK id = null;
		Map<String, Object> mapField = new HashMap<>();
		Field[] arrfields = getPersistenceType().getDeclaredFields();
		for (Field field : arrfields) {
			if (field.isAnnotationPresent(JsonProperty.class)) {
				String annotationValue = field.getAnnotation(JsonProperty.class).value();
				field.setAccessible(true);
				Object value = field.get(objT);
				if ("id".equals(annotationValue)) {
					id = (PK) value;
				} else if (mapOrginalField.containsKey(annotationValue)) {
					mapField.put(annotationValue, value);
				}
			}
		}

		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "write"));
		lstParams.add(Arrays.asList(Arrays.asList(id), new HashMap<String, Object>() {
			{
				for (String key : mapField.keySet()) {
					setParamIVN(this, key, mapField.get(key));
				}
			}
		}));
		executeToObject(lstParams.toArray());
	}

	public void update(T objT, String[] fields) throws Exception {
		PK id = null;
		Map<String, Object> mapField = new HashMap<>();
		Field[] arrfields = getPersistenceType().getDeclaredFields();
		for (Field field : arrfields) {
			if (field.isAnnotationPresent(JsonProperty.class)) {
				String annotationValue = field.getAnnotation(JsonProperty.class).value();
				field.setAccessible(true);
				Object value = field.get(objT);
				if ("id".equals(annotationValue)) {
					id = (PK) value;
				} else {
					mapField.put(annotationValue, value);
				}
			}
		}

		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "write"));
		lstParams.add(Arrays.asList(Arrays.asList(id), new HashMap<String, Object>() {
			{
				for (String key : fields) {
					makeValueByCustomKey(mapField, key);
					if (mapField.containsKey(key)) {
						setParamIVN(this, key, mapField.get(key));
					}
				}
			}
		}));
		executeToObject(lstParams.toArray());
	}

	public void updateByProperty(PK pk, Object[] conditions) throws Exception {
		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "write"));
		lstParams.add(Arrays.asList(Arrays.asList(pk), new HashMap<String, Object>() {
			{
				for (Object item : conditions) {
					Object[] arr = (Object[]) item;
					setParamIVN(this, arr[0].toString(), arr[1]);
				}
			}
		}));
		executeToObject(lstParams.toArray());
	}

	public void delete(PK pk) throws Exception {
		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "unlink"));
		lstParams.add(Arrays.asList(Arrays.asList(pk)));
		client.execute("execute_kw", lstParams.toArray());
	}

	public void deleteBatch(List<PK> objT) throws Exception {
		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "unlink"));
		lstParams.add("");
		lstParams.add(Arrays.asList(objT));
		client.execute("execute_kw", lstParams.toArray());
	}

	public T findById(PK pk) throws Exception {
		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "search_read"));

		lstCond.add(Arrays.asList(new Object[] { "id", "=", pk }));
		lstParams.add(Arrays.asList(lstCond));

		lstParams.add(new HashMap<String, Object>() {
			{
				setParamIVN(this, "fields", getFilterFields());
			}
		});

		List<T> lstRs = executeToList(getPersistenceType(), lstParams.toArray());
		if (lstRs.size() > 0)
			return lstRs.get(0);
		return null;
	}

	public T findById(PK pk, String[] fields) throws Exception {
		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "search_read"));

		lstCond.add(Arrays.asList(new Object[] { "id", "=", pk }));
		lstParams.add(Arrays.asList(lstCond));

		lstParams.add(new HashMap<String, Object>() {
			{
				if (fields != null) {
					put("fields", fields);
				}
				setParamIVN(this, "fields", getFilterFields());
			}
		});

		List<T> lstRs = executeToList(getPersistenceType(), lstParams.toArray());
		if (lstRs.size() > 0)
			return lstRs.get(0);
		return null;
	}

	public List<T> findByIds(PK[] pk, String[] fields) throws Exception {
		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "search_read"));
		lstCond.add(Arrays.asList("state", "in", Arrays.asList(pk)));
		lstParams.add(Arrays.asList(lstCond));

		lstParams.add(new HashMap<String, Object>() {
			{
				if (fields != null) {
					put("fields", fields);
				}
				setParamIVN(this, "fields", getFilterFields());
			}
		});

		List<T> lstRs = executeToList(getPersistenceType(), lstParams.toArray());
		return lstRs;
	}

	public Map<PK, T> findMapByIds(PK[] pk, String[] fields) throws Exception {
		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "search_read"));
		lstCond.add(Arrays.asList("id", "in", Arrays.asList(pk)));
		lstParams.add(Arrays.asList(lstCond));

		lstParams.add(new HashMap<String, Object>() {
			{
				if (fields != null) {
					put("fields", fields);
				} else {
					setParamIVN(this, "fields", getFilterFields());
				}
			}
		});

		try {
			Map<PK, T> mapRs = new HashMap<PK, T>();
			Object executeRs = client.execute("execute_kw", lstParams.toArray());
			List<Object> lstExecuteRs = exeRsToLstObject(executeRs);
			for (Object obj : lstExecuteRs) {
				HashMap<String, Object> map = rsItemToMap(obj);
				T eObj = (T) RpcXmlUtil.convertToObject(map, getPersistenceType());
				mapRs.put((PK) map.get("id"), eObj);
			}
			return mapRs;
		} catch (XmlRpcException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new HashMap<PK, T>();
	}

	public List<T> findAll() throws Exception {
		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "search_read"));
		lstParams.add(Arrays.asList(lstCond));
		lstParams.add(new HashMap<String, Object>() {
			{
				setParamIVN(this, "fields", getFilterFields());
			}
		});

		List<T> lstRs = executeToList(getPersistenceType(), lstParams.toArray());
		return lstRs;
	}

	@Override
	public List<T> findAll(Object[] conditions) throws Exception {
		return findAll(conditions, null, true, null, null, true);
	}

	@Override
	public List<T> findAll(Object[] conditions, String[] fields) throws Exception {
		return findAll(conditions, fields, null, true, null, null, true);
	}

	@Override
	public List<T> findAll(Object[] conditions, String[] arrOrderByColumn, boolean blnAscOrder) throws Exception {
		return findAll(conditions, arrOrderByColumn, blnAscOrder, null, null, true);
	}

	@Override
	public List<T> findAll(Object[] conditions, String[] fields, String[] arrOrderByColumn, boolean blnAscOrder)
			throws Exception {
		return findAll(conditions, fields, arrOrderByColumn, blnAscOrder, null, null, true);
	}

	@Override
	public List<T> findAll(Object[] conditions, String[] arrOrderByColumn, boolean blnAscOrder,
			Integer intFirstItemIndex, Integer intMaxItems, boolean blnExactMatch) throws Exception {
		return findAll(conditions, null, arrOrderByColumn, blnAscOrder, intFirstItemIndex, intMaxItems, blnExactMatch);
	}

	@Override
	public List<T> findAll(Object[] conditions, String[] fields, String[] arrOrderByColumn, boolean blnAscOrder,
			Integer intFirstItemIndex, Integer intMaxItems, boolean blnExactMatch) throws Exception {
		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "search_read"));
		for (Object item : conditions) {
			Object[] arrCond = (Object[]) item;
			lstCond.add(Arrays.asList(arrCond));
		}
		lstParams.add(Arrays.asList(lstCond));
		lstParams.add(new HashMap<String, Object>() {
			{
				if (fields != null) {
					put("fields", fields);
				} else {
					setParamIVN(this, "fields", getFilterFields());
				}
				if (intFirstItemIndex != null && intMaxItems != null && intMaxItems > 0) {
					setParamIVN(this, "limit", intMaxItems);
					setParamIVN(this, "offset", intFirstItemIndex);
				}
				if (arrOrderByColumn != null) {
					setParamIVN(this, "order",
							String.join(",", arrOrderByColumn) + " " + (blnAscOrder ? "asc" : "desc"));
				}
			}
		});

		List<T> lstRs = executeToList(getPersistenceType(), lstParams.toArray());
		return lstRs;
	}

	public T findSingleByProperty(Object[] conditions, String[] fields) throws Exception {
		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "search_read"));
		for (Object item : conditions) {
			Object[] arrCond = (Object[]) item;
			lstCond.add(Arrays.asList(arrCond));
		}
		lstParams.add(Arrays.asList(lstCond));
		lstParams.add(new HashMap<String, Object>() {
			{
				if (fields != null) {
					put("fields", fields);
				} else {
					setParamIVN(this, "fields", getFilterFields());
				}
			}
		});

		List<T> lstRs = executeToList(getPersistenceType(), lstParams.toArray());
		if (lstRs.size() > 0)
			return lstRs.get(0);
		return null;
	}

	public PK countByProperty(Object[] conditions) throws Exception {
		List<Object> lstParams = initClient(initAuthenParam(request));
		List<List<Object>> lstCond = new ArrayList<>();
		lstParams.addAll(Arrays.asList(getModelName(), "search_count"));
		for (Object item : conditions) {
			Object[] arrCond = (Object[]) item;
			lstCond.add(Arrays.asList(arrCond));
		}
		lstParams.add(Arrays.asList(lstCond));
		lstParams.add(new HashMap<String, Object>() {
			{
				setParamIVN(this, "fields", getFilterFields());
			}
		});

		Object objRs = executeToObject(Object.class, lstParams.toArray());
		if (objRs != null)
			return (PK) objRs;
		return (PK) "0";
	}

	private Map<String, Object> makeValueByCustomKey(Map<String, Object> mapField, String key) {
		if (key.indexOf("_") > 0 && !mapField.containsKey(key)) {
			String[] arrKey = key.split("_");
			String mname = "";
			for (int i = 0; i < arrKey.length - 1; i++) {
				mname += String.valueOf(arrKey[i].charAt(0)).toUpperCase() + arrKey[i].substring(1);
			}
			String cKey = mname + "Id";
			cKey = String.valueOf(cKey.charAt(0)).toLowerCase() + cKey.substring(1);
			if (mapField.containsKey(cKey)) {
				mapField.put(key, mapField.get(cKey));
			}
		}
		return mapField;
	}
}