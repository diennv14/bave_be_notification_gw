package com.bave.backend.generic.dao.impl;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface OdooEntityAnnotation {

	// public enum Priority {
	// LOW, MEDIUM, HIGH
	// }

	String model();

	String[] fields() default "";

	String[] short_fields() default "";

}
