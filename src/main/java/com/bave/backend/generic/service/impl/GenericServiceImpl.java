package com.bave.backend.generic.service.impl;

/**
 * Generated 12/06/2017 by HaiTX Computer
 */
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.bave.backend.generic.configuration.Condition;
import com.bave.backend.generic.dao.GenericDao;
import com.bave.backend.generic.service.GenericService;


@Service
@Transactional
public abstract class GenericServiceImpl<T, PK> implements GenericService<T, PK> {

	private GenericDao<T, PK> genericDao;

	public GenericServiceImpl() {
	}

	public GenericServiceImpl(GenericDao<T, PK> genericDao) {
		this.genericDao = genericDao;
	}

	public void printMemory() {
		Double totalMemory = Double.valueOf(Runtime.getRuntime().totalMemory());
		Double freeMemory = Double.valueOf(Runtime.getRuntime().freeMemory());

		System.out.println(
				"*****---booooooooooo-----------------------------------------------------------------**********");
		System.out.println("totalMemory:" + totalMemory.doubleValue() / 1048576.0D);
		System.out.println("freeMemory:" + freeMemory.doubleValue() / 1048576.0D);
		System.out.println("user Memory:" + (totalMemory.doubleValue() - freeMemory.doubleValue()) / 1048576.0D);
		System.out.println("%freeMemory:" + 100.0D * freeMemory.doubleValue() / totalMemory.doubleValue() + "%");
		System.out.println("maxMemory:" + Double.valueOf(Runtime.getRuntime().maxMemory() / 1048576L));
		System.out.println(
				"*****----booooooooooo----------------------------------------------------------------**********");
	}

	public void delete(T persistentObject) throws Exception {
		genericDao.delete(persistentObject);
	}

	@Override
	public void deleteBatch(List<T> objT) throws Exception {
		genericDao.deleteBatch(objT);
	}

	public List<T> findAll() throws Exception {
		return genericDao.findAll();
	}

	public T findById(PK id) throws Exception {
		return genericDao.findById(id);
	}

	public T findByIdUnique(PK id) throws Exception {
		return genericDao.findByIdUnique(id);
	}

	@Override
	public PK persist(T newInstance) throws Exception {
		return genericDao.persist(newInstance);
	}

	@Override
	public void persistBatch(List<T> objT) throws Exception {
		genericDao.persistBatch(objT);
	}

	public void update(T transientObject) throws Exception {
		genericDao.update(transientObject);
	}

	public void updateByProperty(PK pk, Map<String, Object> mapProperty) throws Exception {
		genericDao.updateByProperty(pk, mapProperty);
	}

	public void updateByProperty(PK pk, String propertyName, Object value) throws Exception {
		genericDao.updateByProperty(pk, propertyName, value);
	}

	public void saveOrUpdate(T transientObject) {
		genericDao.saveOrUpdate(transientObject);
	}

	public Date getSystemDate() throws Exception {
		return genericDao.getSystemDate();
	}

	public T findByIdLock(PK id, boolean lock) throws Exception {
		return genericDao.findByIdLock(id, lock);
	}

	public T findSingleByObjT(T objT) throws Exception {
		return genericDao.findSingleByObjT(objT);
	}

	public List<T> findByObjT(T objT) throws Exception {
		return genericDao.findByObjT(objT);
	}

	public List<T> findByObjT(T objT, String orderByColumn, boolean ascendingOrder) throws Exception {
		return genericDao.findByObjT(objT, orderByColumn, ascendingOrder);
	}

	public List<T> findAll(T objT, String[] arrOrderByColumn, boolean blnAscOrder, int intFirstItemIndex,
			int intMaxItems, boolean blnExactMatch) throws Exception {
		return genericDao.findAll(objT, arrOrderByColumn, blnAscOrder, intFirstItemIndex, intMaxItems, blnExactMatch);
	}

	public List<T> findAllByIds(List<PK> lstPKs) throws Exception {
		return genericDao.findAllByIds(lstPKs);
	}

	public PK countAll(T objT, boolean blnExactMatch) throws Exception {
		return genericDao.countAll(objT, blnExactMatch);

	}

	public T findSingleByProperty(String propertyName, Object value) throws Exception {
		return genericDao.findSingleByProperty(propertyName, value);
	}

	public T findSingleByProperty(Map<String, Object> mapParams) throws Exception {
		return genericDao.findSingleByProperty(mapParams);
	}

	public List<T> findByProperty(String propertyName, Object value, String[] orderByColumn, boolean blnAscOrder)
			throws Exception {
		return genericDao.findByProperty(propertyName, value, orderByColumn, blnAscOrder);
	}

	public List<T> findByProperty(String propertyName, Object value) throws Exception {
		return genericDao.findByProperty(propertyName, value);
	}

	public List<T> findByProperty(String propertyName, Object value, String[] orderByColumn, boolean blnAscOrder,
			int intFirstItemIndex, int intMaxItems, boolean blnExactMatch) throws Exception {
		return genericDao.findByProperty(propertyName, value, orderByColumn, blnAscOrder, intFirstItemIndex,
				intMaxItems, blnExactMatch);
	}

	public List<T> findByProperty(String propertyName, Object[] value) throws Exception {
		return genericDao.findByProperty(propertyName, value);
	}

	public List<T> findByProperty(String propertyName, Object[] value, String[] arrOrderByColumn, boolean blnAscOrder)
			throws Exception {
		return genericDao.findByProperty(propertyName, value, arrOrderByColumn, blnAscOrder);
	}

	public List<T> findByProperty(String propertyName, Object[] value, String[] arrOrderByColumn, boolean blnAscOrder,
			int intFirstItemIndex, int intMaxItems, boolean blnExactMatch) throws Exception {
		if (value != null && value.length > 1) {
			return genericDao.findByProperty(propertyName, value, arrOrderByColumn, blnAscOrder, intFirstItemIndex,
					intMaxItems, blnExactMatch);
		} else {
			Object sValue = value[0];
			return genericDao.findByProperty(propertyName, sValue, arrOrderByColumn, blnAscOrder, intFirstItemIndex,
					intMaxItems, blnExactMatch);
		}
	}

	public PK countByProperty(String propertyName, Object[] value) throws Exception {
		if (value != null && value.length > 1) {
			return genericDao.countByProperty(propertyName, value);
		} else {
			Object sValue = value[0];
			return genericDao.countByProperty(propertyName, sValue);
		}
	}

	public PK countByColumn(Map<String, Object> mapParams) throws Exception {
		return genericDao.countByColumn(mapParams);
	}

	public void rollback() {
		try {
			genericDao.getTransaction().rollback();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<T> findAllByParams(Condition condition) throws Exception {
		return genericDao.findAllByParams(condition);
	}

	@Override
	public List<T> findAllByParams(Condition condition, String[] fields) throws Exception {
		return genericDao.findAllByParams(condition, fields);
	}

	@Override
	public PK countAllByParams(Condition condition) throws Exception {
		return genericDao.countAllByParams(condition);
	}

	public Integer getReqParamIntegerValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return Integer.valueOf(params.get(key).toString());
		}
		return null;
	}

	public Float getReqParamFloatValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return Float.valueOf(params.get(key).toString());
		}
		return null;
	}

	public Long getReqParamLongValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return Long.valueOf(params.get(key).toString());
		}
		return null;
	}

	public String getReqParamStrValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return (params.get(key) != null) ? params.get(key).toString() : null;
		}
		return null;
	}

	public Date getReqParamDateValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return (Date) params.get(key);
		}
		return null;
	}

	public Boolean getReqParamBooleanValue(Map<String, Object> params, String key) {
		if (params != null && params.containsKey(key) && ("1".equals(getReqParamStrValue(params, key))
				|| "true".equals(getReqParamStrValue(params, key)) || "TRUE".equals(getReqParamStrValue(params, key))))
			return true;
		return false;
	}

	public Double getReqParamDoubleValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return Double.valueOf(params.get(key).toString());
		}
		return null;
	}
}
