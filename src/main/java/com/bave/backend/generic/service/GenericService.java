package com.bave.backend.generic.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.bave.backend.generic.configuration.Condition;


/**
 * Generated 12/06/2017 by HaiTX Computer
 */
public interface GenericService<T, PK> {
	PK persist(T objT) throws Exception;

	void persistBatch(List<T> objT) throws Exception;

	void update(T objT) throws Exception;

	void updateByProperty(PK pk, Map<String, Object> mapProperty) throws Exception;

	void updateByProperty(PK pk, String propertyName, Object value) throws Exception;

	void delete(T objT) throws Exception;

	void deleteBatch(List<T> objT) throws Exception;

	T findById(PK pk) throws Exception;

	T findByIdLock(PK pk, boolean paramBoolean) throws Exception;

	T findByIdUnique(PK pk) throws Exception;

	List<T> findAll() throws Exception;

	Date getSystemDate() throws Exception;

	T findSingleByObjT(T objT) throws Exception;

	List<T> findByObjT(T objT) throws Exception;

	List<T> findByObjT(T objT, String orderByColumn, boolean ascendingOrder) throws Exception;

	List<T> findAll(T objT, String[] arrOrderByColumn, boolean blnAscOrder, int intFirstItemIndex, int intMaxItems,
			boolean blnExactMatch) throws Exception;

	public List<T> findAllByIds(List<PK> lstPKs) throws Exception;

	PK countAll(T objT, boolean blnExactMatch) throws Exception;

	T findSingleByProperty(String propertyName, Object value) throws Exception;

	T findSingleByProperty(Map<String, Object> mapParams) throws Exception;

	public List<T> findByProperty(String propertyName, Object value, String[] orderByColumn, boolean blnAscOrder)
			throws Exception;

	List<T> findByProperty(String propertyName, Object value) throws Exception;

	List<T> findByProperty(String propertyName, Object value, String[] orderByColumn, boolean blnAscOrder,
			int intFirstItemIndex, int intMaxItems, boolean blnExactMatch) throws Exception;

	List<T> findByProperty(String propertyName, Object[] value) throws Exception;

	List<T> findByProperty(String propertyName, Object[] value, String[] arrOrderByColumn, boolean blnAscOrder)
			throws Exception;

	List<T> findByProperty(String propertyName, Object[] value, String[] arrOrderByColumn, boolean blnAscOrder,
			int intFirstItemIndex, int intMaxItems, boolean blnExactMatch) throws Exception;

	PK countByProperty(String propertyName, Object[] value) throws Exception;

	PK countByColumn(Map<String, Object> mapParams) throws Exception;

	List<T> findAllByParams(Condition condition) throws Exception;

	List<T> findAllByParams(Condition condition, String[] fields) throws Exception;

	PK countAllByParams(Condition condition) throws Exception;
}
