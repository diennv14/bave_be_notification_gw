package com.bave.backend.generic.configuration;

import java.util.Properties;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan({ "com.bave.backend.generic.configuration" })
@PropertySource(value = { "classpath:db.properties" })
public class HibernateConfiguration {

	@Autowired
	private Environment environment;

//	private Properties hibernateProperties() {
//		Properties properties = new Properties();
//		properties.put("hibernate.dialect", environment.getRequiredProperty("hibernate.dialect"));
//		properties.put("hibernate.connection.driver_class", environment.getRequiredProperty("jdbc.driver"));
//		properties.put("hibernate.connection.url", environment.getRequiredProperty("jdbc.url"));
//		properties.put("hibernate.connection.username", environment.getRequiredProperty("jdbc.user"));
//		properties.put("hibernate.connection.password", environment.getRequiredProperty("jdbc.password"));
//		properties.put("hibernate.show_sql", environment.getRequiredProperty("hibernate.show_sql"));
//		properties.put("hibernate.format_sql", environment.getRequiredProperty("hibernate.format_sql"));
//		properties.put("hibernate.connection.provider_class",
//				environment.getRequiredProperty("hibernate.connection.provider_class"));
//		properties.put("hibernate.c3p0.min_size", environment.getRequiredProperty("hibernate.c3p0.min_size"));
//		properties.put("hibernate.c3p0.max_size", environment.getRequiredProperty("hibernate.c3p0.max_size"));
//		properties.put("hibernate.c3p0.idle_test_period",
//				environment.getRequiredProperty("hibernate.c3p0.idle_test_period"));
//		properties.put("hibernate.c3p0.preferredTestQuery",
//				environment.getRequiredProperty("hibernate.c3p0.preferredTestQuery"));
//
//		properties.put("hibernate.c3p0.timeout", environment.getRequiredProperty("hibernate.c3p0.timeout"));
//		properties.put("hibernate.c3p0.checkoutTimeout",
//				environment.getRequiredProperty("hibernate.c3p0.checkoutTimeout"));
//
//		if (environment.getProperty("hibernate.temp.use_jdbc_metadata_defaults") != null) {
//			properties.put("hibernate.temp.use_jdbc_metadata_defaults",
//					environment.getRequiredProperty("hibernate.temp.use_jdbc_metadata_defaults"));
//		}
//
//		if (environment.getProperty("hibernate.c3p0.initialPoolSize") != null) {
//			properties.put("hibernate.c3p0.initialPoolSize",
//					environment.getRequiredProperty("hibernate.c3p0.initialPoolSize"));
//		}
//		if (environment.getProperty("hibernate.c3p0.acquireRetryAttempts") != null) {
//			properties.put("hibernate.c3p0.acquireRetryAttempts",
//					environment.getRequiredProperty("hibernate.c3p0.acquireRetryAttempts"));
//		}
//		if (environment.getProperty("hibernate.jdbc.time_zone") != null) {
//			properties.put("hibernate.jdbc.time_zone", environment.getRequiredProperty("hibernate.jdbc.time_zone"));
//		} else {
//			properties.put("hibernate.jdbc.time_zone", "UTC");
//		}
//
//		properties.put("spring.jpa.properties.hibernate.jdbc.time_zone", "UTC");
//
//		return properties;
//	}
//
//	@Bean(name = "dataSource")
//	public DataSource getDataSource() {
//		DriverManagerDataSource dataSource = new DriverManagerDataSource();
//		dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driver"));
//		dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
//		dataSource.setUsername(environment.getRequiredProperty("jdbc.user"));
//		dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
//
//		System.out.println("## getDataSource: " + dataSource);
//
//		return dataSource;
//	}
//
//	@Autowired
//	@Bean(name = "sessionFactory")
//	public SessionFactory getSessionFactory(DataSource dataSource) throws Exception {
//		// Fix Postgres JPA Error:
//		// Method org.postgresql.jdbc.PgConnection.createClob() is not yet implemented.
//		// properties.put("hibernate.temp.use_jdbc_metadata_defaults",false);
//
//		LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
//
//		// Package contain entity classes
//		factoryBean.setPackagesToScan(new String[] { "com.bave.backend.persistence.entity" });
//		factoryBean.setDataSource(dataSource);
//		factoryBean.setHibernateProperties(hibernateProperties());
//		factoryBean.afterPropertiesSet();
//		//
//		SessionFactory sf = factoryBean.getObject();
//		System.out.println("## getSessionFactory: " + sf);
//		return sf;
//	}
//
//	@Autowired
//	@Bean(name = "transactionManager")
//	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
//		HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
//		return transactionManager;
//	}
	
	
	private Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", environment.getRequiredProperty("spring.jpa.database-platform"));
		properties.put("hibernate.connection.driver_class", environment.getRequiredProperty("spring.datasource.driver-class-name"));
		properties.put("hibernate.connection.url", environment.getRequiredProperty("spring.datasource.url"));
		properties.put("hibernate.connection.username", environment.getRequiredProperty("spring.datasource.username"));
		properties.put("hibernate.connection.password", environment.getRequiredProperty("spring.datasource.password"));
		properties.put("hibernate.show_sql", environment.getRequiredProperty("spring.jpa.show-sql"));
		properties.put("hibernate.format_sql", environment.getRequiredProperty("spring.jpa.format_sql"));
		properties.put("hibernate.hikari.connectionTimeout", environment.getRequiredProperty("spring.datasource.hikari.connectionTimeout"));
		properties.put("hibernate.hikari.idleTimeout", environment.getRequiredProperty("spring.datasource.hikari.idleTimeout"));
		properties.put("hibernate.hikari.maximumPoolSize", environment.getRequiredProperty("spring.datasource.hikari.maximum-pool-size"));
		properties.put("hibernate.temp.use_jdbc_metadata_defaults", environment.getRequiredProperty("hibernate.temp.use_jdbc_metadata_defaults"));
		properties.put("hibernate.id.new_generator_mappings", false);

		return properties;
	}

	@Bean(name = "dataSource")
	public DataSource getDataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(environment.getRequiredProperty("spring.datasource.driver-class-name"));
		dataSource.setUrl(environment.getRequiredProperty("spring.datasource.url"));
		dataSource.setUsername(environment.getRequiredProperty("spring.datasource.username"));
		dataSource.setPassword(environment.getRequiredProperty("spring.datasource.password"));

		System.out.println("## getDataSource: " + dataSource);

		return dataSource;
	}

	@Autowired
	@Bean(name = "sessionFactory")
	public SessionFactory getSessionFactory(DataSource dataSource) throws Exception {
		// Fix Postgres JPA Error:
		// Method org.postgresql.jdbc.PgConnection.createClob() is not yet implemented.
		// properties.put("hibernate.temp.use_jdbc_metadata_defaults",false);

		LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();

		// Package contain entity classes
        factoryBean.setPackagesToScan(
                new String[] { "com.bave.backend.persistence.entity" });
		factoryBean.setDataSource(dataSource);
		factoryBean.setHibernateProperties(hibernateProperties());
		factoryBean.afterPropertiesSet();
		//
		SessionFactory sf = factoryBean.getObject();
        System.out.println("## getSessionFactory: " + sf);
		return sf;
	}

	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
		return transactionManager;
	}

}
