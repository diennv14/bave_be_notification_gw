package com.bave.backend.generic.configuration;

import java.beans.Transient;

public class KeyValue {
	private String key;
	private Object value;

	public KeyValue() {

	}

	public KeyValue(String key, Object value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	@Transient
	public Integer getIntValue() {
		return Integer.valueOf(this.value.toString());
	}

	@Transient
	public Long getLongValue() {
		return Long.valueOf(this.value.toString());
	}

	@Transient
	public Double getDoubleValue() {
		return Double.valueOf(this.value.toString());
	}

	@Transient
	public Float getFloatValue() {
		return Float.valueOf(this.value.toString());
	}

	@Transient
	public Boolean getBooleanValue() {
		if ("1".equals(this.value) || "true".equals(this.value.toString().toLowerCase()))
			return true;
		return false;
	}
}