package com.bave.backend.generic.configuration;

import java.sql.Types;

import org.hibernate.dialect.PostgreSQL82Dialect;

public class CustomPostgreSQLDialect extends PostgreSQL82Dialect {
    public CustomPostgreSQLDialect() {
        super();
        registerColumnType(Types.BIGINT, "bigint");
        registerColumnType(Types.INTEGER, "integer");
        registerColumnType(Types.SMALLINT, "smallint");
        registerColumnType(Types.TINYINT, "smallint");
    }

}
