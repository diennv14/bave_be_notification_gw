package com.bave.backend.generic.configuration;

import java.util.Calendar;
import java.util.Date;

public class HibernateUtil {
    Character[] aZ = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
            't', 'u', 'v', 'u', 'x', 'y', 'z', '-', '_', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

    public static String getPaping(String field, Integer lastID) {
        if (field == null || field.equalsIgnoreCase(""))
            return null;
        else if (lastID == null || lastID <= 0)
            return "";
        else
            return " and " + field + " >  " + lastID;
    }

    public static long parseLong(int value) {
        try {
            return Long.valueOf(value);
        } catch (Exception ex) {

        }
        return 0;
    }

    public static String getLimit(Integer records) {
        if (records == null || records <= 0)
            return "";
        else
            return " limit " + records;
    }

    public static Date addOneDay(Date date) {
        Calendar c = Calendar.getInstance();
        if (date != null) {
            c.setTime(date);
            c.add(Calendar.DAY_OF_MONTH, 1);
        }
        return c.getTime();
    }

    public static String convertToLikeString(String source) {
        if (source == null) {
            return "";
        }

        source = source.trim().toLowerCase();
        String des = "";
        Boolean isSpecial;

        char[] specialChar = {'%', '_', '?', '\''};
        for (int i = 0; i < source.length(); i++) {
            isSpecial = false;
            for (int j = 0; j < specialChar.length; j++) {
                if (specialChar[j] == source.charAt(i)) {
                    isSpecial = true;
                    break;

                }
            }
            if (isSpecial) {
                des = des + '!' + source.charAt(i);
            } else {
                des = des + source.charAt(i);
            }

        }
        return des;
    }

    public static String getPapingDesc(String field, int lastID) {
        if (field == null || field.equalsIgnoreCase(""))
            return null;
        else if (lastID <= 0)
            return "";
        else
            return " and " + field + " <  " + lastID;
    }
}
