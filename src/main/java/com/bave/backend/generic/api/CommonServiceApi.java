package com.bave.backend.generic.api;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

public abstract class CommonServiceApi {

	@Autowired
	protected HttpServletRequest request;

	protected HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public String getHeader(String name) {
		return request.getHeader(name);
	}

	public String getDeviceId() {
		return request.getHeader("device_id");
	}

	public Float getReqParamFloatValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return Float.valueOf(params.get(key).toString());
		}
		return null;
	}

	public String getReqParamStrValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return (params.get(key) != null) ? params.get(key).toString() : null;
		}
		return null;
	}

	public Date getReqParamDateValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return (Date) params.get(key);
		}
		return null;
	}

	public Boolean getReqParamBooleanValue(Map<String, Object> params, String key) {
		if (params != null && params.containsKey(key) && ("1".equals(getReqParamStrValue(params, key))
				|| "true".equals(getReqParamStrValue(params, key)) || "TRUE".equals(getReqParamStrValue(params, key))))
			return true;
		return false;
	}

	public Integer getReqParamIntegerValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return Integer.valueOf(params.get(key).toString());
		}
		return null;
	}

	public Long getReqParamLongValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return Long.valueOf(params.get(key).toString());
		}
		return null;
	}

	/////////////
	public Float getReqParamFloatValue(Object val) {
		if (val != null) {
			return Float.valueOf(val.toString());
		}
		return null;
	}

	public String getReqParamStrValue(Object val) {
		if (val != null) {
			return val.toString().trim();
		}
		return null;
	}

	public Date getReqParamDateValue(Object val) {
		if (val != null) {
			return (Date) val;
		}
		return null;
	}

	public Boolean getReqParamBooleanValue(Object val) {
		if (val != null && ("1".equals(val) || "true".equals(val) || "TRUE".equals(val)))
			return true;
		return false;
	}

	public Integer getReqParamIntegerValue(Object val) {
		if (val != null) {
			return Integer.valueOf(val.toString());
		}
		return null;
	}

	public Long getReqParamLongValue(Object val) {
		if (val != null) {
			return Long.valueOf(val.toString());
		}
		return null;
	}

}
