package com.bave.backend.generic.api.impl;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import com.bave.backend.generic.api.CommonServiceApi;
import com.bave.backend.generic.api.GenericAPI;
import com.bave.backend.generic.service.GenericService;

@RestController
@Service
public abstract class GenericServiceAPIImpl<T, PK> extends CommonServiceApi implements GenericAPI<T, PK> {

	private GenericService<T, PK> genericService;

	//
	public GenericServiceAPIImpl() {
	}

	public GenericServiceAPIImpl(GenericService<T, PK> genericService) {
		this.genericService = genericService;
	}

	// @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	// public Response delete(@RequestBody T persistentObject) {
	// Response response = new Response();
	// try {
	// genericService.delete(persistentObject);
	// response.setResult(true);
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/delete-batch", method = RequestMethod.DELETE)
	// public Response deleteBatch(@RequestBody List<T> objT) {
	// Response response = new Response();
	// try {
	// genericService.deleteBatch(objT);
	// response.setResult(true);
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/find-all", method = RequestMethod.GET)
	// public Response findAll() {
	// Response response = new Response();
	// try {
	// response.setResult(genericService.findAll());
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/find-id", method = RequestMethod.GET)
	// public Response findById(@RequestParam("id") PK id) {
	// Response response = new Response();
	// try {
	// response.setResult(genericService.findById(id));
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/find-unique", method = RequestMethod.GET)
	// public Response findByIdUnique(@RequestParam("id") PK id) {
	// Response response = new Response();
	// try {
	// response.setResult(genericService.findByIdUnique(id));
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/add", method = RequestMethod.POST)
	// public Response persist(@RequestBody T newInstance) {
	// Response response = new Response();
	// try {
	// response.setResult(genericService.persist(newInstance));
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/add-batch", method = RequestMethod.POST)
	// public Response persistBatch(@RequestBody List<T> objT) {
	// Response response = new Response();
	// try {
	// genericService.persistBatch(objT);
	// response.setResult(true);
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/update", method = RequestMethod.POST)
	// public Response update(@RequestBody T transientObject) {
	// Response response = new Response();
	// try {
	// genericService.update(transientObject);
	// response.setResult(true);
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// // public void saveOrUpdate(T transientObject) throws Exception {
	// // genericService.saveOrUpdate(transientObject);
	// // }
	// @RequestMapping(value = "/db-date", method = RequestMethod.GET)
	// public Response getSystemDate() {
	// Response response = new Response();
	// try {
	// response.setResult(genericService.getSystemDate());
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/find-id-lock", method = RequestMethod.GET)
	// public Response findByIdLock(@RequestParam("id") PK id,
	// @RequestParam("lock") boolean lock) {
	// Response response = new Response();
	// try {
	// response.setResult(genericService.findByIdLock(id, lock));
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/find-single-obj", method = RequestMethod.POST)
	// public Response findSingleByObjT(@RequestBody T objT) {
	// Response response = new Response();
	// try {
	// response.setResult(genericService.findSingleByObjT(objT));
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/find-by-obj", method = RequestMethod.POST)
	// public Response findByObjT(@RequestBody T objT) {
	// Response response = new Response();
	// try {
	// response.setResult(genericService.findByObjT(objT));
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/find-by-obj-order", method =
	// RequestMethod.POST)
	// public Response findByObjT(@RequestBody T objT,
	// @RequestParam("order_columns") String orderByColumn,
	// @RequestParam("asc_order") Boolean ascendingOrder) {
	// Response response = new Response();
	// try {
	// response.setResult(genericService.findByObjT(objT, orderByColumn,
	// ascendingOrder));
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/find-all-paging", method = RequestMethod.POST)
	// public Response findAll(@RequestBody T objT,
	// @RequestParam(value = "order_columns", required = false) String
	// orderByColumns,
	// @RequestParam(value = "asc_order", defaultValue = "true", required =
	// false) Boolean ascOrder,
	// @RequestParam(value = "first_index", required = false) Integer
	// firstItemIndex,
	// @RequestParam(value = "max_items", required = false) Integer maxItems,
	// @RequestParam(value = "exact_match", defaultValue = "true", required =
	// false) Boolean exactMatch) {
	// Response response = new Response();
	// try {
	// response.setResult(genericService.findAll(objT, (orderByColumns == null)
	// ? null : orderByColumns.split(","),
	// ascOrder, firstItemIndex, maxItems, exactMatch));
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/count-all", method = RequestMethod.POST)
	// public Response countAll(@RequestBody T objT, @RequestParam("exactmatch")
	// Boolean blnExactMatch) {
	// Response response = new Response();
	// try {
	// response.setResult(genericService.countAll(objT, blnExactMatch));
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/find-single-by-prop", method =
	// RequestMethod.GET)
	// public Response findSingleByProperty(@RequestParam("property_name")
	// String propertyName,
	// @RequestParam("value") Object value) {
	// Response response = new Response();
	// try {
	// response.setResult(genericService.findSingleByProperty(propertyName,
	// value));
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/find-by-prop", method = RequestMethod.GET)
	// public Response findByProperty(@RequestParam("property_name") String
	// propertyName,
	// @RequestParam("value") String value,
	// @RequestParam(value = "order_columns", required = false) String
	// orderByColumns,
	// @RequestParam(value = "asc_order", defaultValue = "true", required =
	// false) Boolean ascOrder,
	// @RequestParam(value = "first_index", defaultValue = "0", required =
	// false) Integer firstItemIndex,
	// @RequestParam(value = "max_items", defaultValue = "0", required = false)
	// Integer maxItems,
	// @RequestParam(value = "exact_match", defaultValue = "true", required =
	// false) Boolean exactMatch) {
	// Response response = new Response();
	// try {
	// response.setResult(genericService.findByProperty(propertyName,
	// value.split(","),
	// (orderByColumns == null) ? null : orderByColumns.split(","), ascOrder,
	// firstItemIndex, maxItems,
	// exactMatch));
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/count-by-prop", method = RequestMethod.GET)
	// public Response countByProperty(@RequestParam("property_name") String
	// propertyName,
	// @RequestParam("value") String value) {
	// Response response = new Response();
	// try {
	// response.setResult(genericService.countByProperty(propertyName,
	// value.split(",")));
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/find-by-cond", method = RequestMethod.POST)
	// public Response findAllByParams(@RequestBody Condition condition,
	// @RequestParam(value = "order_columns", required = false) String
	// orderByColumns,
	// @RequestParam(value = "asc_order", required = false) Boolean ascOrder,
	// @RequestParam(value = "first_index", required = false) Integer
	// firstItemIndex,
	// @RequestParam(value = "max_items", required = false) Integer maxItems,
	// @RequestParam(value = "exact_match", required = false) Boolean
	// exactMatch) throws Exception {
	// Response response = new Response();
	// try {
	// condition.reSetCondition(orderByColumns, ascOrder, firstItemIndex,
	// maxItems, exactMatch);
	// response.setResult(genericService.findAllByParams(condition));
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @RequestMapping(value = "/count-by-cond", method = RequestMethod.POST)
	// public Response countAllByParams(@RequestBody Condition condition,
	// @RequestParam(value = "exact_match") Boolean blnExactMatch) throws
	// Exception {
	// Response response = new Response();
	// try {
	// condition.reSetCondition(null, true, 0, 0, blnExactMatch);
	// response.setResult(genericService.countAllByParams(condition));
	// response.setStatus(Response.STATUS_SUCCESS);
	// } catch (Exception e) {
	// e.printStackTrace();
	// response.setStatus(Response.STATUS_FAILED);
	// response.setMessage(e.getMessage());
	// }
	// return response;
	// }
	//
	// @ExceptionHandler
	// @ResponseStatus(HttpStatus.BAD_REQUEST)
	// public Response handleException(MethodArgumentNotValidException
	// exception) {
	// Response response = new Response();
	// response.setStatus(Response.STATUS_INPUT_INVALID);
	// String errorMsg = exception.getBindingResult().getFieldErrors().stream()
	// .map(DefaultMessageSourceResolvable::getDefaultMessage).findFirst().orElse(exception.getMessage());
	// response.setMessage(errorMsg);
	// return response;
	// }
}
