package com.bave.backend.generic.api;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Basic DAO operations dependent with Hibernate's specific classes
 *
 * @see SessionFactory
 */
@SuppressWarnings("unchecked")
@Repository
public abstract class GenericBaseDaoImpl {

	@Autowired
	private SessionFactory sessionFactory;

	public GenericBaseDaoImpl() {
	}

	public Transaction getTransaction() {
		return sessionFactory.getCurrentSession().getTransaction();
	}

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	public Query setResultTransformer(Query query, Class resultClass) {
		query.setResultTransformer(new AliasToBeanNestedResultTransformer(resultClass));
		return query;
	}

	public Query pagingQuery(Query query, Integer firstItem, Integer maxItem) {
		if (firstItem != null && maxItem != null && maxItem > 0 && firstItem >= 0) {
			query.setMaxResults(maxItem);
			query.setFirstResult(firstItem);
		}
		return query;
	}

	public SQLQuery pagingQuery(SQLQuery query, Integer firstItem, Integer maxItem) {
		if (firstItem != null && maxItem != null && maxItem > 0 && firstItem >= 0) {
			query.setMaxResults(maxItem);
			query.setFirstResult(firstItem);
		}
		return query;
	}

	public Query setParametersByMap(Query query, HashMap<String, Object> mapParams) {
		for (String param : mapParams.keySet()) {
			Object value = mapParams.get(param);
			if (value instanceof Collection<?>) {
				query.setParameterList(param, (Collection<?>) value);
				continue;
			} else if (value instanceof String[]) {
				query.setParameterList(param, (Object[]) value);
				continue;
			} else {
				query.setParameter(param, value);
				continue;
			}
		}
		return query;
	}

	public SQLQuery setParametersByMap(SQLQuery query, HashMap<String, Object> mapParams) {
		for (String param : mapParams.keySet()) {
			Object value = mapParams.get(param);
			if (value instanceof Collection<?>) {
				query.setParameterList(param, (Collection<?>) value);
				continue;
			} else if (value instanceof String[]) {
				query.setParameterList(param, (Object[]) value);
				continue;
			} else {
				query.setParameter(param, value);
				continue;
			}
		}
		return query;
	}

	public HashMap<String, Object> newMapParameters() {
		return new HashMap<String, Object>();
	}

	public String createParameter(String condition, String sql, HashMap<String, Object> mapParams, String key,
			Object value) {
		if (value != null) {
			sql += condition;
			mapParams.put(key, value);
		}
		return sql;
	}

	////////////////////////////////////////////////// 5/12/2017

	////////////////////////////////////////////////// 5/12/2017

	public Integer getReqParamIntegerValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return Integer.valueOf(params.get(key).toString());
		}
		return null;
	}

	public Float getReqParamFloatValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return Float.valueOf(params.get(key).toString());
		}
		return null;
	}

	public Long getReqParamLongValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return Long.valueOf(params.get(key).toString());
		}
		return null;
	}

	public String getReqParamStrValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return (params.get(key) != null) ? params.get(key).toString() : null;
		}
		return null;
	}

	public Date getReqParamDateValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return (Date) params.get(key);
		}
		return null;
	}

	public Boolean getReqParamBooleanValue(Map<String, Object> params, String key) {
		if (params != null && params.containsKey(key) && ("1".equals(getReqParamStrValue(params, key))
				|| "true".equals(getReqParamStrValue(params, key)) || "TRUE".equals(getReqParamStrValue(params, key))))
			return true;
		return false;
	}

	public Double getReqParamDoubleValue(Map<String, Object> params, String key) {
		if (params != null && params.get(key) != null) {
			return Double.valueOf(params.get(key).toString());
		}
		return null;
	}
}