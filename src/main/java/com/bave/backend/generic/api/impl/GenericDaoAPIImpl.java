package com.bave.backend.generic.api.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.bave.backend.generic.api.GenericAPI;
import com.bave.backend.generic.configuration.Condition;
import com.bave.backend.generic.dao.GenericDao;
import com.bave.backend.util.Response;

@RestController
@Service
public abstract class GenericDaoAPIImpl<T, PK> implements GenericAPI<T, PK> {

	@Autowired(required = false)
	protected HttpServletRequest request;

	public HttpServletRequest getRequest() {
		return request;
	}

	private GenericDao<T, PK> genericDao;

	public GenericDaoAPIImpl() {
	}

	public GenericDaoAPIImpl(GenericDao<T, PK> genericDao) {
		this.genericDao = genericDao;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public Response delete(@RequestBody T persistentObject) {
		Response response = new Response();
		try {
			genericDao.delete(persistentObject);
			response.setResult(true);
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/delete-batch", method = RequestMethod.DELETE)
	public Response deleteBatch(@RequestBody List<T> objT) {
		Response response = new Response();
		try {
			genericDao.deleteBatch(objT);
			response.setResult(true);
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/find-all", method = RequestMethod.GET)
	public Response findAll() {
		Response response = new Response();
		try {
			response.setResult(genericDao.findAll());
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/find-id", method = RequestMethod.GET)
	public Response findById(@RequestParam("id") PK id) {
		Response response = new Response();
		try {
			response.setResult(genericDao.findById(id));
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/find-unique", method = RequestMethod.GET)
	public Response findByIdUnique(@RequestParam("id") PK id) {
		Response response = new Response();
		try {
			response.setResult(genericDao.findByIdUnique(id));
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public Response persist(@RequestBody T newInstance) {
		Response response = new Response();
		try {
			response.setResult(genericDao.persist(newInstance));
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/add-batch", method = RequestMethod.POST)
	public Response persistBatch(@RequestBody List<T> objT) {
		Response response = new Response();
		try {
			genericDao.persistBatch(objT);
			response.setResult(true);
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public Response update(@RequestBody T transientObject) {
		Response response = new Response();
		try {
			genericDao.update(transientObject);
			response.setResult(true);
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	// public void saveOrUpdate(T transientObject) throws Exception {
	// genericDao.saveOrUpdate(transientObject);
	// }
	@RequestMapping(value = "/db-date", method = RequestMethod.GET)
	public Response getSystemDate() {
		Response response = new Response();
		try {
			response.setResult(genericDao.getSystemDate());
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/find-id-lock", method = RequestMethod.GET)
	public Response findByIdLock(@RequestParam("id") PK id, @RequestParam("lock") boolean lock) {
		Response response = new Response();
		try {
			response.setResult(genericDao.findByIdLock(id, lock));
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/find-single-obj", method = RequestMethod.POST)
	public Response findSingleByObjT(@RequestBody T objT) {
		Response response = new Response();
		try {
			response.setResult(genericDao.findSingleByObjT(objT));
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/find-by-obj", method = RequestMethod.POST)
	public Response findByObjT(@RequestBody T objT) {
		Response response = new Response();
		try {
			response.setResult(genericDao.findByObjT(objT));
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/find-by-obj-order", method = RequestMethod.POST)
	public Response findByObjT(@RequestBody T objT, @RequestParam("order_columns") String orderByColumn,
			@RequestParam("asc_order") Boolean ascendingOrder) {
		Response response = new Response();
		try {
			response.setResult(genericDao.findByObjT(objT, orderByColumn, ascendingOrder));
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/find-all-paging", method = RequestMethod.POST)
	public Response findAll(@RequestBody T objT,
			@RequestParam(value = "order_columns", required = false) String orderByColumns,
			@RequestParam(value = "asc_order", defaultValue = "true", required = false) Boolean ascOrder,
			@RequestParam(value = "first_index", required = false) Integer firstItemIndex,
			@RequestParam(value = "max_items", required = false) Integer maxItems,
			@RequestParam(value = "exact_match", defaultValue = "true", required = false) Boolean exactMatch) {
		Response response = new Response();
		try {
			response.setResult(genericDao.findAll(objT, (orderByColumns == null) ? null : orderByColumns.split(","),
					ascOrder, firstItemIndex, maxItems, exactMatch));
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/count-all", method = RequestMethod.POST)
	public Response countAll(@RequestBody T objT, @RequestParam("exactmatch") Boolean blnExactMatch) {
		Response response = new Response();
		try {
			response.setResult(genericDao.countAll(objT, blnExactMatch));
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/find-single-by-prop", method = RequestMethod.GET)
	public Response findSingleByProperty(@RequestParam("property_name") String propertyName,
			@RequestParam("value") Object value) {
		Response response = new Response();
		try {
			response.setResult(genericDao.findSingleByProperty(propertyName, value));
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/find-by-prop", method = RequestMethod.GET)
	public Response findByProperty(@RequestParam("property_name") String propertyName,
			@RequestParam("value") String value,
			@RequestParam(value = "order_columns", required = false) String orderByColumns,
			@RequestParam(value = "asc_order", defaultValue = "true", required = false) Boolean ascOrder,
			@RequestParam(value = "first_index", defaultValue = "0", required = false) Integer firstItemIndex,
			@RequestParam(value = "max_items", defaultValue = "0", required = false) Integer maxItems,
			@RequestParam(value = "exact_match", defaultValue = "true", required = false) Boolean exactMatch) {
		Response response = new Response();
		try {
			response.setResult(genericDao.findByProperty(propertyName, value.split(","),
					(orderByColumns == null) ? null : orderByColumns.split(","), ascOrder, firstItemIndex, maxItems,
					exactMatch));
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/count-by-prop", method = RequestMethod.GET)
	public Response countByProperty(@RequestParam("property_name") String propertyName,
			@RequestParam("value") String value) {
		Response response = new Response();
		try {
			response.setResult(genericDao.countByProperty(propertyName, value.split(",")));
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/find-by-cond", method = RequestMethod.POST)
	public Response findAllByParams(@RequestBody Condition condition,
			@RequestParam(value = "order_columns", required = false) String orderByColumns,
			@RequestParam(value = "asc_order", required = false) Boolean ascOrder,
			@RequestParam(value = "first_index", required = false) Integer firstItemIndex,
			@RequestParam(value = "max_items", required = false) Integer maxItems,
			@RequestParam(value = "exact_match", required = false) Boolean exactMatch) throws Exception {
		Response response = new Response();
		try {
			condition.reSetCondition(orderByColumns, ascOrder, firstItemIndex, maxItems, exactMatch);
			response.setResult(genericDao.findAllByParams(condition));
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@RequestMapping(value = "/count-by-cond", method = RequestMethod.POST)
	public Response countAllByParams(@RequestBody Condition condition,
			@RequestParam(value = "exact_match") Boolean blnExactMatch) throws Exception {
		Response response = new Response();
		try {
			condition.reSetCondition(null, true, 0, 0, blnExactMatch);
			response.setResult(genericDao.countAllByParams(condition));
			response.setStatus(Response.STATUS_SUCCESS);
		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(Response.STATUS_FAILED);
			response.setMessage(e.getMessage());
		}
		return response;
	}

	@ExceptionHandler
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public Response handleException(MethodArgumentNotValidException exception) {
		Response response = new Response();
		response.setStatus(Response.STATUS_INPUT_INVALID);
		String errorMsg = exception.getBindingResult().getFieldErrors().stream()
				.map(DefaultMessageSourceResolvable::getDefaultMessage).findFirst().orElse(exception.getMessage());
		response.setMessage(errorMsg);
		return response;
	}
}
