package com.bave.backend.security.config;
//package com.bave.backend.config;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;

//import com.bave.backend.model.Role;
//import com.bave.backend.model.RoleResource;
//import com.bave.backend.repository.RoleRepository;
//import com.bave.backend.repository.RoleResourceRepository;

@EnableWebSecurity
@Configuration
@ComponentScan("com.bave")
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private AuthenticationEntryPoint authEntryPoint;

	@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();        
        auth.inMemoryAuthentication()
            .passwordEncoder(encoder)
            .withUser("spring")
            .password(encoder.encode("secret"))            
            .roles("ADMIN");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers("/private/**")
            .authenticated()
            .antMatchers("/**")
            .permitAll();
        http.cors().and().csrf().disable();
    }

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("bave_admin").password("baveAa@aAevab").roles("ADMIN");
		auth.inMemoryAuthentication().withUser("bave_partner").password("partnerAa@123").roles("ADMIN");
		auth.inMemoryAuthentication().withUser("bave_odoo").password("bave_odooAa@bave_odoo").roles("ADMIN");
	}

	// @Autowired
	// CustomAuthenticationProvider customAuthenticationProvider;
	//
	// @Override
	// protected void configure(HttpSecurity http) throws Exception {
	// http.authorizeRequests().antMatchers("/member/login/").permitAll().antMatchers("/services/**")
	// .hasAnyRole("ADMIN").and().httpBasic();
	// //
	// http.csrf().disable().authorizeRequests().antMatchers("/authen").hasAnyRole("ADMIN").and().httpBasic();
	// //
	// http.authorizeRequests().antMatchers("/authen/**").permitAll().anyRequest().authenticated().and();
	//
	// // http.authorizeRequests().antMatchers("/").permitAll();
	// // http.csrf().disable();
	// }
	//
	// @Autowired
	// public void configureGlobal(AuthenticationManagerBuilder auth) throws
	// Exception {
	// auth.authenticationProvider(customAuthenticationProvider);
	// }
	//
	// @Override
	// protected void configure(AuthenticationManagerBuilder auth) throws
	// Exception {
	// auth.authenticationProvider(customAuthenticationProvider);
	// }
	//
	// @Bean
	// @Override
	// public AuthenticationManager authenticationManagerBean() throws Exception
	// {
	// return super.authenticationManagerBean();
	// }
	//
	// // @Bean
	// // public CustomFilter customFilter() {
	// // return new CustomFilter();
	// // }
	//
	public static void main(String[] str) {
		// String s = RestTemplateBuilder.basicAuthorization("user",
		// "password").build();

		// TestRestTemplate restTemplate = new TestRestTemplate("admin",
		// "admin");

		String plainClientCredentials = "bave_odoo:bave_odooAa@bave_odoo"; // YmF2ZV9vZG9vOmJhdmVfb2Rvb0FhQGJhdmVfb2Rvbw==
		String base64ClientCredentials = new String(Base64.getEncoder().encode(plainClientCredentials.getBytes()));

		System.out.println(base64ClientCredentials);
	}

}