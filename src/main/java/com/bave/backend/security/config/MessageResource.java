package com.bave.backend.security.config;

import java.util.Locale;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import com.bave.backend.util.BEException;
import com.bave.backend.util.Response;

@Component
public class MessageResource {

	private static MessageResource instance;

	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	private static MessageSource messageSource;

	@PostConstruct
	public void registerInstance() {
		instance = this;
	}

	public String getMessage(String key) {
		try {
			Locale locale = LocaleContextHolder.getLocale();
			return messageSource.getMessage(key, null, locale);
		} catch (Exception e) {
			e.printStackTrace();
			return key;
		}
	}

	public String getMessage(String key, Object[] params) {
		try {
			Locale locale = LocaleContextHolder.getLocale();
			return messageSource.getMessage(key, params, locale);
		} catch (Exception e) {
			e.printStackTrace();
			return key;
		}
	}

	public static String getI18NMessage(String key) {
		try {
			if (instance != null) {
				if (messageSource == null)
					messageSource = instance.applicationContext.getBean(MessageSource.class);
				Locale locale = LocaleContextHolder.getLocale();
				return messageSource.getMessage(key, null, locale);
			} else {
				return key;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return key;
		}
	}

	public static String getI18NMessage(String key, Object... params) {
		try {
			if (instance != null) {
				if (messageSource == null)
					messageSource = instance.applicationContext.getBean(MessageSource.class);
				Locale locale = LocaleContextHolder.getLocale();
				return messageSource.getMessage(key, params, locale);
			} else {
				return key;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return key;
		}
	}

	public static BEException exception(String errorCode) {
		return new BEException(errorCode, getI18NMessage(errorCode));
	}

	public static BEException exception(String errorCode, Object... params) {
		return new BEException(errorCode, getI18NMessage(errorCode, params));
	}

	public static Response response(String errorCode) {
		Response response = new Response();
		response.setStatus(Response.STATUS_FAILED);
		response.setErrorCode(errorCode);
		response.setMessage(getI18NMessage(errorCode));
		response.setResult(false);
		return response;
	}

	public static Response response(String errorCode, Object... params) {
		Response response = new Response();
		response.setStatus(Response.STATUS_FAILED);
		response.setErrorCode(errorCode);
		response.setMessage(getI18NMessage(errorCode, params));
		response.setResult(false);
		return response;
	}

	public static Response responseRs(String errorCode, Object result) {
		Response response = new Response();
		response.setStatus(Response.STATUS_FAILED);
		response.setErrorCode(errorCode);
		response.setMessage(getI18NMessage(errorCode));
		response.setResult(result);
		return response;
	}

	public static Response responseRs(String errorCode, Object result, Object... params) {
		Response response = new Response();
		response.setStatus(Response.STATUS_FAILED);
		response.setErrorCode(errorCode);
		response.setMessage(getI18NMessage(errorCode, params));
		response.setResult(result);
		return response;
	}
}
