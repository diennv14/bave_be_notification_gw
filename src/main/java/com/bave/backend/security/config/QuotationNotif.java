package com.bave.backend.security.config;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class QuotationNotif implements Serializable {
	// select qresp.*,qreq.member_id, md.member_id md_member_id, md.player_id
	@JsonProperty("id")
	private String id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("member_id")
	private Long memberId;
	@JsonProperty("member_name")
	private String memberName;
	@JsonProperty("player_id")
	private String playerId;
	@JsonProperty("service_provider_id")
	private long serviceProviderId;
	@JsonProperty("service_provider_name")
	private String serviceProviderName;
	@JsonProperty("serviceProviderAvatar")
	private String serviceProviderAvatar;
	@JsonProperty("action")
	private String action;
	@JsonProperty("message")
	private String message;
	@JsonProperty("destination_object")
	private String destinationObject;
	@JsonProperty("unread_notify")
	private Integer unreadNotify;

	public QuotationNotif() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public long getServiceProviderId() {
		return serviceProviderId;
	}

	public void setServiceProviderId(long serviceProviderId) {
		this.serviceProviderId = serviceProviderId;
	}

	public String getServiceProviderName() {
		return serviceProviderName;
	}

	public void setServiceProviderName(String serviceProviderName) {
		this.serviceProviderName = serviceProviderName;
	}

	public String getServiceProviderAvatar() {
		return serviceProviderAvatar;
	}

	public void setServiceProviderAvatar(String serviceProviderAvatar) {
		this.serviceProviderAvatar = serviceProviderAvatar;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDestinationObject() {
		return destinationObject;
	}

	public void setDestinationObject(String destinationObject) {
		this.destinationObject = destinationObject;
	}

	public Integer getUnreadNotify() {
		return unreadNotify;
	}

	public void setUnreadNotify(Integer unreadNotify) {
		this.unreadNotify = unreadNotify;
	}

}
