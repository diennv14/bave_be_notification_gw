package com.bave.backend.security.config;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

@Component
public class RequestLogger extends OncePerRequestFilter {
	private static Logger logger = LoggerFactory.getLogger("api_file");
	private boolean includeResponsePayload = true;
	private int maxPayloadLength = 50000;

	private String getContentAsString(byte[] buf, int maxLength, String charsetName) {
		if (buf == null || buf.length == 0)
			return "";
		int length = Math.min(buf.length, this.maxPayloadLength);
		try {
			return new String(buf, 0, length, charsetName);
		} catch (UnsupportedEncodingException ex) {
			return "Unsupported Encoding";
		}
	}

	/**
	 * Log each request and respponse with full Request URI, content payload and
	 * duration of the request in ms.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @param filterChain
	 *            chain of filters
	 * @throws ServletException
	 * @throws IOException
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {

		long startTime = System.currentTimeMillis();
		StringBuffer reqInfo = new StringBuffer().append("[").append(startTime % 10000) // request
																						// ID
				.append("] ").append(request.getMethod()).append(" ").append(request.getRequestURL());

		String queryString = request.getQueryString();
		if (queryString != null) {
			reqInfo.append("?").append(queryString);
		}

		if (request.getAuthType() != null) {
			reqInfo.append(", authType=").append(request.getAuthType());
		}
		if (request.getUserPrincipal() != null) {
			reqInfo.append(", principalName=").append(request.getUserPrincipal().getName());
		}

		// ========= Log request and response payload ("body") ========
		// We CANNOT simply read the request payload here, because then the
		// InputStream would be consumed and cannot be read again by the actual
		// processing/server.
		// String reqBody =
		// DoogiesUtil._stream2String(request.getInputStream()); // THIS WOULD
		// NOT WORK!
		// So we need to apply some stronger magic here :-)
		ContentCachingRequestWrapper wrappedRequest = new ContentCachingRequestWrapper(request);
		ContentCachingResponseWrapper wrappedResponse = new ContentCachingResponseWrapper(response);

		filterChain.doFilter(wrappedRequest, wrappedResponse); // ======== This
																// performs the
																// actual
																// request!
		long duration = System.currentTimeMillis() - startTime;

		if (request.getRequestURI().indexOf("/update-location") < 0
				&& request.getRequestURI().indexOf("/erp/image/") < 0) {
			// I can only log the request's body AFTER the request has been made
			// and
			// ContentCachingRequestWrapper did its work.
			String logResq = (wrappedRequest.getRemoteAddr() + " Request => " + reqInfo + "\n");

			String params = "HEADER: ";
			Map<String, String> mheader = getHeaders(request);
			for (String key : mheader.keySet()) {
				params += key + "=" + mheader.get(key) + "; ";
			}
			logResq += params + "\n";

			params = "PARAMS: ";
			for (String key : wrappedRequest.getParameterMap().keySet()) {
				for (String value : wrappedRequest.getParameterMap().get(key)) {
					params += value + ",";
				}
				logResq += key + "=" + params + "; ";
				params = "";
			}
			logResq += params;

			String requestBody = this.getContentAsString(wrappedRequest.getContentAsByteArray(), this.maxPayloadLength,
					request.getCharacterEncoding());
			if (requestBody.length() > 0) {
				logResq += ("Request body:\n" + requestBody);
			}
			logger.info(logResq);

			String logRes = "";
			logRes += (wrappedRequest.getRemoteAddr() + " Response <= " + reqInfo + ": returned status="
					+ response.getStatus() + " in " + duration + "ms\n");
			if (includeResponsePayload) {
				byte[] buf = wrappedResponse.getContentAsByteArray();
				logRes += ("Response body:\n"
						+ getContentAsString(buf, this.maxPayloadLength, response.getCharacterEncoding()));
			}
			logger.info(logRes);
		}
		wrappedResponse.copyBodyToResponse(); // IMPORTANT: copy content of
												// response back into
												// original
												// response
	}

	public static Map<String, String> getHeaders(HttpServletRequest r) {
		Map<String, String> result = new HashMap<String, String>();
		Enumeration<?> names = r.getHeaderNames();
		while (names.hasMoreElements()) {
			String name = (String) names.nextElement();
			String value = r.getHeader(name);
			result.put(name, value);
		}
		return result;
	}

}