package com.bave.backend.security.config;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService implements UserDetailsService {

	private Map<String, String> mapUser = new HashMap<String, String>() {
		{
			put("bave", "baveAa@123");
			put("bave_partner", "partnerAa@123");
		}
	};

	// @Autowired
	// private AuthoProfileDao authoProfileDao;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// AuthoProfile user;
		try {
			// user = authoProfileDao.findSingleByProperty(new Object[] { new
			// Object[] { "login", "=", username } }, null);
			// GrantedAuthority authority = new SimpleGrantedAuthority("ADMIN");
			// UserDetails userDetails = (UserDetails) new
			// org.springframework.security.core.userdetails.User(username,
			// user.getPassword(), Arrays.asList(authority));
			// return userDetails;

			GrantedAuthority authority = new SimpleGrantedAuthority("ADMIN");
			UserDetails userDetails = (UserDetails) new org.springframework.security.core.userdetails.User(username,
					mapUser.get(username), Arrays.asList(authority));
			return userDetails;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
