package com.bave.backend.security.config;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bave.backend.persistence.entity.Notification;
import com.bave.backend.persistence.entity.NotificationConfig;
import com.bave.backend.persistence.entity.NotificationTemplate;
import com.bave.backend.util.LinkQueue;

@Component
public class VarStatic {
	private final Logger logger = LoggerFactory.getLogger("api_file");
	
	public static final String kafka_send_failed_topic = "SendFailedTopic";
	public static final String kafka_send_success_topic = "SendSuccessTopic";
	public static final String kafka_send_topic = "SendTopic";
	public static final String kafka_source_topic = "SourceTopic";

	public static final String STATUS_UNAUTHORIZED = "0";
	public static final String STATUS_AUTHORIZED = "1";

	public static final String MEMBER_TYPE_ENDUSER = "1";
	public static final String MEMBER_TYPE_AGENCY = "2";
	public static final String MEMBER_TYPE_PARNER = "3";

	public static final String CHANNEL_ONESIGNAL = "onesignal";
	public static final String CHANNEL_SMS = "sms";
	public static final String CHANNEL_EMAIL = "email";
	public static final String CHANNEL_MARKETING = "marketing";

	public static final String NOTI_TYPE_MARKETING = "marketing";
	public static final String NOTI_TYPE_NOTICE = "notice";

	public static final String NOTI_APP_AGARA = "agara";
	public static final String NOTI_APP_BAVE = "bave";

	public static final String SOURCETONOTI_STATUS_FAILED = "-1";
	public static final String SOURCETONOTI_STATUS_DRAFT = "0";
	public static final String SOURCETONOTI_STATUS_SEND1 = "1";
	public static final String SOURCETONOTI_STATUS_SEND2 = "2";
	public static final String SOURCETONOTI_STATUS_REPLY = "3";

	public static final String NOTI_STATUS_FAILED = "-1";
	public static final String NOTI_STATUS_DRAFT = "0";
	public static final String NOTI_STATUS_PROCESSING = "1";
	public static final String NOTI_STATUS_SEND = "2";

	public static final String NOTI_OSN_STATUS = "1";

	public static final String DESTINATION_OBJECT_GARA = "service_provider";
	public static final String DESTINATION_OBJECT_CAROWNER = "member";

	public static Map<String, NotificationConfig> mapNotiConfig = new HashMap<String, NotificationConfig>();
	public static Map<String, NotificationTemplate> mapNotiTemplate = new HashMap<>();

	public static Integer COMMIT_SIZE = 100;
	public static Integer QUEUE_TO_UPDATE_SIZE = 100;

	public static LinkQueue<Notification> queueAddFromERP = new LinkQueue<>();

	public static String kafka_bootstrap_server = "localhost:9092";
}
