package com.bave.backend.security.config;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	// ResUsersDaoImpl resUsersDaoImpl = new ResUsersDaoImpl();

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		final String username = authentication.getName();
		final String password = authentication.getCredentials().toString();
		if (username != null) {
			try {
				String plainClientCredentials = username + ":" + password;
				String base64ClientCredentials = new String(
						Base64.getEncoder().encode(plainClientCredentials.getBytes()));
				// LoginUser user =
				// EHCacheManager.getSessionCache(base64ClientCredentials);
				if ("YmF2ZV9hZG1pbjpiYXZlQWFAYUFldmFi".equals(base64ClientCredentials)
						|| "YmF2ZV9wYXJ0bmVyOnBhcnRuZXJBYUAxMjM=".equals(base64ClientCredentials)) {
					final List<GrantedAuthority> grantedAuths = new ArrayList<>();
					grantedAuths.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
					final UserDetails principal = new org.springframework.security.core.userdetails.User(username,
							password, grantedAuths);
					final Authentication auth = new UsernamePasswordAuthenticationToken(principal, password,
							grantedAuths);
					return auth;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		} else {
			return null;
		}
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

	public static void main(String[] args) {
		String plainClientCredentials = "bave_admin:baveAa@aAevab";
//		String plainClientCredentials = "bave_partner:partnerAa@123";
		String base64ClientCredentials = new String(Base64.getEncoder().encode(plainClientCredentials.getBytes()));
		System.out.println(base64ClientCredentials);
	}
}