//package com.bave.backend.security.config;
//
//import java.io.IOException;
//import java.security.Principal;
//import java.util.Enumeration;
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.servlet.Filter;
//import javax.servlet.FilterChain;
//import javax.servlet.FilterConfig;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;
//
//@Component
//public class CustomFilter implements Filter {
//	private static Logger logger = LoggerFactory.getLogger("api_file");
//
//	@Override
//	public void init(FilterConfig filterConfig) throws ServletException {
//		// System.out.println("Init::called");
//	}
//
//	@Override
//	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
//			throws IOException, ServletException {
//		logger.info("doFilter::called");
//
//		HttpServletRequest request = (HttpServletRequest) servletRequest;
//		showInfo(request, (HttpServletResponse) servletResponse);
//		Principal userPrincipal = request.getUserPrincipal();
//		logger.info("userPrinciple::" + userPrincipal);
//		filterChain.doFilter(servletRequest, servletResponse);
//	}
//
//	public static void showInfo(HttpServletRequest request, HttpServletResponse response) {
//		logger.info("REQUEST: " + request.getRemoteAddr() + " - " + request.getRequestURL());
//
//		String params = "HEADER: ";
//		Map<String, String> mheader = getHeaders(request);
//		for (String key : mheader.keySet()) {
//			params += key + "=" + mheader.get(key) + "; ";
//		}
//		logger.info(params);
//
//		params = "PARAMS: ";
//		String values = "";
//		for (String key : request.getParameterMap().keySet()) {
//			for (String value : request.getParameterMap().get(key)) {
//				values += value + ",";
//			}
//			params += key + "=" + values + "; ";
//			values = "";
//		}
//		logger.info(params);
//
//		// StringBuffer jb = new StringBuffer();
//		// String line = null;
//		// try {
//		// BufferedReader reader = request.getReader();
//		// while ((line = reader.readLine()) != null)
//		// jb.append(line);
//		// JSONObject jsonObject = HTTP.toJSONObject(jb.toString());
//		// logger.info("BODY:" + jsonObject.toString());
//		// } catch (Exception e) {
//		// e.printStackTrace();
//		// }
//
//		// try {
//		// HttpServletRequest request2 = request;
//		// BufferedReader br = new BufferedReader(new
//		// InputStreamReader(request.getInputStream()));
//		// String json = "";
//		// if (br != null) {
//		// json += br.readLine();
//		// }
//		// logger.info("REQUEST BODY:" + json);
//		// } catch (Exception e) {
//		// e.printStackTrace();
//		// }
//	}
//
//	public static Map<String, String> getHeaders(HttpServletRequest r) {
//		Map<String, String> result = new HashMap<String, String>();
//		Enumeration names = r.getHeaderNames();
//		while (names.hasMoreElements()) {
//			String name = (String) names.nextElement();
//			String value = r.getHeader(name);
//			result.put(name, value);
//		}
//		return result;
//	}
//
//	@Override
//	public void destroy() {
//		// logger.info("Destroy::called");
//	}
//}
