package com.bave.backend.client;

import java.io.InputStream;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bave.backend.persistence.entity.Notification;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class FakeClient {
	private final Logger logger = LoggerFactory.getLogger("kafka_file");
	List<Notification> notification = null;
	@Autowired
	private NotificationClient notificationClient;

	@PostConstruct
	public void init() {
		try {
			InputStream is = FakeClient.class.getClassLoader().getResourceAsStream("fakeData.json");
			ObjectMapper mapper = new ObjectMapper();
			notification = mapper.readValue(is, List.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@Scheduled(fixedRate = 5000)
	public void erpAddListNotification() {
		if (notification.size() > 0) {
			Object rs = notificationClient.erpAddListNotification(notification);
			System.out.println(rs);
		}
	}

}
