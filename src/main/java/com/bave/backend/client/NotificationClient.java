package com.bave.backend.client;

import java.util.List;

//import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.bave.backend.persistence.entity.Notification;

@FeignClient(name = "notification-client", url = "http://localhost:8010", path = "/notification-gw")
//@RibbonClient(name = "log-service")
public interface NotificationClient {
	@PostMapping("/notification/erp/add-list")
	public Object erpAddListNotification(@RequestBody List<Notification> listNotification);
}
