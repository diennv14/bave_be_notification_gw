package com.bave.backend;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;

public class testFireBase {
	private final static String BOOTSTRAP_SERVERS = "localhost:9092";
	private final static String TOPIC = "erpNotification";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//
		// KafkaConsumer<String, Notification> consumer = createConsumer();
		// try {
		// consumer.subscribe(Collections.singletonList(TOPIC));
		// Notification notification = new Notification();
		// while (true) {
		// ConsumerRecords<String, Notification> messages = consumer.poll(0);
		//
		// for (ConsumerRecord<String, Notification> message : messages) {
		// notification = message.value();
		// System.out.println("Message received " +
		// message.value().getMessage());
		// }
		// }
		// } catch (Exception e) {
		// e.printStackTrace();
		// } finally {
		// consumer.close();
		// }

		try {
			sendPushNotification(
					"cYzNFlGMuPA:APA91bH78emQcEqm7V9K20I1blk-UkJW18mbaA50IdYZiT0k01VXNPnlZ2ynYkPKXejYBQbf3AFevBXipuSU712pMQZywvciHUCBtdGpLGvKQjKU92O0p4ytpK9cgM54suTjmiLb3Zzl");
//			
		//	sendPushNotification(
		//			"cgMAZurjNaM:APA91bE_E-V7Tlst2IlitIpa7jBNgCYCmUckDKzS44xu-BFoQiKzkUGv2e8uzXG8p_xRVyRTsipIReGotRZ1YQRcZLQ2E4mJ44R6zQYCXWMy0AFbRJ8EeAiosFJxoei1uZCAtm3lkzba");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static String sendPushNotification(String deviceToken) throws Exception {
		String result = "";
		URL url = new URL("https://fcm.googleapis.com/fcm/send");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();

		conn.setUseCaches(false);
		conn.setDoInput(true);
		conn.setDoOutput(true);

		conn.setRequestMethod("POST");
		conn.setRequestProperty("Authorization", "key=AIzaSyBwsjxYJ21b-SU3RwyWwUtr7nIUAeTcMNY");
		conn.setRequestProperty("Content-Type", "application/json");

		JSONObject json = new JSONObject();

		json.put("to", deviceToken.trim());
		// JSONObject info = new JSONObject();
		// info.put("title", "notification title"); // Notification title
		// info.put("body", "Nhắc lịch bảo dưỡngabc dsfd ưdcsw10:38:06
		// 12-08-2019"); // Notification
		// // body
		// json.put("notification", info);

		String strJsonBody = "{\"id\":441,\"member_id\":9396,\"domain\":\"test_basic\",\"address\":null,\"type\":\"personal\",\"action\":\"reminder_detail\",\"status\":\"0\",\"create_date\":1576826817021,\"destination_object\":\"member\",\"destination_ref_id\":9396,\"short_content\":\"Nhắc lịch Sơn đổi màu không hạ máy tại Test Basic vào lúc 18:26:54 20-12-2019\",\"data_object\":\"reminder\",\"data_ref_id\":\"93\",\"message\":\"<body>Gara Test Basic xin thông báo: quý khách có nhắc lịch <b>Sơn đổi màu không hạ máy</b> với số tiền 3.230.000 VND, tại gara <b>Test Basic</b>, vào lúc <b>18:26:54 20-12-2019</b>\",\"title\":\"Nhắc lịch Sơn đổi màu không hạ máy từ Test Basic\",\"seen\":true,\"send_date\":1576826817580,\"sender_status\":\"1\",\"channel\":\"onesignal\",\"lst_address\":[\"cYzNFlGMuPA:APA91bH78emQcEqm7V9K20I1blk-UkJW18mbaA50IdYZiT0k01VXNPnlZ2ynYkPKXejYBQbf3AFevBXipuSU712pMQZywvciHUCBtdGpLGvKQjKU92O0p4ytpK9cgM54suTjmiLb3Zzl\"],\"notify_code\":\"15921\"}";
		JSONObject data = new JSONObject(strJsonBody);
		json.put("data", data);
		try {
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(json.toString());
			wr.flush();

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
			result = "1";
		} catch (Exception e) {
			e.printStackTrace();
			result = "0";
		}
		System.out.println("GCM Notification is sent successfully");

		return result;
	}
}
