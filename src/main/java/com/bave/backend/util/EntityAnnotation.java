package com.bave.backend.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface EntityAnnotation {

	String table_name();

	String pk_name();

	String seq_name();

	String[] fields_insert() default "";

}
