package com.bave.backend.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonUtil
{
	private static Gson gson;

	public static Gson getGson()
	{
		if (gson == null) {
			GsonBuilder gsonBuilder = new GsonBuilder();
			gson = gsonBuilder.disableHtmlEscaping().create();
		}
		return gson;
	}
}
