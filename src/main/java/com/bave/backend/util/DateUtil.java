package com.bave.backend.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: FPT - FSS
 * </p>
 *
 * @author not Thai Hoang Hiep
 * @version 1.0
 */

public class DateUtil {
	public static final String FORMAT_TIME = "HH:MM:SS";
	public static final String FORMAT_DATE2 = "dd/MM/yyyy";
	public static final String FORMAT_DATE = "DD/MM/YYYY";
	public static final String FORMAT_DATE5 = "yyyy-MM-dd";
	public static final String FORMAT_DATE_TIME = "DD/MM/YYYY HH:MM:SS";
	public static final String FORMAT_DATE_TIME2 = "dd/MM/yyy hh:mm:ss";
	public static final String FORMAT_DATE_TIME3 = "dd/MM/yyyy HH:mm:ss";
	public static final String FORMAT_DATE_TIME4 = "dd/MM/yyyy HH:mm";
	public static final String FORMAT_DATE_TIME5 = "yyyy-MM-dd HH:mm:ss";
	public static final String FORMAT_DMYHMS = "yyyyMMddHHmmssSSS";
	public static final String FORMAT_DB_TIME = "HH24:MI:SS";
	public static final String FORMAT_DB_DATE = "YYYY-MM-DD";
	public static final String FORMAT_DB_DATE_TIME = "YYYY-MM-DD HH24:MI:SS";
	public static final String FORMAT_DB_YDATE_TIME = "SYYYY-MM-DD HH24:MI:SS";
	public static final SimpleDateFormat FM_DATE_TIME = new SimpleDateFormat(FORMAT_DATE_TIME);
	public static final SimpleDateFormat FM_DATE = new SimpleDateFormat(FORMAT_DATE);

	public static Long stringHMSToMillis(String strHMS) {
		if (strHMS == null)
			return null;

		String[] arrStr = strHMS.split(":");
		if (arrStr.length == 3) {
			int h = Integer.parseInt(arrStr[0]);
			int m = Integer.parseInt(arrStr[1]);
			int s = Integer.parseInt(arrStr[2]);
			long mH = h * (60 * (60 * 1000));
			long mM = m * (60 * 1000);
			long mS = s * 1000;
			return (mH + mM + mS);
		} else
			return null;
	}

	public static String convertHourToDateAndHour(int hour) {

		int day = Math.round(hour / 24);
		int hour2 = hour % 24;

		String dateAndHour = "";

		if (day != 0) {
			dateAndHour = day + "d " + hour2 + "h";

		} else {
			dateAndHour = hour2 + "h";
		}

		return dateAndHour;
	}

	public static int convertDateAndHourToHour(int day, int hour) {
		int rs = 0;

		rs = day * 24 + hour;

		return rs;
	}

	public static String formatStringToHHmmss(int strHMS) {
		int h = strHMS / (3600);
		int m = (strHMS % (3600)) / 60;
		int s = (strHMS % (3600)) % 60;
		String strH = String.format("%02d", h);
		String strM = String.format("%02d", m);
		String strS = String.format("%02d", s);
		return strH + ":" + strM + ":" + strS;
	}

	public static Date timestampToDate(Timestamp stamp) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(stamp.getTime());
		return cal.getTime();
	}

	public static String dateToString(Date date, String strFormat) {
		if (date != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(strFormat);
			return simpleDateFormat.format(date);
		} else
			return null;
	}

	public static String longToString(String date, String strFormat) {
		if (date != null && date.length() > 0) {
			long millis = Long.valueOf(date);
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(strFormat);
			Calendar cal = Calendar.getInstance();
			cal.setTimeInMillis(millis);
			return simpleDateFormat.format(cal.getTime());
		} else
			return null;
	}

	public static Date StringToDate(String dateString, String strFormat) {
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(strFormat);
			return simpleDateFormat.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	// //////////////////////////////////////////////////////

	/**
	 * Check string format with current format locale
	 *
	 * @param strSource
	 *            String to check
	 * @return boolean true if strSource represent a date, otherwise false
	 */
	// //////////////////////////////////////////////////////
	public static boolean isDate(String strSource) {
		return isDate(strSource, DateFormat.getDateInstance());
	}

	// //////////////////////////////////////////////////////

	/**
	 * Check string format
	 *
	 * @param strSource
	 *            String
	 * @param strFormat
	 *            Format to check
	 * @return boolean true if strSource represent a date, otherwise false
	 */
	// //////////////////////////////////////////////////////
	public static boolean isDate(String strSource, String strFormat) {
		SimpleDateFormat fmt = new SimpleDateFormat(strFormat);
		fmt.setLenient(false);
		return isDate(strSource, fmt);
	}

	// //////////////////////////////////////////////////////

	/**
	 * Check string format
	 *
	 * @param strSource
	 *            String
	 * @param fmt
	 *            Format to check
	 * @return boolean true if strSource represent a date, otherwise false
	 */
	// //////////////////////////////////////////////////////
	public static boolean isDate(String strSource, DateFormat fmt) {
		try {
			if (fmt.parse(strSource) == null)
				return false;
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	// //////////////////////////////////////////////////////

	/**
	 * Convert string to date using current format locale
	 *
	 * @param strSource
	 *            String to convert
	 * @return Date converted, null if conversion failure
	 */
	// //////////////////////////////////////////////////////
	public static Date toDate(String strSource) {
		return toDate(strSource, DateFormat.getDateInstance());
	}

	// //////////////////////////////////////////////////////

	/**
	 * Convert string to date
	 *
	 * @param strSource
	 *            String to convert
	 * @param strFormat
	 *            Format to convert
	 * @return Date converted, null if conversion failure
	 */
	// //////////////////////////////////////////////////////
	public static Date toDate(String strSource, String strFormat) {
		SimpleDateFormat fmt = new SimpleDateFormat(strFormat);
		fmt.setLenient(false);
		return toDate(strSource, fmt);
	}

	// //////////////////////////////////////////////////////

	/**
	 * Convert string to date
	 *
	 * @param strSource
	 *            String to convert
	 * @param fmt
	 *            Format to convert
	 * @return Date converted, null if conversion failure
	 */
	// //////////////////////////////////////////////////////
	public static Date toDate(String strSource, DateFormat fmt) {
		try {
			return fmt.parse(strSource);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// //////////////////////////////////////////////////////

	/**
	 * Add date value by second
	 *
	 * @param dt
	 *            Date Date to add
	 * @param iValue
	 *            int value to add
	 * @return Date after add
	 */
	// //////////////////////////////////////////////////////
	public static Date addSecond(Date dt, int iValue) {
		return add(dt, iValue, Calendar.SECOND);
	}

	// //////////////////////////////////////////////////////

	/**
	 * Add date value by minute
	 *
	 * @param dt
	 *            Date Date to add
	 * @param iValue
	 *            int value to add
	 * @return Date after add
	 */
	// //////////////////////////////////////////////////////
	public static Date addMinute(Date dt, int iValue) {
		return add(dt, iValue, Calendar.MINUTE);
	}

	// //////////////////////////////////////////////////////

	/**
	 * Add date value by hour
	 *
	 * @param dt
	 *            Date Date to add
	 * @param iValue
	 *            int value to add
	 * @return Date after add
	 */
	// //////////////////////////////////////////////////////
	public static Date addHour(Date dt, int iValue) {
		return add(dt, iValue, Calendar.HOUR);
	}

	// //////////////////////////////////////////////////////

	/**
	 * Add date value by day
	 *
	 * @param dt
	 *            Date Date to add
	 * @param iValue
	 *            int value to add
	 * @return Date after add
	 */
	// //////////////////////////////////////////////////////
	public static Date addDay(Date dt, int iValue) {
		return add(dt, iValue, Calendar.DATE);
	}

	// //////////////////////////////////////////////////////

	/**
	 * Add date value by month
	 *
	 * @param dt
	 *            Date Date to add
	 * @param iValue
	 *            int value to add
	 * @return Date after add
	 */
	// //////////////////////////////////////////////////////
	public static Date addMonth(Date dt, int iValue) {
		return add(dt, iValue, Calendar.MONTH);
	}

	// //////////////////////////////////////////////////////

	/**
	 * Add date value by year
	 *
	 * @param dt
	 *            Date Date to add
	 * @param iValue
	 *            int value to add
	 * @return Date after add
	 */
	// //////////////////////////////////////////////////////
	public static Date addYear(Date dt, int iValue) {
		return add(dt, iValue, Calendar.YEAR);
	}

	// //////////////////////////////////////////////////////

	/**
	 * Add date value
	 *
	 * @param dt
	 *            Date Date to add
	 * @param iValue
	 *            int value to add
	 * @param iType
	 *            type of unit
	 * @return Date after add
	 */
	// //////////////////////////////////////////////////////
	public static Date add(Date dt, int iValue, int iType) {
		Calendar cld = Calendar.getInstance();
		cld.setTime(dt);
		cld.add(iType, iValue);
		return cld.getTime();
	}

	public static int getHoursFromDate(Date date) {
		Calendar cld = Calendar.getInstance();
		cld.setTime(date);
		return cld.get(Calendar.HOUR_OF_DAY);
	}

	public static int getMinuteFromDate(Date date) {
		Calendar cld = Calendar.getInstance();
		cld.setTime(date);
		return cld.get(Calendar.MINUTE);
	}

	public static Date stringToDate(String str) {
		if (StringUtil.isEmpty(str))
			return null;
		DateFormat df = new SimpleDateFormat(FORMAT_DATE_TIME);
		try {
			return df.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String dateToString(Date date) {
		if (date == null)
			return null;
		DateFormat df = new SimpleDateFormat(FORMAT_DATE_TIME);
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return df.format(cal.getTime());
	}

	// //////////////////////////////////////////////////////

	/**
	 * Format date object
	 *
	 * @param dtImput
	 *            date to format
	 * @param strPattern
	 *            format pattern
	 * @return formatted string
	 * @author Thai Hoang Hiep
	 */
	// //////////////////////////////////////////////////////
	public static String format(java.util.Date dtImput, String strPattern) {
		if (dtImput == null)
			return null;
		java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat(strPattern);
		return fmt.format(dtImput);
	}

	public static String getTimeAgo(Date now, Date past) {
		try {
			// milliseconds ago
			// long milliseconds = TimeUnit.MILLISECONDS.toMillis(now.getTime()
			// - past.getTime());
			long days = TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime());
			long hours = TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime());
			long minutes = TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime());
			long seconds = TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime());
			if (days > 0 && hours > 0) {
				Calendar cal = Calendar.getInstance();
				cal.setTimeInMillis(past.getTime());
				return DateUtil.dateToString(cal.getTime(), DateUtil.FORMAT_DATE_TIME4);
			}
			if (hours > 0 && minutes > 0) {
				return hours + " giờ " + (minutes - (hours * 60)) + " phút trước";
			}

			if (minutes > 0 && seconds > 0) {
				return minutes + " phút trước";
			}

			if (seconds > 0) {
				return seconds + " giây trước";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "N/A";
	}

	public static Date now() {
		return Calendar.getInstance().getTime();
	}

	public static Date[] getDaysOfWeek(Date refDate, int firstDayOfWeek) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.DAY_OF_WEEK, firstDayOfWeek);
		calendar.set(Calendar.HOUR_OF_DAY, 00);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		Date[] daysOfWeek = new Date[7];
		for (int i = 0; i < 7; i++) {
			daysOfWeek[i] = calendar.getTime();
			calendar.add(Calendar.DAY_OF_MONTH, 1);
		}
		return daysOfWeek;
	}

	public static Date[] getDaysOfMonth(int refMonth, int refYear) {
		Calendar calendar = Calendar.getInstance();
		int year = refYear;
		int month = refMonth;
		int date = 1;
		calendar.set(year, month, date, 00, 00, 00);
		int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		Date[] days = new Date[maxDay];

		for (int i = date; i <= maxDay; i++) {
			calendar.set(Calendar.DAY_OF_MONTH, i);
			days[i - 1] = calendar.getTime();
		}
		return days;
	}

	public static Date[] getFromDateToDateByType(String dayType) {
		Date[] dates = new Date[2];
		if ("d".equals(dayType)) {
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 00);
			cal.set(Calendar.MINUTE, 00);
			cal.set(Calendar.SECOND, 00);
			dates[0] = cal.getTime();
			cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			dates[1] = cal.getTime();
		}
		if ("w".equals(dayType)) {
			Calendar cal = Calendar.getInstance();
			Date[] dTemp = getDaysOfWeek(cal.getTime(), Calendar.MONDAY);
			dates[0] = dTemp[0];
			cal.setTime(dTemp[dTemp.length-1]);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			dates[1] = cal.getTime();
		}
		if ("m".equals(dayType)) {
			Calendar cal = Calendar.getInstance();
			Date[] dTemp = getDaysOfMonth(cal.get(Calendar.MONTH), cal.get(Calendar.YEAR));
			dates[0] = dTemp[0];
			cal.setTime(dTemp[dTemp.length-1]);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);
			dates[1] = cal.getTime();
		}
		return dates;
	}

	public static Date longToDate(String millis) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(Long.valueOf(millis));
		return cal.getTime();
	}

	public static Date longToDate(Long millis) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(millis);
		return cal.getTime();
	}

	public static Date convertTimeZone(Date date, String fromTimeZone, String toTimeZone) {
		ZoneId fromZoneId = ZoneId.of(fromTimeZone);
		ZoneId toZoneId = ZoneId.of(toTimeZone);
		LocalDateTime fromDateTime = date.toInstant().atZone(fromZoneId).toLocalDateTime();
		ZonedDateTime fromDate = ZonedDateTime.of(fromDateTime, fromZoneId);
		ZonedDateTime toDate = fromDate.withZoneSameInstant(toZoneId);
		Calendar cal = Calendar.getInstance();
		cal.set(toDate.getYear(), toDate.getMonthValue(), toDate.getDayOfMonth(), toDate.getHour(), toDate.getMinute(),
				toDate.getSecond());
		return cal.getTime();
	}

	public static void main(String[] args) {
		
		System.out.println( DateUtil.dateToString(getFromDateToDateByType("m")[0],DateUtil.FORMAT_DATE_TIME5)+"-"+DateUtil.dateToString(getFromDateToDateByType("m")[1],DateUtil.FORMAT_DATE_TIME5));
		
		// String dateInString = "2018-04-11 10:15:55";
		// LocalDateTime ldt = LocalDateTime.parse(dateInString,
		// DateTimeFormatter.ofPattern(FORMAT_DATE_TIME5));
		//
		// ZoneId singaporeZoneId = ZoneId.of("Asia/Singapore");
		// System.out.println("TimeZone : " + singaporeZoneId);
		//
		// // LocalDateTime + ZoneId = ZonedDateTime
		// ZonedDateTime asiaZonedDateTime = ldt.atZone(singaporeZoneId);
		// System.out.println("Date (Singapore) : " + asiaZonedDateTime);
		//
		// ZoneId newYokZoneId = ZoneId.of("America/New_York");
		// System.out.println("TimeZone : " + newYokZoneId);
		//
		// ZonedDateTime nyDateTime =
		// asiaZonedDateTime.withZoneSameInstant(newYokZoneId);
		// System.out.println("Date (New York) : " + nyDateTime);
		//
//		DateTimeFormatter format = DateTimeFormatter.ofPattern(FORMAT_DATE_TIME5);
		// System.out.println("\n---DateTimeFormatter---");
		// System.out.println("Date (Singapore) : " +
		// format.format(asiaZonedDateTime));
		// System.out.println("Date (New York) : " + format.format(nyDateTime));

//		System.out
//				.println(DateUtil.dateToString((convertTimeZone(Calendar.getInstance().getTime(), "GMT+7", "GMT+8"))));

	}

}
