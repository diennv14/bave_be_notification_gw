package com.bave.backend.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class Response {

	public static final String STATUS_SUCCESS = "0";
	public static final String STATUS_FAILED = "1";
	public static final String STATUS_INPUT_INVALID = "2";
	public static final String STATUS_DATA_NOTFOUND = "3";

	public static final String STATUS_SUCCESS_DES = "SUCCESS";
	public static final String STATUS_FAILED_DES = "FAILED";
	public static final String STATUS_INPUT_INVALID_DES = "INPUT PARAM INVALID";
	public static final String STATUS_DATA_NOTFOUND_DES = "DATA NOT FOUND";

	private String errorCode;
	private String status;
	private String message;
	private Object result;

	@JsonInclude(Include.NON_EMPTY)
	private Integer rs_total_size;
	@JsonInclude(Include.NON_EMPTY)
	private Integer rs_page_size;
	@JsonInclude(Include.NON_EMPTY)
	private Integer rs_offset;
	@JsonInclude(Include.NON_EMPTY)
	private Integer rs_limit;

	public Response() {
		super();
	}

	public Response(Object result) {
		this.status = STATUS_SUCCESS;
		this.result = result;
		this.message = getMessageByStatus(status);
	}

	public Response(Object result, String status) {
		this.status = status;
		this.result = result;
		this.message = getMessageByStatus(status);
	}

	public Response(Object result, Integer rs_page_size, Integer rs_total_size, String status) {
		this.rs_page_size = rs_page_size;
		this.rs_total_size = rs_total_size;
		this.result = result;
		this.message = getMessageByStatus(status);
	}

	public Response(Object result, Integer rs_offset, Integer rs_limit, Integer rs_page_size, Integer rs_total_size,
			String status) {
		if (rs_total_size != null) {
			this.rs_page_size = rs_page_size;
			this.rs_total_size = rs_total_size;
			this.rs_offset = rs_offset;
			this.rs_limit = rs_limit;
		}
		this.result = result;
		this.status = status;
		this.message = getMessageByStatus(status);
	}

	public Response(Object result, String status, String message) {
		this.status = status;
		this.result = result;
		this.message = message;
	}

	public Response(String errorCode, String status, String message, Object result) {
		super();
		this.errorCode = errorCode;
		this.status = status;
		this.message = message;
		this.result = result;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		if (message == null && status != null) {
			message = getMessageByStatus(status);
		}
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getRs_total_size() {
		return rs_total_size;
	}

	public void setRs_total_size(Integer rs_total_size) {
		this.rs_total_size = rs_total_size;
	}

	public Integer getRs_page_size() {
		return rs_page_size;
	}

	public void setRs_page_size(Integer rs_page_size) {
		this.rs_page_size = rs_page_size;
	}

	public Integer getRs_offset() {
		return rs_offset;
	}

	public void setRs_offset(Integer rs_offset) {
		this.rs_offset = rs_offset;
	}

	public Integer getRs_limit() {
		return rs_limit;
	}

	public void setRs_limit(Integer rs_limit) {
		this.rs_limit = rs_limit;
	}

	private String getMessageByStatus(String status) {
		if (STATUS_SUCCESS.equals(status))
			return STATUS_SUCCESS_DES;
		if (STATUS_FAILED.equals(status))
			return STATUS_FAILED_DES;
		if (STATUS_INPUT_INVALID.equals(status))
			return STATUS_INPUT_INVALID_DES;
		if (STATUS_DATA_NOTFOUND.equals(status))
			return STATUS_DATA_NOTFOUND_DES;
		return null;
	}

	public Response(BEException e) {
		e.printStackTrace();
		this.setStatus(Response.STATUS_FAILED);
		this.setErrorCode(e.getErrorCode());
		this.setMessage(e.getMessage());
	}

	public Response(Exception e) {
		e.printStackTrace();
		if (e instanceof BEException) {
			BEException ex = (BEException) e;
			this.setStatus(Response.STATUS_FAILED);
			this.setErrorCode(ex.getErrorCode());
			this.setMessage(ex.getMessage());
		} else {
			this.setStatus(Response.STATUS_FAILED);
			this.setErrorCode(Response.STATUS_FAILED);
			this.setMessage(e.getMessage());
		}
	}

}
