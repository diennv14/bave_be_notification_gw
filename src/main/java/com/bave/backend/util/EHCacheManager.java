package com.bave.backend.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bave.backend.persistence.entity.CarGoUser;
import com.bave.backend.persistence.entity.Configuration;
import com.bave.backend.persistence.entity.ErpUser;
import com.bave.backend.persistence.entity.NotificationConfig;
import com.bave.backend.persistence.entity.NotificationTemplate;
import com.bave.backend.persistence.entity.ServiceProvider;
import com.bave.backend.security.config.VarStatic;
import com.bave.backend.service.impl.CommonService;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;

public class EHCacheManager {
	private static final Logger logger = LoggerFactory.getLogger(EHCacheManager.class);

	public static final String CACHE_CONFIGURATION = "configurationCache";

	public static final String CACHE_SERVICEPROVIDER = "serviceProvider";

	public static final String CACHE_ERP_USERS = "erpUserOnline";

	public static final String CACHE_CARGO_USERS = "cargoUserOnline";

	static {
		initCache();
	}

	public EHCacheManager() {

	}

	public static void clearCache() {
		logger.info("Clearing Cache");
		EHCacheManager.get().getCache(EHCacheManager.CACHE_CONFIGURATION).removeAll();
		EHCacheManager.get().getCache(EHCacheManager.CACHE_CONFIGURATION).evictExpiredElements();
		logger.info("Clear Done");

	}

	public static CacheManager get() {
		return CacheManager.getInstance();
	}

	public static Cache getConfigurationCache() {
		return EHCacheManager.get().getCache(EHCacheManager.CACHE_CONFIGURATION);
	}

	public static Cache getServiceProviderCache() {
		return EHCacheManager.get().getCache(EHCacheManager.CACHE_SERVICEPROVIDER);
	}

	public static Cache getERPUsersCache() {
		return EHCacheManager.get().getCache(EHCacheManager.CACHE_ERP_USERS);
	}

	public static Cache getCarGoUsersCache() {
		return EHCacheManager.get().getCache(EHCacheManager.CACHE_CARGO_USERS);
	}

	public static void initCache() {
		try {
			clearCache();
			logger.info("INIT Cache");
			logger.info("INIT Cache Done");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void initCache(CommonService commonService, Date date) {
		try {
//			List<CarGoUser> lstCarGoUser = commonService.getAllCarGoUsers(date);
//			EHCacheManager.initCarGoUsersCache(lstCarGoUser);
//			List<ErpUser> lstErpUserUser = commonService.getAllERPUsers(date);
//			EHCacheManager.initERPUsersCache(lstErpUserUser);

//			List<NotificationConfig> lstNotificationConfig = commonService.getAllNotificationConfig();
//			EHCacheManager.initNotificationConfig(lstNotificationConfig);

//			List<NotificationTemplate> lstNotificationTemplate = commonService.getAllNotificationTemplate();
//			EHCacheManager.initNotificationTemplate(lstNotificationTemplate);

			//List<ServiceProvider> lstServiceProvider = commonService.getAllServiceProvider(date);
			//EHCacheManager.initServiceProviderCache(lstServiceProvider);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Configuration getConfiguration(String type, String value) {
		String key = type + "-" + type;
		if (getConfigurationCache().isKeyInCache(key))
			return (Configuration) getConfigurationCache().get(key).getObjectValue();
		else
			return new Configuration();
	}

	public static List<Configuration> getConfiguration(String type) {
		List<Configuration> lstRs = new ArrayList<>();
		for (Object key : getConfigurationCache().getKeys()) {
			lstRs.add((Configuration) getConfigurationCache().get(key).getObjectValue());
		}
		return lstRs;
	}

	public static ServiceProvider getServiceProviderCache(String key) {
		Element element = getServiceProviderCache().get(key);
		if (element != null) {
			return (ServiceProvider) element.getObjectValue();
		} else
			return null;
	}

	public static void putServiceProviderCache(String key, ServiceProvider val) {
		Element element = new Element(key, val);
		getServiceProviderCache().put(element);
	}

	public static void putServiceProviderCache(String key, ServiceProvider val, int liveTime) {
		Element element = new Element(key, val);
		element.setTimeToLive(liveTime);
		getServiceProviderCache().put(element);
	}

	public static ErpUser getERPUsersCache(String key) {
		Element element = getERPUsersCache().get(key);
		if (element != null) {
			return (ErpUser) element.getObjectValue();
		} else
			return null;
	}

	public static void putERPUsersCache(String key, ErpUser val) {
		Element element = new Element(key, val);
		getERPUsersCache().put(element);
	}

	public static void putERPUsersCache(String key, ErpUser val, int liveTime) {
		Element element = new Element(key, val);
		element.setTimeToLive(liveTime);
		getERPUsersCache().put(element);
	}

	public static List<CarGoUser> getCarGoUsersCache(String key) {
		Element element = getCarGoUsersCache().get(key);
		if (element != null) {
			return (List<CarGoUser>) element.getObjectValue();
		} else
			return null;
	}

	public static void putCarGoUsersCache(String key, List<CarGoUser> val) {
		Element element = new Element(key, val);
		getCarGoUsersCache().put(element);
	}

	public static void putCarGoUsersCache(String key, List<CarGoUser> val, int liveTime) {
		Element element = new Element(key, val);
		element.setTimeToLive(liveTime);
		getCarGoUsersCache().put(element);
	}

	public static void initNotificationConfig(List<NotificationConfig> lstRs) {
		for (NotificationConfig item : lstRs) {
			String key = item.getChannel() + "_" + item.getApp();
			VarStatic.mapNotiConfig.put(key, item);
		}
	}

	public static void initNotificationTemplate(List<NotificationTemplate> lstRs) {
		for (NotificationTemplate item : lstRs) {
			String key = "";
//			String key = item.getId() + "";
			VarStatic.mapNotiTemplate.put(key, item);
		}
	}

	public static boolean initCarGoUsersCache(List<CarGoUser> lstRs) {
		try {
			for (CarGoUser u : lstRs) {
				String key = u.getUsername();
				if (!EHCacheManager.getCarGoUsersCache().isKeyInCache(key)) {
					List<CarGoUser> lst = new ArrayList<>();
					lst.add(u);
					EHCacheManager.putCarGoUsersCache(key, lst);
				} else {
					EHCacheManager.getCarGoUsersCache(key).add(u);
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean initERPUsersCache(List<ErpUser> lstRs) {
		try {
			for (ErpUser u : lstRs) {
				String key = u.getDomain() + "|" + u.getUsername();
				if (!EHCacheManager.getERPUsersCache().isKeyInCache(key)) {
					EHCacheManager.putERPUsersCache(key, u);
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public static boolean initServiceProviderCache(List<ServiceProvider> lstRs) {
		try {
			for (ServiceProvider item : lstRs) {
//				String key = item.getCode();
//				System.out.println("key " + key + "   ProviderCache" + EHCacheManager.getServiceProviderCache());
//				if (!StringUtil.isEmpty(key) && !EHCacheManager.getServiceProviderCache().isKeyInCache(key)) {
//					EHCacheManager.putServiceProviderCache(key, item);
//				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
