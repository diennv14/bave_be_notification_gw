package com.bave.backend.util;

import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

public class IRISSms {
	private static String URL = "https://smsapi2.irismedia.vn";
	private static String username = "sms_bave";
	private static String password = "13Cmbbwfk8p";
	private static String brandname = "Bave";
	private static String smsContentType = "0";
	private static String accessToken = null;

	public static long last_get_access_token = 0;
	public static long token_timeout = 1200000;

	public static void main(String[] args) {
		try {

			IRISSms.sendOneMessage("so dien thoai", "Noi dung test");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String authen() {
		try {
			if (IRISSms.last_get_access_token >= 0) {
				long val = System.currentTimeMillis() - IRISSms.last_get_access_token;
				// System.out.println(IRISSms.last_get_access_token + " > " +
				// IRISSms.token_timeout);
				if (val >= IRISSms.token_timeout) {
					String plainClientCredentials = IRISSms.username + ":" + IRISSms.password;
					String base64ClientCredentials = new String(
							Base64.getEncoder().encode(plainClientCredentials.getBytes("ISO-8859-1")));
					RestTemplate restTemplate = new RestTemplate();
					restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
					HttpHeaders headers = new HttpHeaders();
					headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
					headers.set("Authorization", "Basic " + base64ClientCredentials);
					MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
					map.add("grant_type", "password");
					HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(
							map, headers);
					ResponseEntity<Object> response = restTemplate.exchange(IRISSms.URL + "/oauth2/token",
							HttpMethod.POST, entity, Object.class);
					Map<String, String> mapRs = new ObjectMapper().convertValue(response.getBody(), Map.class);
					IRISSms.accessToken = mapRs.get("access_token");
					IRISSms.last_get_access_token = System.currentTimeMillis();
					System.out.println("new access_token");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return accessToken;
	}

	public static String sendManyMessages(List<Map<String, String>> lstData) throws Exception {
		String code = "";
		try {
			authen();
			RestTemplate restTemplate = new RestTemplate();
			// restTemplate.getMessageConverters().add(new
			// MappingJackson2HttpMessageConverter());
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("Authorization", "Bearer " + IRISSms.accessToken);

			Map<String, Object> mapData = new HashMap<>();
			mapData.put("Brandname", IRISSms.brandname);
			mapData.put("SendingList", lstData);

			String jsonData = new ObjectMapper().writeValueAsString(mapData);
			// System.out.println("json_data: " + jsonData);

			HttpEntity<String> entity = new HttpEntity<String>(jsonData, headers);
			ResponseEntity<Object> response = restTemplate.exchange(IRISSms.URL + "/api/Sms", HttpMethod.POST, entity,
					Object.class);

			code = response.getStatusCode().toString();

		} catch (Exception e) {
			code = e.toString();
		}

		return code;
	}

	public static String sendOneMessage(String phone, String message) throws Exception {
		String code = "";
		try {
			List<Map<String, String>> lstData = Arrays
					.asList(IRISSms.addItem(UUID.randomUUID().toString(), phone, message));

			code = sendManyMessages(lstData);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return code;
	}

	public static Map<String, String> addItem(String smsId, String mobile, String content) {
		Map<String, String> mapDataItem = new HashMap<>();
		mapDataItem.put("SmsId", smsId);
		mapDataItem.put("PhoneNumber", mobile);
		mapDataItem.put("Content", content);
		mapDataItem.put("ContentType", IRISSms.smsContentType);
		return mapDataItem;
	}

}
