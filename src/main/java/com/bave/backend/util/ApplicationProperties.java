package com.bave.backend.util;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.Properties;

/**
 * Entity Manager Util
 * 
 * @version n/a 08/10/2015
 * @author haitx3
 */

public class ApplicationProperties {
	private static Properties props;
	static {
		InputStream is = null;
		try {
			props = PropertyLoader.loadProperties("resource/application.properties");
			// props = PropertyLoader.loadPropertiesFromDirectory(null);
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException ignored) {
					ignored.printStackTrace();
				}
			}
		}
	}

	public static Properties getProperties() {
		return props;
	}

	public static void setProperties(Properties props) {
		ApplicationProperties.props = props;
	}

	public static String getProperty(String strKey) {
		if (strKey == null || strKey.length() <= 0) {
			return "";
		}
		String value = props.getProperty(strKey);
		if (value == null) {
			return strKey;
		} else {
			return value;
		}
	}

	public static String getMenuAction(String strKey) {
		if (strKey == null || strKey.length() <= 0) {
			return "";
		}
		String value = props.getProperty(strKey);
		if (value == null) {
			return "";
		} else {
			return value;
		}
	}

	public static String getValue(String key) {
		String result = "N/A";
		try {
			result = props.getProperty(key);
		} catch (MissingResourceException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String getValue(String key, Object... arguments) {
		String result = "N/A";
		try {
			result = props.getProperty(key);
			result = MessageFormat.format(result, arguments);
		} catch (MissingResourceException e) {
			e.printStackTrace();
		}
		return result;
	}

}
