package com.bave.backend.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;

import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RpcXmlUtil {

	public static Map<String, Map<String, FieldsInfo>> mapModelInfo = new HashMap<>();

	public static <T> T convertValue(Map<String, Object> map, Class<T> clazz) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		T t = mapper.convertValue(map, clazz);
		return t;
	}

	public static <T> T convertToObject(Map<String, Object> map, Class<T> clazz) throws Exception {
		return convertToObject(map, clazz, false, null);
	}

	public static <T> T convertToObject(Map<String, Object> map, Class<T> clazz, Boolean includeParentClazz)
			throws Exception {
		return convertToObject(map, clazz, includeParentClazz, null);
	}

	public static <T> T convertToObject(Map<String, Object> map, Class<T> clazz, Boolean includeParentClazz,
			Map<String, FieldsInfo> mapModelInfo) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		// if (mapModelInfo != null) {
		// map = updateValueByFieldType(map, mapModelInfo);
		// }

		// T t = (T) mapper.convertValue(map, clazz);
		T t = clazz.newInstance();

		List<Field> lstField = new ArrayList<>();
		lstField.addAll(Arrays.asList(clazz.getDeclaredFields()));
		if (includeParentClazz) {
			lstField.addAll(Arrays.asList(clazz.getSuperclass().getDeclaredFields()));
		}

		for (Field field : lstField) {
			boolean sccessible = field.isAccessible();
			field.setAccessible(true);
			String fieldName = field.getName();
			Column columnAnnotation = field.getAnnotation(Column.class);
			JsonProperty jsonAnnotation = field.getAnnotation(JsonProperty.class);
			String jsonPropertyName = fieldName;
			if (columnAnnotation != null) {
				jsonPropertyName = columnAnnotation.name();
			} else if (jsonAnnotation != null) {
				jsonPropertyName = jsonAnnotation.value();
			}

			if (map.containsKey(jsonPropertyName)) {
				Object val = map.get(jsonPropertyName);
				if (val != null) {
					if (field.getType().equals(Date.class)) {
						if (val.toString().length() > 10) {
							BeanUtils.setProperty(t, fieldName,
									DateUtil.StringToDate(val.toString(), DateUtil.FORMAT_DATE_TIME5));
						} else {
							BeanUtils.setProperty(t, fieldName,
									DateUtil.StringToDate(val.toString(), DateUtil.FORMAT_DATE5));
						}
					} else {
						if (val != null && field.getType().equals(val.getClass())) {
							BeanUtils.setProperty(t, fieldName, val);
						} else if (val != null && !field.getType().equals(val.getClass())) {
							T valT = (T) mapper.convertValue(val, field.getType());
							if (valT instanceof Long) {
								BeanUtils.setProperty(t, fieldName, Long.parseLong(valT.toString()));
							} else if (valT instanceof Integer) {
								BeanUtils.setProperty(t, fieldName, Integer.parseInt(valT.toString()));
							} else {
								BeanUtils.setProperty(t, fieldName, valT);
							}
						}
					}
				}
			}
			// }

			// MapingParentValue mapingAnnotation =
			// field.getAnnotation(MapingParentValue.class);
			// if (mapingAnnotation != null) {
			// int index = Integer.valueOf(mapingAnnotation.value_index());
			// if (map.get(mapingAnnotation.parent_name()) instanceof Object[])
			// {
			// Object[] parentValue = (Object[])
			// map.get(mapingAnnotation.parent_name());
			// BeanUtils.setProperty(t, fieldName, parentValue[index]);
			// }
			// }
			//
			// MapingParentValueArrayList mapingAnnotationArray =
			// field.getAnnotation(MapingParentValueArrayList.class);
			// if (mapingAnnotationArray != null) {
			// if (map.get(mapingAnnotationArray.parent_name()) instanceof
			// Object[]) {
			// Object[] arr = (Object[])
			// map.get(mapingAnnotationArray.parent_name());
			// BeanUtils.setProperty(t, fieldName, arr);
			// }
			// }

			field.setAccessible(sccessible);
		}
		return t;
	}

	public static <T> T callSetter(T t, String methodName, Object value) throws Exception {
		Method m = t.getClass().getDeclaredMethod(methodName, Map.class);
		if (m != null && value != null) {
			m.invoke(t, value);
		}
		return t;
	}

	public static <T> List<T> mapRSToListEntity(SQLQuery query, Class<T> clazz) throws Exception {
		List<Map<String, Object>> lstMap = query.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP).list();
		List<T> lstRs = new ArrayList<>();
		for (Map<String, Object> map : lstMap) {
			T t = convertToObject(map, clazz);
			lstRs.add(t);
		}
		return lstRs;
	}

	public static <T> List<T> jsonToLst(String json, Class<T> clazz) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			List<T> obj = mapper.readValue(json, new TypeReference<List<T>>() {
			});
			return obj;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
