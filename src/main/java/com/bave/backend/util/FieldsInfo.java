package com.bave.backend.util;

public class FieldsInfo {
	private String name;
	private boolean readonly;
	private boolean sortable;
	private String type;

	public FieldsInfo() {
		super();
	}

	public FieldsInfo(String name, String type) {
		super();
		this.name = name;
		this.type = type;
	}

	public FieldsInfo(String name, boolean readonly, boolean sortable, String type) {
		super();
		this.name = name;
		this.readonly = readonly;
		this.sortable = sortable;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isReadonly() {
		return readonly;
	}

	public void setReadonly(boolean readonly) {
		this.readonly = readonly;
	}

	public boolean isSortable() {
		return sortable;
	}

	public void setSortable(boolean sortable) {
		this.sortable = sortable;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
