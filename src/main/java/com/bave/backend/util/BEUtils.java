package com.bave.backend.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;

import com.bave.backend.api.object.NotificationEntity;
import com.bave.backend.persistence.entity.Configuration;
import com.bave.backend.persistence.entity.Notification;
import com.bave.backend.persistence.entity.NotificationEmail;
import com.bave.backend.persistence.entity.NotificationOneSignal;
import com.bave.backend.persistence.entity.NotificationSMS;
import com.bave.backend.persistence.entity.User;
import com.bave.backend.security.config.VarStatic;

public class BEUtils {

	public static String removeMainDomain(String inputDomain, String mainDomain) {
		if (inputDomain.indexOf(mainDomain) > 0) {
			inputDomain = inputDomain.replace(mainDomain, "");
		}
		return inputDomain;
	}

	public static String convertPhone(String phone) {
		if (phone != null) {
			String result = "";
			String firstLetter = String.valueOf(phone.charAt(0));
			String secondLetter = String.valueOf(phone.charAt(1));

			if ("8".equals(firstLetter) && "4".equals(secondLetter)) {
				result = phone.substring(2);
			} else if ("0".equals(firstLetter)) {
				result = phone.substring(1);
			} else {
				result = phone;
			}
			return result;
		}
		return phone;
	}

	public static List<NotificationEntity> toLstNotificationEntity(List<Notification> lstNotifications,
			List<Configuration> lstConfg) {

		List<NotificationEntity> lstNE = new ArrayList<>();
		Map<String, String> mAction = new HashMap<>();
		for (Configuration configuration : lstConfg) {
			mAction.put(configuration.getCode().trim(), configuration.getValue().trim());
		}
//		for (Notification n : lstNotifications) {
//			NotificationEntity ne = new NotificationEntity();
//			ne.setNotificationId(n.getId());
//			ne.setType(n.getType());
//			ne.setAction(n.getAction());
//
//			if (mAction.containsKey(n.getAction())) {
//				String actionDescription = mAction.get(n.getAction()).toString();
//				ne.setActionDescription(actionDescription);
//			}
//
//			ne.setSourceAvatar(n.getSourceAvatar());
//			ne.setSourceName(n.getSourceName());
//			ne.setSourceObjectCode(n.getSourceObject());
//			ne.setSourceRefId(n.getSourceRefId());
//
//			ne.setDestAvatar(n.getDestAvatar());
//			ne.setDestName(n.getDestName());
//			ne.setDestObjectCode(n.getDestinationObject());
//			ne.setDestRefId(n.getDestinationRefId());
//
//			ne.setDataObjectCode(n.getDataObject());
//			ne.setDataRefId(n.getDataRefId());
//
//			ne.setMemberId(n.getMemberId());
//
//			ne.setMessage(n.getMessage());
//			ne.setShortContent(n.getDestShortcontent());
//
//			ne.setCreateDate(DateUtil.convertTimeZone(n.getCreateDate(), "UTC", "Asia/Ho_Chi_Minh"));
//
//			ne.setTimeAgo(DateUtil.getTimeAgo(DateUtil.now(), ne.getCreateDate()));
//			ne.setSeen(n.getSeen());
//			lstNE.add(ne);

//		}
		return lstNE;
	}

	public static NotificationSMS createNotiSms(Notification notification, User user, String callBackUrl) {
		NotificationSMS notiSms = new NotificationSMS();
		try {
			BeanUtils.copyProperties(notiSms, notification);
		} catch (Exception e) {
			e.printStackTrace();
		}
//		if (user != null) {
//			String phone = user.getListAddress().size() > 0 ? user.getListAddress().get(0) : "";
//			notiSms.setAddress(phone);
//		}
//		notiSms.setDataObject(notification.getDataObject() != null ? notification.getDataObject() : "");
//		notiSms.setDataRefId(notification.getDataRefId() != null ? notification.getDataRefId() : "");
//		notiSms.setDataName(notification.getDataName() != null ? notification.getDataName() : "");
//		notiSms.setCallbackUrl(callBackUrl);
//		notiSms.setStatus(VarStatic.SOURCETONOTI_STATUS_DRAFT);
		return notiSms;
	}

	public static NotificationOneSignal createNotiOneSignal(Notification notification, String playerId,
			String callBackUrl) {
		NotificationOneSignal notiOneSignal = new NotificationOneSignal();
		try {
			BeanUtils.copyProperties(notiOneSignal, notification);
		} catch (Exception e) {
			e.printStackTrace();
		}
//		notiOneSignal.setDataObject(notification.getDataObject() != null ? notification.getDataObject() : "");
//		notiOneSignal.setDataRefId(notification.getDataRefId() != null ? notification.getDataRefId() : "");
//		notiOneSignal.setDataName(notification.getDataName() != null ? notification.getDataName() : "");
//		notiOneSignal.setAddress(playerId);
//		notiOneSignal.setCallbackUrl(callBackUrl);
//		notiOneSignal.setStatus(VarStatic.SOURCETONOTI_STATUS_DRAFT);
		return notiOneSignal;
	}

	public static NotificationEmail createNotiEmail(Notification notification, User user, String callBackUrl) {
		NotificationEmail notiEmail = new NotificationEmail();
		try {
			BeanUtils.copyProperties(notiEmail, notification);
		} catch (Exception e) {
			e.printStackTrace();
		}
//		String email = user.getListAddress().size() > 0 ? user.getListAddress().get(0) : "";
//		notiEmail.setDataObject(notification.getDataObject() != null ? notification.getDataObject() : "");
//		notiEmail.setDataRefId(notification.getDataRefId() != null ? notification.getDataRefId() : "");
//		notiEmail.setDataName(notification.getDataName() != null ? notification.getDataName() : "");
//		notiEmail.setAddress(email);
//		notiEmail.setTitle(notification.getAdditionData());
//		notiEmail.setCallbackUrl(callBackUrl);
//		notiEmail.setStatus(VarStatic.SOURCETONOTI_STATUS_DRAFT);
		return notiEmail;
	}
}
