package com.bave.backend.util;

import java.io.File;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * Load all properties from properties file
 * 
 * @version 08/10/2015
 * @author haitx3
 */

public class PropertyLoader {
	// private static final boolean THROW_ON_LOAD_FAILURE = true;
	// private static final boolean LOAD_AS_RESOURCE_BUNDLE = false;
	private static final String SUFFIX = ".properties";
	private static final String PATH = "resource";

	public static Properties loadPropertiesFromDirectory(String resourcePath) {
		Properties properties = new Properties();
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		if (resourcePath == null) {
			resourcePath = classLoader.getResource(PATH).getPath();
		}
		File folder = new File(resourcePath);
		for (File fileEntry : folder.listFiles()) {
			if (!fileEntry.getName().toLowerCase().endsWith(".properties")) {
				continue;
			}
			System.out.println("Properties file: " + fileEntry.getName());
			Properties props = loadProperties(PATH + File.separator + fileEntry.getName(), classLoader);
			if (props != null) {
				properties.putAll(props);
			}
		}
		return properties;
	}

	public static Properties loadProperties(String name, ClassLoader loader) {
		if (name == null) {
			throw new IllegalArgumentException("loadProperties(String name, ClassLoader loader): name is null");
		}

		final char ch = '/';
		if (name.charAt(0) == ch) {
			name = name.substring(1);
		}
		if (name.endsWith(SUFFIX)) {
			name = name.substring(0, name.length() - SUFFIX.length());
		}
		Properties result = null;

		InputStreamReader in = null;
		try {
			if (loader == null) {
				loader = ClassLoader.getSystemClassLoader();
			}

			name = name.replace('.', '/');

			if (!name.endsWith(SUFFIX)) {
				name = name.concat(SUFFIX);
			}

			in = new InputStreamReader(loader.getResourceAsStream(name), "utf-8");
			if (in != null) {
				result = new Properties();
				result.load(in);
				System.out.println("load properties file");
			}
		} catch (Exception e) {
			result = null;

			if (in != null)
				try {
					in.close();
				} catch (Throwable localThrowable) {
				}
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (Throwable localThrowable1) {
				}
		}
		if (result == null) {
			throw new IllegalArgumentException("could not load [" + name + "]" + " as " + "a classloader resource");
		}

		return result;
	}

	public static Properties loadProperties(String name) {
		return loadProperties(name, Thread.currentThread().getContextClassLoader());
	}
}