package com.bave.backend.util;

import java.util.Date;

public class Main {

	public static void main(String[] args) {
		// try {
		// Date past = DateUtil.toDate("14/01/2018 05:12:12",
		// DateUtil.FORMAT_DATE_TIME3);
		// Date now = new Date();
		// System.out.println(TimeUnit.MILLISECONDS.toMillis(now.getTime() -
		// past.getTime()) + " milliseconds ago");
		// System.out.println(TimeUnit.MILLISECONDS.toSeconds(now.getTime() -
		// past.getTime()) + " seconds ago");
		// System.out.println(TimeUnit.MILLISECONDS.toMinutes(now.getTime() -
		// past.getTime()) + " minutes ago");
		// System.out.println(TimeUnit.MILLISECONDS.toHours(now.getTime() -
		// past.getTime()) + " hours ago");
		// System.out.println(TimeUnit.MILLISECONDS.toDays(now.getTime() -
		// past.getTime()) + " days ago");
		// } catch (Exception e) {
		// e.printStackTrace();
		// }

		Date date = DateUtil.toDate("03/01/2018 13:10:12", DateUtil.FORMAT_DATE_TIME3);
		System.out.println(DateUtil.getTimeAgo(DateUtil.now(), date));
	}

}
