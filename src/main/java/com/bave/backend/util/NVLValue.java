package com.bave.backend.util;

import java.math.BigDecimal;
import java.util.Date;

public class NVLValue {

    public static String strNvl(Object objInput, String nullValue) {
        if (objInput == null) {
            return nullValue;
        } else {
            return objInput.toString();
        }
    }

    public static Integer intNvl(Object objInput, Integer nullValue) {
        if (objInput == null) {
            return nullValue;
        } else {
            return Integer.parseInt(objInput.toString());
        }
    }

    public static Long longNvl(Object objInput, Long nullValue) {
        if (objInput == null) {
            return nullValue;
        } else {
            return Long.parseLong(objInput.toString());
        }
    }

    public static Double doubleNvl(Object objInput, Double nullValue) {
        if (objInput == null) {
            return nullValue;
        } else {
            return Double.parseDouble(objInput.toString());
        }
    }

    public static BigDecimal bigDecimalNvl(Object objInput, BigDecimal nullValue) {
        if (objInput == null) {
            return nullValue;
        } else {
            return new BigDecimal(objInput.toString());
        }
    }

    public static Date sqlTimestampToUtilDate(java.sql.Timestamp sqlDate) {
        if (sqlDate != null) {
            Date utilDate = new Date(sqlDate.getTime());
            return utilDate;
        } else
            return null;
    }
}
