package com.bave.backend.util;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.bave.backend.kafka.DataProcessAbstr;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class ServiceClientUtil {
	private final Logger logger = LoggerFactory.getLogger("kafka_file");

	public boolean postToErp(String url, Map<String, Object> mapData, String channel, String address, String sequence) {
		try {
			if (!StringUtil.isEmpty(url)) {
				RestTemplate restTemplate = new RestTemplate();
				// Add the Jackson message converter
				restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
				// create request body
				String jsonData = new ObjectMapper().writeValueAsString(mapData);

				// set headers
				HttpHeaders headers = new HttpHeaders();

//				if (VarStatic.CHANNEL_SMS.equals(type) || VarStatic.CHANNEL_ONESIGNAL.equals(type)
//						|| VarStatic.CHANNEL_MARKETING.equals(type)) {
//					headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//				} else {
				headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//				}

				// headers.set("Authorization", "Basic " + "xxxxxxxxxxxx");
				HttpEntity<String> entity = new HttpEntity<String>(jsonData, headers);
				// send request and parse result
				ResponseEntity<String> response = restTemplate.postForEntity(url, entity, String.class);
				logger.info("callback " + channel + ": address=" + address + ", seq=" + sequence + ", rs:" + response
						+ ", url:" + url + " data: " + jsonData);
				if (200 == response.getStatusCodeValue()) {
					return true;
				}
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return false;
	}
}
