package com.bave.backend.util;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.juddi.v3.client.mapping.MockSSLSocketFactory;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

public class EmailAction {

	private String email;
	private String message;
	private String subject;

	public static void main(String[] abc) {

		try {
			// disableSSLValidation();
			EmailAction emailAction = new EmailAction();
			emailAction.setEmail("haitx@bave.io");
			emailAction.setMessage("dddddddddđ");
			emailAction.setSubject("ddddddddddd");
			JsonNode result = null;
			result = emailAction.sendGetJson("key-2ac192ce408b1480e0eaeccc3858bc69");
			if (result != null && result.toString().contains("\"id\"")) {
				System.out.println(true);
			} else {
				System.out.println(false);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static Boolean disableSSLValidation() throws Exception {
		final SSLContext sslContext = SSLContext.getInstance("TLS");

		sslContext.init(null, new TrustManager[] { new X509TrustManager() {
			@Override
			public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
			}

			@Override
			public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
			}

			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return new X509Certificate[0];
			}
		} }, null);

		HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
		HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		});

		return true;
	}

	public JsonNode sendGetJson(String key) {
		HttpResponse<JsonNode> request = null;
		try {
			Unirest.setHttpClient(makeClient());
			request = Unirest.post("https://api.mailgun.net/v3/bave.io/messages").basicAuth("api", key)
					.queryString("from", "noreply@bave.io").queryString("to", this.email)
					.queryString("subject", this.subject).queryString("text", this.message).asJson();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return request.getBody();
	}

	public static HttpClient makeClient() {
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", 80, PlainSocketFactory.getSocketFactory()));
		try {
			schemeRegistry.register(new Scheme("https", 443, new MockSSLSocketFactory()));
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		ClientConnectionManager cm = new SingleClientConnManager(schemeRegistry);
		DefaultHttpClient httpclient = new DefaultHttpClient(cm);
		return httpclient;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
}