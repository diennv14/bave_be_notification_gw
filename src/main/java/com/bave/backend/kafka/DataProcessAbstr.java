package com.bave.backend.kafka;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bave.backend.persistence.entity.Notification;
import com.bave.backend.persistence.entity.UserNotificationSticky;
import com.bave.backend.security.config.VarStatic;
import com.bave.backend.util.DateUtil;
import com.bave.backend.util.ServiceClientUtil;
import com.bave.be.entity.serialz.UserNotificationStickySerializer;

@Service
public abstract class DataProcessAbstr {

	@Autowired
	protected ServiceClientUtil serviceClientUtil;

	protected KafkaProducer<String, UserNotificationSticky> producer = null;
	protected Properties producerConfig;

	public DataProcessAbstr() {
		producerConfig = createProducerConfig(VarStatic.kafka_bootstrap_server);
	}

	protected abstract void processData(UserNotificationSticky notification) throws Exception;

	protected Properties createProducerConfig(String brokers) {
		Properties props = new Properties();
		props.put("bootstrap.servers", brokers);
		props.put("acks", "all");
		props.put("retries", 0);
		props.put("batch.size", 16384);
		props.put("linger.ms", 1);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", UserNotificationStickySerializer.class.getName());
		props.put("value.serializer", UserNotificationStickySerializer.class.getName());
		return props;
	}

	protected void sendMessageToSendSuccessTopic(UserNotificationSticky notification) {
		producer = new KafkaProducer<String, UserNotificationSticky>(producerConfig);
		producer.send(
				new ProducerRecord<String, UserNotificationSticky>(VarStatic.kafka_send_success_topic, notification),
				new Callback() {
					public void onCompletion(RecordMetadata metadata, Exception e) {
						if (e != null) {
							e.printStackTrace();
						}
					}
				});
		producer.close();
	}

	protected void sendMessageToSendFailedTopic(UserNotificationSticky notification) {
		producer = new KafkaProducer<String, UserNotificationSticky>(producerConfig);
		producer.send(
				new ProducerRecord<String, UserNotificationSticky>(VarStatic.kafka_send_failed_topic, notification),
				new Callback() {
					public void onCompletion(RecordMetadata metadata, Exception e) {
						if (e != null) {
							e.printStackTrace();
						}
					}
				});
		producer.close();
	}

	protected void sendMessageToSendTopic(UserNotificationSticky notification) {
		producer = new KafkaProducer<String, UserNotificationSticky>(producerConfig);
		producer.send(new ProducerRecord<String, UserNotificationSticky>(VarStatic.kafka_send_topic, notification),
				new Callback() {
					public void onCompletion(RecordMetadata metadata, Exception e) {
						if (e != null) {
							e.printStackTrace();
						}
					}
				});
		producer.close();
	}

	protected boolean callBackUrl(String url, boolean success, String failReason, String type, String channel,
			String address, String sequence) {
		try {
			Map<String, Object> mapData = new HashMap<>();
			if ("marketing".equals(type)) {
				if (VarStatic.CHANNEL_ONESIGNAL.equals(channel)) {
					mapData.put("result", success ? "1" : "0");
					mapData.put("message", failReason);
				} else if (VarStatic.CHANNEL_SMS.equals(channel)) {
					mapData.put("result", success ? "1" : "0");
					mapData.put("message", failReason);
				} else {
					mapData.put("result", success ? "1" : "0");
					mapData.put("reason", failReason);
					mapData.put("time", DateUtil.dateToString(new Date(), DateUtil.FORMAT_DATE_TIME5));
				}
			} else if ("notice".equals(type)) {
				if (VarStatic.CHANNEL_ONESIGNAL.equals(channel)) {
					mapData.put("result", success ? "1" : "0");
				} else if (VarStatic.CHANNEL_SMS.equals(channel)) {
					mapData.put("result", success ? "1" : "0");
				} else {
					mapData.put("result", success ? "1" : "0");
				}
			}
			return serviceClientUtil.postToErp(url, mapData, type, address, sequence);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
