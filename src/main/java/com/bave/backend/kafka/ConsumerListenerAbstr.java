package com.bave.backend.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bave.backend.persistence.entity.Notification;
import com.bave.backend.persistence.entity.UserNotificationSticky;

public abstract class ConsumerListenerAbstr {
	protected final Logger logger = LoggerFactory.getLogger("kafka_file");

	protected void logger(ConsumerRecord<String, UserNotificationSticky> record) {
		UserNotificationSticky notification = record.value();
		logger.info("Received " + record.topic() + " - " + record.partition() + " - " + record.offset() + ", channel:"
				+ notification.getChannel() != null ? notification.getChannel()
						: "" + ", address=" + notification.getAddress() != null ? notification.getAddress()
								: "" + ", seq=" + notification.getSequence() != null ? notification.getSequence() : "");
	}
}
