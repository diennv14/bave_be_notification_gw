package com.bave.backend.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.bave.backend.dao.NotificationDao;
import com.bave.backend.dao.NotificationEmailDao;
import com.bave.backend.dao.NotificationSMSDao;
import com.bave.backend.dao.UserNotificationStickyDao;
import com.bave.backend.persistence.entity.NotificationEmail;
import com.bave.backend.persistence.entity.NotificationSMS;
import com.bave.backend.persistence.entity.UserNotificationSticky;
import com.bave.backend.security.config.VarStatic;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Transactional
public class SendFailedDataProcess extends DataProcessAbstr {
	private final Logger logger = LoggerFactory.getLogger("kafka_file");
	@Autowired
	NotificationDao notificationDao;
	@Autowired
	UserNotificationStickyDao notificationOneSignalDao;
	@Autowired
	NotificationSMSDao notificationSMSDao;
	@Autowired
	NotificationEmailDao notificationEmailDao;

	public SendFailedDataProcess() {
	}

	@Override
	public void processData(UserNotificationSticky notification) throws Exception {
		if (notification != null) {
			if (VarStatic.CHANNEL_ONESIGNAL.equals(notification.getChannel())) {
				UserNotificationSticky noti = new UserNotificationSticky();
				BeanUtils.copyProperties(notification, noti);
				callBackUrl(noti.getCallbackUrl(), false, "cargo member not found", notification.getType(),
						notification.getChannel(), notification.getAddress(), notification.getSequence());
				notificationOneSignalDao.persist(noti);
			} else if (VarStatic.CHANNEL_SMS.equals(notification.getChannel())) {
				NotificationSMS noti = new NotificationSMS();
				BeanUtils.copyProperties(notification, noti);
//				callBackUrl(noti.getCallbackUrl(), false, "", notification.getType(), notification.getChannel(),
//						notification.getAddress(), notification.getSequence());
				notificationSMSDao.persist(noti);
			} else if (VarStatic.CHANNEL_EMAIL.equals(notification.getChannel())) {
				NotificationEmail noti = new NotificationEmail();
				BeanUtils.copyProperties(notification, noti);
//				callBackUrl(noti.getCallbackUrl(), false, "", notification.getType(), notification.getChannel(),
//						notification.getAddress(), notification.getSequence());
				notificationEmailDao.persist(noti);
			} else {
				logger.info("channel n/a: " + new ObjectMapper().writeValueAsString(notification));
			}
		}
	}

}
