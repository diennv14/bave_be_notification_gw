package com.bave.backend.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bave.backend.onesignal.OneSignalPostNotification;
import com.bave.backend.persistence.entity.Notification;
import com.bave.backend.persistence.entity.UserNotificationSticky;
import com.bave.backend.security.config.VarStatic;
import com.bave.backend.util.DateUtil;
import com.bave.backend.util.IRISSms;
import com.bave.backend.util.StringUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Transactional
public class SendDataProcess extends DataProcessAbstr {
	private final Logger logger = LoggerFactory.getLogger("kafka_file");

	public SendDataProcess() {
		super();
	}

	@Override
	public void processData(UserNotificationSticky notification) throws Exception {
		if (notification != null) {
			boolean rs = false;
			if (VarStatic.CHANNEL_ONESIGNAL.equals(notification.getChannel())) {
				rs = postOneSingal(notification);
			} else if (VarStatic.CHANNEL_SMS.equals(notification.getChannel())) {
				rs = true;
				// rs = postSMS(notification);
			} else if (VarStatic.CHANNEL_EMAIL.equals(notification.getChannel())) {
				rs = true;
				// rs = postEmail(notification);
			} else {
				logger.error(new ObjectMapper().writeValueAsString(notification));
			}
			notification.setSendDate(DateUtil.now());
			if (!StringUtil.isEmpty(notification.getChannel())) {
				if (rs) {
					notification.setStatus(VarStatic.NOTI_STATUS_SEND);
					notification.setSenderStatus("1");
					sendMessageToSendSuccessTopic(notification);
				} else {
					notification.setStatus(VarStatic.NOTI_STATUS_FAILED);
					notification.setSenderStatus("-1");
					sendMessageToSendFailedTopic(notification);
				}
			}
		}
	}

	private boolean postSMS(Notification notification) {
		try {
			System.out.println("gui smssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss \n");
			//return true;

//			 String phone = notification.getAddress() != null ?
//			 notification.getAddress() : "0";
//			 String message = notification.getMessage() != null ?
//			 StringUtil.removeAccent(notification.getMessage())
//			 : "";
//			 String result = IRISSms.sendOneMessage(phone, message);
//			 logger.info("SENDER " + notification.getChannel() + ": address="
//			 + notification.getAddress() + ", seq="
//			 + notification.getSequence() + ", rs:" + result);
//			 if ("201".equals(result)) {
//			 return true;
//			 }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private boolean postEmail(Notification notification) {
		System.out.println("gui emaillllllllllllllllllllllllllllllllllllllllllllllllllllllllllllll \n");
		return true;

		// EmailAction emailAction = new EmailAction();
		// emailAction.setEmail(notification.getAddress() != null ?
		// notification.getAddress() : "0");
		// emailAction.setMessage(notification.getMessage() != null ?
		// notification.getMessage() : "");
		// emailAction.setSubject(notification.getTitle() != null ?
		// notification.getTitle() : "");
		// NotificationConfig notiConfig =
		// VarStatic.mapNotiConfig.get(notification.getChannel() + "_null");
		// JsonNode result = null;
		// if (notiConfig != null && notiConfig.getApiKey() != null) {
		// result = emailAction.sendGetJson(notiConfig.getApiKey());
		// }
		// logger.info("SENDER " + notification.getChannel() + ": address=" +
		// notification.getAddress() + ", seq="
		// + notification.getSequence() + ", rs:" + result);
		// if (result != null && result.toString().contains("\"id\"")) {
		// return true;
		// } else {
		// return false;
		// }
	}

	private boolean postOneSingal(UserNotificationSticky notification) {
		// QuotationNotif quotationNotif = new QuotationNotif();
		// quotationNotif.setAction(notification.getAction());
		// quotationNotif.setId(notification.getDataRefId());
		// quotationNotif.setMemberId(notification.getMemberId());
		// quotationNotif.setMemberName("");
		// quotationNotif.setPlayerId(notification.getAddress());
		// quotationNotif.setMessage(notification.getMessage());
		// quotationNotif.setDestinationObject(notification.getDestinationObject());
		// quotationNotif.setUnreadNotify(1);
		// lam sau tam fix = 1
		// NotificationConfig notiConfig = null;
		// if
		// (VarStatic.DESTINATION_OBJECT_CAROWNER.equals(notification.getDestinationObject()))
		// {
		// notiConfig = VarStatic.mapNotiConfig.get("onesignal_bave");
		// } else {
		// notiConfig = VarStatic.mapNotiConfig.get("onesignal_agara");
		// }
		// if (notiConfig != null && notiConfig.getApiKey() != null &&
		// notiConfig.getAppKey() != null) {
		// if (quotationNotif.getPlayerId() != null) {
		OneSignalPostNotification osn = new OneSignalPostNotification();
		// HashMap<String, Object> mapContent = new HashMap<>();
		// mapContent.put("type", quotationNotif.getAction());
		// mapContent.put("action", quotationNotif.getAction());
		// mapContent.put("data_ref_id", quotationNotif.getId());
		// mapContent.put("contents", new
		// MessageResult(quotationNotif.getId(),
		// quotationNotif.getMessage()));
		//
		// if ("booking".equals(quotationNotif.getAction())) {
		// if (quotationNotif.getMessage() == null) {
		// mapContent.put("contents", new
		// MessageResult(quotationNotif.getId(), ApplicationProperties
		// .getValue("notif.msg.booking.booked",
		// quotationNotif.getMemberName())));
		// }
		// } else if
		// ("quotation_request".equals(quotationNotif.getAction())) {
		// if (quotationNotif.getMessage() == null) {
		// mapContent.put("contents", new
		// MessageResult(quotationNotif.getId(), ApplicationProperties
		// .getValue("notif.msg.quot.request",
		// quotationNotif.getMemberName())));
		// }
		// } else if
		// ("quotation_response".equals(quotationNotif.getAction())) {
		// if (quotationNotif.getMessage() == null) {
		// mapContent.put("contents",
		// new MessageResult(quotationNotif.getId(),
		// ApplicationProperties.getValue("notif.msg.quot.response",
		// quotationNotif.getServiceProviderName(),
		// quotationNotif.getId())));
		// }
		// }
		// if (mapContent.size() > 0) {
		// List<String> playerIds =
		// Arrays.asList(quotationNotif.getPlayerId());
		// boolean rs = osn.postNotification(notification, null, playerIds,
		// null, null, null,
		// quotationNotif.getUnreadNotify(), logger);

		boolean rs = osn.postNotification(notification, logger);
		return rs;
		// }
		// }
		// } else {
		// logger.error(
		// "Lacking config for action: " + notification.getAction() + ",
		// message: " + notification.toString());
		// }
		// return false;
	}

}
