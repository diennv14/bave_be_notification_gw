package com.bave.backend.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;

import com.bave.backend.persistence.entity.UserNotificationSticky;

public class ListenerSendSuccessNotification extends ConsumerListenerAbstr {

	@Autowired
	private SendSuccessDataProcess sendSuccessDataProcess;

	@KafkaListener(groupId = "group1", id = "SendSuccessTopic_ID0", topicPartitions = {
			@TopicPartition(topic = "SendSuccessTopic", partitions = { "0" }) })
	public void listenSendSuccessTopicPartition0(ConsumerRecord<String, UserNotificationSticky> record) {
		try {
			sendSuccessDataProcess.processData(record.value());
			logger(record);
		} catch (Exception e) {
			logger.error("sendSuccessDataProcess {}", e.getMessage());
		}
	}

	@KafkaListener(groupId = "group1", id = "SendSuccessTopic_ID1", topicPartitions = {
			@TopicPartition(topic = "SendSuccessTopic", partitions = { "1" }) })
	public void listenSendSuccessTopicPartition1(ConsumerRecord<String, UserNotificationSticky> record) {
		try {
			sendSuccessDataProcess.processData(record.value());
			logger(record);
		} catch (Exception e) {
			logger.error("sendSuccessDataProcess {}", e.getMessage());
		}
	}

	@KafkaListener(groupId = "group1", id = "SendSuccessTopic_ID2", topicPartitions = {
			@TopicPartition(topic = "SendSuccessTopic", partitions = { "2" }) })
	public void listenSendSuccessTopicPartition2(ConsumerRecord<String, UserNotificationSticky> record) {
		try {
			sendSuccessDataProcess.processData(record.value());
			logger(record);
		} catch (Exception e) {
			logger.error("sendSuccessDataProcess {}", e.getMessage());
		}
	}

}
