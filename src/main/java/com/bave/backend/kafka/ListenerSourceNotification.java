package com.bave.backend.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;

import com.bave.backend.persistence.entity.UserNotificationSticky;

public class ListenerSourceNotification extends ConsumerListenerAbstr {

	@Autowired
	private SourceDataProcess sourceDataProcess;

	@KafkaListener(groupId = "group1", id = "SourceTopic_ID0", topicPartitions = {
			@TopicPartition(topic = "SourceTopic", partitions = { "0" }) })
	public void listenSourceTopicPartition0(ConsumerRecord<String, UserNotificationSticky> record) {
		try {
			sourceDataProcess.processData(record.value());
			logger(record);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@KafkaListener(groupId = "group1", id = "SourceTopic_ID1", topicPartitions = {
			@TopicPartition(topic = "SourceTopic", partitions = { "1" }) })
	public void listenSourceTopicPartition1(ConsumerRecord<String, UserNotificationSticky> record) {
		try {
			sourceDataProcess.processData(record.value());
			logger(record);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@KafkaListener(groupId = "group1", id = "SourceTopic_ID2", topicPartitions = {
			@TopicPartition(topic = "SourceTopic", partitions = { "2" }) })
	public void listenSourceTopicPartition2(ConsumerRecord<String, UserNotificationSticky> record) {
		try {
			sourceDataProcess.processData(record.value());
			logger(record);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
