package com.bave.backend.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import com.bave.backend.persistence.entity.UserNotificationSticky;

public class ListenerSendFailedNotification extends ConsumerListenerAbstr {

	@Autowired
	private SendFailedDataProcess sendFailedDataProcess;

	@KafkaListener(groupId = "group1", id = "SendFailedTopic_ID0", topicPartitions = {
			@TopicPartition(topic = "SendFailedTopic", partitions = { "0" }) })
	public void listenSendFailedTopicPartition0(ConsumerRecord<String, UserNotificationSticky> record) {
		try {
			sendFailedDataProcess.processData(record.value());
			logger(record);
		} catch (Exception e) {
			logger.error("sendFailedDataProcess {}", e.getMessage());
		}
	}

	@KafkaListener(groupId = "group1", id = "SendFailedTopic_ID1", topicPartitions = {
			@TopicPartition(topic = "SendFailedTopic", partitions = { "1" }) })
	public void listenSendFailedTopicPartition1(ConsumerRecord<String, UserNotificationSticky> record) {
		try {
			sendFailedDataProcess.processData(record.value());
			logger(record);
		} catch (Exception e) {
			logger.error("sendFailedDataProcess {}", e.getMessage());
		}
	}

	@KafkaListener(groupId = "group1", id = "SendFailedTopic_ID2", topicPartitions = {
			@TopicPartition(topic = "SendFailedTopic", partitions = { "2" }) })
	public void listenSendFailedTopicPartition2(ConsumerRecord<String, UserNotificationSticky> record) {
		try {
			sendFailedDataProcess.processData(record.value());
			logger(record);
		} catch (Exception e) {
			logger.error("sendFailedDataProcess {}", e.getMessage());
		}
	}
}
