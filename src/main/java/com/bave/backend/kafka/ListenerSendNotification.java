package com.bave.backend.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;

import com.bave.backend.persistence.entity.Notification;
import com.bave.backend.persistence.entity.UserNotificationSticky;

public class ListenerSendNotification extends ConsumerListenerAbstr {

	@Autowired
	private SendDataProcess sendDataProcess;

	@KafkaListener(groupId = "group1", id = "SendTopic_ID0", topicPartitions = {
			@TopicPartition(topic = "SendTopic", partitions = { "0" }) })
	public void listenSendTopicPartition0(ConsumerRecord<String, UserNotificationSticky> record) {
		try {
			sendDataProcess.processData(record.value());
			logger(record);
		} catch (Exception e) {
			logger.error("sendDataprocess {}", e.getMessage());
		}
	}

	@KafkaListener(groupId = "group1", id = "SendTopic_ID1", topicPartitions = {
			@TopicPartition(topic = "SendTopic", partitions = { "1" }) })
	public void listenSendTopicPartition1(ConsumerRecord<String, UserNotificationSticky> record) {
		try {
			sendDataProcess.processData(record.value());
			logger(record);
		} catch (Exception e) {
			logger.error("sendDataprocess {}", e.getMessage());
		}
	}

	@KafkaListener(groupId = "group1", id = "SendTopic_ID2", topicPartitions = {
			@TopicPartition(topic = "SendTopic", partitions = { "2" }) })
	public void listenSendTopicPartition2(ConsumerRecord<String, UserNotificationSticky> record) {
		try {
			sendDataProcess.processData(record.value());
			logger(record);
		} catch (Exception e) {
			logger.error("sendDataprocess {}", e.getMessage());
		}
	}

}
