package com.bave.backend.kafka;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bave.backend.dao.UserNotificationStickyDao;
import com.bave.backend.onesignal.OneSignalPostNotification;
import com.bave.backend.persistence.entity.CarGoUser;
import com.bave.backend.persistence.entity.Notification;
import com.bave.backend.persistence.entity.ServiceProvider;
import com.bave.backend.persistence.entity.UserNotificationSticky;
import com.bave.backend.security.config.VarStatic;
import com.bave.backend.util.BEUtils;
import com.bave.backend.util.DateUtil;
import com.bave.backend.util.EHCacheManager;
import com.bave.backend.util.StringUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
@Transactional
public class SourceDataProcess extends DataProcessAbstr {

	private static final Logger logger = LoggerFactory.getLogger(SourceDataProcess.class);

	
	@Autowired
	private UserNotificationStickyDao userNotificationStickyDao;

	private Set<String> senderTables = null;

	public SourceDataProcess() {
		super();

		senderTables = new HashSet<String>();
		senderTables.add("notification_onesignal");
	}

	@Override
	public void processData(UserNotificationSticky notification) throws Exception {
		// if (notification != null) {
		// notification = buildNotificationInfo(notification);
		// List<Notification> lstRoot = new ArrayList<>();
		//
		// List<Notification> lstSplitByDataRef = new ArrayList<>();
		// if (notification.getListDataRefId() != null &&
		// notification.getListDataRefId().size() > 0) {
		// for (String dataRefId : notification.getListDataRefId()) {
		// Notification noti = new Notification();
		// BeanUtils.copyProperties(noti, notification);
		// noti.setDataRefId(dataRefId);
		// noti.setStatus(VarStatic.NOTI_STATUS_PROCESSING);
		// lstSplitByDataRef.add(noti);
		// }
		// } else {
		// lstSplitByDataRef.add(notification);
		// }
		//
		// List<Notification> lstSplitByUser = new ArrayList<>();
		// for (Notification item : lstSplitByDataRef) {
		// // User gara
		// if (VarStatic.CHANNEL_MARKETING.equals(item.getType())) {
		// if (item.getListPartner() != null && item.getListPartner().size() >
		// 0) {
		// for (User u : item.getListPartner()) {
		// Notification noti = new Notification();
		// BeanUtils.copyProperties(noti, item);
		// noti.setDestName(u.getName());
		// noti.setLstAddress(u.getListAddress());
		// noti.setCallbackUrl(u.getCallbackUrl());
		// noti.setChannel(
		// "app".equals(u.getChannel()) ? VarStatic.CHANNEL_ONESIGNAL :
		// u.getChannel());
		// noti.setApp(VarStatic.NOTI_APP_BAVE);
		// lstSplitByUser.add(noti);
		// }
		// lstRoot.add(item);
		// } else {
		// lstSplitByUser.add(item);
		// Notification nroot = new Notification();
		// BeanUtils.copyProperties(nroot, item);
		// nroot.setApp(VarStatic.NOTI_APP_BAVE);
		// lstRoot.add(nroot);
		// }
		// } else {
		// if (item.getListUser() != null && item.getListUser().size() > 0) {
		// for (User u : item.getListUser()) {
		// Notification noti = new Notification();
		// BeanUtils.copyProperties(noti, item);
		// noti.setDestName(u.getName());
		// noti.setLstAddress(u.getListAddress());
		// noti.setCallbackUrl(u.getCallbackUrl());
		// noti.setChannel(
		// "app".equals(u.getChannel()) ? VarStatic.CHANNEL_ONESIGNAL :
		// u.getChannel());
		// noti.setApp(VarStatic.NOTI_APP_AGARA);
		// if (VarStatic.CHANNEL_SMS.equals(noti.getChannel())) {
		// continue;
		// }
		// lstSplitByUser.add(noti);
		// }
		// Notification nroot = new Notification();
		// BeanUtils.copyProperties(nroot, item);
		// nroot.setApp(VarStatic.NOTI_APP_AGARA);
		// lstRoot.add(nroot);
		// } else {
		// lstSplitByUser.add(item);
		// Notification nroot = new Notification();
		// BeanUtils.copyProperties(nroot, item);
		// nroot.setApp(VarStatic.NOTI_APP_AGARA);
		// lstRoot.add(nroot);
		// }
		// }
		// }
		//
		// // List<Notification> lstChildAddress =
		// // splitByAddress(lstSplitByUser);
		//
		// sendMessage(lstSplitByUser, lstRoot);
		// }

		if (notification != null) {
			boolean rs = false;
			if (VarStatic.CHANNEL_ONESIGNAL.equals(notification.getChannel())) {
				logger.info("SourceDataProcess channel {}", notification.getChannel());
				rs = postOneSingal(notification);
			} else if (VarStatic.CHANNEL_SMS.equals(notification.getChannel())) {
				rs = true;
				// rs = postSMS(notification);
			} else if (VarStatic.CHANNEL_EMAIL.equals(notification.getChannel())) {
				rs = true;
				// rs = postEmail(notification);
			} else {
				logger.error(new ObjectMapper().writeValueAsString(notification));
			}
			notification.setSendDate(DateUtil.now());
			if (!StringUtil.isEmpty(notification.getChannel())) {
				if (rs) {
					notification.setStatus(VarStatic.NOTI_STATUS_SEND);
					notification.setSenderStatus("1");
					sendMessageToSendSuccessTopic(notification);
				} else {
					notification.setStatus(VarStatic.NOTI_STATUS_FAILED);
					notification.setSenderStatus("-1");
					sendMessageToSendFailedTopic(notification);
				}
			}
		}
	}
	
	private boolean postOneSingal(UserNotificationSticky notification) {
		// QuotationNotif quotationNotif = new QuotationNotif();
		// quotationNotif.setAction(notification.getAction());
		// quotationNotif.setId(notification.getDataRefId());
		// quotationNotif.setMemberId(notification.getMemberId());
		// quotationNotif.setMemberName("");
		// quotationNotif.setPlayerId(notification.getAddress());
		// quotationNotif.setMessage(notification.getMessage());
		// quotationNotif.setDestinationObject(notification.getDestinationObject());
		// quotationNotif.setUnreadNotify(1);
		// lam sau tam fix = 1
		// NotificationConfig notiConfig = null;
		// if
		// (VarStatic.DESTINATION_OBJECT_CAROWNER.equals(notification.getDestinationObject()))
		// {
		// notiConfig = VarStatic.mapNotiConfig.get("onesignal_bave");
		// } else {
		// notiConfig = VarStatic.mapNotiConfig.get("onesignal_agara");
		// }
		// if (notiConfig != null && notiConfig.getApiKey() != null &&
		// notiConfig.getAppKey() != null) {
		// if (quotationNotif.getPlayerId() != null) {
		OneSignalPostNotification osn = new OneSignalPostNotification();
		// HashMap<String, Object> mapContent = new HashMap<>();
		// mapContent.put("type", quotationNotif.getAction());
		// mapContent.put("action", quotationNotif.getAction());
		// mapContent.put("data_ref_id", quotationNotif.getId());
		// mapContent.put("contents", new
		// MessageResult(quotationNotif.getId(),
		// quotationNotif.getMessage()));
		//
		// if ("booking".equals(quotationNotif.getAction())) {
		// if (quotationNotif.getMessage() == null) {
		// mapContent.put("contents", new
		// MessageResult(quotationNotif.getId(), ApplicationProperties
		// .getValue("notif.msg.booking.booked",
		// quotationNotif.getMemberName())));
		// }
		// } else if
		// ("quotation_request".equals(quotationNotif.getAction())) {
		// if (quotationNotif.getMessage() == null) {
		// mapContent.put("contents", new
		// MessageResult(quotationNotif.getId(), ApplicationProperties
		// .getValue("notif.msg.quot.request",
		// quotationNotif.getMemberName())));
		// }
		// } else if
		// ("quotation_response".equals(quotationNotif.getAction())) {
		// if (quotationNotif.getMessage() == null) {
		// mapContent.put("contents",
		// new MessageResult(quotationNotif.getId(),
		// ApplicationProperties.getValue("notif.msg.quot.response",
		// quotationNotif.getServiceProviderName(),
		// quotationNotif.getId())));
		// }
		// }
		// if (mapContent.size() > 0) {
		// List<String> playerIds =
		// Arrays.asList(quotationNotif.getPlayerId());
		// boolean rs = osn.postNotification(notification, null, playerIds,
		// null, null, null,
		// quotationNotif.getUnreadNotify(), logger);

		boolean rs = osn.postNotification(notification, logger);
		return rs;
		// }
		// }
		// } else {
		// logger.error(
		// "Lacking config for action: " + notification.getAction() + ",
		// message: " + notification.toString());
		// }
		// return false;
	}

	private List<Notification> splitByAddress(List<Notification> lstChildUser) throws Exception {
		List<Notification> lstChildAddress = new ArrayList<>();
//		for (Notification item : lstChildUser) {
//			if (item.getLstAddress() != null && item.getLstAddress().size() > 0) {
//				for (String address : item.getLstAddress()) {
//					lstChildAddress = copyNotificationWithAddress(lstChildAddress, item, address, null, null);
//				}
//			} else {
//				lstChildAddress.add(item);
//			}
//		}
		return lstChildAddress;
	}

	private void sendMessage(List<UserNotificationSticky> lstChildAddress, List<UserNotificationSticky> lstRoot) {
		for (UserNotificationSticky noti : lstChildAddress) {
			sendMessageToSendTopic(noti);
		}
		for (UserNotificationSticky noti : lstRoot) {
			// noti.setRoot(true);
			sendMessageToSendSuccessTopic(noti);
		}
	}

	private List<Notification> copyNotificationWithAddress(List<Notification> lstChildAddress, Notification item,
			String address, String destObject, Long destRefId) throws Exception {
		Notification noti = new Notification();
		BeanUtils.copyProperties(noti, item);
//		noti.setAddress(address);
//		if (destObject != null && destRefId != null) {
//			noti.setDestinationObject(destObject);
//			noti.setDestinationRefId(destRefId);
//		}
		lstChildAddress.add(noti);
		return lstChildAddress;
	}

	private Notification buildNotificationInfo(Notification notification) throws Exception {
//		notification.setSeen(false);
		// if (notification.getListUser() != null &&
		// notification.getListUser().size() > 0) {
		// notification.setDestinationObject(VarStatic.DESTINATION_OBJECT_GARA);
		// ServiceProvider sp =
		// EHCacheManager.getServiceProviderCache(notification.getDomain());
		// notification.setDestinationRefId((sp != null) ? (long) sp.getId() :
		// null);
		// } else if (notification.getListPartner() != null &&
		// notification.getListPartner().size() > 0) {
		// notification.setDestinationObject(VarStatic.DESTINATION_OBJECT_CAROWNER);
		// Long memberId = getMemberIdByPhone(notification.getPhone());
		// notification.setDestinationRefId((memberId != null) ? memberId :
		// null);
		// }

//		if (VarStatic.NOTI_APP_AGARA.equals(notification.getApp())) {
//			notification.setDestinationObject(VarStatic.DESTINATION_OBJECT_GARA);
//			ServiceProvider sp = EHCacheManager.getServiceProviderCache(notification.getDomain());
////			notification.setDestinationRefId((sp != null) ? (long) sp.getId() : null);
//		} else if (VarStatic.NOTI_APP_BAVE.equals(notification.getApp())) {
//			notification.setDestinationObject(VarStatic.DESTINATION_OBJECT_CAROWNER);
//			if (notification.getDestinationRefId() == null) {
//				Long memberId = getMemberIdByPhone(notification.getPhone());
//				notification.setDestinationRefId((memberId != null) ? memberId : null);
//			}
//
//		}
//
//		String action = (notification.getDataObject() != null ? notification.getDataObject().trim() : "") + "_"
//				+ (notification.getAction() != null ? notification.getAction().trim() : "");
//
//		if (action.equals("fleet.repair_create")) {
//			notification.setAction("create_repair");
//		} else if (action.equals("sale.order_confirm") || action.equals("sale.order_done")) {
//			notification.setAction("saleorder_update");
//		} else if (action.equals("fleet.workorder_write")) {
//			notification.setAction("workorder_completed");
//		}
//
//		notification.setStatus(VarStatic.NOTI_STATUS_PROCESSING);
//
//		// removing agara.vn
//		notification.setDomain(BEUtils.removeMainDomain(notification.getDomain(), ".agara.vn"));

		return notification;
	}

	private Long getMemberIdByPhone(String phone) throws Exception {
		List<CarGoUser> lstRs = EHCacheManager.getCarGoUsersCache(phone);
		if (lstRs == null) {
			return userNotificationStickyDao.getMemberIdByPhone(phone);
		} else if (lstRs.size() > 0) {
			return lstRs.get(0).getMemberId();
		}
		return null;
	}

}
