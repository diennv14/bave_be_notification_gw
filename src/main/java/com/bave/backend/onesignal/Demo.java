package com.bave.backend.onesignal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.bave.backend.persistence.entity.Notification;

public class Demo {
	private final static Logger logger = LoggerFactory.getLogger(Demo.class);

	public static void main(String[] args) {
		OneSignalPostNotification n = new OneSignalPostNotification();
		List<String> playerId = new ArrayList<>();
		playerId.add("55f1f0b1-fbb5-47e7-94d4-b65562a9ee1b");
		HashMap<String, Object> mapContent = new HashMap<>();
		mapContent.put("domain", "car_erp");
		mapContent.put("type", "create_repair");
		MessageResult messageResult = new MessageResult();
		messageResult.setMessage("ios_badgeCount");
		messageResult.setRefId("1");
		mapContent.put("contents", messageResult);
//		n.postNotification(new Notification(), "Test ios_badgeCount", playerId, mapContent,
//				"MmYyOTRhOGYtMWQzYS00NzlhLWEwMDEtZjExYjJjZWY3MDdk", "95dc5be1-ea4d-46b3-9ef0-ea71e7bb0d30", 14, logger);
	}

}
