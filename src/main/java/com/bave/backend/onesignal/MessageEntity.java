package com.bave.backend.onesignal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageEntity {
	@JsonProperty("app_id")
	private String appId;
	@JsonProperty("include_player_ids")
	private List<String> includePlayerIds = new ArrayList<>();
	@JsonProperty("data")
	private HashMap<String, Object> data = new HashMap<>();
	@JsonProperty("contents")
	private HashMap<String, Object> contents = new HashMap<>();

	@JsonProperty("ios_badgeType")
	private String iosBadgeType;
	@JsonProperty("ios_badgeCount")
	private Integer iosBadgeCount;

	public MessageEntity() {
		super();
	}

	public MessageEntity(String appId, List<String> includePlayerIds, HashMap<String, Object> data,
			HashMap<String, Object> contents) {
		super();
		this.appId = appId;
		this.includePlayerIds = includePlayerIds;
		this.data = data;
		this.contents = contents;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public List<String> getIncludePlayerIds() {
		return includePlayerIds;
	}

	public void setIncludePlayerIds(List<String> includePlayerIds) {
		this.includePlayerIds = includePlayerIds;
	}

	public HashMap<String, Object> getData() {
		return data;
	}

	public void setData(HashMap<String, Object> data) {
		this.data = data;
	}

	public HashMap<String, Object> getContents() {
		return contents;
	}

	public void setContents(HashMap<String, Object> contents) {
		this.contents = contents;
	}

	public String getIosBadgeType() {
		return iosBadgeType;
	}

	public void setIosBadgeType(String iosBadgeType) {
		this.iosBadgeType = iosBadgeType;
	}

	public Integer getIosBadgeCount() {
		return iosBadgeCount;
	}

	public void setIosBadgeCount(Integer iosBadgeCount) {
		this.iosBadgeCount = iosBadgeCount;
	}

}
