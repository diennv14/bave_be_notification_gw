package com.bave.backend.onesignal;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bave.backend.persistence.entity.Notification;
import com.bave.backend.persistence.entity.UserNotificationSticky;
import com.bave.backend.util.StringUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author NetoDevel
 */

public class OneSignalPostNotification {
	private final Logger logger = LoggerFactory.getLogger("api_file");

	// public boolean postNotification1(Notification notification, String
	// message, List<String> playerIds,
	// HashMap<String, Object> mapContent, String API_KEY, String APP_ID,
	// Integer iosBadgeCount, Logger logger) {
	//
	// try {
	// String jsonResponse;
	// URL url = new URL("https://onesignal.com/api/v1/notifications");
	// HttpURLConnection con = (HttpURLConnection) url.openConnection();
	// con.setUseCaches(false);
	// con.setDoOutput(true);
	// con.setDoInput(true);
	//
	// con.setRequestProperty("Content-Type", "application/json;
	// charset=UTF-8");
	// con.setRequestProperty("Authorization", "Basic " + API_KEY + "");
	// con.setRequestMethod("POST");
	//
	// MessageEntity me = new MessageEntity();
	// me.setAppId(APP_ID);
	// me.setIncludePlayerIds(playerIds);
	//
	// MessageResult msgRs = (MessageResult) mapContent.get("contents");
	// if (message == null && mapContent != null) {
	// me.getContents().put("en", msgRs.getMessage());
	// } else {
	// me.getContents().put("en", msgRs.getMessage());
	// }
	// if (iosBadgeCount != null) {
	// me.setIosBadgeType("SetTo");
	// me.setIosBadgeCount(iosBadgeCount);
	// mapContent.put("unread_notify", iosBadgeCount);
	// }
	// me.getData().putAll(mapContent);
	//
	// String strJsonBody = new ObjectMapper().writeValueAsString(me);
	//
	// byte[] sendBytes = strJsonBody.getBytes("UTF-8");
	// con.setFixedLengthStreamingMode(sendBytes.length);
	//
	// OutputStream outputStream = con.getOutputStream();
	// outputStream.write(sendBytes);
	//
	// int httpResponse = con.getResponseCode();
	//
	// if (httpResponse >= HttpURLConnection.HTTP_OK && httpResponse <
	// HttpURLConnection.HTTP_BAD_REQUEST) {
	// Scanner scanner = new Scanner(con.getInputStream(), "UTF-8");
	// jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() :
	// "";
	// scanner.close();
	// if (jsonResponse != null && jsonResponse.contains("error")) {
	// logger.info("SENDER " + notification.getChannel() + ": address=" +
	// notification.getAddress()
	// + ", seq=" + notification.getSequence() + ", rs:" + jsonResponse);
	// return false;
	// } else {
	// logger.info("SENDER " + notification.getChannel() + ": address=" +
	// notification.getAddress()
	// + ", seq=" + notification.getSequence() + ", rs:" + jsonResponse);
	// return true;
	// }
	// } else {
	// Scanner scanner = new Scanner(con.getErrorStream(), "UTF-8");
	// jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() :
	// "";
	// scanner.close();
	// logger.info("SENDER " + notification.getChannel() + ": address=" +
	// notification.getAddress() + ", seq="
	// + notification.getSequence() + ", rs:" + jsonResponse);
	// return false;
	// }
	// } catch (Throwable t) {
	// t.printStackTrace();
	// }
	// return false;
	// }

	public boolean postNotification(UserNotificationSticky userNotificationSticky, Logger logger) {
		try {
			String type = "";
			if (!StringUtil.isEmpty(userNotificationSticky.getDestinationObject())
					&& userNotificationSticky.getDestinationObject().equals("erp_user")) {
				type = "agara app";
			} else {
				type = "bave app";
			}
			logger.info("START ... posting notification to "+ type + " {}");
			for (String address : userNotificationSticky.getLstAddress()) {
				URL url = new URL("https://fcm.googleapis.com/fcm/send");
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();

				conn.setUseCaches(false);
				conn.setDoInput(true);
				conn.setDoOutput(true);

				conn.setRequestMethod("POST");
				String authenKey = "";

				if (!StringUtil.isEmpty(userNotificationSticky.getDestinationObject())
						&& userNotificationSticky.getDestinationObject().equals("erp_user")) {
					authenKey = "key=AIzaSyCjW6PtRssivrjuzip8BcDb1hhd-YCxlsg";
				} else {
					authenKey = "key=AIzaSyBwsjxYJ21b-SU3RwyWwUtr7nIUAeTcMNY";
				}


				conn.setRequestProperty("Authorization", authenKey);
				conn.setRequestProperty("Content-Type", "application/json");

				JSONObject json = new JSONObject();
				String deviceToken = address;
				json.put("to", deviceToken.trim());

				JSONObject notificationJson = new JSONObject();
				notificationJson.put("sound", "noti_booking.mp3");
				notificationJson.put("title", userNotificationSticky.getTitle()); // Notification
				notificationJson.put("body", userNotificationSticky.getMessage()); // Notification
				json.put("notification", notificationJson);

				String strJsonBody = new ObjectMapper().writeValueAsString(userNotificationSticky);
				JSONObject data = new JSONObject(strJsonBody);
				data.remove("lst_address");
				json.put("data", data);
				logger.info("postNotification request body {}", json.toString());
				try {
					OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
					wr.write(json.toString());
					wr.flush();

					BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

					String output;
					while ((output = br.readLine()) != null) {
						logger.info("result_fire_base postNotification {}, token: {}", output, deviceToken);
					}

				} catch (Exception e) {
					logger.error("Error call firebase {}", e);
				}
				
				
				logger.info("END ... post notification to "+ type + " succsess {}");
			}
			return true;
		} catch (Throwable t) {
			logger.error("Error postNotification {}", t);
			return false;
		}
	}
}