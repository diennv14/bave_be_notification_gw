package com.bave.backend.onesignal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MessageResult {
	@JsonProperty("ref_id")
	private String refId;
	@JsonProperty("message")
	private String message;

	public MessageResult() {
		super();
	}

	public MessageResult(String refId, String message) {
		super();
		this.refId = refId;
		this.message = message;
	}

	public String getRefId() {
		return refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
