package com.bave.backend.api.object;

import java.util.Date;
import com.bave.backend.util.RpcXmlConfig;
import com.fasterxml.jackson.annotation.JsonProperty;

public class NotificationEntity {

	@JsonProperty("id")
	private Long notificationId;
	@JsonProperty("source_object_code")
	private String sourceObjectCode;
	@JsonProperty("source_ref_id")
	private Long sourceRefId;
	@JsonProperty("source_name")
	private String sourceName;
	@JsonProperty("source_avatar")
	private String sourceAvatar;
	@JsonProperty("dest_object_code")
	private String destObjectCode;
	@JsonProperty("dest_ref_id")
	private Long destRefId;
	@JsonProperty("dest_name")
	private String destName;
	@JsonProperty("dest_avatar")
	private String destAvatar;
	@JsonProperty("action")
	private String action;
	@JsonProperty("action_description")
	private String actionDescription;
	@JsonProperty("type")
	private String type;
	@JsonProperty("short_content")
	private String shortContent;
	@JsonProperty("member_id")
	private Long memberId;
	@JsonProperty("member_name")
	private String memberName;
	@JsonProperty("member_avatar")
	private String memberAvatar;
	@JsonProperty("message")
	private String message;
	@JsonProperty("create_date")
	private Date createDate;
	@JsonProperty("time_ago")
	private String timeAgo;
	@JsonProperty("seen")
	private boolean seen;
	@JsonProperty("data_object_code")
	private String dataObjectCode;
	@JsonProperty("data_ref_id")
	private String dataRefId;

	public NotificationEntity() {
		super();
	}

	public Long getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	public String getSourceObjectCode() {
		return sourceObjectCode;
	}

	public void setSourceObjectCode(String sourceObjectCode) {
		this.sourceObjectCode = sourceObjectCode;
	}

	public Long getSourceRefId() {
		return sourceRefId;
	}

	public void setSourceRefId(Long sourceRefId) {
		this.sourceRefId = sourceRefId;
	}

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public String getSourceAvatar() {
		if (sourceAvatar == null && getSourceRefId() != null) {
			String model = getSourceObjectCode();
			sourceAvatar = String.format(RpcXmlConfig.AVATAR_MEDIUM_URL, model.replace("_", "."), getSourceRefId());
		}
		return sourceAvatar;
	}

	public void setSourceAvatar(String sourceAvatar) {
		this.sourceAvatar = sourceAvatar;
	}

	public String getDestObjectCode() {
		return destObjectCode;
	}

	public void setDestObjectCode(String destObjectCode) {
		this.destObjectCode = destObjectCode;
	}

	public Long getDestRefId() {
		return destRefId;
	}

	public void setDestRefId(Long destRefId) {
		this.destRefId = destRefId;
	}

	public String getDestName() {
		return destName;
	}

	public void setDestName(String destName) {
		this.destName = destName;
	}

	public String getDestAvatar() {
		if (destAvatar == null && getDestRefId() != null) {
			String model = getDestObjectCode();
			destAvatar = String.format(RpcXmlConfig.AVATAR_MEDIUM_URL, model.replace("_", "."), getDestRefId());
		}
		return destAvatar;
	}

	public void setDestAvatar(String destAvatar) {
		this.destAvatar = destAvatar;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getActionDescription() {
		return actionDescription;
	}

	public void setActionDescription(String actionDescription) {
		this.actionDescription = actionDescription;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getShortContent() {
		return shortContent;
	}

	public void setShortContent(String shortContent) {
		this.shortContent = shortContent;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long memberId) {
		this.memberId = memberId;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getMemberAvatar() {
		return memberAvatar;
	}

	public void setMemberAvatar(String memberAvatar) {
		this.memberAvatar = memberAvatar;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getTimeAgo() {
		return timeAgo;
	}

	public void setTimeAgo(String timeAgo) {
		this.timeAgo = timeAgo;
	}

	public boolean isSeen() {
		return seen;
	}

	public void setSeen(boolean seen) {
		this.seen = seen;
	}

	public String getDataObjectCode() {
		return dataObjectCode;
	}

	public void setDataObjectCode(String dataObjectCode) {
		this.dataObjectCode = dataObjectCode;
	}

	public String getDataRefId() {
		return dataRefId;
	}

	public void setDataRefId(String dataRefId) {
		this.dataRefId = dataRefId;
	}

}
