//package com.bave.backend;
//
//import java.io.BufferedReader;
//import java.io.FileReader;
//import java.io.IOException;
//import java.util.Collections;
//import java.util.List;
//import java.util.Properties;
//
//import org.apache.kafka.clients.consumer.ConsumerConfig;
//import org.apache.kafka.clients.consumer.KafkaConsumer;
//
//import com.bave.be.entity.Notification;
//import com.bave.be.entity.NotificationDeserializer;
//import com.google.gson.Gson;
//import com.google.gson.reflect.TypeToken;
//
//public class testConsumer {
//	private final static String BOOTSTRAP_SERVERS = "localhost:9092";
//	private final static String TOPIC = "erpNotification";
//
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		//
//		// KafkaConsumer<String, Notification> consumer = createConsumer();
//		// try {
//		// consumer.subscribe(Collections.singletonList(TOPIC));
//		// Notification notification = new Notification();
//		// while (true) {
//		// ConsumerRecords<String, Notification> messages = consumer.poll(0);
//		//
//		// for (ConsumerRecord<String, Notification> message : messages) {
//		// notification = message.value();
//		// System.out.println("Message received " +
//		// message.value().getMessage());
//		// }
//		// }
//		// } catch (Exception e) {
//		// e.printStackTrace();
//		// } finally {
//		// consumer.close();
//		// }
//		readJson();
//	}
//
//	private static KafkaConsumer<String, Notification> createConsumer() {
//		final Properties props = new Properties();
//		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
//		props.put(ConsumerConfig.GROUP_ID_CONFIG, "KafkaExampleConsumer");
//		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, NotificationDeserializer.class.getName());
//		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, NotificationDeserializer.class.getName());
//		// props.put("value.serializer",
//		// "com.bave.be.entity.NotificationSerializer");
//
//		// Create the consumer using props.
//		final KafkaConsumer<String, Notification> consumer = new KafkaConsumer<>(props);
//
//		// Subscribe to the topic.
//		consumer.subscribe(Collections.singletonList(TOPIC));
//		return consumer;
//	}
//
//	private static final String FILENAME = "E:\\tai lieu\\BAVE\\du lieu thanh pho\\districts.json";
//
//	private static void readJson() {
//		BufferedReader reader;
//		String result = "";
//		try {
//			reader = new BufferedReader(new FileReader(FILENAME));
//			String line = reader.readLine();
//			while (line != null) {
//				result += line;
//				// System.out.println(line);
//				// read next line
//				line = reader.readLine();
//			}
//			System.out.println(result);
//			
//			Gson gson = new Gson();
//			TypeToken<List<Object>> token = new TypeToken<List<Object>>(){};
//			gson.fromJson(result,token.getType());
//			
//			
//			
//			reader.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
//
//}
