package com.bave.backend.dao;

import java.util.Date;
import java.util.List;

import com.bave.backend.persistence.entity.ServiceProvider;

public interface ServiceProviderDao {
	public List<ServiceProvider> getAllServiceProvider(Date fromDate) throws Exception;

	public ServiceProvider getServiceProvider(String code) throws Exception;
	
	public List<ServiceProvider> getServiceProviderUseBcs() throws Exception;
	
}
