package com.bave.backend.dao;

// Generated 12/06/2017 by HaiTX Computer

import com.bave.backend.generic.dao.GenericDao;
import com.bave.backend.persistence.entity.NotificationEmail;

public interface NotificationEmailDao extends GenericDao<NotificationEmail, Long> {
	public boolean exeSql(String sql) throws Exception;
}
