package com.bave.backend.dao;

// Generated 12/06/2017 by HaiTX Computer

import com.bave.backend.generic.dao.GenericDao;
import com.bave.backend.persistence.entity.NotificationSMS;

public interface NotificationSMSDao extends GenericDao<NotificationSMS, Long> {
	public boolean exeSql(String sql) throws Exception;
}
