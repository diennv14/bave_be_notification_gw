package com.bave.backend.dao;

import java.util.Date;
import java.util.List;

import com.bave.backend.persistence.entity.CarGoUser;
import com.bave.backend.persistence.entity.ErpUser;

public interface UserDao {
	public List<ErpUser> getAllERPUsers(Date fromDate) throws Exception;

	public List<CarGoUser> getAllCarGoUsers(Date fromDate) throws Exception;

	public ErpUser getERPUsers(String username) throws Exception;

	public List<CarGoUser> getCarGoUsers(String username) throws Exception;

	public CarGoUser getCarGoUsers(Long id) throws Exception;
}
