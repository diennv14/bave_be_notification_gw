package com.bave.backend.dao.impl;

// Generated 12/06/2017 by HaiTX Computer

import java.io.Serializable;

import org.springframework.stereotype.Repository;

import com.bave.backend.dao.NotificationConfigDao;
import com.bave.backend.generic.dao.impl.GenericDaoImpl;
import com.bave.backend.persistence.entity.NotificationConfig;

@Repository("notificationConfigDaoImpl")
public class NotificationConfigDaoImpl extends GenericDaoImpl<NotificationConfig, Long>
		implements NotificationConfigDao, Serializable {

}
