package com.bave.backend.dao.impl;

// Generated Nov 6, 2017 4:46:49 PM by Hibernate Tools 4.3.1

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.bave.backend.dao.PromotionConditionDao;
import com.bave.backend.generic.api.GenericDaoImpl;
import com.bave.backend.persistence.entity.PromotionCondition;
import com.bave.backend.util.StringUtil;

@Repository("promotionConditionDaoImpl")
public class PromotionConditionDaoImpl extends GenericDaoImpl<PromotionCondition, Integer>
		implements PromotionConditionDao, Serializable {

	@Override
	public void deleteProCondByCondition(Integer pmNewId) {
		Session session = getSession();
		if (pmNewId != null && pmNewId > 0) {
			String sql = "delete from promotion_condition where pm_new_id = " + pmNewId;

			SQLQuery query = session.createSQLQuery(sql);
			query.executeUpdate();
		}
	}

	@Override
	public boolean exeSql(String sql) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Integer> getLstMemberApplyPromotion(List<PromotionCondition> lstProCond, String garas) {
		// TODO Auto-generated method stub
		Session session = getSession();

		String sql = "";
		for (PromotionCondition proCond : lstProCond) {
			if (proCond.getMemberType().equals("brand") || proCond.getMemberType().equals("model")
					|| proCond.getMemberType().equals("brand")) {
				Integer brandId = proCond.getBrandId() != null ? proCond.getBrandId() : -1;
				Integer modelId = proCond.getModelId() != null ? proCond.getModelId() : -1;
				Integer veGenId = proCond.getVeGenId() != null ? proCond.getVeGenId() : -1;
				Integer pmNewId = proCond.getPmNewId();

				if (StringUtil.isEmpty(sql)) {
					sql += "select mem.id from member mem inner join vehicle ve on mem.id = ve.member_id where (ve.brand_id = "
							+ brandId + " or ve.model_id = " + modelId + " or ve.variant_id = " + veGenId
							+ ") and (select count(*) from user_notification_sticky where member_id = mem.id and type='promotion' and data_ref_id = '"
							+ pmNewId + "') = 0 ";
				} else {
					sql += " union select mem.id from member mem inner join vehicle ve on mem.id = ve.member_id where (ve.brand_id = "
							+ brandId + " or ve.model_id = " + modelId + " or ve.variant_id = " + veGenId
							+ ") and (select count(*) from user_notification_sticky where member_id = mem.id and type='promotion' and data_ref_id = '"
							+ pmNewId + "') = 0 ";

				}
			}

			if (proCond.getMemberType().equals("all_member")) {
				Integer pmNewId = proCond.getPmNewId();

				if (StringUtil.isEmpty(sql)) {
					sql += "select mem.id from member mem  inner join provider_member promem on mem.id = promem.member_id where promem.invite_code in ("
							+ garas
							+ ") and  (select count(*) from user_notification_sticky where member_id = mem.id and type='promotion' and data_ref_id = '"
							+ pmNewId + "') = 0 ";
				} else {
					sql += " union select mem.id from member mem  inner join provider_member promem on mem.id = promem.member_id where promem.invite_code in ("
							+ garas
							+ ") and  (select count(*) from user_notification_sticky where member_id = mem.id and type='promotion' and data_ref_id = '"
							+ pmNewId + "') = 0 ";
				}
			}

			if (proCond.getMemberType().equals("group_member")) {
				Integer pmNewId = proCond.getPmNewId();
				Integer groupId = Integer.parseInt(proCond.getMemberValue());

				if (StringUtil.isEmpty(sql)) {
					sql += "select mem.id from member mem inner join member_group_mapping mgm on mgm.member_id = mem.id inner join member_group mg on mg.id = mgm.member_group_id where mg.id = "
							+ groupId
							+ " and  (select count(*) from user_notification_sticky where member_id = mem.id and type='promotion' and data_ref_id = '"
							+ pmNewId + "') = 0 ";
				} else {
					sql += " union select mem.id from member mem inner join member_group_mapping mgm on mgm.member_id = mem.id inner join member_group mg on mg.id = mgm.member_group_id where mg.id = "
							+ groupId
							+ " and  (select count(*) from user_notification_sticky where member_id = mem.id and type='promotion' and data_ref_id = '"
							+ pmNewId + "') = 0 ";
				}
			}
		}
		List<Integer> lstMemberId = new ArrayList();
		if (!StringUtil.isEmpty(sql)) {
			SQLQuery query = session.createSQLQuery(sql);
			lstMemberId = query.list();
		}

		return lstMemberId;
	}

	@Override
	public List<String> getGarasOfPromotion(Integer pmNewId) {
		Session session = getSession();

		String sql = "select gara_code from gara_promotion_mapping gpm inner join service_provider sp on gpm.gara_code = sp.code where gpm.pm_new_id ="
				+ pmNewId;
		SQLQuery query = session.createSQLQuery(sql);
		List<String> lstGara = query.list();

		return lstGara;
	}

}
