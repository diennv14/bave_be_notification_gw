package com.bave.backend.dao.impl;

import org.springframework.stereotype.Repository;

import com.bave.backend.dao.ErpPartnerDao;
import com.bave.backend.generic.dao.impl.GenericDaoImpl;
import com.bave.backend.persistence.entity.ErpPartner;
@Repository
public class ErpPartnerDaoImpl extends GenericDaoImpl<ErpPartner, Integer> implements ErpPartnerDao {

}
