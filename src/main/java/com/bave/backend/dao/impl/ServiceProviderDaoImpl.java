package com.bave.backend.dao.impl;

// Generated Nov 6, 2017 4:46:49 PM by Hibernate Tools 4.3.1

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import com.bave.backend.dao.ServiceProviderDao;
import com.bave.backend.generic.dao.impl.GenericBaseDaoImpl;
import com.bave.backend.persistence.entity.ServiceProvider;

@Repository("serviceProvider")
@SuppressWarnings("unchecked")
public class ServiceProviderDaoImpl extends GenericBaseDaoImpl implements ServiceProviderDao, Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public List<ServiceProvider> getAllServiceProvider(Date fromDate) throws Exception {
		Session session = getSession();
		HashMap<String, Object> mapParams = newMapParameters();
		String sql = " select * from service_provider ";
		if (fromDate != null) {
			sql += " where create_date >= :create_date ";
			mapParams.put("create_date", new java.sql.Date(fromDate.getTime()));
		}
		sql += " order by create_date desc ";
		Query<ServiceProvider> query = session.createNativeQuery(sql, ServiceProvider.class);
		query = setParametersByMap(query, mapParams);
		return query.list();
	}

	@Override
	public ServiceProvider getServiceProvider(String code) throws Exception {
		Session session = getSession();
		HashMap<String, Object> mapParams = newMapParameters();
		String sql = " select ́* from service_provider ";
		sql += " where code = :code ";
		Query<ServiceProvider> query = session.createNativeQuery(sql, ServiceProvider.class);
		query.setParameter("code", code);
		query = setParametersByMap(query, mapParams);
		List<ServiceProvider> lstRs = query.list();
		return (lstRs != null && lstRs.size() > 0) ? lstRs.get(0) : null;
	}

	@Override
	public List<ServiceProvider> getServiceProviderUseBcs() throws Exception {
		Session ses = getSession();
		String sql = "select * from service_provider where use_bave_car_sv = true";
		return ses.createNativeQuery(sql, ServiceProvider.class).list();
	}

}
