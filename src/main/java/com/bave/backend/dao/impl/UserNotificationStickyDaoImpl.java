package com.bave.backend.dao.impl;

// Generated 12/06/2017 by HaiTX Computer

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.TemporalType;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import com.bave.backend.dao.UserNotificationStickyDao;
import com.bave.backend.generic.configuration.Condition;
import com.bave.backend.generic.dao.impl.GenericDaoImpl;
import com.bave.backend.persistence.entity.ErpUser;
import com.bave.backend.persistence.entity.PromotionNews;
import com.bave.backend.persistence.entity.UserNotificationSticky;
import com.bave.backend.util.EHCacheManager;

@Repository("notificationOneSignalDaoImpl")
public class UserNotificationStickyDaoImpl extends GenericDaoImpl<UserNotificationSticky, Long>
		implements UserNotificationStickyDao, Serializable {

	@Override
	public Set<String> getAllUserOnlineOfGara(String domain) throws Exception {
		try {
			Session session = getSession();
			HashMap<String, Object> mapParams = newMapParameters();
			String sql = " select distinct player_id from erp_user_online where domain = :domain and authen_key is not null ";
			mapParams.put("domain", domain);
			Query query = session.createNativeQuery(sql);
			query = setParametersByMap(query, mapParams);
			List<Object> objRs = query.list();
			Set<String> setRs = new HashSet<>();
			for (Object object : objRs) {
				Object[] obj = (Object[]) object;
				setRs.add(obj[0].toString());
			}
			return setRs;
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	@Override
	public List<String> getPlayerIdsByPhone(String phone) throws Exception {
		try {
			ErpUser rs = EHCacheManager.getERPUsersCache(phone);
			if (rs == null) {
				Session session = getSession();
				HashMap<String, Object> mapParams = newMapParameters();
				String sql = "select distinct md.player_id from erp_partner ep inner join member_device md on ep.member_id = md.member_id "
						+ "where md.active = '1' and player_id is not null and ep.phone like '%" + phone + "%'";
				Query query = session.createNativeQuery(sql);
				query = setParametersByMap(query, mapParams);
				List<Object> objRs = query.list();
				List<String> setRs = new ArrayList<>();
				for (Object object : objRs) {
					Object[] obj = (Object[]) object;
					setRs.add(obj[0].toString());
				}
				return setRs;
			} else {
				rs.getPlayerId();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
		return null;
	}

	@Override
	public Long getMemberIdByPhone(String phone) throws Exception {
		Session session = getSession();
		HashMap<String, Object> mapParams = newMapParameters();
		String sql = " select member_id from autho_profile where user_name = '" + phone + "' ";
		Query query = session.createNativeQuery(sql);
		query = setParametersByMap(query, mapParams);
		Object objRs = query.uniqueResult();
		if (objRs != null) {
			return Long.valueOf(objRs.toString());
		}
		return null;
	}

	@Override
	public List<UserNotificationSticky> findByCondition(Condition condition) throws Exception {
		Session session = getSession();
		HashMap<String, Object> mapParams = newMapParameters();
		String sql = " select us.id, us.member_id, us.not_notify_id, us.domain, us.type, us.action, us.status, us.create_date, us.source_object, us.source_ref_id, us.source_name, "
				+ "us.source_avatar, us.destination_object, us.destination_ref_id, us.dest_name, us.dest_avatar, us.short_content, us.data_object, us.data_ref_id, us.data_name, "
				+ "us.addition_data, us.datatype, us.message, us.title, us.content, us.template_id, us.sequence, us.callback_url, us.app, us.notification_id, us.seen, us.send_date, "
				+ "us.sender_status, us.fail_reason, us.channel, us.icon, us.source_type, us.creater_user_type, us.creater_user_do, us.approved_user_type, us.approved_method, us.approved_user_do, "
				+ "us.approved_datetime, us.start_datetime, us.real_time_to_live, us.expect_finish_datetime, us.real_finish_datetime, us.priority"
				+ ", address, notify_code from user_notification_sticky us where 1=1 ";
		if (condition.get("status") != null) {
			sql += " and us.status ='" + condition.get("status").toString() + "'";
		}

		Query query = session.createNativeQuery(sql, UserNotificationSticky.class);

		return query.list();
	}

	@Override
	public List<String> getTokenAddressByMemberId(Long memberId) throws Exception {
		Session session = getSession();
		HashMap<String, Object> mapParams = newMapParameters();
		String sql = "";
		List<String> lstResult = new ArrayList<String>();
		if (memberId != null) {
			sql = " select distinct player_id from member_device where member_id = " + memberId;
			Query query = session.createNativeQuery(sql);

			lstResult = query.list();
		}

		return lstResult;
	}

	@Override
	public List<PromotionNews> getLstActivePromotionNews() throws Exception {
		Session session = getSession();
		String sql = " select * from promotion_news where "
				+ "to_date(to_char(now(),'dd/MM/YYYY hh:mm:ss'),'dd/MM/YYYY hh:mm:ss') <= "
				+ "to_date(to_char(end_date,'dd/MM/YYYY hh:mm:ss'),'dd/MM/YYYY hh:mm:ss') and status = '1'  ";

		Query query = session.createNativeQuery(sql, PromotionNews.class);

		return query.list();
	}

	@Override
	public boolean checkExistNotification(int memberId, Date reminderDate, int dateDiff, int reminderId,
			String domain) {
		Session ses = getSession();
		String sql = "select count(*) from user_notification_sticky uns "
				+ "where member_id  = ?1 and data_ref_id = ?2 and (?3::date - create_date::date) <= ?4 "
				+ "and domain = ?5 and action = 'reminder_detail' ";
		BigInteger count = (BigInteger) ses.createNativeQuery(sql).setParameter(1, memberId)
				.setParameter(2, String.valueOf(reminderId)).setParameter(3, reminderDate, TemporalType.TIMESTAMP)
				.setParameter(4, dateDiff).setParameter(5, domain).getSingleResult();
		return count != null && count.intValue() > 0;
	}

	// @Override
	// public List<UserNotificationSticky>
	// getLstUserNotificationSticky(Condition condition) {
	// try {
	// String status = "";
	// if (condition.get("status") != null) {
	// status = condition.get("status").toString();
	// }
	//
	// Session session = getSession();
	// HashMap<String, Object> mapParams = newMapParameters();
	// String sql = "select * from user_notification_sticky uns inner join
	// member_device md "
	// + "uns.member_id = md.member_id where uns.status like '" + status + "'";
	// Query query = session.createNativeQuery(sql);
	// List<Object> objRs = query.list();
	// List<String> setRs = new ArrayList<>();
	// for (Object object : objRs) {
	// Object[] obj = (Object[]) object;
	// setRs.add(obj[0].toString());
	// }
	// return null;
	//
	// } catch (Exception ex) {
	// ex.printStackTrace();
	// throw ex;
	// }
	// return null;
	// }

}
