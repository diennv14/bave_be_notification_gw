package com.bave.backend.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.bave.backend.dao.MaintainReminderDao;
import com.bave.backend.generic.api.GenericDaoImpl;
import com.bave.backend.generic.configuration.Condition;
import com.bave.backend.persistence.entity.CarserviceMaintainReminder;

import com.bave.backend.dao.MaintainReminderDao;
import com.bave.backend.generic.configuration.Condition;
//import com.bave.backend.generic.dao.impl.GenericDaoImpl;
import com.bave.backend.persistence.entity.CarserviceMaintainReminder;

@Repository
public class MaintainReminderDaoImpl extends GenericDaoImpl<CarserviceMaintainReminder, Integer>
		implements MaintainReminderDao {

	@Override
	public List<CarserviceMaintainReminder> getListReminder(Condition cond) throws Exception {
		String db = cond.getStringValue("db");
		int dateDiff = cond.getIntValue("date_diff");
		Session ses = getSession();
		String sql = "select * from dblink('dblink_" + db
				+ "','select id,partner_id,name,time_reminder from carservice_maintain_reminder where time_reminder >= current_timestamp "
				+ "and (time_reminder::date - current_timestamp::date) <= "
				+ dateDiff
				+ "  and partner_id is not null and state <> ''cancel'' ') as table2(id integer, partner_id integer, name character varying, time_reminder timestamp)";
		List<?> list = ses.createNativeQuery(sql).list();
		List<CarserviceMaintainReminder> result = new ArrayList<CarserviceMaintainReminder>();
		list.forEach(obj -> {
			Object[] cmr = (Object[]) obj;
			CarserviceMaintainReminder c = new CarserviceMaintainReminder();
			c.setId((int) cmr[0]);
			c.setPartnerId((int) cmr[1]);
			c.setName(cmr[2].toString());
			c.setTimeReminder((Date) cmr[3]);
			result.add(c);
		});
		return result;
	}

	@Override
	public boolean exeSql(String sql) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

}
