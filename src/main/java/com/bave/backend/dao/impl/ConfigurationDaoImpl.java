package com.bave.backend.dao.impl;

// Generated 12/06/2017 by HaiTX Computer

import java.io.Serializable;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.bave.backend.dao.ConfigurationDao;
import com.bave.backend.generic.dao.impl.GenericDaoImpl;
import com.bave.backend.persistence.entity.Configuration;

@Repository("configurationDaoImpl")
public class ConfigurationDaoImpl extends GenericDaoImpl<Configuration, Long>
		implements ConfigurationDao, Serializable {

	@Override
	@Cacheable(value = "configurationCache", key = "#type+#code")
	public Configuration getConfig(String type, String code) throws Exception {
		Configuration config = new Configuration();
		config.setType(type);
		if (code != null) {
			config.setCode(code);
		}
		return findSingleByObjT(config);
	}

	@Override
	@Cacheable(value = "configurationCache", key = "#type")
	public List<Configuration> getConfig(String type) throws Exception {
		Configuration config = new Configuration();
		config.setType(type);
		return findByObjT(config);
	}

	@Override
	@CacheEvict(value = "configurationCache", allEntries = true)
	public void resetCacheAllEntries() {
		System.out.println("configurationCache CacheEvict");
	}

}
