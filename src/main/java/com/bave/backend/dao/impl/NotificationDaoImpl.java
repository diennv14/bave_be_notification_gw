package com.bave.backend.dao.impl;

// Generated 12/06/2017 by HaiTX Computer

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.bave.backend.dao.NotificationDao;
import com.bave.backend.generic.configuration.Condition;
import com.bave.backend.generic.dao.impl.GenericDaoImpl;
import com.bave.backend.persistence.entity.Notification;
import com.bave.backend.util.RpcXmlUtil;
import com.bave.backend.util.StringUtil;

@Repository("notificationDaoImpl")
public class NotificationDaoImpl extends GenericDaoImpl<Notification, Long> implements NotificationDao, Serializable {

	@Override
	public List<Notification> getNEAllByParams(Condition condition) throws Exception {
		try {
			Session session = getSession();
			HashMap<String, Object> mapParams = newMapParameters();
			String sql = " select * from notification n where 1=1 ";
			if (condition.get("member_id") != null) {
				sql += " and n.member_id = :member_id ";
				mapParams.put("member_id", condition.getIntValue("member_id"));
			}
			if (condition.get("action") != null) {
				sql += " and n.action in :action ";
				mapParams.put("action", condition.get("action"));
			}
			if (!StringUtil.isEmpty(condition.getOrderByColumns())) {
				sql += " ORDER BY  " + condition.getOrderByColumns() + "  " + (condition.isAscOrder() ? "ASC" : "DESC")
						+ "  ";
			} else {
				sql += " order by create_date desc ";
			}
			SQLQuery query = session.createSQLQuery(sql);
			query = setParametersByMap(query, mapParams);
			query = pagingQuery(query, condition.getFirstItemIndex(), condition.getMaxItems());
			return RpcXmlUtil.mapRSToListEntity(query, Notification.class);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
		}
	}

	@Override
	public int updateSeen(int id) throws Exception {
		try {
			Session session = getSession();
			String sql = " update notification set seen = true where id = :id ";
			SQLQuery query = session.createSQLQuery(sql);
			query.setInteger("id", id);
			return query.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
		}
	}

	@Override
	public Integer getPlayerIdAmountByPhones(List<String> phones) throws Exception {
		Session session = getSession();
		List<BigInteger> lstRs = new ArrayList<BigInteger>();
		Integer result = 0;
		try {
			String phoneString = "";
			for (String phone : phones) {
				if (phoneString.equals("")) {
					phoneString = phone;
				} else {
					phoneString += "|" + phone;
				}
			}
			String sql = "select count(*) from erp_partner ep inner join member_device md on ep.member_id = md.member_id "
					+ "where md.active = '1' and player_id is not null and ep.phone ~ '(" + phoneString + ")'";
			SQLQuery query = session.createSQLQuery(sql);
			lstRs = query.list();
			if (lstRs.size() > 0) {
				result = lstRs.get(0).intValue();
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
		}
		return result;
	}

	@Override
	public boolean exeSql(String sql) throws Exception {
		Session session = getSession();
		HashMap<String, Object> mapParams = newMapParameters();
		SQLQuery query = session.createSQLQuery(sql);
		query.executeUpdate();
		return true;
	}

}
