package com.bave.backend.dao.impl;

// Generated 12/06/2017 by HaiTX Computer

import java.io.Serializable;

import org.springframework.stereotype.Repository;

import com.bave.backend.dao.NotificationTemplateDao;
import com.bave.backend.generic.dao.impl.GenericDaoImpl;
import com.bave.backend.persistence.entity.NotificationTemplate;

@Repository("notificationTemplateDaoImpl")
public class NotificationTemplateDaoImpl extends GenericDaoImpl<NotificationTemplate, Long>
		implements NotificationTemplateDao, Serializable {

}
