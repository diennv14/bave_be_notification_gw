package com.bave.backend.dao.impl;

// Generated Nov 6, 2017 4:46:49 PM by Hibernate Tools 4.3.1

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import com.bave.backend.dao.UserDao;
import com.bave.backend.generic.dao.impl.GenericBaseDaoImpl;
import com.bave.backend.persistence.entity.CarGoUser;
import com.bave.backend.persistence.entity.ErpUser;

@Repository("userDaoImpl")
public class UserDaoImpl extends GenericBaseDaoImpl implements UserDao, Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public List<ErpUser> getAllERPUsers(Date fromDate) throws Exception {
		Session session = getSession();
		HashMap<String, Object> mapParams = newMapParameters();
		String sql = " select id, domain, erp_user_id, player_id, status, erp_user_name, create_date, device_id from erp_user_online ";
		if (fromDate != null) {
			sql += " where create_date >= :create_date ";
			mapParams.put("create_date", new java.sql.Date(fromDate.getTime()));
		}
		sql += " order by create_date desc ";
		Query query = session.createNativeQuery(sql, ErpUser.class);
		query = setParametersByMap(query, mapParams);
		return query.list();
	}

	@Override
	public List<CarGoUser> getAllCarGoUsers(Date fromDate) throws Exception {
		Session session = getSession();
		HashMap<String, Object> mapParams = newMapParameters();
		String sql = " select md.*,ap.user_name as username from member_device md ";
		sql += " inner join autho_profile ap on ap.member_id = md.member_id ";
		if (fromDate != null) {
			sql += " where create_date >= :create_date ";
			mapParams.put("create_date", new java.sql.Date(fromDate.getTime()));
		}
		sql += " order by create_date desc ";
		Query query = session.createNativeQuery(sql, CarGoUser.class);
		query = setParametersByMap(query, mapParams);
		return query.list();
	}

	@Override
	public ErpUser getERPUsers(String username) throws Exception {
		Session session = getSession();
		HashMap<String, Object> mapParams = newMapParameters();
		String sql = " select id, domain, erp_user_id, player_id, status, erp_user_name, create_date, device_id from erp_user_online ";
		sql += " where  erp_user_name = :erp_user_name ";
		Query query = session.createNativeQuery(sql, ErpUser.class);
		query.setParameter("erp_user_name", username);
		query = setParametersByMap(query, mapParams);
		List<ErpUser> lstRs = query.list();
		return (lstRs != null && lstRs.size() > 0) ? lstRs.get(0) : null;
	}

	@Override
	public List<CarGoUser> getCarGoUsers(String username) throws Exception {
		Session session = getSession();
		HashMap<String, Object> mapParams = newMapParameters();
		String sql = " select md.*,ap.user_name as username from member_device md ";
		sql += " inner join autho_profile ap on ap.member_id = md.member_id ";
		sql += " where  ap.user_name = :user_name  order by create_date desc ";
		Query query = session.createNativeQuery(sql, CarGoUser.class);
		query.setParameter("user_name", username);
		query = setParametersByMap(query, mapParams);
		List<CarGoUser> lstRs = query.list();
		return (lstRs != null && lstRs.size() > 0) ? lstRs : new ArrayList<>();
	}

	@Override
	public CarGoUser getCarGoUsers(Long id) throws Exception {
		Session session = getSession();
		HashMap<String, Object> mapParams = newMapParameters();
		String sql = " select md.*,ap.user_name as username from member_device md ";
		sql += " inner join autho_profile ap on ap.member_id = md.member_id ";
		sql += " where  md.member_id = :member_id  order by create_date desc ";
		Query query = session.createNativeQuery(sql, CarGoUser.class);
		query.setParameter("member_id", id);
		query = setParametersByMap(query, mapParams);
		List<CarGoUser> lstRs = query.list();
		return (lstRs != null && lstRs.size() > 0) ? lstRs.get(0) : null;
	}

}
