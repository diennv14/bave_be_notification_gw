package com.bave.backend.dao;

import com.bave.backend.generic.dao.GenericDao;
import com.bave.backend.persistence.entity.ErpPartner;

public interface ErpPartnerDao extends GenericDao<ErpPartner, Integer> {

}
