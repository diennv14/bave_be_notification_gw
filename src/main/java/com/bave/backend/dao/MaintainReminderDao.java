package com.bave.backend.dao;

import java.util.List;

import com.bave.backend.generic.configuration.Condition;
import com.bave.backend.generic.dao.GenericDao;
import com.bave.backend.persistence.entity.CarserviceMaintainReminder;

public interface MaintainReminderDao extends GenericDao<CarserviceMaintainReminder, Integer> {
	
	public List<CarserviceMaintainReminder> getListReminder(Condition cond) throws Exception;

}
