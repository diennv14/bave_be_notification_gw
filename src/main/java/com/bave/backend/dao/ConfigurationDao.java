package com.bave.backend.dao;

import java.util.List;

// Generated 12/06/2017 by HaiTX Computer

import com.bave.backend.generic.dao.GenericDao;
import com.bave.backend.persistence.entity.Configuration;

public interface ConfigurationDao extends GenericDao<Configuration, Long> {
	public Configuration getConfig(String type, String code) throws Exception;

	public List<Configuration> getConfig(String type) throws Exception;

	public void resetCacheAllEntries();
}
