package com.bave.backend.dao;

// Generated 12/06/2017 by HaiTX Computer

import com.bave.backend.generic.dao.GenericDao;
import com.bave.backend.persistence.entity.NotificationConfig;

public interface NotificationConfigDao extends GenericDao<NotificationConfig, Long> {
}
