package com.bave.backend.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;

import com.bave.backend.generic.dao.GenericDao;
import com.bave.backend.persistence.entity.PromotionCondition;

@Configurable
public interface PromotionConditionDao extends GenericDao<PromotionCondition, Integer> {

	public void deleteProCondByCondition(Integer pmNewId);

	public List<Integer> getLstMemberApplyPromotion(List<PromotionCondition> lstProCond, String garas);

	public List<String> getGarasOfPromotion(Integer pmNewId);
}