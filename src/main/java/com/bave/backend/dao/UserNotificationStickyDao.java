package com.bave.backend.dao;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.bave.backend.generic.configuration.Condition;

// Generated 12/06/2017 by HaiTX Computer

import com.bave.backend.generic.dao.GenericDao;
import com.bave.backend.persistence.entity.PromotionNews;
import com.bave.backend.persistence.entity.UserNotificationSticky;

public interface UserNotificationStickyDao extends GenericDao<UserNotificationSticky, Long> {
	public boolean exeSql(String sql) throws Exception;

	public Set<String> getAllUserOnlineOfGara(String domain) throws Exception;

	public List<String> getPlayerIdsByPhone(String phone) throws Exception;

	public Long getMemberIdByPhone(String phone) throws Exception;

	// public List<UserNotificationSticky>
	// getLstUserNotificationSticky(Condition condition);
	public List<UserNotificationSticky> findByCondition(Condition condition) throws Exception;

	public List<String> getTokenAddressByMemberId(Long memberId) throws Exception;

	public List<PromotionNews> getLstActivePromotionNews() throws Exception;

	public boolean checkExistNotification(int memberId, Date reminderDate, int dateDiff, int reminderId, String domain);
}
