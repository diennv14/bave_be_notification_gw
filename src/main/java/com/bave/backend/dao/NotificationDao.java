package com.bave.backend.dao;

import java.util.List;

import com.bave.backend.generic.configuration.Condition;

// Generated 12/06/2017 by HaiTX Computer

import com.bave.backend.generic.dao.GenericDao;
import com.bave.backend.persistence.entity.Notification;

public interface NotificationDao extends GenericDao<Notification, Long> {
	public List<Notification> getNEAllByParams(Condition condition) throws Exception;

	public int updateSeen(int id) throws Exception;

	public Integer getPlayerIdAmountByPhones(List<String> phones) throws Exception;

	public boolean exeSql(String sql) throws Exception;
}
