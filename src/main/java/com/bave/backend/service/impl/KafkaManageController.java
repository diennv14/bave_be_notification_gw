package com.bave.backend.service.impl;

import java.util.Iterator;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bave.backend.util.StringUtil;

@RestController
@RequestMapping("/kafka")
public class KafkaManageController {

	@Autowired
	private KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry;

	@RequestMapping("/stop")
	public void stop(@RequestParam(name = "id", required = false) String id) {
		if (StringUtil.isEmpty(id)) {
			Set<String> ids = kafkaListenerEndpointRegistry.getListenerContainerIds();
			Iterator<String> iterator = ids.iterator();
			while (iterator.hasNext()) {
				String iid = iterator.next();
				MessageListenerContainer listenerContainer = kafkaListenerEndpointRegistry.getListenerContainer(iid);
				listenerContainer.stop();
			}
		} else {
			MessageListenerContainer listenerContainer = kafkaListenerEndpointRegistry.getListenerContainer(id);
			listenerContainer.stop();
		}
	}

	@RequestMapping("/start")
	public void start(@RequestParam(name = "id", required = false) String id) {
		if (StringUtil.isEmpty(id)) {
			Set<String> ids = kafkaListenerEndpointRegistry.getListenerContainerIds();
			Iterator<String> iterator = ids.iterator();
			while (iterator.hasNext()) {
				String iid = iterator.next();
				MessageListenerContainer listenerContainer = kafkaListenerEndpointRegistry.getListenerContainer(iid);
				listenerContainer.start();
			}
		} else {
			MessageListenerContainer listenerContainer = kafkaListenerEndpointRegistry.getListenerContainer(id);
			listenerContainer.stop();
		}
	}
}