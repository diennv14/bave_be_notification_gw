//package com.bave.backend.service.impl;
//
//import java.io.Serializable;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Properties;
//import java.util.UUID;
//
//import javax.annotation.PostConstruct;
//
//import org.apache.kafka.clients.producer.Callback;
//import org.apache.kafka.clients.producer.KafkaProducer;
//import org.apache.kafka.clients.producer.Producer;
//import org.apache.kafka.clients.producer.ProducerRecord;
//import org.apache.kafka.clients.producer.RecordMetadata;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//// Generated 12/06/2017 by HaiTX Computer
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.bave.backend.api.object.NotificationEntity;
//import com.bave.backend.dao.ConfigurationDao;
//import com.bave.backend.dao.NotificationDao;
//import com.bave.backend.generic.configuration.Condition;
//import com.bave.backend.persistence.entity.CarGoUser;
//import com.bave.backend.persistence.entity.Configuration;
//import com.bave.backend.persistence.entity.ErpUser;
//import com.bave.backend.persistence.entity.Notification;
//import com.bave.backend.persistence.entity.NotificationConfig;
//import com.bave.backend.persistence.entity.NotificationTemplate;
//import com.bave.backend.security.config.VarStatic;
//import com.bave.backend.util.BEUtils;
//import com.bave.backend.util.DateUtil;
//import com.bave.backend.util.EHCacheManager;
//import com.bave.backend.util.Response;
//import com.bave.be.entity.serialz.NotificationSerializer;
//
//@Service
//@RestController
//@RequestMapping("/notification")
//@Transactional
//public class NotificationServiceImpl2 implements Serializable {
//	private final Logger logger = LoggerFactory.getLogger("api_file");
//
//	private static final long serialVersionUID = 1L;
//
//	@Autowired
//	private NotificationDao notificationDao;
//
//	@Autowired
//	private ConfigurationDao configurationDao;
//
//	private Producer<String, Notification> producer;
//	private Properties producerPropConfig;
//
//	@Autowired
//	private CommonService commonService;
//
//	@PostConstruct
//	public void init() {
//		producerPropConfig = createProducerConfig(VarStatic.kafka_bootstrap_server);
//	}
//
//	public List<NotificationEntity> getNEAllByParams(Condition condition) throws Exception {
//		List<Notification> lstRs = notificationDao.getNEAllByParams(condition);
//		List<Configuration> lstConfg = configurationDao.getConfig("NOTIF_ACTION_NAME");
//		return BEUtils.toLstNotificationEntity(lstRs, lstConfg);
//	}
//
//	public List<NotificationEntity> getNEPostOneSignal(Condition condition) throws Exception {
//		condition.add("onesignalStatus", VarStatic.NOTI_OSN_STATUS);
//		List<Notification> lstRs = notificationDao.findAllByParams(condition);
//		List<Configuration> lstConfg = configurationDao.getConfig("NOTIF_ACTION_NAME");
//		return BEUtils.toLstNotificationEntity(lstRs, lstConfg);
//	}
//
//	public boolean updateSeen(String channel, int id) throws Exception {
//		int rs = notificationDao.updateSeen(id);
//		return (rs > 0) ? true : false;
//	}
//
//	@RequestMapping(value = "/reload-cache", method = RequestMethod.GET)
//	public Response reloadCache() {
//		EHCacheManager.initCache(commonService, null);
//		return new Response("init cache done!", Response.STATUS_SUCCESS);
//	}
//
//	@RequestMapping(value = "/list", method = RequestMethod.GET)
//	public Response list(@RequestParam(value = "member_id", required = false) String memberId,
//			@RequestParam(value = "order_columns", required = false) String orderByColumns,
//			@RequestParam(value = "asc_order", defaultValue = "true", required = false) Boolean ascOrder,
//			@RequestParam(value = "first_index", required = false) Integer firstItemIndex,
//			@RequestParam(value = "max_items", required = false) Integer maxItems,
//			@RequestParam(value = "exact_match", defaultValue = "true", required = false) Boolean exactMatch) {
//		try {
//			Condition extCondition = new Condition(orderByColumns, ascOrder, firstItemIndex, maxItems, exactMatch);
//			extCondition.add("memberId", memberId);
//			extCondition.add("action", Arrays.asList("quotation_response"));
//			List<Notification> lstNitifi = notificationDao.getNEAllByParams(extCondition);
//			List<Configuration> lstConfg = configurationDao.getConfig("NOTIF_ACTION_NAME");
//			List<NotificationEntity> lstRs = BEUtils.toLstNotificationEntity(lstNitifi, lstConfg);
//			return new Response(lstRs);
//		} catch (Exception e) {
//			return new Response(e);
//		}
//	}
//
//	@RequestMapping(value = "/erp/add-list", method = RequestMethod.POST)
//	public Response erpAddListNotification(@RequestBody List<Notification> listNotification) {
//		try {
//			Map<String, String> mapSequence = new HashMap<>();
//			if (listNotification != null) {
//				for (Notification notification : listNotification) {
//					notification.setApp(VarStatic.NOTI_APP_AGARA);
//					notification.setType(VarStatic.NOTI_TYPE_NOTICE);
//					mapSequence = erpAddListNotificationToMap(mapSequence, notification);
//				}
//			}
//			return new Response(mapSequence);
//		} catch (Exception e) {
//			return new Response(e);
//		}
//	}
//
//	public Map<String, String> erpAddListNotificationToMap(Map<String, String> mapSequence, Notification notification) {
//		if (mapSequence == null) {
//			mapSequence = new HashMap<String, String>();
//		}
//		mapSequence.put(notification.getId().toString(), getSequence());
//		notification.setSequence(getSequence());
//		notification.setStatus(VarStatic.NOTI_STATUS_DRAFT);
//		notification.setCreateDate(DateUtil.now());
//		notification.setNotificationId(notification.getId());
//		sendMessage(notification);
//		return mapSequence;
//	}
//
//	@RequestMapping(value = "/erp/sms/add-list", method = RequestMethod.POST)
//	public Response erpAddListSMSNotification(@RequestBody List<Notification> listNotification) {
//		try {
//			Map<String, String> mapSequence = new HashMap<>();
//			if (listNotification != null) {
//				for (Notification notification : listNotification) {
//					String sequence = getSequence();
//					notification.setSequence(sequence);
//					notification.setAddress(notification.getPhone());
//					notification.setStatus(VarStatic.NOTI_STATUS_DRAFT);
//					notification.setDestinationObject(VarStatic.DESTINATION_OBJECT_CAROWNER);
//					notification.setCreateDate(DateUtil.now());
//					notification.setNotificationId(notification.getId());
//					notification.setApp(VarStatic.NOTI_APP_AGARA);
//					notification.setStatus(VarStatic.NOTI_STATUS_DRAFT);
//					if ("sms".equals(notification.getType())) {
//						notification.setChannel(VarStatic.CHANNEL_SMS);
//						notification.setType(VarStatic.NOTI_TYPE_MARKETING);
//					} else if ("one_signal".equals(notification.getType())
//							|| "onesignal".equals(notification.getType())) {
//						notification.setChannel(VarStatic.CHANNEL_ONESIGNAL);
//						notification.setType(VarStatic.NOTI_TYPE_MARKETING);
//
//						List<CarGoUser> cargoUser = getCarGoUsersCache(notification.getPhone());
//						if (cargoUser != null && cargoUser.size() > 0) {
//							List<String> lstAddress = new ArrayList<>();
//							for (CarGoUser u : cargoUser) {
//								if (!lstAddress.contains(u.getPlayerId())) {
//									lstAddress.add(u.getPlayerId());
//								}
//							}
//							notification.setLstAddress(lstAddress);
//							notification.setDestinationRefId(cargoUser.get(0).getMemberId());
//							notification.setDestinationObject(VarStatic.DESTINATION_OBJECT_CAROWNER);
//							notification.setChannel(VarStatic.CHANNEL_ONESIGNAL);
//							mapSequence = erpAddListNotificationToMap(mapSequence, notification);
//							continue;
//						} else {
//							logger.info("member " + notification.getPhone() + " offline");
//						}
//					}
//
//					if (notification.getLstAddress() != null && notification.getLstAddress().size() > 0) {
//						for (String phone : notification.getLstAddress()) {
//							if (notification.getId() != null) {
//								mapSequence.put(notification.getId().toString(), sequence);
//							} else {
//								mapSequence.put(phone, sequence);
//							}
//							notification.setAddress(phone);
//							sendMessage(notification);
//						}
//					} else {
//						if (notification.getId() != null) {
//							mapSequence.put(notification.getId().toString(), sequence);
//						}
//						notification.setAddress(notification.getPhone());
//						sendMessage(notification);
//					}
//				}
//			}
//			return new Response(mapSequence);
//		} catch (Exception e) {
//			return new Response(e);
//		}
//	}
//
//	private Properties createProducerConfig(String brokers) {
//		Properties props = new Properties();
//		props.put("bootstrap.servers", brokers);
//		props.put("acks", "all");
//		props.put("retries", 0);
//		props.put("batch.size", 16384);
//		props.put("linger.ms", 1);
//		props.put("buffer.memory", 33554432);
//		props.put("key.serializer", NotificationSerializer.class.getName());
//		props.put("value.serializer", NotificationSerializer.class.getName());
//		return props;
//	}
//
//	private void sendMessage(Notification notification) {
//		try {
//			producer = new KafkaProducer<String, Notification>(producerPropConfig);
//			producer.send(new ProducerRecord<String, Notification>(VarStatic.kafka_source_topic, notification),
//					new Callback() {
//						public void onCompletion(RecordMetadata metadata, Exception e) {
//							if (e != null) {
//								e.printStackTrace();
//							}
//						}
//					});
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			producer.flush();
//			producer.close();
//		}
//	}
//
//	private String getSequence() {
//		return UUID.randomUUID().toString() + "-" + System.currentTimeMillis();
//	}
//
//	private List<CarGoUser> getCarGoUsersCache(String username) throws Exception {
//		List<CarGoUser> cargoUser = EHCacheManager.getCarGoUsersCache(username);
//		if (cargoUser != null) {
//			return cargoUser;
//		} else {
//			return commonService.getCarGoUsers(username);
//		}
//	}
//}
