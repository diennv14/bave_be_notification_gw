package com.bave.backend.service.impl;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Generated 12/06/2017 by HaiTX Computer

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bave.backend.api.object.NotificationEntity;
import com.bave.backend.dao.ConfigurationDao;
import com.bave.backend.dao.NotificationDao;
import com.bave.backend.dao.UserNotificationStickyDao;
import com.bave.backend.generic.configuration.Condition;
import com.bave.backend.persistence.entity.CarGoUser;
import com.bave.backend.persistence.entity.Configuration;
import com.bave.backend.persistence.entity.Notification;
import com.bave.backend.persistence.entity.UserNotificationSticky;
import com.bave.backend.security.config.VarStatic;
import com.bave.backend.util.BEUtils;
import com.bave.backend.util.EHCacheManager;
import com.bave.backend.util.GsonUtil;
import com.bave.backend.util.Response;
import com.bave.be.entity.serialz.UserNotificationStickySerializer;
import com.google.gson.Gson;

@Service
@RestController
@RequestMapping("/notification")
@Transactional
public class NotificationServiceImpl implements Serializable {
	private final Logger logger = LoggerFactory.getLogger("api_file");

	private static final long serialVersionUID = 1L;

	@Autowired
	private NotificationDao notificationDao;

	@Autowired
	private UserNotificationStickyDao userNotificationStickyDao;

	@Autowired
	private ConfigurationDao configurationDao;

	private Producer<String, UserNotificationSticky> producer;
	private Properties producerPropConfig;

	@Autowired
	private CommonService commonService;
	
	@Value("${kafka.bootstrap.server}")
	String kafka_bootstrap_server;

	@PostConstruct
	public void init() {
		producerPropConfig = createProducerConfig(kafka_bootstrap_server);
	}

	public List<NotificationEntity> getNEAllByParams(Condition condition) throws Exception {
		List<Notification> lstRs = notificationDao.getNEAllByParams(condition);
		List<Configuration> lstConfg = configurationDao.getConfig("NOTIF_ACTION_NAME");
		return BEUtils.toLstNotificationEntity(lstRs, lstConfg);
	}

	public List<NotificationEntity> getNEPostOneSignal(Condition condition) throws Exception {
		condition.add("onesignalStatus", VarStatic.NOTI_OSN_STATUS);
		List<Notification> lstRs = notificationDao.findAllByParams(condition);
		List<Configuration> lstConfg = configurationDao.getConfig("NOTIF_ACTION_NAME");
		return BEUtils.toLstNotificationEntity(lstRs, lstConfg);
	}

	public boolean updateSeen(String channel, int id) throws Exception {
		int rs = notificationDao.updateSeen(id);
		return (rs > 0) ? true : false;
	}

	@RequestMapping(value = "/reload-cache", method = RequestMethod.GET)
	public Response reloadCache() {
		EHCacheManager.initCache(commonService, null);
		return new Response("init cache done!", Response.STATUS_SUCCESS);
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public Response list(@RequestParam(value = "member_id", required = false) String memberId,
			@RequestParam(value = "order_columns", required = false) String orderByColumns,
			@RequestParam(value = "asc_order", defaultValue = "true", required = false) Boolean ascOrder,
			@RequestParam(value = "first_index", required = false) Integer firstItemIndex,
			@RequestParam(value = "max_items", required = false) Integer maxItems,
			@RequestParam(value = "exact_match", defaultValue = "true", required = false) Boolean exactMatch) {
		try {
			Condition extCondition = new Condition(orderByColumns, ascOrder, firstItemIndex, maxItems, exactMatch);
			extCondition.add("memberId", memberId);
			extCondition.add("action", Arrays.asList("quotation_response"));
			List<Notification> lstNitifi = notificationDao.getNEAllByParams(extCondition);
			List<Configuration> lstConfg = configurationDao.getConfig("NOTIF_ACTION_NAME");
			List<NotificationEntity> lstRs = BEUtils.toLstNotificationEntity(lstNitifi, lstConfg);
			return new Response(lstRs);
		} catch (Exception e) {
			return new Response(e);
		}
	}

	// @RequestMapping(value = "/erp/add-list", method = RequestMethod.POST)
	// public Response erpAddListNotification(@RequestBody List<Notification>
	// listNotification) {
	// try {
	// List<Notification> listNotificationGara = new ArrayList<>();
	// List<Notification> listNotificationCarOwner = new ArrayList<>();
	// Map<String, String> mapSequence = new HashMap<>();
	// if (listNotification != null) {
	// for (Notification notification : listNotification) {
	// if (notification.getListUser() != null &&
	// notification.getListUser().size() > 0) {
	// notification.setType(VarStatic.NOTI_TYPE_NOTICE);
	// listNotificationGara.add(notification);
	// }
	// if (notification.getListPartner() != null &&
	// notification.getListPartner().size() > 0) {
	// listNotificationCarOwner.add(notification);
	// }
	//
	// }
	// for (Notification item : listNotificationGara) {
	// mapSequence = toGara(mapSequence, item);
	// }
	// mapSequence = toCarOwner(mapSequence, listNotificationCarOwner);
	// }
	// return new Response(mapSequence);
	// } catch (Exception e) {
	// return new Response(e);
	// }
	// }

	// @RequestMapping(value = "/erp/remind-maintain", method =
	// RequestMethod.POST)
	// public Response erpRemindMaintain(@RequestBody List<Notification>
	// listNotification) {
	// try {
	// List<Notification> listNotificationCarOwner = new ArrayList<>();
	// Map<String, String> mapSequence = new HashMap<>();
	// if (listNotification != null) {
	// for (Notification notification : listNotification) {
	//
	// if (notification.getListPartner() != null &&
	// notification.getListPartner().size() > 0) {
	// listNotificationCarOwner.add(notification);
	// }
	// }
	// mapSequence = toCarOwner(mapSequence, listNotificationCarOwner);
	// }
	// return new Response(mapSequence);
	// } catch (Exception e) {
	// return new Response(e);
	// }
	// }

	// @RequestMapping(value = "/erp/sms/add-list", method = RequestMethod.POST)
	// public Response erpAddListSMSNotificationAPI(@RequestBody
	// List<Notification> listNotification) {
	// try {
	// Map<String, String> mapSequence = erpAddListSMSNotification(null,
	// listNotification);
	// return new Response(mapSequence);
	// } catch (Exception e) {
	// return new Response(e);
	// }
	// }

	// public Map<String, String> toGara(Map<String, String> mapSequence,
	// Notification notification) {
	// if (mapSequence == null) {
	// mapSequence = new HashMap<String, String>();
	// }
	// mapSequence.put(notification.getId().toString(), getSequence());
	// notification.setSequence(getSequence());
	// notification.setStatus(VarStatic.NOTI_STATUS_DRAFT);
	// notification.setApp(VarStatic.NOTI_APP_AGARA);
	// notification.setCreateDate(DateUtil.now());
	// notification.setNotificationId(notification.getId());
	// sendMessageToSourceTopic(notification);
	// return mapSequence;
	// }

	// private Map<String, String> toCarOwner(Map<String, String> mapSequence,
	// List<Notification> listNotification)
	// throws Exception {
	// List<Notification> listNotificationCarOwner = new ArrayList<>();
	// Notification noti = null;
	// for (Notification item : listNotification) {
	// for (User u : item.getListPartner()) {
	// noti = new Notification();
	// BeanUtils.copyProperties(noti, item);
	// noti.setApp(VarStatic.NOTI_APP_BAVE);
	// noti.setLstAddress(u.getListAddress());
	// noti.setCallbackUrl(u.getCallbackUrl());
	// noti.setType(u.getChannel()); // do /sms/add-list dang truyen
	// // sai type
	// noti.setMemberId(u.getMemberId());
	// noti.setDestinationRefId(u.getMemberId());
	// if (u.getListAddress() != null && u.getListAddress().size() > 0) {
	// if (!u.getListAddress().get(0).matches(".*[a-zA-Z]+.*")) {
	// noti.setPhone(u.getListAddress().get(0));
	// }
	// }
	// noti.setListPartner(null);
	// listNotificationCarOwner.add(noti);
	// }
	// }
	// mapSequence = erpAddListSMSNotification(mapSequence,
	// listNotificationCarOwner);
	// return mapSequence;
	// }

	// private Map<String, String> erpAddListSMSNotification(Map<String, String>
	// mapSequence,
	// @RequestBody List<Notification> listNotification) throws Exception {
	// if (mapSequence == null) {
	// mapSequence = new HashMap<String, String>();
	// }
	// if (listNotification != null) {
	// for (Notification notification : listNotification) {
	// String sequence = getSequence();
	// notification.setSequence(sequence);
	// notification.setAddress(notification.getPhone());
	// notification.setStatus(VarStatic.NOTI_STATUS_DRAFT);
	// notification.setDestinationObject(VarStatic.DESTINATION_OBJECT_CAROWNER);
	// notification.setCreateDate(DateUtil.now());
	// notification.setNotificationId(notification.getId());
	// if ("sms".equals(notification.getType())) {
	// notification.setChannel(VarStatic.CHANNEL_SMS);
	// notification.setType(VarStatic.NOTI_TYPE_MARKETING);
	// } else if ("one_signal".equals(notification.getType()) ||
	// "app".equals(notification.getType())) {
	// notification.setChannel(VarStatic.CHANNEL_ONESIGNAL);
	// notification.setType(VarStatic.NOTI_TYPE_MARKETING);
	//
	// List<CarGoUser> cargoUser = getCarGoUsersCache(notification.getPhone());
	// if (cargoUser != null && cargoUser.size() > 0) {
	// List<String> lstAddress = new ArrayList<>();
	// for (CarGoUser u : cargoUser) {
	// if (!lstAddress.contains(u.getPlayerId())) {
	// lstAddress.add(u.getPlayerId());
	// }
	// }
	// notification.setLstAddress(lstAddress);
	// notification.setDestinationRefId(cargoUser.get(0).getMemberId());
	// notification.setChannel(VarStatic.CHANNEL_ONESIGNAL);
	// // mapSequence = toCarOwner(mapSequence, notification);
	// if (notification.getId() != null) {
	// mapSequence.put(notification.getId().toString(), sequence);
	// }
	// sendMessageToSourceTopic(notification);
	// continue;
	// } else {
	// logger.info("member " + notification.getPhone() + " offline");
	// }
	// }
	//
	// if (notification.getLstAddress() != null &&
	// notification.getLstAddress().size() > 0) {
	// for (String phone : notification.getLstAddress()) {
	// if (notification.getId() != null) {
	// mapSequence.put(notification.getId().toString(), sequence);
	// } else {
	// mapSequence.put(phone, sequence);
	// }
	// notification.setAddress(phone);
	// sendMessageToSourceTopic(notification);
	// }
	// } else {
	// if (notification.getId() != null) {
	// mapSequence.put(notification.getId().toString(), sequence);
	// }
	// notification.setAddress(notification.getPhone());
	// sendMessageToSourceTopic(notification);
	// }
	// }
	// }
	// return mapSequence;
	// }

	public void test() {
		System.out.print("called api");
	}

	private Properties createProducerConfig(String brokers) {
		Properties props = new Properties();
		props.put("bootstrap.servers", brokers);
		props.put("acks", "all");
		props.put("retries", 0);
		props.put("batch.size", 16384);
		props.put("linger.ms", 1);
		props.put("buffer.memory", 33554432);
		props.put("key.serializer", UserNotificationStickySerializer.class.getName());
		props.put("value.serializer", UserNotificationStickySerializer.class.getName());
		return props;
	}

	private void sendMessageToSourceTopic(UserNotificationSticky notification) {
		try {
			logger.info("Notification Address ");
			producer = new KafkaProducer<String, UserNotificationSticky>(producerPropConfig);
			producer.send(
					new ProducerRecord<String, UserNotificationSticky>(VarStatic.kafka_source_topic, notification),
					new Callback() {
						public void onCompletion(RecordMetadata metadata, Exception e) {
							logger.info("producerPropConfig {}", GsonUtil.getGson().toJson(producerPropConfig));
							if (e != null) {
								logger.error("sendMessageToSourceTopic Kafka returned {}", notification.getMemberId(), e);
							} else {
								logger.info("sendMessageToSourceTopic SUCCESS: {}", notification.getAddress());
							}
						}
					});
		} catch (Exception e) {
			logger.error("Loi sendMessageToSourceTopic: {}", notification.getMemberId(), e);
		} finally {
			producer.flush();
			producer.close();
		}
	}

	private String getSequence() {
		return UUID.randomUUID().toString() + "-" + System.currentTimeMillis();
	}

	private List<CarGoUser> getCarGoUsersCache(String username) throws Exception {
		if (username != null) {
			List<CarGoUser> cargoUser = EHCacheManager.getCarGoUsersCache(username);
			if (cargoUser != null) {
				return cargoUser;
			} else {
				return commonService.getCarGoUsers(username);
			}
		}
		return null;
	}

	// @RequestMapping(value = "/cargo/add-list", method = RequestMethod.POST)
	// public Response cargoAddListNotification(@RequestBody List<Notification>
	// listNotification) {
	// try {
	// List<Notification> listNotificationCarOwner = new ArrayList<>();
	// Map<String, String> mapSequence = new HashMap<>();
	// if (listNotification != null) {
	// for (Notification notification : listNotification) {
	//
	// if (notification.getListPartner() != null &&
	// notification.getListPartner().size() > 0) {
	// listNotificationCarOwner.add(notification);
	// }
	// }
	// mapSequence = toCarOwner(mapSequence, listNotificationCarOwner);
	// }
	//
	// return new Response(mapSequence);
	// } catch (Exception e) {
	// return new Response(e);
	// }
	// }

	@RequestMapping(value = "/cargo/add-list", method = RequestMethod.POST)
	public Response cargoAddListUserNotificationSticky(@RequestBody List<UserNotificationSticky> listNotification) {
		try {
			Map<String, String> mapSequence = new HashMap<>();

			if (listNotification != null && listNotification.size() > 0) {
				for (UserNotificationSticky userNotification : listNotification) {
					// if ("one_signal".equals(userNotification.getType()) ||
					// "app".equals(userNotification.getType()))
					// {
					List<String> lstAddress = userNotificationStickyDao
							.getTokenAddressByMemberId(userNotification.getMemberId());
					userNotification.setLstAddress(lstAddress);
					userNotification.setCreateDate(new Date());
					userNotification.setNotifyCode(ramdom().toString());
					if (userNotification.getDataRefId() != null && "false".equals(userNotification.getDataRefId())) {
						userNotification.setDataRefId(null);
					}

					if ("promotion".equals(userNotification.getAction())) {
						userNotification.setSeen(false);
						userNotification.setDestinationObject("member");
						userNotification.setDestinationRefId(userNotification.getMemberId());
					}
					mapSequence.put(userNotification.getMemberId().toString(), getSequence());
					sendMessageToSourceTopic(userNotification);
					// }
				}
			}

			return new Response(mapSequence);
		} catch (Exception e) {
			logger.error("cargoAddListUserNotificationSticky", e);
			return new Response(e);
		}
	}

	@RequestMapping(value = "/erp/add-list", method = RequestMethod.POST)
	public Response erpAddListUserNotificationSticky(@RequestBody List<UserNotificationSticky> listNotification) {
		try {
			Map<String, String> mapSequence = new HashMap<>();

			if (listNotification != null && listNotification.size() > 0) {
				for (UserNotificationSticky userNotification : listNotification) {
					// if ("one_signal".equals(userNotification.getType()) ||
					// "app".equals(userNotification.getType()))
					// {
					List<String> lstAddress = userNotificationStickyDao
							.getTokenAddressByMemberId(userNotification.getMemberId());
					userNotification.setLstAddress(lstAddress);
					userNotification.setCreateDate(new Date());
					userNotification.setNotifyCode(ramdom().toString());
					if (userNotification.getDataRefId() != null && "false".equals(userNotification.getDataRefId())) {
						userNotification.setDataRefId(null);
					}
					// if
					// ("reminder_detail".equals(userNotification.getAction())
					// || "promotion".equals(userNotification.getAction())
					// || "booking_status".equals(userNotification.getAction()))
					// {
					// userNotification.setSeen(false);
					// userNotification.setDestinationObject("member");
					// userNotification.setDestinationRefId(userNotification.getMemberId());
					// }
					userNotification.setSeen(false);
					if (userNotification.getMemberId() != null) {
						mapSequence.put(userNotification.getMemberId().toString(), getSequence());
					}
					sendMessageToSourceTopic(userNotification);
					// }
				}
			}

			return new Response(mapSequence);
		} catch (Exception e) {
			logger.error("erpAddListUserNotificationSticky", e);
			return new Response(e);
		}
	}

	@RequestMapping(value = "/cargo/test-api", method = RequestMethod.GET)
	public Response testApi() {
		try {

			return new Response("test api by Tung!");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new Response("test api by Tung!");
	}

	public Integer ramdom() {
		Random r = new Random(System.currentTimeMillis());
		return 10000 + r.nextInt(20000);
	}
	
}
