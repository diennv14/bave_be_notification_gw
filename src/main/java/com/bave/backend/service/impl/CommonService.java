package com.bave.backend.service.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

// Generated 12/06/2017 by HaiTX Computer

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bave.backend.dao.NotificationConfigDao;
import com.bave.backend.dao.NotificationTemplateDao;
import com.bave.backend.dao.ServiceProviderDao;
import com.bave.backend.dao.UserDao;
import com.bave.backend.persistence.entity.CarGoUser;
import com.bave.backend.persistence.entity.ErpUser;
import com.bave.backend.persistence.entity.NotificationConfig;
import com.bave.backend.persistence.entity.NotificationTemplate;
import com.bave.backend.persistence.entity.ServiceProvider;

@Service
@Transactional
public class CommonService implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private UserDao userDao;

	@Autowired
	private ServiceProviderDao serviceProviderDao;

	@Autowired
	private NotificationConfigDao notificationConfigDao;

	@Autowired
	private NotificationTemplateDao notificationTemplateDao;

	public List<NotificationConfig> getAllNotificationConfig() throws Exception {
		return notificationConfigDao.findAll();
	}

	public List<NotificationTemplate> getAllNotificationTemplate() throws Exception {
		return notificationTemplateDao.findAll();
	}

	public List<ErpUser> getAllERPUsers(Date fromDate) throws Exception {
		return userDao.getAllERPUsers(fromDate);
	}

	public List<CarGoUser> getAllCarGoUsers(Date fromDate) throws Exception {
		return userDao.getAllCarGoUsers(fromDate);
	}

	public ErpUser getERPUsers(String username) throws Exception {
		return userDao.getERPUsers(username);
	}

	public List<CarGoUser> getCarGoUsers(String username) throws Exception {
		return userDao.getCarGoUsers(username);
	}

	public CarGoUser getCarGoUsers(Long id) throws Exception {
		return userDao.getCarGoUsers(id);
	}

	public List<ServiceProvider> getAllServiceProvider(Date fromDate) throws Exception {
		return serviceProviderDao.getAllServiceProvider(fromDate);
	}

	public ServiceProvider getServiceProvider(String code) throws Exception {
		return serviceProviderDao.getServiceProvider(code);
	}

}
