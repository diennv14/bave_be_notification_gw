package com.bave.backend.service;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.springframework.stereotype.Service;

import com.bave.backend.persistence.entity.AuthenParam;
import com.bave.backend.security.config.MessageResource;
import com.bave.backend.util.FieldsInfo;
import com.bave.backend.util.RpcXmlConfig;
import com.bave.backend.util.RpcXmlUtil;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class CommonERPService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public XmlRpcClient client = new XmlRpcClient() {
		@Override
		public Object execute(String pMethodName, Object[] pParams) throws XmlRpcException {
			// pParams[1] = getUserInParams(pParams).getId();
			if ("search_read".equals(pParams[4])) {
				if (pParams.length >= 7 && pParams[6] != null) {
					LinkedHashMap<String, Object> map = new LinkedHashMap<>();
					map.putAll((Map<String, Object>) pParams[6]);
					Map<String, String> mapContext = buildMapContext(null);
					if (mapContext.size() > 0) {
						map.put("context", mapContext);
						pParams[6] = map;
					}
				} else if (pParams.length == 6) {
					LinkedHashMap<String, Object> map = new LinkedHashMap<>();
					Map<String, String> mapContext = buildMapContext(null);
					if (mapContext.size() > 0) {
						map.put("context", mapContext);
						pParams = Arrays.copyOf(pParams, pParams.length + 1);
						pParams[6] = map;
					}
				}
			}
			try {
				Object[] pParamsTemp = pParams.clone();
				pParamsTemp[2] = "***";
				String exec = new ObjectMapper().writeValueAsString(pParamsTemp);
				if (exec.indexOf("get_unsent_message") < 0
						&& exec.indexOf("\"fields\":[\"id\",\"image_medium\"]") < 0) {
					System.out.println(pMethodName + ": " + exec);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return execute(getClientConfig(), pMethodName, pParams);
			// return null;
		}
	};

	public XmlRpcClientConfigImpl clientConfigImpl = new XmlRpcClientConfigImpl();
	protected AuthenParam authenPar = null;
	private HttpServletRequest request;

	public AuthenParam initAuthenParam(HttpServletRequest request) throws Exception {
		this.request = request;
		if (request == null) {
			authenPar = new AuthenParam();
			authenPar.setDb(RpcXmlConfig.DB);
			authenPar.setUid(RpcXmlConfig.UID);
			authenPar.setPassword(RpcXmlConfig.PASSWORD);
			authenPar.setTz(RpcXmlConfig.TIME_ZONE);
			authenPar.setLang(RpcXmlConfig.LANG);
			return authenPar;
		}
		return null;
	}

	public List<Object> initClient(AuthenParam loginPar) throws Exception {
		return initClient(loginPar.getDb(), loginPar.getUid(), loginPar.getPassword(), loginPar.getTz(),
				loginPar.getLang(), null);
	}

	public List<Object> initClient(AuthenParam loginPar, String pathExt) throws Exception {
		return initClient(loginPar.getDb(), loginPar.getUid(), loginPar.getPassword(), loginPar.getTz(),
				loginPar.getLang(), pathExt);
	}

	public List<Object> initClient(String db, String uid, String password, String tz, String lang, String pathExt)
			throws Exception {
		try {
			List<Object> lstParams = null;
			String path = RpcXmlConfig.URL_PATH;
			if (pathExt != null) {
				path = pathExt;
			}
			if (path.indexOf(RpcXmlConfig.URL_PATH_LOGIN) < 0) {
				if (db == null) {
					throw MessageResource.exception("BE101001");
				}
				lstParams = initParams(db, uid, password, tz, lang, lstParams);
			}
			String url = String.format(path, String.format(RpcXmlConfig.URL, db));
			clientConfigImpl.setServerURL(new URL(url));
			client.setConfig(clientConfigImpl);
			return lstParams;
		} catch (

		MalformedURLException e)

		{
			e.printStackTrace();
			return new ArrayList<>();
		}

	}

	public List<Object> initClientLocal(String url, String db, String uid, String password, boolean login,
			List<Object> lstParams) {
		try {
			clientConfigImpl.setServerURL(new URL(url));
			client.setConfig(clientConfigImpl);
			return initParams(db, uid, password, null, null, lstParams);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	public XmlRpcClient getClient() {
		return client;
	}

	public Object executeToObject(Object[] arrParams) throws XmlRpcException {
		try {
			Object executeRs = client.execute("execute_kw", arrParams);
			return executeRs;
		} catch (XmlRpcException e) {
			e.printStackTrace();
			throw e;
		}
	}

	public <T> T executeToObject(Class objectType, Object[] arrParams) throws XmlRpcException {
		try {
			Object executeRs = client.execute("execute_kw", arrParams);
			return (T) executeRs;
		} catch (XmlRpcException e) {
			e.printStackTrace();
			throw e;
		}
	}

	public <T> T executeToEntity(Class entityType, Object[] arrParams) throws XmlRpcException {
		try {
			List<T> lstRs = executeToList(entityType, arrParams);
			if (lstRs != null && lstRs.size() > 0)
				return lstRs.get(0);
			return null;
		} catch (XmlRpcException e) {
			e.printStackTrace();
			throw e;
		}
	}

	public <T> List<T> executeToList(Class objectType, Object[] arrParams) throws XmlRpcException {
		try {
			List<T> lstRs = new ArrayList<T>();
			Object executeRs = client.execute("execute_kw", arrParams);
			List<Object> lstExecuteRs = exeRsToLstObject(executeRs);
			for (Object obj : lstExecuteRs) {
				HashMap<String, Object> map = rsItemToMap(obj);
				T eObj = (T) RpcXmlUtil.convertToObject(map, objectType);
				lstRs.add(eObj);
			}
			return lstRs;
		} catch (XmlRpcException e) {
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<T>();
	}

	public <T> T executeCount(Class objectType, Object[] arrParams) throws XmlRpcException {
		try {
			Object[] arrCountParams = new Object[6];
			arrCountParams[0] = arrParams[0];
			arrCountParams[1] = arrParams[1];
			arrCountParams[2] = arrParams[2];
			arrCountParams[3] = arrParams[3];
			arrCountParams[4] = "search_count";
			arrCountParams[5] = arrParams[5];
			T rs = (T) client.execute("execute_kw", arrCountParams);
			return rs;
		} catch (XmlRpcException e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<Object> executeToList(Object[] arrParams) throws XmlRpcException {
		try {
			Object executeRs = client.execute("execute_kw", arrParams);
			List<Object> lstExecuteRs = exeRsToLstObject(executeRs);
			return lstExecuteRs;
		} catch (XmlRpcException e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<Object> exeRsToLstObject(Object executeRs) throws XmlRpcException {
		List<Object> lstObject = Arrays.asList((Object[]) executeRs);
		return lstObject;
	}

	public List<Object> initParams(String db, String uid, String password, String tz, String lang,
			List<Object> lstParams) {
		if (lstParams != null) {
			lstParams.clear();
		} else {
			lstParams = new ArrayList<>();
		}
		lstParams.add(db); // DB name
		lstParams.add(Integer.parseInt(uid));
		lstParams.add(password); // DB pass
		return lstParams;
	}

	public Object nullValue(Object obj, Object replace) {
		if (obj == null)
			return replace;
		else
			return obj;
	}

	public void setParamIVN(Map<String, Object> mParams, String paramName, Object value) {
		if (value != null) {
			mParams.put(paramName, value.toString());
		}
	}

	public Map<String, String> buildMapContext(Map<String, String> mapContext) {
		if (mapContext == null)
			mapContext = new HashMap<>();
		String acceptLanguage = null;
		if (authenPar != null) {
			acceptLanguage = authenPar.getLang();
		}
		if (acceptLanguage != null)
			mapContext.put("lang", acceptLanguage);
		return mapContext;
	}

	public HashMap<String, Object> rsItemToMap(Object objItem) {
		if (objItem != null)
			return ((HashMap<String, Object>) objItem);
		else
			return new HashMap<String, Object>();
	}

	// @Cacheable(value = "modelFieldsInfoCache", key = "#model")
	public HashMap<String, FieldsInfo> getFieldsInfo(String model) {
		try {
			List<Object> lstParams = initClient(initAuthenParam(request));
			List<List<Object>> lstCond = new ArrayList<>();
			lstParams.add(model);
			lstParams.add("fields_get");
			lstParams.add(Arrays.asList(lstCond));
			lstParams.add(new HashMap<String, Object>() {
				{
				}
			});
			Object executeRs = client.execute("execute_kw", lstParams.toArray());
			HashMap<String, Object> mapExecuteRs = ((HashMap<String, Object>) executeRs);
			HashMap<String, FieldsInfo> mapRs = new HashMap<String, FieldsInfo>();
			FieldsInfo modelInfo = null;
			Map<String, String> childMap = null;
			String fields = "";
			for (String key : mapExecuteRs.keySet()) {
				childMap = (Map<String, String>) mapExecuteRs.get(key);
				modelInfo = new FieldsInfo(key, childMap.get("type"));
				mapRs.put(key, modelInfo);
			}
			return mapRs;
		} catch (Exception e) {
			e.printStackTrace();
			return new HashMap<String, FieldsInfo>();
		}
	}
}
