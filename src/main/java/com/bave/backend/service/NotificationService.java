//package com.bave.backend.service;
//
//import java.util.List;
//
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.bave.backend.api.object.notif.NotificationEntity;
//import com.bave.backend.generic.configuration.Condition;
//import com.bave.backend.persistence.entity.Notification;
//import com.bave.backend.util.Response;
//
//@Service
//@RestController
//@RequestMapping("/notification")
//@Transactional
//public interface NotificationService {
//
//	@RequestMapping(value = "/list", method = RequestMethod.GET)
//	public Response list(@RequestParam(value = "member_id", required = false) String memberId,
//			@RequestParam(value = "order_columns", required = false) String orderByColumns,
//			@RequestParam(value = "asc_order", defaultValue = "true", required = false) Boolean ascOrder,
//			@RequestParam(value = "first_index", required = false) Integer firstItemIndex,
//			@RequestParam(value = "max_items", required = false) Integer maxItems,
//			@RequestParam(value = "exact_match", defaultValue = "true", required = false) Boolean exactMatch);
//
//	@RequestMapping(value = "/erp/add-list", method = RequestMethod.POST)
//	public Response erpAddListNotification(@RequestBody List<Notification> listNotification);
//
//	@RequestMapping(value = "/erp/sms/add-list", method = RequestMethod.POST)
//	public Response erpAddListSMSNotification(@RequestBody List<Notification> listNotification);
//
//	public boolean updateSeen(String channel, int id) throws Exception;
//
//	public List<NotificationEntity> getNEAllByParams(Condition condition) throws Exception;
//
//	public List<NotificationEntity> getNEPostOneSignal(Condition condition) throws Exception;
//}
