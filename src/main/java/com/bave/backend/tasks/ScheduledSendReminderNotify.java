package com.bave.backend.tasks;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bave.backend.dao.ErpPartnerDao;
import com.bave.backend.dao.MaintainReminderDao;
import com.bave.backend.dao.ServiceProviderDao;
import com.bave.backend.dao.UserNotificationStickyDao;
import com.bave.backend.generic.configuration.Condition;
import com.bave.backend.persistence.entity.CarserviceMaintainReminder;
import com.bave.backend.persistence.entity.ErpPartner;
import com.bave.backend.persistence.entity.ServiceProvider;
import com.bave.backend.persistence.entity.UserNotificationSticky;
import com.bave.backend.service.impl.CommonService;
import com.bave.backend.service.impl.NotificationServiceImpl;
import com.bave.backend.util.DateUtil;
import com.bave.backend.util.EHCacheManager;

@Component
@Transactional
public class ScheduledSendReminderNotify {
	private final Logger logger = LoggerFactory.getLogger("kafka_file");

	@Autowired
	private Environment env;

	@Autowired
	private CommonService commonService;

	@Autowired
	private NotificationServiceImpl notifiService;

	@Autowired
	private ServiceProviderDao serviceProviderDao;

	@Autowired
	private MaintainReminderDao reminderDao;

	@Autowired
	private ErpPartnerDao erpPartnerDao;

	@Autowired
	private UserNotificationStickyDao userNotificationStickyDao;

	@Scheduled(fixedRate = 30000)
	public void storeLogToDB() {
		try {
			Date date = DateUtil.addDay(new Date(), -1);
			EHCacheManager.initCache(commonService, date);
		} catch (Exception e) {
			logger.error("fail store log to db", e);
			e.printStackTrace();
		}
	}

	@Scheduled(cron = "0 0 2,10 ? * *")
	//@Scheduled(cron = " * * 0/9 ? * *")
	public void sendReminderNotification() {
		try {
			// get list gara with bcs
			List<UserNotificationSticky> lstNotify = new ArrayList<UserNotificationSticky>();
			List<ServiceProvider> serviceProviders = serviceProviderDao.getServiceProviderUseBcs();
			int dateDiff = Integer.parseInt(env.getProperty("before.reminder.date", "1"));
			// get list maintain_reminder from each gara
			Condition cond = new Condition();
			cond.add("date_diff", dateDiff);
			for (ServiceProvider sp : serviceProviders) {
//				cond.add("db", sp.getCode());
				List<CarserviceMaintainReminder> listReminders = reminderDao.getListReminder(cond);
				for (CarserviceMaintainReminder c : listReminders) {
					Map<String, Object> m = new HashMap<String, Object>();
					m.put("partnerId", c.getPartnerId());
//					m.put("domain", sp.getCode());
					ErpPartner partner = erpPartnerDao.findSingleByProperty(m);
					Integer memberId = partner.getMemberId();
					boolean existNotify = false; //userNotificationStickyDao.checkExistNotification(memberId, c.getTimeReminder(), dateDiff, c.getId(),sp.getCode());
					if (memberId != null && !existNotify) {
						UserNotificationSticky notification = new UserNotificationSticky();
//						notification.setDomain(sp.getCode());
						notification.setMemberId(memberId.longValue());
						notification.setDestinationObject("member");
						notification.setAction("reminder_detail");
						notification.setType("personal");
						notification.setDataRefId(c.getId().toString());
						notification.setDataObject("reminder");
						notification.setChannel("onesignal");
						notification.setTitle(c.getName());
						notification.setMessage(c.getName());
						notification.setShortContent(c.getName());
						notification.setAddress("");
						notification.setSequence("");
						lstNotify.add(notification);
					}
				}
			}

			// send notification
			if (lstNotify.size() > 0) {
				notifiService.erpAddListUserNotificationSticky(lstNotify);
			}

		} catch (Exception e) {
			logger.error("fail in sending maintain notification process", e);
			e.printStackTrace();
		}
	}

}
