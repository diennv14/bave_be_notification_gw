package com.bave.backend.tasks;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bave.backend.dao.PromotionConditionDao;
import com.bave.backend.dao.UserNotificationStickyDao;
import com.bave.backend.onesignal.OneSignalPostNotification;
import com.bave.backend.persistence.entity.PromotionCondition;
import com.bave.backend.persistence.entity.PromotionNews;
import com.bave.backend.persistence.entity.UserNotificationSticky;
import com.bave.backend.service.impl.CommonService;
import com.bave.backend.service.impl.NotificationServiceImpl;
import com.bave.backend.util.DateUtil;
import com.bave.backend.util.EHCacheManager;

@Component
@Transactional
public class ScheduledSendNotify {
	private final Logger logger = LoggerFactory.getLogger("kafka_file");

	@Autowired
	private CommonService commonService;

	@Autowired
	private UserNotificationStickyDao userNotificationStickyDao;

	@Autowired
	private PromotionConditionDao promotionConditionDao;

	@Autowired
	private NotificationServiceImpl notifiService;

	@Scheduled(fixedRate = 30000)
	public void storeLogToDB() {
		try {
			Date date = DateUtil.addDay(new Date(), -1);
			EHCacheManager.initCache(commonService, date);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Scheduled(cron = "0 08-11 02 ? * *")
	public void sendPromotionNotificationForNewMember() {
		try {
			Date date = new Date();
			// get list promotion
			List<PromotionNews> lstPromotionNew = userNotificationStickyDao.getLstActivePromotionNews();
			List<PromotionCondition> lstProCond;

			for (PromotionNews pm : lstPromotionNew) {
				List<String> lstGara = promotionConditionDao.getGarasOfPromotion(pm.getPmNewId().intValue());
				String garas = "";
				for (String garaCode : lstGara) {
					if (lstGara.indexOf(garaCode) == 0) {
						garas = "'" + garaCode + "'";
					} else {
						garas += ",'" + garaCode + "'";
					}
				}
				// get list condition of a promotion
				lstProCond = promotionConditionDao.findByProperty("pmNewId", pm.getPmNewId());

				// get new member today to send notifications about the
				// promotion
				List<Integer> lstMemberId = promotionConditionDao.getLstMemberApplyPromotion(lstProCond, garas);

				System.out.println(" pmID: " + pm.getPmNewId() + ", member id : " + lstMemberId + " time " + date);

				List<UserNotificationSticky> lstNotify = new ArrayList<UserNotificationSticky>();
				for (Integer memberId : lstMemberId) {
					UserNotificationSticky notification = new UserNotificationSticky();
					notification.setMemberId(memberId.longValue());
					notification.setDestinationObject("member");
					notification.setAction("promotion");
					notification.setType("promotion");
					notification.setDataRefId(pm.getPmNewId().toString());
					notification.setDataObject("promotion_new");
					notification.setChannel("onesignal");
					notification.setTitle(pm.getWebTitle());
					notification.setMessage(pm.getWebContent());
					notification.setShortContent(pm.getWebShortContent());
					notification.setAddress("");
					notification.setSequence("");
					notification.setIcon("https://carservice.bave.io/images/thumb_notification/khuyenmai_icon.jpg");
					lstNotify.add(notification);
				}

				// send notification
				if (lstNotify.size() > 0) {
					notifiService.erpAddListUserNotificationSticky(lstNotify);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void scheduleTaskWithFixedDelay() {
	}

	public void scheduleTaskWithInitialDelay() {
	}

	public void scheduleTaskWithCronExpression() {
	}
}
