package com.bave.backend.tasks;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bave.backend.dao.UserNotificationStickyDao;
import com.bave.backend.generic.configuration.Condition;
import com.bave.backend.onesignal.OneSignalPostNotification;
import com.bave.backend.persistence.entity.UserNotificationSticky;
import com.bave.backend.service.impl.CommonService;
import com.bave.backend.util.DateUtil;
import com.bave.backend.util.EHCacheManager;

@Component
@Transactional
public class ScheduledTasks {
	private final Logger logger = LoggerFactory.getLogger("kafka_file");

	@Autowired
	private CommonService commonService;

	@Autowired
	private UserNotificationStickyDao userNotificationStickyDao;

	@Scheduled(fixedRate = 30000)
	public void storeLogToDB() {
		try {
			Date date = DateUtil.addDay(new Date(), -1);
			EHCacheManager.initCache(commonService, date);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Scheduled(fixedRate = 30000)
	public void sendRateForRepair() {
		try {
			Condition condition = new Condition();
			condition.add("status", "0");
			;
			List<UserNotificationSticky> lst = userNotificationStickyDao.findByCondition(condition);
			// .findByProperty(new Object[] { new Object[] { "status", "=", "0"
			// } });
			for (UserNotificationSticky userNotificationSticky : lst) {
				userNotificationSticky.setLstAddress(
						userNotificationStickyDao.getTokenAddressByMemberId(userNotificationSticky.getMemberId()));
				boolean rs = postOneSingal(userNotificationSticky);
				if (rs) {
					userNotificationStickyDao.updateByProperty(userNotificationSticky.getId(), "status", "2");
				} else {
					userNotificationStickyDao.updateByProperty(userNotificationSticky.getId(), "status", "-1");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean postOneSingal(UserNotificationSticky userNotificationSticky) {
		
		if (userNotificationSticky.getLstAddress().size()>0) {
			OneSignalPostNotification osn = new OneSignalPostNotification();
			
			boolean rs = osn.postNotification(userNotificationSticky, logger);
			return rs;
			
		}
		
		return false;
	}

	public void scheduleTaskWithFixedDelay() {
	}

	public void scheduleTaskWithInitialDelay() {
	}

	public void scheduleTaskWithCronExpression() {
	}
}
