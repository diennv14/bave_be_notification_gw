package bave_backend_api;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import com.bave.backend.util.Response;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class ThreadRunner {

	public static void main(String[] args) {
//		new RunnableDemo("t1").start();
//		new RunnableDemo("t2").start();
//		new RunnableDemo("t3").start();
//		new RunnableDemo("t4").start();
//		new RunnableDemo("t5").start();

		for (int i = 0; i < 1000; i++) {
			new RunnableDemo("t" + i).start();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}

class RunnableDemo implements Runnable {
	private Thread t;
	private String threadName;

	RunnableDemo(String name) {
		threadName = name;
	}

	public void get(String uri, String threadName, int i) throws Exception {
		try {
			Client client = Client.create();
			WebResource webResource = client
					.resource("http://localhost:8010/utils/notification-gw/notification/erp/add/");
			Map<String, Object> mapData = new HashMap<>();
			mapData.put("id", null);
			mapData.put("member_id", "8858");
			mapData.put("not_notify_id", "null");
			mapData.put("message", " test ");
			mapData.put("status", "0");
			mapData.put("type", "1");
			mapData.put("action", "quotation_response");
			mapData.put("create_date", "1534118550846");
			mapData.put("source_object", "service_provider");
			mapData.put("source_ref_id", "14");
			mapData.put("destination_object", "member");
			mapData.put("destination_ref_id", "8858");
			mapData.put("source_name", "BTek Duy Tân");
			mapData.put("source_avatar", "null");
			mapData.put("dest_name", "Trần Xuân Hải");
			mapData.put("dest_avatar", "null");
			mapData.put("dest_shortcontent", "null");
			mapData.put("onesignal_status", "1");
			mapData.put("seen", "false");
			mapData.put("data_object", "quotation_request");
			mapData.put("data_ref_id", "225");
			String jsonData = new ObjectMapper().writeValueAsString(mapData);
			long a = System.currentTimeMillis();
			ClientResponse response = webResource.type("application/json").post(ClientResponse.class, jsonData);

			if (response.getStatus() == 200) {
				System.out.println(threadName + "-" + i + ": " + (System.currentTimeMillis() - a));
			} else {
				System.err.println(threadName + "-" + i + ": " + response.getStatus());
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

	}

	public void run() {
		// System.out.println("Running " + threadName);
		try {
			for (int i = 0; i < 100; i++) {
				get(null, threadName, i);
				Thread.sleep(5000);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Thread " + threadName + " interrupted.");
		}
		// System.out.println("Thread " + threadName + " exiting.");
	}

	public void start() {
		// System.out.println("Starting " + threadName);
		if (t == null) {
			t = new Thread(this, threadName);
			t.start();
		}
	}
}
