package com.bave.be.thread.util;

public interface PrintStatistic
{
	String printStatistic();
	
	String printWarning();
}
