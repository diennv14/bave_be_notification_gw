package com.bave.be.thread.util;

public class SchemaDatabase {
	
	private String url;
	private String userName;
	private String password;
	private int numberConnectionPool = 1;
	
	
	public SchemaDatabase() {
		// TODO Auto-generated constructor stub
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getNumnerConnectionPool() {
		return numberConnectionPool;
	}
	public void setNumnerConnectionPool(int numberConnectionPool) {
		this.numberConnectionPool = numberConnectionPool;
	}
}
