package com.bave.be.thread.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.bave.backend.persistence.entity.Notification;
import com.bave.be.entity.NotificationTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TemplateParser {
	public static Notification parser(Notification notification, NotificationTemplate template) {
		if (template != null) {
			String msg = template.getMessage();
			List<String> lstKey = getKey(msg);
			Map<String, Object> mapEntity = new ObjectMapper().convertValue(notification, Map.class);
			for (String key : lstKey) {
				String templateKey = "#{" + key + "}";
				Object value = mapEntity.get(key.toLowerCase());
				if (value != null) {
					String val = value.toString();
					if (Character.isUpperCase(key.charAt(0))) {
						val = val.replaceFirst(val.substring(0, 1), val.substring(0, 1).toUpperCase());
						msg = msg.replace(templateKey, val);
					} else {
						val = val.replaceFirst(val.substring(0, 1), val.substring(0, 1).toLowerCase());
						msg = msg.replace(templateKey, val);
					}
				}
			}
			notification.setMessage(msg);
		}
		return notification;
	}

	private static List<String> getKey(String msg) {
		List<String> lstKey = new ArrayList<>();
		long count = msg.codePoints().filter(ch -> ch == '#').count();
		for (int i = 0; i < count; i++) {
			int indexa = msg.indexOf("#{");
			int indexb = msg.indexOf("}", indexa);
			String key = msg.substring(indexa + 2, indexb);
			lstKey.add(key);
			msg = msg.substring(indexb);
		}
		return lstKey;
	}

}
