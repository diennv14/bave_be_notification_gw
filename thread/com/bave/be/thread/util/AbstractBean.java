package com.bave.be.thread.util;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;

import com.bave.backend.security.config.VarStatic;
import com.bave.backend.util.EntityAnnotation;
import com.bave.be.utils.DateUtil;
import com.bave.be.utils.ResultSetConverter;
import com.fss.sql.Database;

/**
 * <p>
 * Title: MCA GW
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2011
 * </p>
 * 
 * <p>
 * Company: FIS-SOT
 * </p>
 * 
 * @author LiemLT
 * @version 1.0
 */

public abstract class AbstractBean {
	public AbstractBean() {
	}

	protected Connection mcnMain = null;

	// set connection
	public void setConnection(Connection cn) {
		this.mcnMain = cn;
	}

	public Connection getConnection() {
		return mcnMain;
	}

	// init all db object
	public abstract void init() throws SQLException;

	// Close all db object
	public abstract void close();

	// close connection
	public void closeConnection() {
		try {
			if (mcnMain != null && !mcnMain.isClosed()) {
				Database.closeObject(mcnMain);
				mcnMain = null;
			}
		} catch (SQLException ex) {
		}
	}

	public Date getSysDate() throws Exception {
		PreparedStatement pstm = null;
		try {
			pstm = mcnMain.prepareStatement(" select now() sysdate at time zone 'utc' ");
			ResultSet resultSet = pstm.executeQuery();
			while (resultSet.next()) {
				return resultSet.getTimestamp("sysdate");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
		return null;
	}

	public Long getSeq(String seqName) throws Exception {
		PreparedStatement pstm = null;
		try {
			pstm = mcnMain.prepareStatement(" select nextval('" + seqName + "') ");
			ResultSet resultSet = pstm.executeQuery();
			while (resultSet.next()) {
				return resultSet.getLong(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
		return null;
	}

	public int getSqlType(String clzType) {
		if (clzType.equals(String.class.getName())) {
			return java.sql.Types.VARCHAR;
		} else if (clzType.equals(Integer.class.getName())) {
			return java.sql.Types.INTEGER;
		} else if (clzType.equals(Float.class.getName())) {
			return java.sql.Types.FLOAT;
		} else if (clzType.equals(Long.class.getName())) {
			return java.sql.Types.INTEGER;
		} else if (clzType.equals(Double.class.getName())) {
			return java.sql.Types.DOUBLE;
		} else if (clzType.equals(Date.class.getName())) {
			return java.sql.Types.TIMESTAMP;
		} else if (clzType.equals(Array.class.getName())) {
			return java.sql.Types.ARRAY;
		}
		return 0;
	}

	public PreparedStatement buildParams(PreparedStatement pstm, Object[] arrValueObject, int columnCount,
			LinkedHashMap<Integer, String> mapIndexType) throws Exception {
		for (int i = 1; i <= columnCount; i++) {
			Object val = arrValueObject[i - 1];
			if (mapIndexType != null && mapIndexType.containsKey(i)) {
				String clzType = mapIndexType.get(i);
				if (val != null && clzType.equals(String.class.getName())) {
					pstm.setString(i, val.toString());
				} else if (val != null && clzType.equals(Integer.class.getName())) {
					pstm.setInt(i, Integer.valueOf(val.toString()));
				} else if (val != null && clzType.equals(Float.class.getName())) {
					pstm.setFloat(i, Float.valueOf(val.toString()));
				} else if (val != null && clzType.equals(Long.class.getName())) {
					pstm.setLong(i, Long.valueOf(val.toString()));
				} else if (val != null && clzType.equals(Double.class.getName())) {
					pstm.setDouble(i, Double.valueOf(val.toString()));
				} else if (val != null && clzType.equals(Date.class.getName())) {
					pstm.setTimestamp(i, DateUtil.utilLongDateToSqlDate((Date) val));
				} else if (val != null && clzType.equals(Array.class.getName())) {
					pstm.setArray(i, (Array) val);
				} else {
					// pstm.setNull(i, java.sql.Types.NULL);
					pstm.setObject(i, null);
				}
			} else {
				if (val != null && val instanceof String) {
					pstm.setString(i, val.toString());
				} else if (val != null && val instanceof Integer) {
					pstm.setInt(i, Integer.valueOf(val.toString()));
				} else if (val != null && val instanceof Float) {
					pstm.setFloat(i, Float.valueOf(val.toString()));
				} else if (val != null && val instanceof Long) {
					pstm.setLong(i, Long.valueOf(val.toString()));
				} else if (val != null && val instanceof Double) {
					pstm.setDouble(i, Double.valueOf(val.toString()));
				} else if (val != null && val instanceof Date) {
					pstm.setTimestamp(i, DateUtil.utilLongDateToSqlDate((Date) val));
				} else if (val != null && val instanceof Arrays) {
					pstm.setArray(i, (Array) val);
				} else {
					pstm.setObject(i, null);
				}
			}
		}
		return pstm;
	}

	public BuildPSTM buildPreparedUpdate(PreparedStatement pstm, String tableName, List<String> lstSetColumn,
			List<String> lstWhereColumn, List<Object> lstValue) throws Exception {
		String sql = " update " + tableName + " ";
		String sqlParams = null;
		for (String column : lstSetColumn) {
			if (sqlParams == null) {
				sqlParams = column + "=?";
			} else {
				sqlParams += ", " + column + "=?";
			}
		}
		if (sqlParams != null) {
			sql += "set " + sqlParams;
		}

		if (lstWhereColumn.size() > 0) {
			sql += " where ";
			sqlParams = null;
			for (String column : lstWhereColumn) {
				if (sqlParams == null) {
					sqlParams = column + "=?";
				} else {
					sqlParams += " and " + column + "=?";
				}
			}
			sql += sqlParams;
		} else {
			return null;
		}

		pstm = mcnMain.prepareStatement(sql);
		return new BuildPSTM(pstm, lstValue);
	}

	public BuildPSTM buildPreparedInsert(PreparedStatement pstm, String tableName, List<String> lstSetColumn, String pk,
			String seq) throws Exception {
		String sql = " insert into " + tableName + "(";
		String sqlParams = null;
		for (String column : lstSetColumn) {
			if (sqlParams == null) {
				sqlParams = column;
			} else {
				sqlParams += ", " + column;
			}
		}
		if (sqlParams != null) {
			sql += sqlParams + ")";
		}

		sql += " values (";
		sqlParams = null;
		for (String column : lstSetColumn) {
			if (sqlParams == null) {
				if (pk != null && seq != null) {
					if (pk.equals(column)) {
						sqlParams = (seq != null) ? "nextval('" + seq + "')" : "?";
					}
				} else {
					sqlParams = "?";
				}
			} else {
				sqlParams += ",? ";
			}
		}
		if (sqlParams != null) {
			sql += sqlParams + ")";
		}
		pstm = mcnMain.prepareStatement(sql);
		return new BuildPSTM(pstm, null);
	}

	public PreparedStatement updateEntity(PreparedStatement pstm, Object[] arrValueObject, Integer columnCount,
			boolean batch) {
		return updateEntity(pstm, arrValueObject, columnCount, batch, null);
	}

	public PreparedStatement updateEntity(PreparedStatement pstm, Object[] arrValueObject, Integer columnCount,
			boolean batch, LinkedHashMap<Integer, String> mapIndexType) {
		try {
			pstm = buildParams(pstm, arrValueObject, (columnCount != null) ? columnCount : arrValueObject.length,
					mapIndexType);
			if (batch) {
				pstm.addBatch();
			} else {
				pstm.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pstm;
	}

	public PreparedStatement insertEntity(PreparedStatement pstm, Object[] arrValueObject, Integer columnCount,
			boolean batch) throws Exception {
		return insertEntity(pstm, arrValueObject, columnCount, batch, null);
	}

	public PreparedStatement insertEntity(PreparedStatement pstm, Object[] arrValueObject, Integer columnCount,
			boolean batch, LinkedHashMap<Integer, String> mapIndexType) throws Exception {
		pstm = buildParams(pstm, arrValueObject, (columnCount != null) ? columnCount : arrValueObject.length,
				mapIndexType);
		if (batch) {
			pstm.addBatch();
		} else {
			pstm.execute();
		}
		return pstm;
	}

	public PreparedStatement updateEntity(Connection mcnMain, PreparedStatement pstm, String tableName,
			LinkedHashMap<String, Object> mapSetParams, LinkedHashMap<String, Object> mapWhereParams,
			LinkedHashMap<Integer, String> mapIndexType) throws Exception {
		List<Object> lstValue = new ArrayList<>();
		String sql = " update " + tableName + " ";
		String sqlParams = null;
		for (String column : mapSetParams.keySet()) {
			if (sqlParams == null) {
				sqlParams = column + "=?";
			} else {
				sqlParams += ", " + column + "=?";
			}
			Object val = mapSetParams.get(column);
			lstValue.add(val);
		}
		if (sqlParams != null) {
			sql += "set " + sqlParams;
		}

		if (mapWhereParams != null && mapWhereParams.size() > 0) {
			sql += " where ";
			sqlParams = null;
			for (String column : mapWhereParams.keySet()) {
				if (sqlParams == null) {
					sqlParams = column + "=?";
				} else {
					sqlParams += " and " + column + "=?";
				}
				lstValue.add(mapWhereParams.get(column));
			}
			sql += sqlParams;
		} else {
			return pstm;
		}

		pstm = mcnMain.prepareStatement(sql);
		pstm = buildParams(pstm, lstValue.toArray(), lstValue.size(), mapIndexType);
		pstm.executeUpdate();

		return pstm;

	}

	public <T> List<T> insertBatch(List<T> lstData, String table, List<String> lstColumn, String pk, String sequence)
			throws Exception {
		List<T> lstExecuted = new ArrayList<>();
		List<T> lstAddBatch = new ArrayList<>();
		PreparedStatement pstm = null;
		try {
			mcnMain.setAutoCommit(false);
			int commit = 0;
			BuildPSTM buildPSTM = buildPreparedInsert(pstm, table, lstColumn, pk, sequence);
			for (T item : lstData) {
				if (buildPSTM != null) {
					Object[] arrValueObject = ResultSetConverter.getAllValuesByLstName(lstColumn, item, pk).toArray();
					pstm = insertEntity(buildPSTM.getPstm(), arrValueObject, null, true);
					lstAddBatch.add(item);
					commit++;
					if (commit == VarStatic.COMMIT_SIZE) {
						pstm.executeBatch();
						mcnMain.commit();
						if (lstAddBatch.size() > 0) {
							lstExecuted.addAll(lstAddBatch);
							lstAddBatch.clear();
						}
					}
				}
			}
			if (lstAddBatch.size() > 0) {
				pstm.executeBatch();
				mcnMain.commit();
				lstExecuted.addAll(lstAddBatch);
				lstAddBatch.clear();
			}
			return lstExecuted;
		} catch (Exception e) {
			mcnMain.rollback();
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
			mcnMain.setAutoCommit(true);
		}
	}

	public <T> List<T> updateBatch(String table, List<String> lstSetColumn, List<T> lstSetData,
			List<String> lstWhereColumn, Object whereData) throws Exception {
		List<T> lstExecuted = new ArrayList<>();
		List<T> lstAddBatch = new ArrayList<>();
		PreparedStatement pstmUpdate = null;
		try {
			mcnMain.setAutoCommit(false);
			int commit = 0;
			BuildPSTM buildPSTM = buildPreparedUpdate(pstmUpdate, table, lstSetColumn, lstWhereColumn, null);

			for (T item : lstSetData) {
				if (buildPSTM != null) {
					Object[] arrValueSet = ResultSetConverter.getAllValuesByLstName(lstSetColumn, item, null).toArray();
					Object[] arrValueWhere = ResultSetConverter.getAllValuesByLstName(lstWhereColumn, whereData, null)
							.toArray();
					Object[] arrValue = (Object[]) ArrayUtils.addAll(arrValueSet, arrValueWhere);
					pstmUpdate = updateEntity(buildPSTM.getPstm(), arrValue, null, true);

					lstAddBatch.add(item);
					commit++;
					if (commit == VarStatic.COMMIT_SIZE) {
						pstmUpdate.executeBatch();
						mcnMain.commit();
						if (lstAddBatch.size() > 0) {
							lstExecuted.addAll(lstAddBatch);
							lstAddBatch.clear();
						}
					}
				}
			}

			if (lstAddBatch.size() > 0) {
				pstmUpdate.executeBatch();
				mcnMain.commit();
				lstExecuted.addAll(lstAddBatch);
				lstAddBatch.clear();
			}

			return lstExecuted;
		} catch (Exception e) {
			mcnMain.rollback();
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstmUpdate);
			mcnMain.setAutoCommit(true);
		}
	}

	public Long insert(Object data, String[] fields) throws Exception {
		PreparedStatement pstm = null;
		try {
			Class<? extends Object> clazz = data.getClass();
			EntityAnnotation entityAnnotation = (EntityAnnotation) clazz.getAnnotation(EntityAnnotation.class);
			String table_name = entityAnnotation.table_name();
			String pk_name = entityAnnotation.pk_name();
			String seq_name = entityAnnotation.seq_name();
			if (fields == null) {
				fields = entityAnnotation.fields_insert();
			}
			List<String> lstField = Arrays.asList(fields);
			// lstField.add(pk_name);
			pstm = buildPreparedInsert(pstm, table_name, lstField, null, null).getPstm();
			List<Object> lstValueSet = ResultSetConverter.getAllValuesByLstName(lstField, data, null);
			Long seq = 0l;
			int pkIndex = lstField.indexOf(pk_name);
			if (pkIndex >= 0) {
				seq = getSeq(seq_name);
				lstValueSet.set(pkIndex, seq);
			}
			pstm = buildParams(pstm, lstValueSet.toArray(), lstValueSet.size(), null);
			pstm.execute();
			return seq;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
	}

	public boolean update(Object data, String[] fields) throws Exception {
		PreparedStatement pstm = null;
		try {
			Class<? extends Object> clazz = data.getClass();
			EntityAnnotation entityAnnotation = (EntityAnnotation) clazz.getAnnotation(EntityAnnotation.class);
			String table_name = entityAnnotation.table_name();
			String pk_name = entityAnnotation.pk_name();
			if (fields == null) {
				fields = entityAnnotation.fields_insert();
			}
			List<String> lstSetColumn = Arrays.asList(fields);
			List<String> lstWhereColumn = Arrays.asList(pk_name);
			Object[] arrValueSet = ResultSetConverter.getAllValuesByLstName(lstSetColumn, data, pk_name).toArray();
			Object[] arrValueWhere = ResultSetConverter.getAllValuesByLstName(lstWhereColumn, data, null).toArray();
			Object[] arrValue = (Object[]) ArrayUtils.addAll(arrValueSet, arrValueWhere);
			pstm = buildPreparedUpdate(pstm, table_name, lstSetColumn, lstWhereColumn, Arrays.asList(arrValue))
					.getPstm();
			pstm = buildParams(pstm, arrValue, arrValue.length, null);
			int rs = pstm.executeUpdate();
			return (rs >= 0) ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
	}

	public boolean update(String table, Map<String, Object> mapSet, Map<String, Object> mapWhere) throws Exception {
		PreparedStatement pstm = null;
		try {
			List<String> lstSetColumn = new ArrayList<>();
			List<String> lstWhereColumn = new ArrayList<>();

			for (String item : mapSet.keySet()) {
				lstSetColumn.add(item);
			}
			for (String item : mapWhere.keySet()) {
				lstWhereColumn.add(item);
			}

			List<Object> lstValue = new ArrayList<>();
			lstValue.addAll(mapSet.values());
			lstValue.addAll(mapWhere.values());
			pstm = buildPreparedUpdate(pstm, table, lstSetColumn, lstWhereColumn, lstValue).getPstm();
			pstm = buildParams(pstm, lstValue.toArray(), lstValue.size(), null);
			int rs = pstm.executeUpdate();
			return (rs >= 0) ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
	}

	public <T> T findById(Class<T> clazz, Object id) throws Exception {
		PreparedStatement pstm = null;
		try {
			EntityAnnotation entityAnnotation = (EntityAnnotation) clazz.getAnnotation(EntityAnnotation.class);
			String table_name = entityAnnotation.table_name();
			String pk_name = entityAnnotation.pk_name();
			pstm = mcnMain.prepareStatement(" select * from " + table_name + " where " + pk_name + "= ? ");
			pstm = buildParams(pstm, new Object[] { id }, 1, null);
			ResultSet resultSet = pstm.executeQuery();
			while (resultSet.next()) {
				return (T) ResultSetConverter.convertToObject(resultSet, clazz);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
		return null;
	}

	public <T> List<T> findAllByMapParams(Class<T> clazz, Map<String, Object> mapParams) throws Exception {
		PreparedStatement pstm = null;
		try {
			List<T> lstRs = new ArrayList<>();
			EntityAnnotation entityAnnotation = (EntityAnnotation) clazz.getAnnotation(EntityAnnotation.class);
			String table_name = entityAnnotation.table_name();

			Object[] arrVal = new Object[mapParams.size()];
			String sqlParams = null;
			int index = 0;
			for (String column : mapParams.keySet()) {
				if (sqlParams == null) {
					sqlParams = column + "=?";
				} else {
					sqlParams += " and " + column + "=?";
				}
				arrVal[index] = mapParams.get(column);
				index++;
			}

			pstm = mcnMain.prepareStatement(" select * from " + table_name + " where " + sqlParams);

			pstm = buildParams(pstm, arrVal, mapParams.size(), null);
			ResultSet resultSet = pstm.executeQuery();
			while (resultSet.next()) {
				T t = ResultSetConverter.convertToObject(resultSet, clazz);
				lstRs.add(t);
			}
			return lstRs;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
	}

	public Object executeSqlQuery(String sql) throws Exception {
		PreparedStatement pstm = null;
		try {
			pstm = mcnMain.prepareStatement(sql);
			return pstm.execute();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
	}

	public boolean checkExistedByParams(String table, Date fromDate, Date toDate, String status) throws Exception {
		PreparedStatement pstm = null;
		try {
			pstm = mcnMain.prepareStatement(
					" select count(id) total from " + table + " where status = ? and create_date >= ? ");
			pstm.setString(1, status);
			pstm.setTimestamp(2, Timestamp.valueOf(DateUtil.dateToString(fromDate, DateUtil.FORMAT_SQLDATE2)));
			ResultSet resultSet = pstm.executeQuery();
			while (resultSet.next()) {
				return resultSet.getInt("total") > 0 ? true : false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
		return false;
	}
}
