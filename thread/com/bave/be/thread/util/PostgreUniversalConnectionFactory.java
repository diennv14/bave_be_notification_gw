//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.bave.be.thread.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;

import org.apache.log4j.Logger;

import com.fis.database.oracle.FptOracleConnectionWrapper;
import com.fss.dictionary.Dictionary;
import com.fss.util.AppException;

import oracle.jdbc.driver.OracleConnection;
import oracle.ucp.UniversalConnectionPoolAdapter;
import oracle.ucp.UniversalConnectionPoolException;
import oracle.ucp.UniversalConnectionPoolStatistics;
import oracle.ucp.admin.UniversalConnectionPoolManager;
import oracle.ucp.admin.UniversalConnectionPoolManagerImpl;
import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;

public class PostgreUniversalConnectionFactory {
	public UniversalConnectionPoolManager poolManager;
	private PoolDataSource pds;
	public Logger logger;
	public long sleep;
	private boolean mblMonitor;
	private boolean mblRefresh;
	private int LOGIN_TIMEOUT_DEFAULT;
	private boolean isOpen;
	private Thread threadRefeshAndTrace;
	private Runnable runnableRefreshTrace;

	public PostgreUniversalConnectionFactory(Dictionary dic) throws Exception {
		this(dic, "".equals(dic.getString("CacheName")) ? getCacheNameDefault() : dic.getString("CacheName").trim());
	}

	public PostgreUniversalConnectionFactory(Properties prt) throws Exception {
		this(prt, prt.getProperty("CacheName") == null ? getCacheNameDefault() : prt.getProperty("CacheName").trim());
	}

	public PostgreUniversalConnectionFactory(Properties cacheProperties, String cacheName) throws Exception {
		this.poolManager = UniversalConnectionPoolManagerImpl.getUniversalConnectionPoolManager();
		this.pds = null;
		this.logger = Logger.getLogger(PostgreUniversalConnectionFactory.class);
		this.sleep = 120000L;
		this.mblMonitor = false;
		this.mblRefresh = false;
		this.LOGIN_TIMEOUT_DEFAULT = 10;
		this.isOpen = false;
		this.threadRefeshAndTrace = null;
		this.runnableRefreshTrace = new Runnable() {
			public void run() {
				while (PostgreUniversalConnectionFactory.this.isOpen) {
					try {
						if (PostgreUniversalConnectionFactory.this.poolManager != null
								&& PostgreUniversalConnectionFactory.this.pds != null) {
							String PoolName = PostgreUniversalConnectionFactory.this.pds.getConnectionPoolName();
							if (PostgreUniversalConnectionFactory.this.mblMonitor) {
								PostgreUniversalConnectionFactory.this.statistic();
							}

							if (PostgreUniversalConnectionFactory.this.mblRefresh) {
								PostgreUniversalConnectionFactory.this.logger
										.info("Refresh invalid connection in pool " + PoolName);
								PostgreUniversalConnectionFactory.this.poolManager.refreshConnectionPool(PoolName);
							}
						} else {
							PostgreUniversalConnectionFactory.this.logger.warn("Pool is null => no connection");

							try {
								Thread.sleep(PostgreUniversalConnectionFactory.this.sleep / 10L);
							} catch (InterruptedException var12) {
								;
							}
						}
					} catch (Exception var13) {
						PostgreUniversalConnectionFactory.this.logger.error("Error occur:" + var13.getMessage());
						var13.printStackTrace();
					} finally {
						try {
							Thread.sleep(PostgreUniversalConnectionFactory.this.sleep);
						} catch (InterruptedException var11) {
							;
						}

					}
				}

			}
		};
		this.logger.info("UCP:Starting create PoolDataSource " + cacheName + "...");
		String strUrl = cacheProperties.getProperty("Url");
		strUrl = strUrl == null ? cacheProperties.getProperty("URL") : strUrl;
		String strUserName = cacheProperties.getProperty("UserName");
		String strPassword = cacheProperties.getProperty("Password");
		this.pds = this.createOraclePoolDataSource(strUrl, strUserName, strPassword);
		this.pds.setConnectionPoolName(cacheName);
		this.setConnectionPoolPreperties(cacheProperties);
		this.logger.info("UCP:Finished create pool connection " + cacheName);
		this.startThreadMonitorRefreshCache(cacheProperties.getProperty("ThreadMonitorRefreshCache"),
				cacheProperties.getProperty("IntervalMonitorRefreshCache"));
	}

	public PostgreUniversalConnectionFactory(Dictionary dic, String poolName) throws Exception {
		this.poolManager = UniversalConnectionPoolManagerImpl.getUniversalConnectionPoolManager();
		this.pds = null;
		this.logger = Logger.getLogger(PostgreUniversalConnectionFactory.class);
		this.sleep = 120000L;
		this.mblMonitor = false;
		this.mblRefresh = false;
		this.LOGIN_TIMEOUT_DEFAULT = 10;
		this.isOpen = false;
		this.threadRefeshAndTrace = null;
		this.runnableRefreshTrace = new Runnable() {
			public void run() {
				while (PostgreUniversalConnectionFactory.this.isOpen) {
					try {
						if (PostgreUniversalConnectionFactory.this.poolManager != null
								&& PostgreUniversalConnectionFactory.this.pds != null) {
							String PoolName = PostgreUniversalConnectionFactory.this.pds.getConnectionPoolName();
							if (PostgreUniversalConnectionFactory.this.mblMonitor) {
								PostgreUniversalConnectionFactory.this.statistic();
							}

							if (PostgreUniversalConnectionFactory.this.mblRefresh) {
								PostgreUniversalConnectionFactory.this.logger
										.info("Refresh invalid connection in pool " + PoolName);
								PostgreUniversalConnectionFactory.this.poolManager.refreshConnectionPool(PoolName);
							}
						} else {
							PostgreUniversalConnectionFactory.this.logger.warn("Pool is null => no connection");

							try {
								Thread.sleep(PostgreUniversalConnectionFactory.this.sleep / 10L);
							} catch (InterruptedException var12) {
								;
							}
						}
					} catch (Exception var13) {
						PostgreUniversalConnectionFactory.this.logger.error("Error occur:" + var13.getMessage());
						var13.printStackTrace();
					} finally {
						try {
							Thread.sleep(PostgreUniversalConnectionFactory.this.sleep);
						} catch (InterruptedException var11) {
							;
						}

					}
				}

			}
		};
		this.logger.info("UCP:Starting create PoolDataSource " + poolName + "...");
		String strUrl = dic.getString("Url");
		String strUserName = dic.getString("UserName");
		String strPassword = dic.getString("Password");
		this.pds = this.createOraclePoolDataSource(strUrl, strUserName, strPassword);
		this.pds.setConnectionPoolName(poolName);
		this.setConnectionPoolPreperties(dic.toProperties());
		this.logger.info("UCP:Finished create pool connection " + poolName);
		this.startThreadMonitorRefreshCache(dic.getString("ThreadMonitorRefreshCache"),
				dic.getString("IntervalMonitorRefreshCache"));
	}

	public PostgreUniversalConnectionFactory(String strUrl, String strUserName, String strPassword, String poolName,
			Properties cacheProperties) throws Exception {
		this.poolManager = UniversalConnectionPoolManagerImpl.getUniversalConnectionPoolManager();
		this.pds = null;
		this.logger = Logger.getLogger(PostgreUniversalConnectionFactory.class);
		this.sleep = 120000L;
		this.mblMonitor = false;
		this.mblRefresh = false;
		this.LOGIN_TIMEOUT_DEFAULT = 10;
		this.isOpen = false;
		this.threadRefeshAndTrace = null;
		this.runnableRefreshTrace = new Runnable() {
			public void run() {
				while (PostgreUniversalConnectionFactory.this.isOpen) {
					try {
						if (PostgreUniversalConnectionFactory.this.poolManager != null
								&& PostgreUniversalConnectionFactory.this.pds != null) {
							String PoolName = PostgreUniversalConnectionFactory.this.pds.getConnectionPoolName();
							if (PostgreUniversalConnectionFactory.this.mblMonitor) {
								PostgreUniversalConnectionFactory.this.statistic();
							}

							if (PostgreUniversalConnectionFactory.this.mblRefresh) {
								PostgreUniversalConnectionFactory.this.logger
										.info("Refresh invalid connection in pool " + PoolName);
								PostgreUniversalConnectionFactory.this.poolManager.refreshConnectionPool(PoolName);
							}
						} else {
							PostgreUniversalConnectionFactory.this.logger.warn("Pool is null => no connection");

							try {
								Thread.sleep(PostgreUniversalConnectionFactory.this.sleep / 10L);
							} catch (InterruptedException var12) {
								;
							}
						}
					} catch (Exception var13) {
						PostgreUniversalConnectionFactory.this.logger.error("Error occur:" + var13.getMessage());
						var13.printStackTrace();
					} finally {
						try {
							Thread.sleep(PostgreUniversalConnectionFactory.this.sleep);
						} catch (InterruptedException var11) {
							;
						}

					}
				}

			}
		};
		this.logger.info("UCP:Starting create PoolDataSource " + poolName + "...");
		this.pds = this.createOraclePoolDataSource(strUrl, strUserName, strPassword);
		this.pds.setConnectionPoolName(poolName);
		this.setConnectionPoolPreperties(cacheProperties);
		this.logger.info("UCP:Finished create pool connection " + poolName);
		this.startThreadMonitorRefreshCache(cacheProperties.getProperty("ThreadMonitorRefreshCache"),
				cacheProperties.getProperty("IntervalMonitorRefreshCache"));
	}

	public PostgreUniversalConnectionFactory(String strUrl, String strUserName, String strPassword, int iMaxConnection,
			String startThread, String interval) throws Exception {
		this(strUrl, strUserName, strPassword, iMaxConnection);
		this.startThreadMonitorRefreshCache(startThread, interval);
	}

	public PostgreUniversalConnectionFactory(String strUrl, String strUserName, String strPassword, int iMaxConnection)
			throws Exception {
		this(getCacheNameDefault(), strUrl, strUserName, strPassword, iMaxConnection);
	}

	public PostgreUniversalConnectionFactory(String strUrl, String strUserName, String strPassword, int iMaxConnection,
			boolean fastConnectionFailover) throws Exception {
		this(getCacheNameDefault(), strUrl, strUserName, strPassword, iMaxConnection);
		this.pds.setFastConnectionFailoverEnabled(fastConnectionFailover);
	}

	public PostgreUniversalConnectionFactory(String cacheName, String strUrl, String strUserName, String strPassword,
			int iMaxConnection) throws Exception {
		this.poolManager = UniversalConnectionPoolManagerImpl.getUniversalConnectionPoolManager();
		this.pds = null;
		this.logger = Logger.getLogger(PostgreUniversalConnectionFactory.class);
		this.sleep = 120000L;
		this.mblMonitor = false;
		this.mblRefresh = false;
		this.LOGIN_TIMEOUT_DEFAULT = 10;
		this.isOpen = false;
		this.threadRefeshAndTrace = null;
		this.runnableRefreshTrace = new Runnable() {
			public void run() {
				while (PostgreUniversalConnectionFactory.this.isOpen) {
					try {
						if (PostgreUniversalConnectionFactory.this.poolManager != null
								&& PostgreUniversalConnectionFactory.this.pds != null) {
							String PoolName = PostgreUniversalConnectionFactory.this.pds.getConnectionPoolName();
							if (PostgreUniversalConnectionFactory.this.mblMonitor) {
								PostgreUniversalConnectionFactory.this.statistic();
							}

							if (PostgreUniversalConnectionFactory.this.mblRefresh) {
								PostgreUniversalConnectionFactory.this.logger
										.info("Refresh invalid connection in pool " + PoolName);
								PostgreUniversalConnectionFactory.this.poolManager.refreshConnectionPool(PoolName);
							}
						} else {
							PostgreUniversalConnectionFactory.this.logger.warn("Pool is null => no connection");

							try {
								Thread.sleep(PostgreUniversalConnectionFactory.this.sleep / 10L);
							} catch (InterruptedException var12) {
								;
							}
						}
					} catch (Exception var13) {
						PostgreUniversalConnectionFactory.this.logger.error("Error occur:" + var13.getMessage());
						var13.printStackTrace();
					} finally {
						try {
							Thread.sleep(PostgreUniversalConnectionFactory.this.sleep);
						} catch (InterruptedException var11) {
							;
						}

					}
				}

			}
		};
		this.logger.info("UCP:Starting create PoolDataSource " + cacheName + "...");
		this.pds = this.createOraclePoolDataSource(strUrl, strUserName, strPassword);
		this.pds.setLoginTimeout(this.LOGIN_TIMEOUT_DEFAULT);
		this.pds.setMaxPoolSize(iMaxConnection);
		this.logger.info("UCP:Finished create pool connection " + cacheName);
	}

	public PostgreUniversalConnectionFactory(String strUrl, String strUserName, String strPassword, int iMaxConnection,
			int iMinConnection, int iInitConnection, String startThreadMR, String interval) throws Exception {
		this(strUrl, strUserName, strPassword, iMaxConnection, iMinConnection, iInitConnection);
		this.startThreadMonitorRefreshCache(startThreadMR, interval);
	}

	public PostgreUniversalConnectionFactory(String strUrl, String strUserName, String strPassword, int iMaxConnection,
			int iMinConnection, int iInitConnection) throws Exception {
		this.poolManager = UniversalConnectionPoolManagerImpl.getUniversalConnectionPoolManager();
		this.pds = null;
		this.logger = Logger.getLogger(PostgreUniversalConnectionFactory.class);
		this.sleep = 120000L;
		this.mblMonitor = false;
		this.mblRefresh = false;
		this.LOGIN_TIMEOUT_DEFAULT = 10;
		this.isOpen = false;
		this.threadRefeshAndTrace = null;
		this.runnableRefreshTrace = new Runnable() {
			public void run() {
				while (PostgreUniversalConnectionFactory.this.isOpen) {
					try {
						if (PostgreUniversalConnectionFactory.this.poolManager != null
								&& PostgreUniversalConnectionFactory.this.pds != null) {
							String PoolName = PostgreUniversalConnectionFactory.this.pds.getConnectionPoolName();
							if (PostgreUniversalConnectionFactory.this.mblMonitor) {
								PostgreUniversalConnectionFactory.this.statistic();
							}

							if (PostgreUniversalConnectionFactory.this.mblRefresh) {
								PostgreUniversalConnectionFactory.this.logger
										.info("Refresh invalid connection in pool " + PoolName);
								PostgreUniversalConnectionFactory.this.poolManager.refreshConnectionPool(PoolName);
							}
						} else {
							PostgreUniversalConnectionFactory.this.logger.warn("Pool is null => no connection");

							try {
								Thread.sleep(PostgreUniversalConnectionFactory.this.sleep / 10L);
							} catch (InterruptedException var12) {
								;
							}
						}
					} catch (Exception var13) {
						PostgreUniversalConnectionFactory.this.logger.error("Error occur:" + var13.getMessage());
						var13.printStackTrace();
					} finally {
						try {
							Thread.sleep(PostgreUniversalConnectionFactory.this.sleep);
						} catch (InterruptedException var11) {
							;
						}

					}
				}

			}
		};
		String CacheName = getCacheNameDefault();
		this.logger.info("UCP:Starting create PoolDataSource " + CacheName + "...");
		this.pds = this.createOraclePoolDataSource(strUrl, strUserName, strPassword);
		this.pds.setLoginTimeout(this.LOGIN_TIMEOUT_DEFAULT);
		this.pds.setConnectionPoolName(CacheName);
		this.pds.setInitialPoolSize(iInitConnection);
		this.pds.setMinPoolSize(iMinConnection);
		this.pds.setMaxPoolSize(iMaxConnection);
		this.logger.info("UCP:Finished create pool connection " + CacheName);
	}

	public PostgreUniversalConnectionFactory(String strUrl, String strUserName, String strPassword, int iMaxConnection,
			int iMinConnection, int iInitConnection, String strCacheName, String startThread, String interval)
					throws Exception {
		this(strUrl, strUserName, strPassword, iMaxConnection, iMinConnection, iInitConnection, strCacheName);
		this.startThreadMonitorRefreshCache(startThread, interval);
	}

	public PostgreUniversalConnectionFactory(String strUrl, String strUserName, String strPassword, int iMaxConnection,
			int iMinConnection, int iInitConnection, String strCacheName) throws Exception {
		this.poolManager = UniversalConnectionPoolManagerImpl.getUniversalConnectionPoolManager();
		this.pds = null;
		this.logger = Logger.getLogger(PostgreUniversalConnectionFactory.class);
		this.sleep = 120000L;
		this.mblMonitor = false;
		this.mblRefresh = false;
		this.LOGIN_TIMEOUT_DEFAULT = 10;
		this.isOpen = false;
		this.threadRefeshAndTrace = null;
		this.runnableRefreshTrace = new Runnable() {
			public void run() {
				while (PostgreUniversalConnectionFactory.this.isOpen) {
					try {
						if (PostgreUniversalConnectionFactory.this.poolManager != null
								&& PostgreUniversalConnectionFactory.this.pds != null) {
							String PoolName = PostgreUniversalConnectionFactory.this.pds.getConnectionPoolName();
							if (PostgreUniversalConnectionFactory.this.mblMonitor) {
								PostgreUniversalConnectionFactory.this.statistic();
							}

							if (PostgreUniversalConnectionFactory.this.mblRefresh) {
								PostgreUniversalConnectionFactory.this.logger
										.info("Refresh invalid connection in pool " + PoolName);
								PostgreUniversalConnectionFactory.this.poolManager.refreshConnectionPool(PoolName);
							}
						} else {
							PostgreUniversalConnectionFactory.this.logger.warn("Pool is null => no connection");

							try {
								Thread.sleep(PostgreUniversalConnectionFactory.this.sleep / 10L);
							} catch (InterruptedException var12) {
								;
							}
						}
					} catch (Exception var13) {
						PostgreUniversalConnectionFactory.this.logger.error("Error occur:" + var13.getMessage());
						var13.printStackTrace();
					} finally {
						try {
							Thread.sleep(PostgreUniversalConnectionFactory.this.sleep);
						} catch (InterruptedException var11) {
							;
						}

					}
				}

			}
		};
		this.logger.info("UCP:Starting create PoolDataSource " + strCacheName + "...");
		this.pds = this.createOraclePoolDataSource(strUrl, strUserName, strPassword);
		this.pds.setLoginTimeout(this.LOGIN_TIMEOUT_DEFAULT);
		this.pds.setConnectionPoolName(strCacheName);
		this.pds.setInitialPoolSize(iInitConnection);
		this.pds.setMinPoolSize(iMinConnection);
		this.pds.setMaxPoolSize(iMaxConnection);
		this.logger.info("UCP:Finished create pool connection " + strCacheName);
	}

	public void setFastConnectionFailover(boolean fastConnectionFailover) throws SQLException {
		if (this.pds != null) {
			this.pds.setFastConnectionFailoverEnabled(fastConnectionFailover);
		}

	}

	private PoolDataSource createOraclePoolDataSource(String strUrl, String strUserName, String strPassword)
			throws Exception {
		if (strUrl == null) {
			throw new AppException("FSS-10025", "You must fill 'Url' parameter to connect to database");
		} else if (strUserName == null) {
			throw new AppException("FSS-10026",
					"You must fill 'UserName' and 'Password' parameters to connect to database");
		} else if (strPassword == null) {
			throw new AppException("FSS-10026",
					"You must fill 'UserName' and 'Password' parameters to connect to database");
		} else {
			PoolDataSource pds = PoolDataSourceFactory.getPoolDataSource();
			// pds.setConnectionFactoryClassName("oracle.jdbc.pool.OracleDataSource");
			pds.setConnectionFactoryClassName("org.postgresql.Driver");
			pds.setURL(strUrl);
			pds.setUser(strUserName);
			pds.setPassword(strPassword);
			this.poolManager.createConnectionPool((UniversalConnectionPoolAdapter) pds);
			return pds;
		}
	}

	public void refreshConnectionPool() throws Exception {
		this.logger.warn("Refreshing pool will replace all connection...");
		this.poolManager.refreshConnectionPool(this.pds.getConnectionPoolName());
		this.logger.warn("Finished refresh pool.");
	}

	public void recycleConnectionPool() throws Exception {
		this.logger.warn("RecycleConnectionPool will destroy and replace invalid connections ...");
		this.poolManager.recycleConnectionPool(this.pds.getConnectionPoolName());
		this.logger.warn("Finished RecycleConnectionPool");
	}

	public void purgeConnectionPool() throws Exception {
		this.logger.warn("PurgeConnectionPool...");
		this.poolManager.purgeConnectionPool(this.pds.getConnectionPoolName());
		this.logger.warn("Finished PurgeConnectionPool.");
	}

	public void close() throws SQLException {
		this.isOpen = false;

		try {
			this.logger.warn("UCP:CLOSE POOL " + this.pds.getConnectionPoolName());
			this.poolManager.destroyConnectionPool(this.pds.getConnectionPoolName());
		} catch (Exception var8) {
			;
		}

		this.pds = null;

		try {
			this.threadRefeshAndTrace.join(1000L);
			if (this.threadRefeshAndTrace.isAlive()) {
				this.threadRefeshAndTrace.stop();
			}
		} catch (Exception var6) {
			;
		} finally {
			this.threadRefeshAndTrace = null;
		}

	}

	public void finalize() throws Throwable {
		this.close();
		super.finalize();
	}

	public Connection getConnection() throws Exception {
		Connection con = this.pds.getConnection();
		if (con == null) {
			throw new AppException("FSS-10022", "Cache exceed MaxLimit connections");
		} else {
			return con;
		}
	}

	public Connection getConnection(String key) throws Exception {
		Properties pc = new Properties();
		pc.setProperty("key", key);
		OracleConnection oc = (OracleConnection) this.pds.getConnection(pc);
		if (oc == null) {
			throw new AppException("FSS-10022", "Cache exceed MaxLimit connections");
		} else {
			Properties poc = oc.getConnectionAttributes();
			if (poc == null || poc.get("key") == null) {
				oc.applyConnectionAttributes(pc);
			}

			FptOracleConnectionWrapper focw = new FptOracleConnectionWrapper(oc);
			return focw;
		}
	}

	public Connection getConnection(Properties pc) throws Exception {
		OracleConnection oc = (OracleConnection) this.pds.getConnection(pc);
		if (oc == null) {
			throw new AppException("FSS-10022", "Cache exceed MaxLimit connections");
		} else {
			Properties poc = oc.getConnectionAttributes();
			if (!compareMap(pc, poc)) {
				oc.applyConnectionAttributes(pc);
			}

			return oc;
		}
	}

	public static boolean compareMap(Map mapSrc, Map mapDest) {
		if (mapDest != null && mapDest.size() != 0) {
			if (mapSrc != null && mapSrc.size() != 0) {
				Iterator iterator = mapSrc.entrySet().iterator();

				String strKey;
				Object obj;
				label31: do {
					Object objValue;
					do {
						if (!iterator.hasNext()) {
							return true;
						}

						Entry entry = (Entry) iterator.next();
						strKey = (String) entry.getKey();
						objValue = entry.getValue();
						if (objValue == null) {
							obj = mapDest.get(strKey);
							continue label31;
						}

						obj = mapDest.get(strKey);
					} while (objValue.equals(obj));

					return false;
				} while (obj == null && mapDest.containsKey(strKey));

				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	public UniversalConnectionPoolManager getConnectionPoolManager() {
		return this.poolManager;
	}

	private void statistic() throws SQLException, UniversalConnectionPoolException {
		UniversalConnectionPoolStatistics statistic = this.poolManager
				.getConnectionPool(this.pds.getConnectionPoolName()).getStatistics();
		this.logger.debug("statistic.getAbandonedConnectionsCount():" + statistic.getAbandonedConnectionsCount());
		this.logger.debug(
				"statistic.getAverageBorrowedConnectionsCount():" + statistic.getAverageBorrowedConnectionsCount());
		this.logger.debug("statistic.getBorrowedConnectionsCount():" + statistic.getBorrowedConnectionsCount());
		this.logger.debug("statistic.getConnectionsClosedCount():" + statistic.getConnectionsClosedCount());
		this.logger.debug("statistic.getConnectionsCreatedCount():" + statistic.getConnectionsCreatedCount());
		this.logger.debug(
				"statistic.getCumulativeConnectionBorrowedCount():" + statistic.getCumulativeConnectionBorrowedCount());
		this.logger.debug("statistic.getRemainingPoolCapacityCount():" + statistic.getRemainingPoolCapacityCount());
		this.logger.debug("statistic.getAvailableConnectionsCount():" + statistic.getAvailableConnectionsCount());
		this.logger.debug("statistic.getTotalConnectionsCount():" + statistic.getTotalConnectionsCount());
	}

	public void setLogMonitorInterval(String interval) {
		try {
			this.sleep = (long) (Integer.parseInt(interval) * 1000);
		} catch (NumberFormatException var3) {
			this.sleep = 120000L;
		}

	}

	public void startThreadMonitorRefreshCache(String start, String interval) {
		this.setLogMonitorInterval(interval);
		if (start != null && start.trim() != "") {
			String[] MonitorRefresh = start.split(":");
			this.mblMonitor = "Y".equals(MonitorRefresh[0]);
			if (MonitorRefresh.length >= 2) {
				this.mblRefresh = "Y".equals(MonitorRefresh[1]);
			}
		}

		if ((this.mblMonitor || this.mblRefresh)
				&& (this.threadRefeshAndTrace == null || !this.threadRefeshAndTrace.isAlive())) {
			this.isOpen = true;
			this.logger.info("Starting thread check cache status...");
			this.threadRefeshAndTrace = new Thread(this.runnableRefreshTrace);
			this.threadRefeshAndTrace.setDaemon(true);
			this.threadRefeshAndTrace.setName("PostgreUniversalConnectionFactory:Thread refesh and trace");
			this.threadRefeshAndTrace.start();
		}

	}

	public void setLogger(Logger log) {
		this.logger = log;
	}

	private int getInteger(String iValue, int defaultValue) {
		try {
			return Integer.parseInt(iValue);
		} catch (NumberFormatException var4) {
			return defaultValue;
		}
	}

	private static String getCacheNameDefault() {
		SimpleDateFormat dtf = new SimpleDateFormat("yyyyMMddHHmmss");
		String strDate = dtf.format(new Date());
		Random random = new Random();
		return "CACHE" + strDate + "_" + random.nextInt();
	}

	public String getPoolName() throws SQLException {
		return this.pds.getConnectionPoolName();
	}

	public PoolDataSource getPoolDataSource() throws SQLException {
		return this.pds;
	}

	private void setConnectionPoolPreperties(Properties p) throws SQLException {
		int inittialLimit = this.getInteger(p.getProperty("InitialLimit"), 1);
		this.pds.setInitialPoolSize(inittialLimit);
		int minPoolSize = this.getInteger(p.getProperty("MinLimit"), 1);
		this.pds.setMinPoolSize(minPoolSize);
		int maxPoolSize = this.getInteger(p.getProperty("MaxLimit"), 2147483647);
		this.pds.setMaxPoolSize(maxPoolSize);
		int maxConnectionReuseTime = this.getInteger(p.getProperty("MaxConnectionReuseTime"), 0);
		this.pds.setMaxConnectionReuseTime((long) maxConnectionReuseTime);
		int maxConnectionReuseCount = this.getInteger(p.getProperty("MaxConnectionReuseCount"), 0);
		this.pds.setMaxConnectionReuseCount(maxConnectionReuseCount);
		int abandon = this.getInteger(p.getProperty("AbandonedConnectionTimeout"), 0);
		this.pds.setAbandonedConnectionTimeout(abandon);
		int timeToLiveTimeout = this.getInteger(p.getProperty("TimeToLiveTimeout"), 0);
		this.pds.setTimeToLiveConnectionTimeout(timeToLiveTimeout);
		int connectionWaitTimeout = this.getInteger(p.getProperty("ConnectionWaitTimeout"), 3);
		this.pds.setConnectionWaitTimeout(connectionWaitTimeout);
		int inactivityTimeout = this.getInteger(p.getProperty("InactivityTimeout"), 0);
		this.pds.setInactiveConnectionTimeout(inactivityTimeout);
		int propertyCheckInterval = this.getInteger(p.getProperty("PropertyCheckInterval"), 30);
		this.pds.setTimeoutCheckInterval(propertyCheckInterval);
		String validateConnection = p.getProperty("ValidateConnection");
		if ("true".equalsIgnoreCase(validateConnection) || "Y".equalsIgnoreCase(validateConnection)) {
			this.pds.setValidateConnectionOnBorrow(true);
		}

		this.pds.setLoginTimeout(this.getInteger(p.getProperty("LoginTimeout"), this.LOGIN_TIMEOUT_DEFAULT));
		String fastConnectionFailover = p.getProperty("FastConnectionFailover");
		if ("true".equalsIgnoreCase(fastConnectionFailover) || "Y".equalsIgnoreCase(fastConnectionFailover)) {
			this.logger.info("UCP:Using FastConnectionFailover " + this.pds.getConnectionPoolName());
			this.pds.setFastConnectionFailoverEnabled(true);
		}

	}
}
