package com.bave.be.thread.util;

public class StatusUnknownException extends Exception{
	
	private static final long serialVersionUID = 1L;
	
	public StatusUnknownException(){
		super("Trans Status is unknown");
	}
	
	public StatusUnknownException(String status){
		super("Trans Status is unknown: " + status);
	}	
}
