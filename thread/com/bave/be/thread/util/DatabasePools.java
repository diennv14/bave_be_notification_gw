package com.bave.be.thread.util;

import java.sql.Connection;
import java.util.HashMap;

import com.fss.dictionary.Dictionary;
import com.fss.dictionary.DictionaryNode;
import com.fss.sql.OracleConnectionFactory;

public class DatabasePools {
	Dictionary dictionaryDatabase = null;

	private HashMap<String, OracleConnectionFactory> dbPools = new HashMap<String, OracleConnectionFactory>();
	
	public void init(String urlConfig) throws Exception{
		try {
			dictionaryDatabase = new Dictionary(urlConfig);
			String activesDatabase = dictionaryDatabase.getString("ActivesDatabase");
			String[] arrayActivesDatabase = activesDatabase.split(",");
			for(int i = 0;i< arrayActivesDatabase.length;i++){
				SchemaDatabase schemaDatabase = new SchemaDatabase();
				DictionaryNode node = dictionaryDatabase.getNode(arrayActivesDatabase[i]);
				if(node == null){
					node = new DictionaryNode();
				}
				String url = node.getChild("Url").getValue();
				String userName = node.getChild("UserName").getValue();
				String password = node.getChild("Password").getValue();
				int numberConnectionPool = Integer.valueOf(node.getChild("ConnectionPoolSize").getValue());
				schemaDatabase.setUrl(url);
				schemaDatabase.setUserName(userName);
				schemaDatabase.setPassword(password);
				schemaDatabase.setNumnerConnectionPool(numberConnectionPool);
				OracleConnectionFactory pool = new OracleConnectionFactory(url, userName,password, numberConnectionPool);
				dbPools.put(arrayActivesDatabase[i], pool);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
    public Connection getConnection(String DBNAME) throws Exception {
    	OracleConnectionFactory pool = dbPools.get(DBNAME);
    	if (pool==null)
    		return null;
		return pool.getConnection();
	}
}
