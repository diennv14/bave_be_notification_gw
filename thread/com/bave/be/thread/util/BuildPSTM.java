package com.bave.be.thread.util;

import java.sql.PreparedStatement;
import java.util.List;

public class BuildPSTM {
	private PreparedStatement pstm;
	private List<Object> lstParamValue;

	public BuildPSTM() {
		super();
	}

	public BuildPSTM(PreparedStatement pstm, List<Object> lstParamValue) {
		super();
		this.pstm = pstm;
		this.lstParamValue = lstParamValue;
	}

	public PreparedStatement getPstm() {
		return pstm;
	}

	public void setPstm(PreparedStatement pstm) {
		this.pstm = pstm;
	}

	public List<Object> getLstParamValue() {
		return lstParamValue;
	}

	public void setLstParamValue(List<Object> lstParamValue) {
		this.lstParamValue = lstParamValue;
	}

}
