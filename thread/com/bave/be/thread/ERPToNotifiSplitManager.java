package com.bave.be.thread;

import java.util.Vector;

import com.fis.thread.ThreadSplitManager;
import com.fss.thread.ParameterType;
import com.fss.util.AppException;

public class ERPToNotifiSplitManager extends ThreadSplitManager {
	public int backDate;
	public int backDay;
	public int backMinute;
	public String sender;
	public boolean firstLoad = true;

	@Override
	public Vector getParameterDefinition() {
		Vector vt = new Vector();
		vt.add(createParameterDefinition("BackDate", "", ParameterType.PARAM_TEXTAREA_MAX, "9999"));
		vt.add(createParameterDefinition("BackDay", "", ParameterType.PARAM_TEXTAREA_MAX, "9999"));
		vt.add(createParameterDefinition("BackMinute", "", ParameterType.PARAM_TEXTAREA_MAX, "9999"));
		vt.add(createParameterDefinition("Sender", "", ParameterType.PARAM_TEXTAREA_MAX, "9999"));
		vt.addAll(super.getParameterDefinition());
		removeParameterDefinition(vt, "WarningPolicy");
		removeParameterDefinition(vt, "L3CriticalTime");
		removeParameterDefinition(vt, "L2ErrorTime");
		removeParameterDefinition(vt, "L1WarningTime");
		removeParameterDefinition(vt, "PrintWarningCycle");
		removeParameterDefinition(vt, "PrintStatisticCycle");
		return vt;
	}

	public void fillParameter() throws AppException {
		super.fillParameter();
		backDate = (getParameter("BackDate") != null) ? Integer.parseInt(getParameter("BackDate").toString()) : -1;
		backDay = (getParameter("BackDay") != null) ? Integer.parseInt(getParameter("BackDay").toString()) : -1;
		backMinute = (getParameter("BackMinute") != null) ? Integer.parseInt(getParameter("BackMinute").toString())
				: 10;
		sender = (getParameter("Sender") != null) ? getParameter("Sender").toString() : null;
		firstLoad = true;
	}

	public void resetCounter() {
	}
}
