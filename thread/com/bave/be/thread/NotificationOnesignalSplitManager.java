//package com.bave.be.thread;
//
//import java.util.Collections;
//import java.util.Properties;
//import java.util.Vector;
//import java.util.concurrent.ExecutionException;
//
//import org.apache.kafka.clients.consumer.ConsumerConfig;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.apache.kafka.clients.consumer.ConsumerRecords;
//import org.apache.kafka.clients.consumer.KafkaConsumer;
//import org.apache.kafka.clients.producer.KafkaProducer;
//import org.apache.kafka.clients.producer.Producer;
//import org.apache.kafka.clients.producer.ProducerConfig;
//import org.apache.kafka.clients.producer.ProducerRecord;
//
//import com.bave.backend.persistence.entity.NotificationOneSignal;
//import com.bave.backend.security.config.VarStatic;
//import com.bave.be.bean.NotificationConfigBean;
//import com.bave.be.bean.NotificationOnesignalBean;
//import com.bave.be.entity.serialz.NotificationOneSignalDeserializer;
//import com.bave.be.entity.serialz.NotificationOneSignalSerializer;
//import com.bave.be.thread.util.LinkQueue;
//import com.fis.thread.ThreadSplitManager;
//import com.fss.thread.ParameterType;
//import com.fss.thread.ThreadConstant;
//import com.fss.util.AppException;
//
//public class NotificationOnesignalSplitManager extends ThreadSplitManager {
//	public LinkQueue<NotificationOneSignal> queueSend = new LinkQueue();
//	public int backDate;
//	public int backDay;
//	public int backMinute;
//	public boolean firstLoad = true;
//	public int threadNumber;
//
//	protected NotificationOnesignalBean notificationBean = null;
//	protected NotificationConfigBean notiConfigBean = null;
//	protected NotificationOnesignalSplitManager threadManager;
//
//	protected String BOOTSTRAP_SERVERS_CONFIG = VarStatic.kafka_bootstrap_server;
//	protected String notify_send_topic = "notify_send_topic";
//	protected String notify_send_group = "notify_send_group";
//
//	protected String notify_success_topic = "notify_success_topic";
//	protected String notify_success_group = "notify_success_group";
//
//	protected String notify_failed_topic = "notify_failed_topic";
//	protected String notify_failed_group = "notify_failed_group";
//
//	public KafkaConsumer<String, NotificationOneSignal> sendTopicConsumer;
//	protected Producer<String, NotificationOneSignal> producerSuccess = createProducerTopic(notify_success_topic);
//	protected Producer<String, NotificationOneSignal> producerFailed = createProducerTopic(notify_success_topic);
//
//	private ConsumerRecords<String, NotificationOneSignal> messages;
//
//	@Override
//	public Vector getParameterDefinition() {
//		Vector vt = new Vector();
//		vt.add(createParameterDefinition("BackDate", "", ParameterType.PARAM_TEXTAREA_MAX, "9999"));
//		vt.add(createParameterDefinition("BackDay", "", ParameterType.PARAM_TEXTAREA_MAX, "9999"));
//		vt.add(createParameterDefinition("BackMinute", "", ParameterType.PARAM_TEXTAREA_MAX, "9999"));
//		vt.addAll(super.getParameterDefinition());
//		removeParameterDefinition(vt, "WarningPolicy");
//		removeParameterDefinition(vt, "L3CriticalTime");
//		removeParameterDefinition(vt, "L2ErrorTime");
//		removeParameterDefinition(vt, "L1WarningTime");
//		removeParameterDefinition(vt, "PrintWarningCycle");
//		removeParameterDefinition(vt, "PrintStatisticCycle");
//		return vt;
//	}
//
//	public void fillParameter() throws AppException {
//		super.fillParameter();
//
//		backDate = (getParameter("BackDate") != null) ? Integer.parseInt(getParameter("BackDate").toString()) : -1;
//		backDay = (getParameter("BackDay") != null) ? Integer.parseInt(getParameter("BackDay").toString()) : -1;
//		backMinute = (getParameter("BackMinute") != null) ? Integer.parseInt(getParameter("BackMinute").toString())
//				: 10;
//		threadNumber = (getParameter("ThreadNumber") != null)
//				? Integer.parseInt(getParameter("ThreadNumber").toString())
//				: 1;
//		firstLoad = true;
//	}
//
//	public void resetCounter() {
//	}
//
//	private Producer<String, NotificationOneSignal> createProducerTopic(String topic) {
//		Properties props = new Properties();
//		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS_CONFIG);
//		props.put(ProducerConfig.CLIENT_ID_CONFIG, topic);
//		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, NotificationOneSignalSerializer.class.getName());
//		props.put("value.serializer", NotificationOneSignalSerializer.class.getName());
//		return new KafkaProducer<>(props);
//	}
//
//	public KafkaConsumer<String, NotificationOneSignal> createConsumerTopic(String group, String topic) {
//		final Properties props = new Properties();
//		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS_CONFIG);
//		props.put(ConsumerConfig.GROUP_ID_CONFIG, group);
//		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, NotificationOneSignalDeserializer.class.getName());
//		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, NotificationOneSignalDeserializer.class.getName());
//		final KafkaConsumer<String, NotificationOneSignal> consumer = new KafkaConsumer<>(props);
//		consumer.subscribe(Collections.singletonList(topic));
//		return consumer;
//	}
//
//	public void sendMessageToSuccessTopic(NotificationOneSignal notification) {
//		try {
//			final ProducerRecord<String, NotificationOneSignal> record = new ProducerRecord<String, NotificationOneSignal>(
//					notify_success_topic, notification);
//			producerSuccess.send(record).get();
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		} catch (ExecutionException e) {
//			e.printStackTrace();
//		} finally {
//			producerSuccess.flush();
//			producerSuccess.close();
//		}
//	}
//
//	public void sendMessageToFailedTopic(NotificationOneSignal notification) {
//		try {
//			final ProducerRecord<String, NotificationOneSignal> record = new ProducerRecord<String, NotificationOneSignal>(
//					notify_failed_topic, notification);
//			producerFailed.send(record).get();
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		} catch (ExecutionException e) {
//			e.printStackTrace();
//		} finally {
//			producerFailed.flush();
//			producerFailed.close();
//		}
//	}
//
//	@Override
//	protected void beforeSession() throws Exception {
//		super.beforeSession();
////		setAutoConnectDb(true);
//		mcnMain = getConnection();
//
//		notificationBean = new NotificationOnesignalBean();
//		notificationBean.setConnection(mcnMain);
//		notificationBean.init();
//
//		notiConfigBean = new NotificationConfigBean();
//		notiConfigBean.setConnection(mcnMain);
//		notiConfigBean.init();
//
////		if (notiConfigBean == null) {
//		VarStatic.mapNotiConfig = notiConfigBean.loadNotiConfig();
////		}
//
//		sendTopicConsumer = createConsumerTopic(notify_send_group, notify_send_topic);
////		successTopicConsumer = createConsumerSendTopic(notify_success_group, notify_success_topic);
////		failedTopicConsumer = createConsumerSendTopic(notify_failed_group, notify_failed_topic);
//	}
//
//	@SuppressWarnings("deprecation")
//	@Override
//	public void processSession() throws Exception {
////		super.processSession();
//		while (miThreadCommand != ThreadConstant.THREAD_STOP && (queueSend != null && queueSend.isEmpty())) {
//
//			messages = sendTopicConsumer.poll(1000);
//			int totalSize = messages.count();
//			logMonitor("messages" + totalSize);
//			for (ConsumerRecord<String, NotificationOneSignal> message : messages) {
//				NotificationOneSignal notification = message.value();
//				queueSend.enqueueNotify(notification);
//			}
//
//			for (int iIndex = 0; iIndex < miDelayTime && miThreadCommand != ThreadConstant.THREAD_STOP; iIndex++) {
//				Thread.sleep(1000); // Time unit is second
//			}
//			fillLogFile();
//		}
//	}
//
//	@Override
//	protected void afterSession() throws Exception {
//		notificationBean.close();
//		notiConfigBean.close();
//		super.afterSession();
////		sendTopicConsumer.close();
////		successTopicConsumer.close();
////		failedTopicConsumer.close();
//	}
//
//}
