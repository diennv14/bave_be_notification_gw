//package com.bave.be.thread.process;
//
//import java.util.Arrays;
//import java.util.HashMap;
//
//import org.apache.kafka.clients.consumer.KafkaConsumer;
//
//import com.bave.backend.api.util.VarStatic;
//import com.bave.backend.onesignal.MessageResult;
//import com.bave.backend.onesignal.OneSignalPostNotification;
//import com.bave.backend.persistence.entity.NotificationOneSignal;
//import com.bave.be.entity.NotificationConfig;
//import com.bave.be.entity.QuotationNotif;
//import com.bave.be.thread.NotificationOnesignalSplitManager;
//import com.bave.be.utils.ApplicationProperties;
//import com.bave.be.utils.StringUtil;
//import com.fis.thread.ThreadSplitBase;
//import com.fss.thread.ThreadConstant;
//
//public class NotificationOnesignalThread extends ThreadSplitBase {
////	private NotificationOnesignalBean notificationBean = null;
////	private NotificationConfigBean notiConfigBean = null;
//	protected NotificationOnesignalSplitManager threadManager;
//
//	KafkaConsumer<String, NotificationOneSignal> sendTopicConsumer;
////	KafkaConsumer<String, NotificationOneSignal> successTopicConsumer;
////	KafkaConsumer<String, NotificationOneSignal> failedTopicConsumer;
//
//	@Override
//	protected void beforeSession() throws Exception {
//		logMonitor("start service");
//		setAutoConnectDb(true);
//		super.beforeSession();
//		threadManager = (NotificationOnesignalSplitManager) mParentThreadManager;
//
////		threadManager.notificationBean = new NotificationOnesignalBean();
////		threadManager.notificationBean.setConnection(mcnMain);
////		threadManager.notificationBean.init();
//
////		threadManager.notiConfigBean = new NotificationConfigBean();
////		threadManager.notiConfigBean.setConnection(mcnMain);
////		threadManager.notiConfigBean.init();
//
////		if (threadManager.notiConfigBean == null) {
////			VarStatic.mapNotiConfig = threadManager.notiConfigBean.loadNotiConfig();
////		}
//
////		sendTopicConsumer = threadManager.createConsumerTopic(threadManager.notify_send_group,
////				threadManager.notify_send_topic);
////		threadManager.successTopicConsumer = createConsumerSendTopic(notify_success_group, notify_success_topic);
////		threadManager.failedTopicConsumer = createConsumerSendTopic(notify_failed_group, notify_failed_topic);
//	}
//
//	@Override
//	public void processSession(int index) throws Exception {
//		while (threadManager.miThreadCommand != ThreadConstant.THREAD_STOP) {
//			NotificationOneSignal notification = threadManager.queueSend.dequeueWait(1);
//			if (notification != null && VarStatic.NOTI_STATUS_PROCESSING.equals(notification.getStatus())) {
//				QuotationNotif quotationNotif = new QuotationNotif();
//				quotationNotif.setAction(notification.getAction());
//				quotationNotif.setId(notification.getDataRefId());
//				quotationNotif.setMemberId(notification.getMemberId());
//				quotationNotif.setMemberName("");
//				quotationNotif.setPlayerId(notification.getPlayerId());
//				quotationNotif.setMessage(notification.getMessage());
//				quotationNotif.setDestinationObject(notification.getDestinationObject());
//				quotationNotif.setUnreadNotify(notification.getUnreadNotify());
//				NotificationConfig notiConfig = null;
//				if (VarStatic.DESTINATION_OBJECT_CAROWNER.equals(notification.getDestinationObject())) {
//					notiConfig = VarStatic.mapNotiConfig.get("marketing_onesignal");
//				} else {
//					notiConfig = VarStatic.mapNotiConfig.get(notification.getAction() + "_onesignal");
//				}
//
//				boolean onesignalRs = false;
//				if (notiConfig != null && notiConfig.getApiKey() != null && notiConfig.getAppKey() != null) {
//					onesignalRs = postOneSingal(quotationNotif, notiConfig);
//				} else {
//					logMonitor("Lacking config for action: " + notification.getAction() + ", message: "
//							+ notification.getMessage());
//				}
//
//				if (onesignalRs) {
//					threadManager.sendMessageToSuccessTopic(notification);
//					logMonitor("PA=" + notification.getAction() + " (" + notification.getUnreadNotify() + "),ref_id="
//							+ notification.getDataRefId() + ",status=0>1,player_id=" + notification.getPlayerId()
//							+ ",msg=" + StringUtil.removeAccent(notification.getMessage()));
//				} else {
//					threadManager.sendMessageToFailedTopic(notification);
//					logError("Failed A=" + notification.getAction() + " (" + notification.getUnreadNotify()
//							+ "),ref_id=" + notification.getDataRefId() + ",status=0>1,player_id="
//							+ notification.getPlayerId() + ",msg="
//							+ StringUtil.removeAccent(notification.getMessage()));
//				}
//				threadManager.fillLogFile();
//			}
//		}
//	}
//
//	public boolean postOneSingal(QuotationNotif quotationNotif, NotificationConfig notiConfig) throws Exception {
//		if (quotationNotif.getPlayerId() != null) {
//			OneSignalPostNotification osn = new OneSignalPostNotification();
//			HashMap<String, Object> mapContent = new HashMap<>();
//			mapContent.put("type", quotationNotif.getAction());
//			mapContent.put("action", quotationNotif.getAction());
//			mapContent.put("data_ref_id", quotationNotif.getId());
//			mapContent.put("contents", new MessageResult(quotationNotif.getId(), quotationNotif.getMessage()));
//
//			if ("booking".equals(quotationNotif.getAction())) {
//				if (quotationNotif.getMessage() == null) {
//					mapContent.put("contents", new MessageResult(quotationNotif.getId(), ApplicationProperties
//							.getValue("notif.msg.booking.booked", quotationNotif.getMemberName())));
//				}
//			} else if ("quotation_request".equals(quotationNotif.getAction())) {
//				if (quotationNotif.getMessage() == null) {
//					mapContent.put("contents", new MessageResult(quotationNotif.getId(),
//							ApplicationProperties.getValue("notif.msg.quot.request", quotationNotif.getMemberName())));
//				}
//			} else if ("quotation_response".equals(quotationNotif.getAction())) {
//				if (quotationNotif.getMessage() == null) {
//					mapContent.put("contents",
//							new MessageResult(quotationNotif.getId(),
//									ApplicationProperties.getValue("notif.msg.quot.response",
//											quotationNotif.getServiceProviderName(), quotationNotif.getId())));
//				}
//			}
//
//			if (mapContent.size() > 0) {
//				boolean rs = osn.postNotification(null, Arrays.asList(quotationNotif.getPlayerId()), mapContent,
//						notiConfig.getApiKey(), notiConfig.getAppKey(), quotationNotif.getUnreadNotify());
//				return rs;
//			}
//		}
//		return false;
//	}
//
//	@Override
//	protected void afterSession() throws Exception {
////		threadManager.notificationBean.close();
//		super.afterSession();
////		threadManager.sendTopicConsumer.close();
////		threadManager.successTopicConsumer.close();
////		threadManager.failedTopicConsumer.close();
//	}
//
//}
