//package com.bave.be.thread.process;
//
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import com.bave.backend.api.util.VarStatic;
//import com.bave.backend.persistence.entity.NotificationEmail;
//import com.bave.backend.util.ServiceClientUtil;
//import com.bave.be.bean.NotificationConfigBean;
//import com.bave.be.bean.NotificationEmailBean;
//import com.bave.be.entity.NotificationConfig;
//import com.bave.be.thread.NotificationEmailSplitManager;
//import com.bave.be.utils.DateUtil;
//import com.bave.be.utils.EmailAction;
//import com.bave.be.utils.StringUtil;
//import com.fis.thread.ThreadSplitBase;
//import com.fss.thread.ThreadConstant;
//import com.mashape.unirest.http.JsonNode;
//
//public class NotificationEmailThread extends ThreadSplitBase {
//	private NotificationEmailBean notificationBean = null;
//	private NotificationConfigBean notiConfigBean = null;
//	protected NotificationEmailSplitManager threadManager;
//
//	@Override
//	protected void beforeSession() throws Exception {
//		setAutoConnectDb(true);
//		super.beforeSession();
//		threadManager = (NotificationEmailSplitManager) mParentThreadManager;
//		super.beforeSession();
//
//		notificationBean = new NotificationEmailBean();
//		notificationBean.setConnection(mcnMain);
//		notificationBean.init();
//
//		notiConfigBean = new NotificationConfigBean();
//		notiConfigBean.setConnection(mcnMain);
//		notiConfigBean.init();
//
//		VarStatic.mapNotiConfig = notiConfigBean.loadNotiConfig();
//	}
//
//	@Override
//	public void processSession(int index) throws Exception {
//		while (threadManager.miThreadCommand != ThreadConstant.THREAD_STOP) {
//			threadManager.resetCounter();
//			List<NotificationEmail> rs = null;
//			Calendar cal;
//			if (threadManager.firstLoad) {
//				Date fromdate = DateUtil.getOverDay(threadManager.backDate, 00, 00, 00);
//				cal = Calendar.getInstance();
//				cal.add(Calendar.MINUTE, threadManager.backMinute);
//				Date todate = cal.getTime();
//				boolean existed = notificationBean.checkExistedByParams("notification_email", fromdate, todate, "0");
//				if (existed) {
//					rs = notificationBean.getAllNotification(fromdate, todate, 1000);
//					// logMonitor("From " + DateUtil.dateToString(fromdate,
//					// DateUtil.FORMAT_DMHHMMSS2) + " to "
//					// + DateUtil.dateToString(todate,
//					// DateUtil.FORMAT_DMHHMMSS2) +
//					// " Total records: " + rs.size());
//				}
//				threadManager.firstLoad = false;
//			} else {
//				Date fromdate = DateUtil.getOverDay(-1, 00, 00, 00);
//				cal = Calendar.getInstance();
//				cal.add(Calendar.MINUTE, threadManager.backMinute);
//				Date todate = cal.getTime();
//				boolean existed = notificationBean.checkExistedByParams("notification_email", fromdate, todate, "0");
//				if (existed) {
//					rs = notificationBean.getAllNotification(fromdate, todate, 1000);
//					// logMonitor("From " + DateUtil.dateToString(fromdate,
//					// DateUtil.FORMAT_DMHHMMSS2) + " to "
//					// + DateUtil.dateToString(todate,
//					// DateUtil.FORMAT_DMHHMMSS2) +
//					// " Total records: " + rs.size());
//				}
//			}
//			if (rs != null && rs.size() > 0) {
//				List<NotificationEmail> lstUpdateStatus = notificationBean.updateProcessStatus(rs,
//						VarStatic.NOTI_STATUS_DRAFT, VarStatic.NOTI_STATUS_PROCESSING);
//				notifiAndUpdate(lstUpdateStatus);
//			}
//			cal = Calendar.getInstance();
//			cal.add(Calendar.SECOND, miDelayTime);
//			// logMonitor("Continue at " + DateUtil.dateToString(cal.getTime(),
//			// DateUtil.FORMAT_DMHHMMSS2));
//			for (int iIndex = 0; iIndex < miDelayTime
//					&& threadManager.miThreadCommand != ThreadConstant.THREAD_STOP; iIndex++) {
//				Thread.sleep(1000); // Time unit is second
//			}
//			threadManager.fillLogFile();
//		}
//	}
//
//	private void notifiAndUpdate(List<NotificationEmail> rs) throws Exception {
//		List<NotificationEmail> lstPostOneSingal = new ArrayList<>();
//		for (NotificationEmail notification : rs) {
//			if (VarStatic.NOTI_STATUS_PROCESSING.equals(notification.getStatus())) {
//				/* sending email */
//				EmailAction emailAction = new EmailAction();
//				emailAction.setEmail(notification.getEmail() != null ? notification.getEmail() : "0");
//				emailAction.setMessage(notification.getMessage() != null ? notification.getMessage() : "");
//				emailAction.setSubject(notification.getTitle() != null ? notification.getTitle() : "");
//
//				NotificationConfig notiConfig = VarStatic.mapNotiConfig.get(notification.getAction() + "_email");
//
//				JsonNode result = null;
//
//				if (notiConfig != null && notiConfig.getApiKey() != null) {
//					result = emailAction.sendGetJson(notiConfig.getApiKey());
//				}
//
//				if (result != null && result.toString().contains("\"id\"")) {
//					notification.setStatus(VarStatic.NOTI_STATUS_PROCESSING);
//					callBackUrl(notification.getCallBackUrl(), true, "");
//				} else {
//					notification.setStatus(VarStatic.NOTI_STATUS_FAILED);
//					callBackUrl(notification.getCallBackUrl(), false, "could not sending this email");
//				}
//
//				lstPostOneSingal.add(notification);
//			}
//		}
//		List<NotificationEmail> lstExecuted = notificationBean.updateProcessStatus(lstPostOneSingal,
//				VarStatic.NOTI_STATUS_PROCESSING, VarStatic.NOTI_STATUS_SEND);
//		for (NotificationEmail notification : lstExecuted) {
//			logMonitor("A=" + notification.getAction() + ",ref_id=" + notification.getDestinationRefId() + ",status=0>"
//					+ notification.getStatus() + ",player_id=" + notification.getPlayerId() + ",email="
//					+ notification.getEmail() + ",msg=" + StringUtil.removeAccent(notification.getMessage()));
//		}
//	}
//
//	public void callBackUrl(String url, boolean success, String failReason) {
//		ServiceClientUtil.setUrl(url);
//		Map<String, Object> mapData = new HashMap<>();
//		mapData.put("result", success ? "1" : "0");
//		mapData.put("reason", failReason);
//		//mapData.put("time", new Date());
//		ServiceClientUtil.updateSeenNotif(mapData, "");
//	}
//
//	@Override
//	protected void afterSession() throws Exception {
//		notificationBean.close();
//		super.afterSession();
//		// scheduler.stop();
//	}
//
//}
