//package com.bave.be.thread.process;
//
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//
//import com.bave.backend.api.util.VarStatic;
//import com.bave.be.bean.NotificationBean;
//import com.bave.be.bean.NotificationTemplateBean;
//import com.bave.be.thread.NotificationToSenderSplitManager;
//import com.bave.be.utils.DateUtil;
//import com.bave.be.utils.StringUtil;
//import com.fis.thread.ThreadSplitBase;
//import com.fss.thread.ThreadConstant;
//
//public class NotificationToSenderThread extends ThreadSplitBase {
//	private NotificationBean notificationBean = null;
//	private NotificationTemplateBean templateBean = null;
//	protected NotificationToSenderSplitManager threadManager;
//	private Set<String> senderTables = null;
//
//	@Override
//	protected void beforeSession() throws Exception {
//		setAutoConnectDb(true);
//		super.beforeSession();
//		threadManager = (NotificationToSenderSplitManager) mParentThreadManager;
//
//		notificationBean = new NotificationBean();
//		notificationBean.setConnection(mcnMain);
//		notificationBean.init();
//
//		templateBean = new NotificationTemplateBean();
//		templateBean.setConnection(mcnMain);
//		templateBean.init();
//
//		VarStatic.mapNotiTemplate = templateBean.selectNotificationTemplate();
//
//		if (!StringUtil.isEmpty(threadManager.sender)) {
//			String[] arrSenderTables = threadManager.sender.split(";");
//			senderTables = new HashSet<>();
//			for (String table : arrSenderTables) {
//				senderTables.add(table);
//			}
//		}
//	}
//
//	@Override
//	public void processSession(int index) throws Exception {
//		while (threadManager.miThreadCommand != ThreadConstant.THREAD_STOP) {
//			threadManager.resetCounter();
//			List<Notification> rs = null;
//			Calendar cal;
//			if (threadManager.firstLoad) {
//				Date fromdate = DateUtil.getOverDayUTC(threadManager.backDate, 00, 00, 00);
//				Date todate = DateUtil.getOverDayUTC(0, 23, 59, 59);
//				boolean existed = notificationBean.checkExistedByParams("notification", fromdate, todate, "0");
//				if (existed) {
//					rs = notificationBean.selectForSender(fromdate, todate, null, null, null);
//					// logMonitor("From " + DateUtil.dateToString(fromdate,
//					// DateUtil.FORMAT_DMHHMMSS2) + " to "
//					// + DateUtil.dateToString(todate, DateUtil.FORMAT_DMHHMMSS2) +
//					// " Total records: " + rs.size());
//				}
//				threadManager.firstLoad = false;
//			} else {
//				Date fromdate = DateUtil.getOverDayUTC(threadManager.backDay, 00, 00, 00);
//				Date todate = DateUtil.getOverDayUTC(0, 23, 59, 59);
//				boolean existed = notificationBean.checkExistedByParams("notification", fromdate, todate, "0");
//				if (existed) {
//					rs = notificationBean.selectForSender(fromdate, todate, null, null, null);
//					// logMonitor("From " + DateUtil.dateToString(fromdate,
//					// DateUtil.FORMAT_DMHHMMSS2) + " to "
//					// + DateUtil.dateToString(todate, DateUtil.FORMAT_DMHHMMSS2) +
//					// " Total records: " + rs.size());
//				}
//			}
//			if (rs != null && rs.size() > 0) {
//				insertAndUpdate(rs);
//			}
//			cal = Calendar.getInstance();
//			cal.add(Calendar.SECOND, miDelayTime);
//			// logMonitor("Continue at " + DateUtil.dateToString(cal.getTime(),
//			// DateUtil.FORMAT_DMHHMMSS2));
//			for (int iIndex = 0; iIndex < miDelayTime
//					&& threadManager.miThreadCommand != ThreadConstant.THREAD_STOP; iIndex++) {
//				Thread.sleep(1000); // Time unit is second
//			}
//			threadManager.fillLogFile();
//		}
//	}
//
//	private void insertAndUpdate(List<Notification> rs) throws Exception {
//		String sequence = "";
//		List<Notification> listResult = new ArrayList<Notification>();
//
//		if (senderTables != null) {
//			for (String table : senderTables) {
//				List<Notification> listAfterReduce = new ArrayList<Notification>();
//				if (senderTables.contains("notification_email")) {
//					sequence = "notification_email_seq";
//					listAfterReduce = removeDuplicatedItems(rs, table);
//				} else if (senderTables.contains("notification_sms")) {
//					sequence = "notification_sms_seq";
//					listAfterReduce = removeDuplicatedItems(rs, table);
//				} else if (senderTables.contains("notification_onesignal")) {
//					sequence = "notification_onesignal_seq";
//					// if table is notification_onesignal, insert the list to
//					// notification_onesignal_seq
//					listAfterReduce = rs;
//				}
//
//				if (listAfterReduce.size() > 0) {
//					listResult = notificationBean.insertToNotificationOnesignal(listAfterReduce, table, sequence,
//							VarStatic.mapNotiTemplate);
//				}
//			}
//
//			List<Notification> lstExecuted = notificationBean.updateProcessStatus(listResult,
//					VarStatic.NOTI_STATUS_DRAFT, VarStatic.NOTI_STATUS_SEND);
//			for (Notification notification : lstExecuted) {
//				logMonitor("A=" + notification.getAction() + ",ref_id=" + notification.getDataRefId()
//						+ ",status=0>1,player_id=" + notification.getPlayerId() + ",msg="
//						+ StringUtil.removeAccent(notification.getMessage()));
//				Thread.sleep(50);
//			}
//		}
//	}
//
//	@Override
//	protected void afterSession() throws Exception {
//		notificationBean.close();
//		super.afterSession();
//		// scheduler.stop();
//	}
//
//	public List<Notification> removeDuplicatedItems(List<Notification> rs, String table) {
//		List<Notification> result = new ArrayList<Notification>();
//		Map<String, Notification> map = new HashMap<String, Notification>();
//
//		for (Notification item : rs) {
//			if ("notification_email".equals(table)) {
//				if (item.getEmail() == null) {
//					continue;
//				}
//				map.put(item.getEmail() + "/" + item.getId(), item);
//			} else if ("notification_sms".equals(table)) {
//				if (item.getPhone() == null) {
//					continue;
//				}
//				map.put(item.getPhone() + "/" + item.getId(), item);
//			}
//		}
//
//		if (map.values().size() > 0) {
//			result = new ArrayList<Notification>(map.values());
//		}
//
//		return result;
//	}
//}
