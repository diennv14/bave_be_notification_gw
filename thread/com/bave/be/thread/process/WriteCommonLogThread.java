package com.bave.be.thread.process;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import com.bave.be.entity.CommonLog;
import com.bave.be.entity.CommonLogDeserializer;
import com.fis.thread.ManageableThreadEx;
import com.fss.thread.ThreadConstant;

public class WriteCommonLogThread extends ManageableThreadEx {

	private String rootPath = "E:\\bave\\COMMON_LOG";
	private final String TOPIC = "commonLog";
	private final String BOOTSTRAP_SERVERS = "localhost:9092";

	@Override
	protected void beforeSession() throws Exception {
		super.beforeSession();
		super.beforeSession();

		rootPath = (getParameter("RootPath") != null) ? getParameter("RootPath").toString() : "";
	}

	@Override
	public void processSession() throws Exception {
		while (miThreadCommand != ThreadConstant.THREAD_STOP) {

			receiveLog();

			for (int iIndex = 0; iIndex < miDelayTime && miThreadCommand != ThreadConstant.THREAD_STOP; iIndex++) {
				Thread.sleep(1000); // Time unit is second
			}
			fillLogFile();
		}
	}

	@Override
	protected void afterSession() throws Exception {
		super.afterSession();
	}

	public void receiveLog() {
		try {
			// creating a consumer to receive messages
			KafkaConsumer<String, CommonLog> consumer = createConsumer();
			try {
				boolean noMessage = true;
				// while (noMessage) {
				consumer.subscribe(Collections.singletonList(TOPIC));
				ConsumerRecords<String, CommonLog> messages = consumer.poll(1000);
				if (messages.count() > 0) {
					noMessage = false;
				}
				for (ConsumerRecord<String, CommonLog> message : messages) {
					CommonLog commonLog = new CommonLog();
					commonLog = message.value();

					String logFolder = createFolder();

					Date date = new Date();
					Calendar cal = Calendar.getInstance();
					cal.setTime(date);
					int day = cal.get(Calendar.DAY_OF_MONTH);

					String logName = "CommonLog_" + day + ".txt";
					String filePath = logFolder + "/" + logName;

					File file = new File(rootPath + "/" + filePath);

					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY hh:mm:ss");
					String dateString = sdf.format(new Date());

					if (!file.exists()) {
						try {
							Writer writer = new BufferedWriter(new FileWriter(file));
							String content = commonLog.getContent();
							writer.write(dateString + ": " + content);
							writer.close();
							logMonitor("created file " + commonLog.getGaraName());

						} catch (IOException e) {
							e.printStackTrace();
						}
					} else {
						try (FileWriter fw = new FileWriter(rootPath + "/" + filePath, true);
								BufferedWriter bw = new BufferedWriter(fw);
								PrintWriter out = new PrintWriter(bw);) {
							out.println("\n" + dateString + ": " + commonLog.getContent());
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
				// }
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				consumer.close();
			}

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	private KafkaConsumer<String, CommonLog> createConsumer() {
		final Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, "CommonLogConsumer");
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, CommonLogDeserializer.class.getName());
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, CommonLogDeserializer.class.getName());

		// Create the consumer using props.
		final KafkaConsumer<String, CommonLog> consumer = new KafkaConsumer<>(props);

		// Subscribe to the topic.
		consumer.subscribe(Collections.singletonList(TOPIC));
		return consumer;
	}

	public String createFolder() {
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		month++;
		int day = cal.get(Calendar.DAY_OF_MONTH);

		String logFolder = year + "/" + month;
		File file = new File(rootPath + "/" + logFolder);
		if (!file.exists()) {
			file.mkdirs();
		}
		return logFolder;
	}
}
