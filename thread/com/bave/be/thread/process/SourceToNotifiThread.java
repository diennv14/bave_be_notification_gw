//package com.bave.be.thread.process;
//
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.List;
//
//import com.bave.be.bean.SourceToNotifiBean;
//import com.bave.be.thread.SourceToNotifiSplitManager;
//import com.bave.be.utils.DateUtil;
//import com.fis.thread.ThreadSplitBase;
//import com.fss.thread.ThreadConstant;
//
//public class SourceToNotifiThread extends ThreadSplitBase {
//
//	protected SourceToNotifiBean bean;
//	protected SourceToNotifiSplitManager threadManager;
//
//	@Override
//	protected void beforeSession() throws Exception {
//		setAutoConnectDb(true);
//		super.beforeSession();
//		threadManager = (SourceToNotifiSplitManager) mParentThreadManager;
//		bean = new SourceToNotifiBean();
//		bean.setConnection(mcnMain);
//		bean.init();
//	}
//
//	@Override
//	public void processSession(int index) throws Exception {
//		while (threadManager.miThreadCommand != ThreadConstant.THREAD_STOP) {
//			threadManager.resetCounter();
//			List<Notification> rs = null;
//			Calendar cal;
//			if (threadManager.firstLoad) {
//				Date fromdate = DateUtil.getOverDayUTC(threadManager.backDate, 00, 00, 00);
//				Date todate = DateUtil.getOverDayUTC(0, 23, 59, 59);
//				boolean existed = bean.countExisted(index, fromdate);
//				if (existed) {
//					rs = bean.selectOtherData(fromdate, todate, null, null, null, index);
////				logMonitor("From " + DateUtil.dateToString(fromdate, DateUtil.FORMAT_DMHHMMSS2) + " to "
////						+ DateUtil.dateToString(todate, DateUtil.FORMAT_DMHHMMSS2) + " Total records: " + rs.size());
//				}
//				threadManager.firstLoad = false;
//			} else {
//				Date fromdate = DateUtil.getOverDayUTC(threadManager.backDay, 00, 00, 00);
//				Date todate = DateUtil.getOverDayUTC(0, 23, 59, 59);
//				boolean existed = bean.countExisted(index, fromdate);
//				if (existed) {
//					rs = bean.selectOtherData(fromdate, todate, null, null, null, index);
////				logMonitor("From " + DateUtil.dateToString(fromdate, DateUtil.FORMAT_DMHHMMSS2) + " to "
////						+ DateUtil.dateToString(todate, DateUtil.FORMAT_DMHHMMSS2) + " Total records: " + rs.size());
//				}
//			}
//			if (rs != null && rs.size() > 0) {
//				insertNotifiAndUpdateData(rs);
//			}
//			cal = Calendar.getInstance();
//			cal.add(Calendar.SECOND, miDelayTime);
//			// logMonitor("Continue at " + DateUtil.dateToString(cal.getTime(),
//			// DateUtil.FORMAT_DMHHMMSS2));
//			for (int iIndex = 0; iIndex < miDelayTime
//					&& threadManager.miThreadCommand != ThreadConstant.THREAD_STOP; iIndex++) {
//				Thread.sleep(1000); // Time unit is second
//			}
//			threadManager.fillLogFile();
//		}
//	}
//
//	private void insertNotifiAndUpdateData(List<Notification> rs) throws Exception {
//		rs = bean.insertToNotification(rs);
//		for (Notification notification : rs) {
//			logMonitor("insert to notification action=" + notification.getAction() + ", ref_id="
//					+ notification.getDataRefId() + "");
//		}
//		List<Notification> lstExecuted = bean.updateStatusDataObject(new ArrayList<>(rs), "quotation_request");
//		for (Notification notification : lstExecuted) {
//			logMonitor("update to quotation action=" + notification.getAction() + ", ref_id="
//					+ notification.getDataRefId() + "");
//		}
//		lstExecuted = bean.updateStatusDataObject(new ArrayList<>(rs), "booking");
//		for (Notification notification : lstExecuted) {
//			logMonitor("update to quotation action=" + notification.getAction() + ", ref_id="
//					+ notification.getDataRefId() + "");
//		}
//	}
//
//	@Override
//	protected void afterSession() throws Exception {
//		bean.close();
//		super.afterSession();
//		// scheduler.stop();
//	}
//
//}
