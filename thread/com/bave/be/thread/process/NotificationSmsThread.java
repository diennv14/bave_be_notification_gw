//package com.bave.be.thread.process;
//
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import com.bave.backend.api.util.VarStatic;
//import com.bave.backend.persistence.entity.NotificationSms;
//import com.bave.backend.util.IRISSms;
//import com.bave.backend.util.ServiceClientUtil;
//import com.bave.be.bean.NotificationConfigBean;
//import com.bave.be.bean.NotificationSmsBean;
//import com.bave.be.thread.NotificationSmsSplitManager;
//import com.bave.be.utils.DateUtil;
//import com.bave.be.utils.StringUtil;
//import com.fis.thread.ThreadSplitBase;
//import com.fss.thread.ThreadConstant;
//
//public class NotificationSmsThread extends ThreadSplitBase {
//
//	private NotificationSmsBean notificationBean = null;
//	private NotificationConfigBean notiConfigBean = null;
//	protected NotificationSmsSplitManager threadManager;
//
//	@Override
//	protected void beforeSession() throws Exception {
//		logMonitor("Start service");
//		setAutoConnectDb(true);
//		super.beforeSession();
//		threadManager = (NotificationSmsSplitManager) mParentThreadManager;
//		super.beforeSession();
//
//		notificationBean = new NotificationSmsBean();
//		notificationBean.setConnection(mcnMain);
//		notificationBean.init();
//
//		notiConfigBean = new NotificationConfigBean();
//		notiConfigBean.setConnection(mcnMain);
//		notiConfigBean.init();
//
//		VarStatic.mapNotiConfig = notiConfigBean.loadNotiConfig();
//	}
//
//	@Override
//	public void processSession(int index) throws Exception {
//		try {
//			while (threadManager.miThreadCommand != ThreadConstant.THREAD_STOP) {
//				threadManager.resetCounter();
//				List<NotificationSms> rs = null;
//				Calendar cal;
//				if (threadManager.firstLoad) {
//					Date fromdate = DateUtil.getOverDay(threadManager.backDate, 00, 00, 00);
//					cal = Calendar.getInstance();
//					cal.add(Calendar.MINUTE, threadManager.backMinute);
//					Date todate = cal.getTime();
//					boolean existed = notificationBean.checkExistedByParams("notification_sms", fromdate, todate, "0");
//					if (existed) {
//						rs = notificationBean.getAllNotification(fromdate, todate, 1000);
//					}
//					threadManager.firstLoad = false;
//				} else {
//					Date fromdate = DateUtil.getOverDay(-1, 00, 00, 00);
//					cal = Calendar.getInstance();
//					cal.add(Calendar.MINUTE, threadManager.backMinute);
//					Date todate = cal.getTime();
//					boolean existed = notificationBean.checkExistedByParams("notification_sms", fromdate, todate, "0");
//					if (existed) {
//						rs = notificationBean.getAllNotification(fromdate, todate, 1000);
//					}
//				}
//				if (rs != null && rs.size() > 0) {
//					List<NotificationSms> lstUpdateStatus = notificationBean.updateProcessStatus(rs,
//							VarStatic.NOTI_STATUS_DRAFT, VarStatic.NOTI_STATUS_PROCESSING);
//
//					notifiAndUpdate(lstUpdateStatus);
//				}
//				cal = Calendar.getInstance();
//				cal.add(Calendar.SECOND, miDelayTime);
//				for (int iIndex = 0; iIndex < miDelayTime
//						&& threadManager.miThreadCommand != ThreadConstant.THREAD_STOP; iIndex++) {
//					Thread.sleep(1000); // Time unit is second
//				}
//				threadManager.fillLogFile();
//			}
//		} catch (Exception e) {
//			logMonitor("1st load: " + e.toString());
//		}
//	}
//
//	private void notifiAndUpdate(List<NotificationSms> rs) throws Exception {
//		List<NotificationSms> lstPostOneSingal = new ArrayList<>();
//		for (NotificationSms notificationSms : rs) {
//			if (VarStatic.NOTI_STATUS_PROCESSING.equals(notificationSms.getStatus())) {
//				/* sending sms */
//				// SMSAction sms = new SMSAction();
//				// sms.setPhone(notificationSms.getPhone() != null ?
//				// notificationSms.getPhone() : "0");
//				// sms.setMessage(notificationSms.getMessage() != null
//				// ? StringUtil.removeAccent(notificationSms.getMessage()) :
//				// "");
//				//
//				// NotificationConfig notiConfig =
//				// VarStatic.mapNotiConfig.get(notificationSms.getAction() +
//				// "_sms");
//				//
//				// String result = "";
//				//
//				// if (notiConfig != null && notiConfig.getApiKey() != null &&
//				// notiConfig.getAppKey() != null) {
//				// result = sms.sendGetJSON(notiConfig.getApiKey(),
//				// notiConfig.getAppKey());
//				// }
//				String phone = notificationSms.getPhone() != null ? notificationSms.getPhone() : "0";
//				String message = notificationSms.getMessage() != null
//						? StringUtil.removeAccent(notificationSms.getMessage()) : "";
//
//				String result = IRISSms.sendOneMessage(phone, message);
//
//				// sending message successfully
//				if ("201".equals(result)) {
//					notificationSms.setStatus(VarStatic.NOTI_STATUS_SEND);
//					callBackUrl(notificationSms.getCallBackUrl(), true, "Thành công", notificationSms.getType());
//				} else {
//					logMonitor(result + " , message: " + message + ", phone: " + phone);
//					notificationSms.setStatus(VarStatic.NOTI_STATUS_FAILED);
//					callBackUrl(notificationSms.getCallBackUrl(), false, result, notificationSms.getType());
//				}
//
//				lstPostOneSingal.add(notificationSms);
//			}
//		}
//		List<NotificationSms> lstExecuted = notificationBean.updateProcessStatus(lstPostOneSingal,
//				VarStatic.NOTI_STATUS_PROCESSING, VarStatic.NOTI_STATUS_SEND);
//		for (NotificationSms notification : lstExecuted) {
//			logMonitor("A=" + notification.getAction() + ",ref_id=" + notification.getDestinationRefId() + ",status=0>"
//					+ notification.getStatus() + ",phone=" + notification.getPhone() + ",msg="
//					+ StringUtil.removeAccent(notification.getMessage()));
//		}
//	}
//
//	public void callBackUrl(String url, boolean success, String failReason, String type) {
//		ServiceClientUtil.setUrl(url);
//		Map<String, Object> mapData = new HashMap<>();
//		mapData.put("result", success ? "1" : "0");
//		mapData.put("message", failReason);
//
//		ServiceClientUtil.updateSeenNotif(mapData, type);
//	}
//
//	@Override
//	protected void afterSession() throws Exception {
//		notificationBean.close();
//		super.afterSession();
//	}
//
//}
