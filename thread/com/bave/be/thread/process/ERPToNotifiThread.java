//package com.bave.be.thread.process;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Collections;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Properties;
//import java.util.Set;
//import java.util.concurrent.ExecutionException;
//
//import org.apache.commons.beanutils.BeanUtils;
//import org.apache.kafka.clients.consumer.ConsumerConfig;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.apache.kafka.clients.consumer.ConsumerRecords;
//import org.apache.kafka.clients.consumer.KafkaConsumer;
//import org.apache.kafka.clients.producer.KafkaProducer;
//import org.apache.kafka.clients.producer.Producer;
//import org.apache.kafka.clients.producer.ProducerConfig;
//import org.apache.kafka.clients.producer.ProducerRecord;
//
//import com.bave.backend.api.util.VarStatic;
//import com.bave.backend.persistence.entity.Notification;
//import com.bave.backend.persistence.entity.NotificationEmail;
//import com.bave.backend.persistence.entity.NotificationOneSignal;
//import com.bave.backend.persistence.entity.NotificationSms;
//import com.bave.backend.util.BEUtils;
//import com.bave.backend.util.DateUtil;
//import com.bave.backend.util.StringUtil;
//import com.bave.be.bean.ERPToNotifiBean;
//import com.bave.be.bean.NotificationBean;
//import com.bave.be.bean.NotificationEmailBean;
//import com.bave.be.bean.NotificationOnesignalBean;
//import com.bave.be.bean.NotificationSmsBean;
//import com.bave.be.entity.User;
//import com.bave.be.entity.serialz.NotificationDeserializer;
//import com.bave.be.entity.serialz.NotificationOneSignalSerializer;
//import com.bave.be.thread.ERPToNotifiSplitManager;
//import com.bave.be.thread.util.EntityAnnotation;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fis.thread.ThreadSplitBase;
//import com.fss.thread.ThreadConstant;
//
//public class ERPToNotifiThread extends ThreadSplitBase {
//
//	protected ERPToNotifiBean bean;
//	protected ERPToNotifiSplitManager threadManager;
//
//	protected NotificationBean notiBean = null;
//	protected NotificationEmailBean notiEmailBean = null;
//	protected NotificationSmsBean notiSmsBean = null;
//	protected NotificationOnesignalBean notiOneSignalBean = null;
//
//	private EntityAnnotation entityAnnotation = null;
//	private EntityAnnotation entityAnnotationSMS = null;
//	private EntityAnnotation entityAnnotationEmail = null;
//	private EntityAnnotation entityAnnotationOnesignal = null;
//
//	private ObjectMapper objectMapper = new ObjectMapper();
//	private Set<String> senderTables = null;
//
//	private final static String TOPIC = "erpNotification";
//	private final static String BOOTSTRAP_SERVERS = VarStatic.kafka_bootstrap_server;
//
//	protected String notify_send_topic = "notify_send_topic";
//	protected String notify_send_group = "notify_send_group";
//	protected Producer<String, Notification> producerSend = createProducerTopic(notify_send_topic);
//
//	@Override
//	protected void beforeSession() throws Exception {
//		setAutoConnectDb(true);
//		super.beforeSession();
//		threadManager = (ERPToNotifiSplitManager) mParentThreadManager;
//		bean = new ERPToNotifiBean();
//		bean.setConnection(mcnMain);
//		bean.init();
//
//		notiBean = new NotificationBean();
//		notiBean.setConnection(mcnMain);
//		notiBean.init();
//
//		notiEmailBean = new NotificationEmailBean();
//		notiEmailBean.setConnection(mcnMain);
//		notiEmailBean.init();
//
//		notiSmsBean = new NotificationSmsBean();
//		notiSmsBean.setConnection(mcnMain);
//		notiSmsBean.init();
//
//		notiOneSignalBean = new NotificationOnesignalBean();
//		notiOneSignalBean.setConnection(mcnMain);
//		notiOneSignalBean.init();
//
//		entityAnnotation = (EntityAnnotation) Notification.class.getAnnotation(EntityAnnotation.class);
//		entityAnnotationSMS = (EntityAnnotation) NotificationSms.class.getAnnotation(EntityAnnotation.class);
//		entityAnnotationEmail = (EntityAnnotation) NotificationEmail.class.getAnnotation(EntityAnnotation.class);
//		entityAnnotationOnesignal = (EntityAnnotation) NotificationOneSignal.class
//				.getAnnotation(EntityAnnotation.class);
//
//		if (!StringUtil.isEmpty(threadManager.sender)) {
//			String[] arrSenderTables = threadManager.sender.split(";");
//			senderTables = new HashSet<>();
//			for (String table : arrSenderTables) {
//				senderTables.add(table);
//			}
//			logMonitor("sender:" + objectMapper.writeValueAsString(arrSenderTables));
//		}
//
//	}
//
//	@Override
//	public void processSession(int index) throws Exception {
//		List<Notification> lstInsert = new ArrayList<>();
//
//		while (threadManager.miThreadCommand != ThreadConstant.THREAD_STOP) {
//			threadManager.resetCounter();
//
//			if (senderTables == null) {
//				logMonitor("No Sender");
//				Thread.sleep(30000);
//				continue;
//			}
//
//			// creating a consumer to receive messages
//			KafkaConsumer<String, Notification> consumer = createConsumer();
//			try {
//				boolean noMessage = true;
//				// while (noMessage) {
//				consumer.subscribe(Collections.singletonList(TOPIC));
//
//				ConsumerRecords<String, Notification> messages = consumer.poll(1000);
//				if (messages.count() > 0) {
//					noMessage = false;
//				}
//				for (ConsumerRecord<String, Notification> message : messages) {
//					Notification notification = new Notification();
//					notification = message.value();
//
//					logMonitor(StringUtil.removeAccent(objectMapper.writeValueAsString(notification)));
//					System.out.println(objectMapper.writeValueAsString(notification));
//
//					if ("sms".equals(notification.getType()) || "one_signal".equals(notification.getType())) {
//						lstInsert.add(notification);
//					} else {
//						for (String dataRefId : notification.getListDataRefId()) {
//							Notification noti = new Notification();
//							BeanUtils.copyProperties(noti, notification);
//							noti.setDataRefId(dataRefId);
//							lstInsert.add(noti);
//							sendMessageToSuccessTopic(noti);
//						}
//					}
//				}
//				// }
//			} catch (Exception e) {
//				e.printStackTrace();
//			} finally {
//				consumer.close();
//			}
//
//			if (lstInsert.size() > 0) {
//				insertNotification(lstInsert);
//				lstInsert.clear();
//				threadManager.fillLogFile();
//			}
//
//			for (int iIndex = 0; iIndex < miDelayTime
//					&& threadManager.miThreadCommand != ThreadConstant.THREAD_STOP; iIndex++) {
//				Thread.sleep(1000); // Time unit is second
//			}
//			threadManager.fillLogFile();
//		}
//	}
//
//	private void insertNotification(List<Notification> lstNotification) throws Exception {
//		List<NotificationEmail> lstNotiEmail = new ArrayList<NotificationEmail>();
//		List<NotificationSms> lstNotiSms = new ArrayList<NotificationSms>();
//		List<NotificationOneSignal> lstNotiOneSignal = new ArrayList<NotificationOneSignal>();
//
//		for (Notification notification : lstNotification) {
//			if ("sms".equals(notification.getType()) || "one_signal".equals(notification.getType())) {
//				notification.setDestinationObject(VarStatic.DESTINATION_OBJECT_CAROWNER);
//				continue;
//			}
//			String action = (notification.getDataObject() != null ? notification.getDataObject().trim() : "") + "_"
//					+ (notification.getAction() != null ? notification.getAction().trim() : "");
//
//			if (action.equals("fleet.repair_create")) {
//				notification.setAction("create_repair");
//			} else if (action.equals("sale.order_confirm") || action.equals("sale.order_done")) {
//				notification.setAction("saleorder_update");
//			} else if (action.equals("fleet.workorder_write")) {
//				notification.setAction("workorder_completed");
//			}
//
//			if (notification.getListUser() != null && notification.getListUser().size() > 0) {
//				notification.setDestinationObject(VarStatic.DESTINATION_OBJECT_GARA);
//			} else if (notification.getListPartner() != null && notification.().size() > 0) {
//				notification.setDestinationObject(VarStatic.DESTINATION_OBJECT_CAROWNER);
//			} else {
//				logMonitor("No DestinationObject:"
//						+ StringUtil.removeAccent(objectMapper.writeValueAsString(notification)));
//			}
//
//			// insert notification.
//			// setting value again for dataRefId
//			notification.setCreateDate(DateUtil.now());
//			notification.setStatus(VarStatic.NOTI_STATUS_PROCESSING);
//
//			// removing agara.vn
//			notification.setDomain(BEUtils.removeMainDomain(notification.getDomain(), ".agara.vn"));
//
//			Long notificationId = bean.insert(notification, entityAnnotation.fields_insert());
//			// updating unread notify
//			bean.executeSqlQuery("update service_provider set unread_notify = unread_notify + 1 where code = '"
//					+ notification.getDomain() + "'");
//
//			// get list user
//			List<User> listUser = notification.getListUser();
//			Set<String> lstOnlineUser = notiOneSignalBean.getAllUserOnlineOfGara(notification.getDomain());
//			List<String> listForCheckDuplicatedPlayId = new ArrayList<String>();
//
//			if (VarStatic.DESTINATION_OBJECT_GARA.equals(notification.getDestinationObject())) {
//				for (User user : listUser) {
//					if (user.getChannel() == null) {
//						continue;
//					}
//
//					if (user.getChannel().equals(VarStatic.CHANNEL_EMAIL)
//							&& senderTables.contains(entityAnnotationEmail.table_name())) {
//						NotificationEmail notiEmail = createNotiEmail(notification, user, user.getCallbackUrl());
//						notiEmail.setNotificationId(notificationId);
//						lstNotiEmail.add(notiEmail);
//
//					} else if ((user.getChannel().equals("app")
//							|| VarStatic.CHANNEL_ONESIGNAL.equals(user.getChannel()))
//							&& senderTables.contains(entityAnnotationOnesignal.table_name())) {
//						// one signal
//						List<String> lstAddress = user.getListAddress();
//						listForCheckDuplicatedPlayId.addAll(lstAddress);
//
//						for (String address : lstAddress) {
//							NotificationOneSignal notiOneSignal = createNotiOneSignal(notification, address,
//									user.getCallbackUrl());
//							notiOneSignal.setNotificationId(notificationId);
//							lstNotiOneSignal.add(notiOneSignal);
//						}
//					}
//				}
//			}
//
//			// adding online users to lstNotiOneSignal
//			for (String playerId : lstOnlineUser) {
//				if (!listForCheckDuplicatedPlayId.contains(playerId)) {
//					NotificationOneSignal notiOneSignal = createNotiOneSignal(notification, playerId, "");
//					notiOneSignal.setNotificationId(notificationId);
//					lstNotiOneSignal.add(notiOneSignal);
//				}
//			}
//
//			// get list partner
//			List<User> listPartner = notification.getListPartner();
//
//			if (VarStatic.DESTINATION_OBJECT_CAROWNER.equals(notification.getDestinationObject())) {
//				for (User user : listPartner) {
//					if (user.getChannel() == null) {
//						continue;
//					}
//
//					if (VarStatic.CHANNEL_EMAIL.equals(user.getChannel())
//							&& senderTables.contains(entityAnnotationEmail.table_name())) {
//						NotificationEmail notiEmail = createNotiEmail(notification, user, user.getCallbackUrl());
//						notiEmail.setNotificationId(notificationId);
//						lstNotiEmail.add(notiEmail);
//
//					} else if (VarStatic.CHANNEL_SMS.equals(user.getChannel())
//							&& senderTables.contains(entityAnnotationSMS.table_name())
//							&& notification.getAction().equals("workorder_completed")) {
//						NotificationSms notiSms = createNotiSms(notification, user, user.getCallbackUrl());
//						notiSms.setNotificationId(notificationId);
//						lstNotiSms.add(notiSms);
//
//					} else if ((user.getChannel().equals("app")
//							|| VarStatic.CHANNEL_ONESIGNAL.equals(user.getChannel()))
//							&& senderTables.contains(entityAnnotationOnesignal.table_name())) {
//						List<String> lstAddress = user.getListAddress();
//
//						for (String address : lstAddress) {
//							NotificationOneSignal notiOneSignal = createNotiOneSignal(notification, address,
//									user.getCallbackUrl());
//							notiOneSignal.setNotificationId(notificationId);
//							lstNotiOneSignal.add(notiOneSignal);
//						}
//					}
//				}
//			}
//		}
//
//		for (Notification notification : lstNotification) {
//
//			notification.setCreateDate(DateUtil.now());
//			notification.setStatus(VarStatic.NOTI_STATUS_SEND);
//
//			// removing agara.vn
//			notification.setDomain(BEUtils.removeMainDomain(notification.getDomain(), ".agara.vn"));
//
//			String phone = BEUtils.convertPhone(notification.getPhone());
//			Long memberId = notiOneSignalBean.getMemberIdByPhone(phone);
//			Long notificationId = null;
//			if (memberId != null) {
//				notification.setDestinationObject(VarStatic.DESTINATION_OBJECT_CAROWNER);
//				notification.setDestinationRefId(memberId);
//				notificationId = bean.insert(notification, entityAnnotation.fields_insert());
//				bean.executeSqlQuery(" update member set unread_notify = unread_notify + 1 where id = " + memberId);
//			}
//			if ("sms".equals(notification.getType())) {
//				NotificationSms notiSms = createNotiSms(notification, null, notification.getCallBackUrl());
//				notiSms.setNotificationId(notificationId);
//				notiSms.setCreateDate(DateUtil.now());
//				lstNotiSms.add(notiSms);
//
//			} else if ("one_signal".equals(notification.getType())) {
//				List<String> playerIds = notiOneSignalBean.getPlayerIdsByPhone(phone);
//				for (String playerId : playerIds) {
//					NotificationOneSignal notiOneSignal = createNotiOneSignal(notification, playerId,
//							notification.getCallBackUrl());
//					notiOneSignal.setNotificationId(notificationId);
//					notiOneSignal.setCreateDate(DateUtil.now());
//					lstNotiOneSignal.add(notiOneSignal);
//				}
//
//				if (playerIds.size() == 0) {
//					NotificationOneSignal notiOneSignal = createNotiOneSignal(notification, "",
//							notification.getCallBackUrl());
//					notiOneSignal.setNotificationId(notificationId);
//					notiOneSignal.setCreateDate(DateUtil.now());
//					lstNotiOneSignal.add(notiOneSignal);
//				}
//			}
//		}
//
//		// insert a batch of notificationSms
//		bean.insertBatch(lstNotiSms, entityAnnotationSMS.table_name(),
//				Arrays.asList(entityAnnotationSMS.fields_insert()), entityAnnotationSMS.pk_name(),
//				entityAnnotationSMS.seq_name());
//
//		// insert a batch of notificationEmail
//		bean.insertBatch(lstNotiEmail, entityAnnotationEmail.table_name(),
//				Arrays.asList(entityAnnotationEmail.fields_insert()), entityAnnotationEmail.pk_name(),
//				entityAnnotationEmail.seq_name());
//
//		// insert a batch of notificationOneSignal
//		bean.insertBatch(lstNotiOneSignal, entityAnnotationOnesignal.table_name(),
//				Arrays.asList(entityAnnotationOnesignal.fields_insert()), entityAnnotationOnesignal.pk_name(),
//				entityAnnotationOnesignal.seq_name());
//
//	}
//
//	@Override
//	protected void afterSession() throws Exception {
//		bean.close();
//		super.afterSession();
//	}
//
//	public NotificationSms createNotiSms(Notification notification, User user, String callBackUrl) {
//		NotificationSms notiSms = new NotificationSms();
//		try {
//			// copy from notification to notiSms
//			BeanUtils.copyProperties(notiSms, notification);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		if (user != null) {
//			String phone = user.getListAddress().size() > 0 ? user.getListAddress().get(0) : "";
//			notiSms.setPhone(phone);
//		}
//		notiSms.setDataObject(notification.getDataObject() != null ? notification.getDataObject() : "");
//		notiSms.setDataRefId(notification.getDataRefId() != null ? notification.getDataRefId() : "");
//		notiSms.setDataName(notification.getDataName() != null ? notification.getDataName() : "");
//		notiSms.setCallBackUrl(callBackUrl);
//		notiSms.setStatus(VarStatic.SOURCETONOTI_STATUS_DRAFT);
//
//		return notiSms;
//	}
//
//	public NotificationOneSignal createNotiOneSignal(Notification notification, String playerId, String callBackUrl) {
//		NotificationOneSignal notiOneSignal = new NotificationOneSignal();
//
//		try {
//			// copy from notification to notiOneSignal
//			BeanUtils.copyProperties(notiOneSignal, notification);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		notiOneSignal.setDataObject(notification.getDataObject() != null ? notification.getDataObject() : "");
//		notiOneSignal.setDataRefId(notification.getDataRefId() != null ? notification.getDataRefId() : "");
//		notiOneSignal.setDataName(notification.getDataName() != null ? notification.getDataName() : "");
//		notiOneSignal.setPlayerId(playerId);
//		notiOneSignal.setCallBackUrl(callBackUrl);
//		notiOneSignal.setStatus(VarStatic.SOURCETONOTI_STATUS_DRAFT);
//
//		return notiOneSignal;
//	}
//
//	public NotificationEmail createNotiEmail(Notification notification, User user, String callBackUrl) {
//		NotificationEmail notiEmail = new NotificationEmail();
//
//		try {
//			// copy from notification to notiEmail
//			BeanUtils.copyProperties(notiEmail, notification);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//		String email = user.getListAddress().size() > 0 ? user.getListAddress().get(0) : "";
//		notiEmail.setDataObject(notification.getDataObject() != null ? notification.getDataObject() : "");
//		notiEmail.setDataRefId(notification.getDataRefId() != null ? notification.getDataRefId() : "");
//		notiEmail.setDataName(notification.getDataName() != null ? notification.getDataName() : "");
//		notiEmail.setEmail(email);
//		notiEmail.setTitle(notification.getAdditionData());
//		notiEmail.setCallBackUrl(callBackUrl);
//		notiEmail.setStatus(VarStatic.SOURCETONOTI_STATUS_DRAFT);
//
//		return notiEmail;
//	}
//
//	private static KafkaConsumer<String, Notification> createConsumer() {
//		final Properties props = new Properties();
//		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
//		props.put(ConsumerConfig.GROUP_ID_CONFIG, "KafkaNotifiConsumer");
//		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, NotificationDeserializer.class.getName());
//		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, NotificationDeserializer.class.getName());
//
//		// Create the consumer using props.
//		final KafkaConsumer<String, Notification> consumer = new KafkaConsumer<>(props);
//
//		// Subscribe to the topic.
//		consumer.subscribe(Collections.singletonList(TOPIC));
//		return consumer;
//	}
//
//	private Producer<String, Notification> createProducerTopic(String topic) {
//		Properties props = new Properties();
//		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
//		props.put(ProducerConfig.CLIENT_ID_CONFIG, topic);
//		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, NotificationOneSignalSerializer.class.getName());
//		props.put("value.serializer", NotificationOneSignalSerializer.class.getName());
//		return new KafkaProducer<>(props);
//	}
//
//	public void sendMessageToSuccessTopic(Notification notification) {
//		try {
//			final ProducerRecord<String, Notification> record = new ProducerRecord<String, Notification>(
//					notify_send_topic, notification);
//			producerSend.send(record).get();
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		} catch (ExecutionException e) {
//			e.printStackTrace();
//		} finally {
//			producerSend.flush();
//			producerSend.close();
//		}
//	}
//
//}
