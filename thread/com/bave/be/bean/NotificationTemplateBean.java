package com.bave.be.bean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.bave.be.entity.NotificationTemplate;
import com.bave.be.thread.util.AbstractBean;
import com.bave.be.utils.ResultSetConverter;
import com.fss.sql.Database;

public class NotificationTemplateBean extends AbstractBean {

	private PreparedStatement pstm;
	private String strSelectTemplate;

	@Override
	public void init() throws SQLException {

		strSelectTemplate = " select * from notification_template where active = true ";

	}

	@Override
	public void close() {
		Database.closeObject(pstm);
	}

	public Map<String, NotificationTemplate> selectNotificationTemplate() throws Exception {
		Map<String, NotificationTemplate> mapRs = new HashMap<>();
		try {
			pstm = mcnMain.prepareStatement(strSelectTemplate);
			ResultSet resultSet = pstm.executeQuery();
			while (resultSet.next()) {
				NotificationTemplate template = ResultSetConverter.convertToObject(resultSet,
						NotificationTemplate.class);
				mapRs.put(template.getChannel() + "_" + template.getAction(), template);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
		return mapRs;
	}

}