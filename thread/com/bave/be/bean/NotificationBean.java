package com.bave.be.bean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.bave.backend.persistence.entity.Notification;
import com.bave.backend.security.config.VarStatic;
import com.bave.be.entity.NotificationTemplate;
import com.bave.be.thread.util.AbstractBean;
import com.bave.be.thread.util.BuildPSTM;
import com.bave.be.thread.util.TemplateParser;
import com.bave.be.utils.DateUtil;
import com.bave.be.utils.ResultSetConverter;
import com.bave.be.utils.StringUtil;
import com.fss.sql.Database;

public class NotificationBean extends AbstractBean {

	private PreparedStatement pstm;
	private PreparedStatement pstmUpdate;
	private String strCountForSender;
	private String strSelectForSender;

	@Override
	public void init() throws SQLException {
		strCountForSender = " select count(*) total from notification where status = '0' and create_date >= ? ";

		strSelectForSender = " SELECT n.id as id, n.member_id,0 as not_notify_id,n.message as message,n.status as status,n.type as type,n.action as action,n.create_date as create_date,n.source_object as source_object,n.source_ref_id as source_ref_id, ";
		strSelectForSender += " n.destination_object as destination_object,n.destination_ref_id as destination_ref_id,n.source_name as source_name,null as source_avatar, n.dest_name as dest_name, null as dest_avatar, ";
		strSelectForSender += " null as dest_shortcontent, '0' as onesignal_status, n.domain as domain, euo.player_id as player_id,n.id as notification_id ";
		strSelectForSender += " ,n.data_object as data_object,n.data_ref_id as data_ref_id,n.data_name as data_name ";
		strSelectForSender += " ,null as phone,null as email ";
		strSelectForSender += " ,n.template_id as template_id";
		strSelectForSender += " from notification n ";
		strSelectForSender += " inner join erp_user_online euo on euo.domain = n.domain and euo.status = '1' ";
		strSelectForSender += " where n.action = 'quotation_request' and n.status = '0' and (euo.player_id is not null) ";
		strSelectForSender += " and n.create_date >= ? ";
		strSelectForSender += " UNION ALL ";
		strSelectForSender += " SELECT n.id as id, n.member_id,0 as not_notify_id,n.message as message,n.status as status,n.type as type,n.action as action,n.create_date as create_date,n.source_object as source_object,n.source_ref_id as source_ref_id, ";
		strSelectForSender += " n.destination_object as destination_object,n.destination_ref_id as destination_ref_id,n.source_name as source_name,null as source_avatar, n.dest_name as dest_name, null as dest_avatar, ";
		strSelectForSender += " null as dest_shortcontent, '0' as onesignal_status, n.domain as domain, md.player_id as player_id,n.id as notification_id ";
		strSelectForSender += " ,n.data_object as data_object,n.data_ref_id as data_ref_id,n.data_name as data_name ";
		strSelectForSender += " ,mb.mobile_phone as phone,mb.email ";
		strSelectForSender += " ,n.template_id as template_id";
		strSelectForSender += " from notification n ";
		strSelectForSender += " inner join member_device md on n.member_id = md.member_id ";
		strSelectForSender += " inner join member mb on md.member_id = mb.id ";
		strSelectForSender += " where n.action = 'quotation_response' and n.status = '0' and (md.player_id is not null) ";
		strSelectForSender += " and n.create_date >= ? ";
		strSelectForSender += " UNION ALL ";
		strSelectForSender += " SELECT n.id as id, n.member_id,0 as not_notify_id,n.message as message,n.status as status,n.type as type,n.action as action,n.create_date as create_date,n.source_object as source_object,n.source_ref_id as source_ref_id, ";
		strSelectForSender += " n.destination_object as destination_object,n.destination_ref_id as destination_ref_id,n.source_name as source_name,null as source_avatar, n.dest_name as dest_name, null as dest_avatar, ";
		strSelectForSender += " null as dest_shortcontent, '0' as onesignal_status, n.domain as domain, euo.player_id as player_id,n.id as notification_id ";
		strSelectForSender += " ,n.data_object as data_object,n.data_ref_id as data_ref_id,n.data_name as data_name ";
		strSelectForSender += " ,null as phone,null as email ";
		strSelectForSender += " ,n.template_id as template_id";
		strSelectForSender += " from notification n ";
		strSelectForSender += " inner join erp_user_online euo on euo.domain = n.domain and euo.status = '1' ";
		strSelectForSender += " where n.action = 'quotation_cus_confirm' and n.status = '0' and (euo.player_id is not null) ";
		strSelectForSender += " and n.create_date >= ? ";
		strSelectForSender += " UNION ALL ";
		strSelectForSender += " SELECT n.id as id, n.member_id,0 as not_notify_id,n.message as message,n.status as status,n.type as type,n.action as action,n.create_date as create_date,n.source_object as source_object,n.source_ref_id as source_ref_id, ";
		strSelectForSender += " n.destination_object as destination_object,n.destination_ref_id as destination_ref_id,n.source_name as source_name,null as source_avatar, n.dest_name as dest_name, null as dest_avatar, ";
		strSelectForSender += " null as dest_shortcontent, '0' as onesignal_status, n.domain as domain, euo.player_id as player_id,n.id as notification_id ";
		strSelectForSender += " ,n.data_object as data_object,n.data_ref_id as data_ref_id,n.data_name as data_name ";
		strSelectForSender += " ,null as phone,null as email ";
		strSelectForSender += " ,n.template_id as template_id";
		strSelectForSender += " from notification n ";
		strSelectForSender += " inner join erp_user_online euo on euo.domain = n.domain and euo.status = '1' ";
		strSelectForSender += " where n.action = 'booking' and n.status = '0' and (euo.player_id is not null) ";
		strSelectForSender += " and n.create_date >= ? ";

	}

	@Override
	public void close() {
		Database.closeObject(pstmUpdate);
		Database.closeObject(pstm);
	}

//	public List<Notification> getAllNotification(Date fromDate, Date toDate, Integer limit) throws Exception {
//		List<Notification> lstRs = new ArrayList<Notification>();
//		try {
//			pstm = mcnMain.prepareStatement(strSelectNotification);
//			pstm.setTimestamp(1, DateUtil.utilLongDateToSqlDate(fromDate));
//			// pstm.setTimestamp(1,
//			// Timestamp.valueOf(DateUtil.dateToString(fromDate,
//			// DateUtil.FORMAT_SQLDATE2)));
//			// pstm.setTimestamp(2,
//			// Timestamp.valueOf(DateUtil.dateToString(toDate,
//			// DateUtil.FORMAT_SQLDATE2)));
//			ResultSet resultSet = pstm.executeQuery();
//			while (resultSet.next()) {
//				Notification notification = ResultSetConverter.convertToObject(resultSet, Notification.class);
//				lstRs.add(notification);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw e;
//		} finally {
//			Database.closeObject(pstm);
//		}
//		return lstRs;
//	}

	public List<Notification> selectForSender(Date fromDate, Date toDate, Integer limit, String state,
			String notifiStatus) throws Exception {
		List<Notification> lstRs = new ArrayList<Notification>();
		try {
			pstm = mcnMain.prepareStatement(strSelectForSender);
			pstm.setTimestamp(1, Timestamp.valueOf(DateUtil.dateToString(fromDate, DateUtil.FORMAT_SQLDATE2)));
			pstm.setTimestamp(2, Timestamp.valueOf(DateUtil.dateToString(fromDate, DateUtil.FORMAT_SQLDATE2)));
			pstm.setTimestamp(3, Timestamp.valueOf(DateUtil.dateToString(fromDate, DateUtil.FORMAT_SQLDATE2)));
			pstm.setTimestamp(4, Timestamp.valueOf(DateUtil.dateToString(fromDate, DateUtil.FORMAT_SQLDATE2)));
			ResultSet resultSet = pstm.executeQuery();
			while (resultSet.next()) {
				Notification notification = ResultSetConverter.convertToObject(resultSet, Notification.class);
				lstRs.add(notification);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
		return lstRs;
	}

	public List<Notification> updateProcessStatus(List<Notification> lstNotification, String oldStatus,
			String newStatus) throws Exception {
		List<Notification> lstExecuted = new ArrayList<>();
		List<Notification> lstAddBatch = new ArrayList<>();
		try {
			mcnMain.setAutoCommit(false);
			int commit = 0;
			List<Object> lstValue = new ArrayList<>();
			BuildPSTM buildPSTM = buildPreparedUpdate(pstmUpdate, "notification", Arrays.asList("status"),
					Arrays.asList("status", "id"), lstValue);
			for (Notification notification : lstNotification) {
				if (buildPSTM != null) {
					Object[] arrValueObject = new Object[] { newStatus, oldStatus, notification.getId() };
					pstmUpdate = updateEntity(buildPSTM.getPstm(), arrValueObject, null, true);
					lstAddBatch.add(notification);
					commit++;
					if (commit == VarStatic.COMMIT_SIZE) {
						pstmUpdate.executeBatch();
						mcnMain.commit();
						if (lstAddBatch.size() > 0) {
							lstExecuted.addAll(lstAddBatch);
							lstAddBatch.clear();
						}
					}
				}
			}
			if (lstAddBatch.size() > 0) {
				pstmUpdate.executeBatch();
				mcnMain.commit();
				lstExecuted.addAll(lstAddBatch);
				lstAddBatch.clear();
			}
			return lstExecuted;
		} catch (Exception e) {
			mcnMain.rollback();
			e.printStackTrace();
			throw e;
		} finally {
			mcnMain.setAutoCommit(true);
			Database.closeObject(pstm);
		}
	}

	public List<Notification> insertToNotificationOnesignal(List<Notification> lstNotification, String tableName,
			String sequenceName, Map<String, NotificationTemplate> mapTemplate) throws Exception {
		List<Notification> lstExecuted = new ArrayList<>();
		List<Notification> lstAddBatch = new ArrayList<>();
		try {
			mcnMain.setAutoCommit(false);
			int commit = 0;
			List<String> columns = new ArrayList<String>();

			String setValueAfter = "notification_id";
			if ("notification_onesignal".equals(tableName)) {
				// if table is notification_onesignal, add column "player_id"
				columns = Arrays.asList("id", "member_id", "not_notify_id", "message", "status", "type", "action",
						"create_date", "source_object", "source_ref_id", "destination_object", "destination_ref_id",
						"source_name", "source_avatar", "dest_name", "dest_avatar", "dest_shortcontent",
						"onesignal_status", "domain", "player_id", "data_object", "data_ref_id", "data_name",
						"template_id", setValueAfter);
			} else if ("notification_email".equals(tableName)) {
				columns = Arrays.asList("id", "member_id", "not_notify_id", "message", "status", "type", "action",
						"create_date", "source_object", "source_ref_id", "destination_object", "destination_ref_id",
						"source_name", "source_avatar", "dest_name", "dest_avatar", "dest_shortcontent",
						"onesignal_status", "domain", "email", "data_object", "data_ref_id", "data_name", "template_id",
						setValueAfter);
			} else if ("notification_sms".equals(tableName)) {
				columns = Arrays.asList("id", "member_id", "not_notify_id", "message", "status", "type", "action",
						"create_date", "source_object", "source_ref_id", "destination_object", "destination_ref_id",
						"source_name", "source_avatar", "dest_name", "dest_avatar", "dest_shortcontent",
						"onesignal_status", "domain", "phone", "data_object", "data_ref_id", "data_name", "template_id",
						setValueAfter);
			}

			BuildPSTM buildPSTM = buildPreparedInsert(pstmUpdate, tableName, columns, "id", sequenceName);

			for (Notification notification : lstNotification) {
				// cteate msg by template
				boolean isContinue = ignoreAddressNull(notification, tableName);
				if (isContinue) {
					continue;
				}

				notification = templateParser(notification, tableName, mapTemplate);

				if (buildPSTM != null) {
					List<Object> lstValueObject = ResultSetConverter.getAllValuesByLstName(columns, notification, "id");
					if (setValueAfter != null) {
						int rIndex = columns.indexOf("notification_id");
						if (rIndex >= 0) {
							lstValueObject.set(rIndex - 1, notification.getId());
						}
					}
					pstmUpdate = insertEntity(buildPSTM.getPstm(), lstValueObject.toArray(), null, true);
					lstAddBatch.add(notification);
					commit++;
					if (commit == VarStatic.COMMIT_SIZE) {
						pstmUpdate.executeBatch();
						mcnMain.commit();
						if (lstAddBatch.size() > 0) {
							lstExecuted.addAll(lstAddBatch);
							lstAddBatch.clear();
						}
					}
				}
			}
			if (lstAddBatch.size() > 0) {
				pstmUpdate.executeBatch();
				mcnMain.commit();
				lstExecuted.addAll(lstAddBatch);
				lstAddBatch.clear();
			}
			return lstExecuted;
		} catch (Exception e) {
			mcnMain.rollback();
			e.printStackTrace();
			throw e;
		} finally {
			mcnMain.setAutoCommit(true);
			Database.closeObject(pstm);
		}
	}

	public boolean ignoreAddressNull(Notification notification, String tableName) {
		if ("notification_onesignal".equals(tableName) && StringUtil.isEmpty(notification.getAddress())) {
			return true;
		} else if ("notification_email".equals(tableName) && StringUtil.isEmpty(notification.getAddress())) {
			return true;
		} else if ("notification_sms".equals(tableName) && StringUtil.isEmpty(notification.getAddress())) {
			return true;
		}
		return false;
	}

	public Notification templateParser(Notification notification, String tableName,
			Map<String, NotificationTemplate> mapTemplate) {
		String key = null;
		if ("notification_onesignal".equals(tableName)) {
			key = "onesignal_" + notification.getAction();
		} else if ("notification_email".equals(tableName)) {
			key = "email_" + notification.getAction();
		} else if ("notification_sms".equals(tableName)) {
			key = "sms_" + notification.getAction();
		}
		return TemplateParser.parser(notification, mapTemplate.get(key));
	}

}
