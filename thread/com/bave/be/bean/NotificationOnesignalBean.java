package com.bave.be.bean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.bave.backend.persistence.entity.NotificationOneSignal;
import com.bave.be.thread.util.AbstractBean;
import com.bave.be.utils.ResultSetConverter;
import com.fss.sql.Database;

public class NotificationOnesignalBean extends AbstractBean {

	private PreparedStatement pstm;
	private PreparedStatement pstmUpdate;
	private String strSelectNotificationForGara;
	private String strSysDate;

	@Override
	public void init() throws SQLException {
		// strSelectNotification = " select * from notification where status =
		// '0' and create_date between ? and ? ";
		strSelectNotificationForGara = " select n.*,sp.unread_notify from notification_onesignal n ";
		strSelectNotificationForGara += " left join service_provider sp on n.domain = sp.code ";
		strSelectNotificationForGara += " where destination_object <> 'member' and status = '0' ";
		strSelectNotificationForGara += " UNION ";
		strSelectNotificationForGara += " select n.*,m.unread_notify from notification_onesignal n ";
		strSelectNotificationForGara += " inner join member m on n.destination_ref_id = m.id  ";
		strSelectNotificationForGara += " where destination_object = 'member' and status = '0'  ";
//		strSelectNotificationForGara += " UNION ";
//		strSelectNotificationForGara += " select n.*, null as unread_notify from notification_onesignal n ";
//		strSelectNotificationForGara += " where destination_object = 'member' and status = '0' ";
		// strSelectNotificationForGara += " and n.destination_ref_id is null ";

		strSysDate = " select now() at time zone 'utc'  ";
	}

	@Override
	public void close() {
		Database.closeObject(pstmUpdate);
		Database.closeObject(pstm);
	}

	public Date getSysDate() throws Exception {
		try {
			pstm = mcnMain.prepareStatement(strSysDate);
			ResultSet resultSet = pstm.executeQuery();
			while (resultSet.next()) {
				return resultSet.getTimestamp("SYSDATE");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
		return null;
	}

	public List<NotificationOneSignal> getAllNotification(Date fromDate, Date toDate, Integer limit) throws Exception {
		List<NotificationOneSignal> lstRs = new ArrayList<NotificationOneSignal>();
		try {
			pstm = mcnMain.prepareStatement(strSelectNotificationForGara);
			// pstm.setTimestamp(1, DateUtil.utilLongDateToSqlDate(fromDate));
			ResultSet resultSet = pstm.executeQuery();
			while (resultSet.next()) {
				NotificationOneSignal notification = ResultSetConverter.convertToObject(resultSet,
						NotificationOneSignal.class);
				lstRs.add(notification);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
		return lstRs;
	}

	public Set<String> getAllUserOnlineOfGara(String domain) throws Exception {
		Set<String> lstRs = new HashSet<>();
		try {
			pstm = mcnMain.prepareStatement(
					" select distinct player_id from erp_user_online where domain = ? and authen_key is not null ");
			pstm.setString(1, domain);
			ResultSet resultSet = pstm.executeQuery();
			while (resultSet.next()) {
				lstRs.add(resultSet.getString("player_id"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
		return lstRs;
	}

	public List<String> getPlayerIdsByPhone(String phone) throws Exception {
		List<String> lstRs = new ArrayList<String>();
		try {
			String sql = "select distinct md.player_id from erp_partner ep inner join member_device md on ep.member_id = md.member_id "
					+ "where md.active = '1' and player_id is not null and ep.phone like '%" + phone + "%'";
			pstm = mcnMain.prepareStatement(sql);
			ResultSet resultSet = pstm.executeQuery();
			while (resultSet.next()) {
				String playerId = resultSet.getString("player_id");
				lstRs.add(playerId);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
		return lstRs;
	}

	public Long getMemberIdByPhone(String phone) throws Exception {
		try {
			String sql = " select member_id from autho_profile where user_name like '%" + phone + "%' ";
			pstm = mcnMain.prepareStatement(sql);
			ResultSet resultSet = pstm.executeQuery();
			while (resultSet.next()) {
				String memberId = resultSet.getString("member_id");
				return Long.valueOf(memberId);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
		return null;
	}

}
