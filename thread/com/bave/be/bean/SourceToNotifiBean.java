//package com.bave.be.bean;
//
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.Date;
//import java.util.List;
//
//import com.bave.backend.api.util.VarStatic;
//import com.bave.backend.persistence.entity.Notification;
//import com.bave.be.thread.util.AbstractBean;
//import com.bave.be.thread.util.BuildPSTM;
//import com.bave.be.utils.DateUtil;
//import com.bave.be.utils.ResultSetConverter;
//import com.fss.sql.Database;
//
//public class SourceToNotifiBean extends AbstractBean {
//
//	private PreparedStatement pstm;
//	private PreparedStatement pstmUpdate;
//
//	private String strCount0;
//	private String strCount1;
//	private String strCount2;
//
//	private String strSelect0;
//	private String strSelect1;
//	private String strSelect2;
//
//	@Override
//	public void init() throws SQLException {
//		strCount0 = " select count(id) total from booking b where b.notif = ? and b.create_date >= ? ";
//
//		strCount1 = " select sum(total) total from ( ";
//		strCount1 += " select count(id) total from quotation_request b where b.notif = ? and b.create_date >= ? ";
//		strCount1 += " union ";
//		strCount1 += " select count(id) total from quotation_request b where b.notif = ? and b.write_date >= ? ";
//		strCount1 += " ) as tbl ";
//
//		strCount2 = " select count(id) total from quotation_request b where b.notif = ? and b.create_date >= ? ";
//
//		strSelect0 = " SELECT null as id, b.member_id,null as not_notify_id,('Đặt dịch vụ từ '||m.name) as message,'0' as status,'1' as type,'booking' as action,(now() at time zone 'utc') as create_date,'member' as source_object,m.id as source_ref_id,  ";
//		strSelect0 += " 'booking' as data_object,b.id as data_ref_id,'service_provider' as destination_object,sp.id as destination_ref_id,m.name as source_name,null as source_avatar,sp.name as dest_name, null as dest_avatar,  ";
//		strSelect0 += " null as dest_shortcontent, '0' as onesignal_status, sp.code as domain, null as player_id,b.notif  ";
//		strSelect0 += " ,null as template_id ";
//		strSelect0 += " from booking b inner join member m on m.id = b.member_id ";
//		strSelect0 += " inner join service_provider sp on sp.id = b.service_provider_id ";
//		strSelect0 += " where b.notif = '0' ";
//		strSelect0 += " and b.create_date >= ? ";
////		strSelect += " UNION ALL ";
//		strSelect1 = " SELECT null as id, b.member_id,null as not_notify_id,('Có yêu cầu báo giá từ '||m.name) as message,'0' as status,'1' as type,'quotation_request' as action,(now() at time zone 'utc') as create_date,'member' as source_object,m.id as source_ref_id, ";
//		strSelect1 += " 'quotation_request' as data_object,b.id as data_ref_id,'service_provider' as destination_object,sp.id as destination_ref_id,m.name as source_name,null as source_avatar,sp.name as dest_name, null as dest_avatar, ";
//		strSelect1 += " null as dest_shortcontent, '0' as onesignal_status, sp.code as domain, null as player_id,b.notif ";
//		strSelect1 += " ,null as template_id ";
//		strSelect1 += " from quotation_request b inner join member m on m.id = b.member_id ";
//		strSelect1 += " inner join service_provider sp on sp.id = b.service_provider_id ";
//		strSelect1 += " where b.notif = '0' and b.state = 'draft' ";
//		strSelect1 += " and b.create_date >= ? ";
//		strSelect1 += " UNION ALL ";
//		strSelect1 += " SELECT null as id, b.member_id,null as not_notify_id,(sp.name ||' trả lời báo giá ') as message,'0' as status,'1' as type,'quotation_response' as action,(now() at time zone 'utc') as create_date,'service_provider' as source_object,sp.id as source_ref_id, ";
//		strSelect1 += " 'quotation_request' as data_object,b.id as data_ref_id,'member' as destination_object,m.id as destination_ref_id,sp.name as source_name,null as source_avatar,m.name as dest_name, null as dest_avatar, ";
//		strSelect1 += " null as dest_shortcontent, '0' as onesignal_status, sp.code as domain, null as player_id,b.notif ";
//		strSelect1 += " ,null as template_id ";
//		strSelect1 += " from quotation_request b inner join member m on m.id = b.member_id ";
//		strSelect1 += " inner join service_provider sp on sp.id = b.service_provider_id ";
//		strSelect1 += " where b.notif = '1' and b.state = 'replied' ";
//		strSelect1 += " and b.write_date >= ? ";
////		strSelect += " UNION ALL ";
//		strSelect2 = " SELECT null as id, b.member_id,null as not_notify_id,(m.name ||' đã đồng ý báo giá ') as message,'0' as status,'1' as type,'quotation_cus_confirm' as action,(now() at time zone 'utc') as create_date,'member' as source_object,m.id as source_ref_id, ";
//		strSelect2 += " 'quotation_request' as data_object,b.id as data_ref_id,'service_provider' as destination_object,sp.id as destination_ref_id,m.name as source_name,null as source_avatar,sp.name as dest_name, null as dest_avatar, ";
//		strSelect2 += " null as dest_shortcontent, '0' as onesignal_status, sp.code as domain, null as player_id,b.notif ";
//		strSelect2 += " ,null as template_id ";
//		strSelect2 += " from quotation_request b inner join member m on m.id = b.member_id ";
//		strSelect2 += " inner join service_provider sp on sp.id = b.service_provider_id ";
//		strSelect2 += " where b.notif = '2' and b.state = 'done' ";
//		strSelect2 += " and b.write_date >= ? ";
//
//	}
//
//	@Override
//	public void close() {
//		Database.closeObject(pstmUpdate);
//		Database.closeObject(pstm);
//	}
//
//	public boolean countExisted(int threadIndex, Date fromDate) throws Exception {
//		try {
//			if (threadIndex == 0) {
//				pstm = mcnMain.prepareStatement(strCount0);
//				pstm.setString(1, "0");
//				pstm.setTimestamp(2, DateUtil.utilLongDateToSqlDate(fromDate));
//			} else if (threadIndex == 1) {
//				pstm = mcnMain.prepareStatement(strCount1);
//				pstm.setString(1, "1");
//				pstm.setTimestamp(2, DateUtil.utilLongDateToSqlDate(fromDate));
//				pstm.setString(3, "1");
//				pstm.setTimestamp(4, DateUtil.utilLongDateToSqlDate(fromDate));
//			} else if (threadIndex == 2) {
//				pstm = mcnMain.prepareStatement(strCount2);
//				pstm.setString(1, "2");
//				pstm.setTimestamp(2, DateUtil.utilLongDateToSqlDate(fromDate));
//			}
//			if (pstm != null) {
//				ResultSet resultSet = pstm.executeQuery();
//				while (resultSet.next()) {
//					return resultSet.getInt("total") > 0 ? true : false;
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw e;
//		} finally {
//			Database.closeObject(pstm);
//		}
//		return false;
//	}
//
//	public Long getSeq(String seqName) throws Exception {
//		try {
//			pstm = mcnMain.prepareStatement(" select nextval('" + seqName + "') as seq ");
//			ResultSet resultSet = pstm.executeQuery();
//			while (resultSet.next()) {
//				return resultSet.getLong("seq");
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw e;
//		} finally {
//			Database.closeObject(pstm);
//		}
//		return null;
//	}
//
//	public List<Notification> selectOtherData(Date fromDate, Date toDate, Integer limit, String state,
//			String notifiStatus, int threadIndex) throws Exception {
//		List<Notification> lstRs = new ArrayList<Notification>();
//		try {
//			if (threadIndex == 0) {
//				pstm = mcnMain.prepareStatement(strSelect0);
//				pstm.setTimestamp(1, DateUtil.utilLongDateToSqlDate(fromDate));
//			} else if (threadIndex == 1) {
//				pstm = mcnMain.prepareStatement(strSelect1);
//				pstm.setTimestamp(1, DateUtil.utilLongDateToSqlDate(fromDate));
//				pstm.setTimestamp(2, DateUtil.utilLongDateToSqlDate(fromDate));
//			} else if (threadIndex == 2) {
//				pstm = mcnMain.prepareStatement(strSelect2);
//				pstm.setTimestamp(1, DateUtil.utilLongDateToSqlDate(fromDate));
//			}
//			if (pstm != null) {
//				ResultSet resultSet = pstm.executeQuery();
//				while (resultSet.next()) {
//					Notification notification = ResultSetConverter.convertToObject(resultSet, Notification.class);
//					lstRs.add(notification);
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			throw e;
//		} finally {
//			Database.closeObject(pstm);
//		}
//		return lstRs;
//	}
//
//	public List<Notification> insertToNotification(List<Notification> lstNotification) throws Exception {
//		List<Notification> lstExecuted = new ArrayList<>();
//		List<Notification> lstAddBatch = new ArrayList<>();
//		try {
//			mcnMain.setAutoCommit(false);
//			int commit = 0;
//			// List<String> lstColumn =
//			// ResultSetConverter.getAllColumnsName(Notification.class,
//			// new HashSet<String>(Arrays.asList("notif")));
//			List<String> lstColumn = Arrays.asList("id", "member_id", "not_notify_id", "message", "status", "type",
//					"action", "create_date", "source_object", "source_ref_id", "destination_object",
//					"destination_ref_id", "source_name", "source_avatar", "dest_name", "dest_avatar",
//					"dest_shortcontent", "onesignal_status", "domain", "player_id", "data_object", "data_ref_id",
//					"data_name", "template_id");
//			BuildPSTM buildPSTM = buildPreparedInsert(pstmUpdate, "notification", lstColumn, "id", "notification_seq");
//			for (Notification notification : lstNotification) {
//				if (buildPSTM != null) {
//					Object[] arrValueObject = ResultSetConverter.getAllValuesByLstName(lstColumn, notification, "id")
//							.toArray();
//					pstmUpdate = insertEntity(buildPSTM.getPstm(), arrValueObject, null, true);
//					lstAddBatch.add(notification);
//					commit++;
//					if (commit == VarStatic.COMMIT_SIZE) {
//						pstmUpdate.executeBatch();
//						mcnMain.commit();
//						if (lstAddBatch.size() > 0) {
//							lstExecuted.addAll(lstAddBatch);
//							lstAddBatch.clear();
//						}
//					}
//				}
//			}
//			if (lstAddBatch.size() > 0) {
//				pstmUpdate.executeBatch();
//				mcnMain.commit();
//				lstExecuted.addAll(lstAddBatch);
//				lstAddBatch.clear();
//			}
//			return lstExecuted;
//		} catch (Exception e) {
//			mcnMain.rollback();
//			e.printStackTrace();
//			throw e;
//		} finally {
//			mcnMain.setAutoCommit(true);
//			Database.closeObject(pstm);
//		}
//	}
//
//	public List<Notification> updateStatusDataObject(List<Notification> lstNotification, String dataObject)
//			throws Exception {
//		List<Notification> lstExecuted = new ArrayList<>();
//		List<Notification> lstAddBatch = new ArrayList<>();
//		try {
//			mcnMain.setAutoCommit(false);
//			int commit = 0;
//			List<Object> lstValue = new ArrayList<>();
//			BuildPSTM buildPSTM = buildPreparedUpdate(pstmUpdate, dataObject, Arrays.asList("notif"),
//					Arrays.asList("notif", "id"), lstValue);
//
//			for (Notification notification : lstNotification) {
//				if (buildPSTM != null) {
//					Object[] arrValueObject = null;
//					if ("quotation_request".equals(dataObject) && dataObject.equals(notification.getDataObject())) {
//						if (VarStatic.SOURCETONOTI_STATUS_SEND2.equals(notification.getNotif())) {
//							arrValueObject = new Object[] { VarStatic.SOURCETONOTI_STATUS_REPLY,
//									notification.getNotif(), Integer.parseInt(notification.getDataRefId()) };
//						} else if (VarStatic.SOURCETONOTI_STATUS_SEND1.equals(notification.getNotif())) {
//							arrValueObject = new Object[] { VarStatic.SOURCETONOTI_STATUS_SEND2,
//									notification.getNotif(), Integer.parseInt(notification.getDataRefId()) };
//						} else {
//							arrValueObject = new Object[] { VarStatic.SOURCETONOTI_STATUS_SEND1,
//									notification.getNotif(), Integer.parseInt(notification.getDataRefId()) };
//						}
//					} else if (dataObject.equals(notification.getDataObject())) {
//						arrValueObject = new Object[] { VarStatic.SOURCETONOTI_STATUS_SEND1, notification.getNotif(),
//								Integer.parseInt(notification.getDataRefId()) };
//					} else {
//						continue;
//					}
//					pstmUpdate = updateEntity(buildPSTM.getPstm(), arrValueObject, null, true);
//
//					lstAddBatch.add(notification);
//					commit++;
//					if (commit == VarStatic.COMMIT_SIZE) {
//						pstmUpdate.executeBatch();
//						mcnMain.commit();
//						if (lstAddBatch.size() > 0) {
//							lstExecuted.addAll(lstAddBatch);
//							lstAddBatch.clear();
//						}
//					}
//				}
//			}
//
//			if (lstAddBatch.size() > 0) {
//				pstmUpdate.executeBatch();
//				mcnMain.commit();
//				lstExecuted.addAll(lstAddBatch);
//				lstAddBatch.clear();
//			}
//
//			return lstExecuted;
//		} catch (Exception e) {
//			mcnMain.rollback();
//			e.printStackTrace();
//			throw e;
//		} finally {
//			mcnMain.setAutoCommit(true);
//			Database.closeObject(pstm);
//		}
//	}
//
//	public List<Notification> updateStatusBooking(List<Notification> lstNotification) throws Exception {
//		List<Notification> lstExecuted = new ArrayList<>();
//		List<Notification> lstAddBatch = new ArrayList<>();
//		try {
//			mcnMain.setAutoCommit(false);
//			int commit = 0;
//			List<Object> lstValue = new ArrayList<>();
//			BuildPSTM buildPSTM = buildPreparedUpdate(pstmUpdate, "booking", Arrays.asList("notif"),
//					Arrays.asList("notif", "id"), lstValue);
//
//			for (Notification notification : lstNotification) {
//				if (buildPSTM != null && "booking".equals(notification.getDestinationObject())) {
//					Object[] arrValueObject = new Object[] { VarStatic.SOURCETONOTI_STATUS_SEND1,
//							notification.getNotif(), notification.getDestinationRefId() };
//					pstmUpdate = updateEntity(buildPSTM.getPstm(), arrValueObject, null, true);
//					lstAddBatch.add(notification);
//					commit++;
//					if (commit == VarStatic.COMMIT_SIZE) {
//						pstmUpdate.executeBatch();
//						mcnMain.commit();
//						if (lstAddBatch.size() > 0) {
//							lstExecuted.addAll(lstAddBatch);
//							lstAddBatch.clear();
//						}
//					}
//				}
//			}
//			if (lstAddBatch.size() > 0) {
//				pstmUpdate.executeBatch();
//				mcnMain.commit();
//				lstExecuted.addAll(lstAddBatch);
//				lstAddBatch.clear();
//			}
//			return lstExecuted;
//		} catch (Exception e) {
//			mcnMain.rollback();
//			e.printStackTrace();
//			throw e;
//		} finally {
//			mcnMain.setAutoCommit(true);
//			Database.closeObject(pstm);
//		}
//	}
//
//	public List<Notification> updateStatusQuotation(List<Notification> lstNotification) throws Exception {
//		List<Notification> lstExecuted = new ArrayList<>();
//		List<Notification> lstAddBatch = new ArrayList<>();
//		try {
//			mcnMain.setAutoCommit(false);
//			int commit = 0;
//			List<Object> lstValue = new ArrayList<>();
//			BuildPSTM buildPSTM = buildPreparedUpdate(pstmUpdate, "quotation_request", Arrays.asList("notif"),
//					Arrays.asList("notif", "id"), lstValue);
//
//			for (Notification notification : lstNotification) {
//				if (buildPSTM != null && "quotation_request".equals(notification.getDestinationObject())) {
//					Object[] arrValueObject = null;
//					if (VarStatic.SOURCETONOTI_STATUS_DRAFT.equals(notification.getNotif())) {
//						arrValueObject = new Object[] { VarStatic.SOURCETONOTI_STATUS_SEND1, notification.getNotif(),
//								notification.getDestinationRefId() };
//						pstmUpdate = updateEntity(buildPSTM.getPstm(), arrValueObject, null, true);
//					} else if (VarStatic.SOURCETONOTI_STATUS_SEND1.equals(notification.getNotif())) {
//						arrValueObject = new Object[] { VarStatic.SOURCETONOTI_STATUS_SEND2, notification.getNotif(),
//								notification.getDestinationRefId() };
//						pstmUpdate = updateEntity(buildPSTM.getPstm(), arrValueObject, null, true);
//					}
//					lstAddBatch.add(notification);
//					commit++;
//					if (commit == VarStatic.COMMIT_SIZE) {
//						pstmUpdate.executeBatch();
//						mcnMain.commit();
//						if (lstAddBatch.size() > 0) {
//							lstExecuted.addAll(lstAddBatch);
//							lstAddBatch.clear();
//						}
//					}
//				}
//			}
//			if (lstAddBatch.size() > 0) {
//				pstmUpdate.executeBatch();
//				mcnMain.commit();
//				lstExecuted.addAll(lstAddBatch);
//				lstAddBatch.clear();
//			}
//			return lstExecuted;
//		} catch (Exception e) {
//			mcnMain.rollback();
//			e.printStackTrace();
//			throw e;
//		} finally {
//			mcnMain.setAutoCommit(true);
//			Database.closeObject(pstm);
//		}
//	}
//
//}