package com.bave.be.bean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bave.backend.persistence.entity.Notification;
import com.bave.backend.security.config.VarStatic;
import com.bave.be.thread.util.AbstractBean;
import com.bave.be.thread.util.BuildPSTM;
import com.bave.be.utils.ResultSetConverter;
import com.fss.sql.Database;

public class ERPToNotifiBean extends AbstractBean {

	private PreparedStatement pstm;
	private PreparedStatement pstmUpdate;

	@Override
	public void init() throws SQLException {
	}

	@Override
	public void close() {
		Database.closeObject(pstmUpdate);
		Database.closeObject(pstm);
	}

	public Long getSeq(String seqName) throws Exception {
		try {
			pstm = mcnMain.prepareStatement(" select nextval('" + seqName + "') as seq ");
			ResultSet resultSet = pstm.executeQuery();
			while (resultSet.next()) {
				return resultSet.getLong("seq");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
		return null;
	}

	public List<Notification> insertToNotification(List<Notification> lstNotification) throws Exception {
		List<Notification> lstExecuted = new ArrayList<>();
		List<Notification> lstAddBatch = new ArrayList<>();
		try {
			mcnMain.setAutoCommit(false);
			int commit = 0;
			List<String> lstColumn = Arrays.asList("id", "member_id", "not_notify_id", "message", "status", "type",
					"action", "create_date", "source_object", "source_ref_id", "destination_object",
					"destination_ref_id", "source_name", "source_avatar", "dest_name", "dest_avatar",
					"dest_shortcontent", "onesignal_status", "domain", "player_id", "data_object", "data_ref_id",
					"data_name", "template_id", "sequence");
			BuildPSTM buildPSTM = buildPreparedInsert(pstmUpdate, "notification", lstColumn, "id", "notification_seq");
			for (Notification notification : lstNotification) {
				if (buildPSTM != null) {
					lstColumn.set(0, null);
					Object[] arrValueObject = ResultSetConverter.getAllValuesByLstName(lstColumn, notification, "id")
							.toArray();
					pstmUpdate = insertEntity(buildPSTM.getPstm(), arrValueObject, null, true);
					lstAddBatch.add(notification);
					commit++;
					if (commit == VarStatic.COMMIT_SIZE) {
						pstmUpdate.executeBatch();
						mcnMain.commit();
						if (lstAddBatch.size() > 0) {
							lstExecuted.addAll(lstAddBatch);
							lstAddBatch.clear();
						}
					}
				}
			}
			if (lstAddBatch.size() > 0) {
				pstmUpdate.executeBatch();
				mcnMain.commit();
				lstExecuted.addAll(lstAddBatch);
				lstAddBatch.clear();
			}
			return lstExecuted;
		} catch (Exception e) {
			mcnMain.rollback();
			e.printStackTrace();
			throw e;
		} finally {
			mcnMain.setAutoCommit(true);
			Database.closeObject(pstm);
		}
	}

}