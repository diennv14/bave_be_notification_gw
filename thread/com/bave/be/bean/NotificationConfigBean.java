package com.bave.be.bean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bave.be.entity.NotificationConfig;
import com.bave.be.thread.util.AbstractBean;
import com.bave.be.utils.ResultSetConverter;
import com.fss.sql.Database;

public class NotificationConfigBean extends AbstractBean {

	private PreparedStatement pstm;
	private String strSelectConfig;

	@Override
	public void init() throws SQLException {
		strSelectConfig = " select * from notification_config where active=true ";
	}

	@Override
	public void close() {
		Database.closeObject(pstm);
	}

	public List<NotificationConfig> getAllNotificationCfg(NotificationConfig condition) throws Exception {
		List<NotificationConfig> listNotiCfg = new ArrayList<NotificationConfig>();
		try {
			if (condition.getAction() != null) {
				strSelectConfig += "and action='" + condition.getAction() + "' ";
			}

			if (condition.getChannel() != null) {
				strSelectConfig += "and channel='" + condition.getChannel() + "' ";
			}

			if (condition.getEnvironment() != null) {
				strSelectConfig += "and environment='" + condition.getEnvironment() + "' ";
			}

			if (condition.getApp() != null) {
				strSelectConfig += "and app='" + condition.getApp() + "' ";
			}

			pstm = mcnMain.prepareStatement(strSelectConfig);
			ResultSet resultSet = pstm.executeQuery();
			while (resultSet.next()) {
				NotificationConfig notificationCfg = ResultSetConverter.convertToObject(resultSet,
						NotificationConfig.class);
				listNotiCfg.add(notificationCfg);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
		return listNotiCfg;
	}
}