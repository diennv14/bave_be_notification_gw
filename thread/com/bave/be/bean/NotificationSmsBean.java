package com.bave.be.bean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.bave.backend.persistence.entity.NotificationSMS;
import com.bave.backend.security.config.VarStatic;
import com.bave.be.thread.util.AbstractBean;
import com.bave.be.thread.util.BuildPSTM;
import com.bave.be.utils.DateUtil;
import com.bave.be.utils.ResultSetConverter;
import com.fss.sql.Database;

public class NotificationSmsBean extends AbstractBean {

	private PreparedStatement pstm;
	private PreparedStatement pstmUpdate;
	private String strSelectNotification;
	private String strSysDate;

	@Override
	public void init() throws SQLException {
		// strSelectNotification = " select * from notification where status =
		// '0' and create_date between ? and ? ";
		strSelectNotification = " select * from notification_sms where status = '0' and create_date >= ? ";

		strSysDate = " SELECT NOW() ";
	}

	@Override
	public void close() {
		Database.closeObject(pstmUpdate);
		Database.closeObject(pstm);
	}

	public Date getSysDate() throws Exception {
		try {
			pstm = mcnMain.prepareStatement(strSysDate);
			ResultSet resultSet = pstm.executeQuery();
			while (resultSet.next()) {
				return resultSet.getTimestamp("SYSDATE");
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
		return null;
	}

	public List<NotificationSMS> getAllNotification(Date fromDate, Date toDate, Integer limit) throws Exception {
		List<NotificationSMS> lstRs = new ArrayList<NotificationSMS>();
		try {
			pstm = mcnMain.prepareStatement(strSelectNotification);
			pstm.setTimestamp(1, DateUtil.utilLongDateToSqlDate(fromDate));
			// pstm.setTimestamp(1,
			// Timestamp.valueOf(DateUtil.dateToString(fromDate,
			// DateUtil.FORMAT_SQLDATE2)));
			// pstm.setTimestamp(2,
			// Timestamp.valueOf(DateUtil.dateToString(toDate,
			// DateUtil.FORMAT_SQLDATE2)));
			ResultSet resultSet = pstm.executeQuery();
			while (resultSet.next()) {
				NotificationSMS notification = ResultSetConverter.convertToObject(resultSet, NotificationSMS.class);
				lstRs.add(notification);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			Database.closeObject(pstm);
		}
		return lstRs;
	}

	public List<NotificationSMS> updateProcessStatus(List<NotificationSMS> lstNotification, String oldStatus,
			String newStatus) throws Exception {
		List<NotificationSMS> lstExecuted = new ArrayList<>();
		List<NotificationSMS> lstAddBatch = new ArrayList<>();
		try {
			mcnMain.setAutoCommit(false);
			int commit = 0;
			List<Object> lstValue = new ArrayList<>();
			BuildPSTM buildPSTM = buildPreparedUpdate(pstmUpdate, "notification_sms", Arrays.asList("status"),
					Arrays.asList("status", "id"), lstValue);
			for (NotificationSMS notification : lstNotification) {
				if (buildPSTM != null) {
					notification.setStatus(newStatus);
					Object[] arrValueObject = new Object[] { newStatus, oldStatus, notification.getId() };
					pstmUpdate = updateEntity(buildPSTM.getPstm(), arrValueObject, null, true);
					lstAddBatch.add(notification);
					commit++;
					if (commit == VarStatic.COMMIT_SIZE) {
						pstmUpdate.executeBatch();
						mcnMain.commit();
						if (lstAddBatch.size() > 0) {
							lstExecuted.addAll(lstAddBatch);
							lstAddBatch.clear();
						}
					}
				}
			}
			if (lstAddBatch.size() > 0) {
				pstmUpdate.executeBatch();
				mcnMain.commit();
				lstExecuted.addAll(lstAddBatch);
				lstAddBatch.clear();
			}
			return lstExecuted;
		} catch (Exception e) {
			mcnMain.rollback();
			e.printStackTrace();
			throw e;
		} finally {
			mcnMain.setAutoCommit(true);
			Database.closeObject(pstm);
		}
	}
}
