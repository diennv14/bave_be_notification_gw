package com.bave.be.main;

import java.io.File;
import java.io.PrintStream;
import java.sql.Connection;

import com.bave.be.thread.util.PostgreUniversalConnectionFactory;
import com.fis.thread.ThreadManagerEx;
import com.fss.dictionary.Dictionary;
import com.fss.thread.ProcessorListener;
import com.fss.thread.ThreadConstant;
import com.fss.thread.ThreadManager;
import com.fss.thread.ThreadProcessor;
import com.fss.util.FileUtil;
import com.fss.util.LogOutputStream;
import com.fss.util.StringUtil;


public class AppManager implements ProcessorListener {

	public static final String CONFIG_FILE_NAME = "config/ServerConfig.txt";
	private static Dictionary mdc;

	private PostgreUniversalConnectionFactory mPool = null;

	public Connection getConnection() {
		try {
			return mPool.getConnection();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void onCreate(ThreadProcessor threadProcessor) {
	}
	
	public void onOpen(ThreadProcessor threadProcessor)  {
		try {
			threadProcessor.mcnMain = getConnection();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void setPool(PostgreUniversalConnectionFactory pool) {
		mPool = pool;
	}
	
	public String getParameter(String strKey) {
		return StringUtil.nvl(mdc.getString(strKey), "");
	}

	public static void main(String[] argv) {
		try {
			System.out.println("Thread running...");
			// Change system output to file
			AppManager lsn = new AppManager();
			String strWorkingDir = System.getProperty("user.dir");
			if (strWorkingDir == null || strWorkingDir.equals("")) {
				strWorkingDir = System.getProperty("user.dir");
			}
			if (!strWorkingDir.endsWith("/") || !strWorkingDir.endsWith("\\")) {
				strWorkingDir += "/";
			}
			 
			// loadServerConfig(strConfigFilePath);
			mdc = new Dictionary(CONFIG_FILE_NAME);

			String strLogFile = mdc.getString("LogFile");
			String strOutputFile = mdc.getString("OutputFile");
			String strLogDir = strWorkingDir + mdc.getString("LogDir");

			if (!strLogDir.endsWith("/") || !strLogDir.endsWith("\\")) {
				strLogDir += "/";
			}
			File f = new File(strLogDir + strLogFile);
			FileUtil.forceFolderExist(f.getParent());

//			int iLogKeepDay = Integer.parseInt(mdc.getString("KeepLogFileOnDay"));
//			int iMaxFileToSave = Integer.parseInt(mdc.getString("MaxFileLogSave"));

			//loadFunction(mdc.getString("PacketFunction"));
			PrintStream ps = new PrintStream(new LogOutputStream(strLogDir + strLogFile));
			PrintStream psOutput = new PrintStream(new LogOutputStream(strLogDir + strOutputFile));
//			int iConnectPoolSize = Integer.parseInt(mdc.getString("ConnectionPoolSize"));
			
			String strNodeApp = lsn.getParameter("DefaultDatabaseApp");
			Dictionary ndApp = new Dictionary(lsn.mdc.getChild("Connection."
					+ strNodeApp));
			
			PostgreUniversalConnectionFactory pool = new PostgreUniversalConnectionFactory(ndApp,strNodeApp);
			lsn.setPool(pool);
//			System.setOut(psOutput);
//			System.setErr(ps);
			int iProcessID=Integer.parseInt(mdc.getString("ProcessID"));
			ThreadManagerEx cs = new ThreadManagerEx(Integer.parseInt(mdc.getString("PortID")), lsn);
			cs.setLoadingMethod(ThreadManager.LOAD_FROM_DB);
			cs.setProcessId(iProcessID);
			cs.setClusterType(ThreadConstant.CLUSTER_ACTIVE_STANDBY);
			cs.setConfigActiveNode(true);
			cs.start();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}

	}
}
