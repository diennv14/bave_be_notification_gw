package com.bave.be.main;

import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.bave.backend.persistence.entity.NotificationOneSignal;
import com.bave.backend.security.config.VarStatic;
import com.bave.be.entity.serialz.NotificationOneSignalSerializer;

class RunnableDemo implements Runnable {
	private Thread t;
	private String threadName;

	RunnableDemo(String name) {
		threadName = name;
		System.out.println("Creating " + threadName);
	}

	private Producer<String, NotificationOneSignal> createProducerTopic(String topic) {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, VarStatic.kafka_bootstrap_server);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, topic);
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, NotificationOneSignalSerializer.class.getName());
		props.put("value.serializer", NotificationOneSignalSerializer.class.getName());
		return new KafkaProducer<>(props);
	}

	private void sendMessageToSendTopic(NotificationOneSignal notification) {
		Producer<String, NotificationOneSignal> producer = createProducerTopic("notify_send_topic");
		try {
			final ProducerRecord<String, NotificationOneSignal> record = new ProducerRecord<String, NotificationOneSignal>(
					"notify_send_topic", notification);
			producer.send(record).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} finally {
			producer.flush();
			producer.close();
		}
	}

	public void run() {
		System.out.println("Running " + threadName);
		try {
			for (int i = 0; i < 1; i++) {
				System.out.println("Thread: " + threadName + ", " + i);
				NotificationOneSignal notification = new NotificationOneSignal();
				notification.setAction("test");
				notification.setCreateDate(new Date());
				notification.setDataName("test");
				notification.setDataObject("booking");
				notification.setDataRefId("123");
				notification.setDestinationObject("member");
				notification.setDestinationRefId(9164l);
				notification.setDomain("test_mobile");
				notification.setMessage("test: " + threadName + " - " + i);
				notification.setAddress("af12be9a-439d-4619-9bfb-6e134cfb062d");
				sendMessageToSendTopic(notification);
				Thread.sleep(50);
			}
		} catch (InterruptedException e) {
			System.out.println("Thread " + threadName + " interrupted.");
		}
		System.out.println("Thread " + threadName + " exiting.");
	}

	public void start() {
		System.out.println("Starting " + threadName);
		if (t == null) {
			t = new Thread(this, threadName);
			t.start();
		}
	}
}

public class TestThread {

	public static void main(String args[]) {
		RunnableDemo R1 = new RunnableDemo("T1");
		R1.start();
	}
}
