package com.bave.be.utils;

import java.util.Calendar;
import java.util.Date;

public class SchedulerUtil {
	private String getTimeSchedulerPattern(String Schedulermode, String minis, String hours, String lsDate) {
		String[] arrScheduler = new String[5];
		Calendar cal = Calendar.getInstance();
		Date now = new Date();
		cal.setTime(now);
		int intDayOfMonth = cal.get(Calendar.DAY_OF_MONTH);
		int intMonth = cal.get(Calendar.MONTH);
		int intDayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		if (Schedulermode.equalsIgnoreCase("DAY")) {
			arrScheduler[0] = minis;
			arrScheduler[1] = hours;
			// arrScheduler[2] = "*";
			// arrScheduler[3] = "*";
			arrScheduler[2] = String.valueOf(intDayOfMonth);
			arrScheduler[3] = "*";
			arrScheduler[4] = "*";
		} else if (Schedulermode.equalsIgnoreCase("WEEKLY")) {
			arrScheduler[0] = minis;
			arrScheduler[1] = hours;
			arrScheduler[2] = "*";
			arrScheduler[3] = "*";
			arrScheduler[4] = lsDate;
		} else if (Schedulermode.equalsIgnoreCase("MONTHLY")) {
			arrScheduler[0] = minis;
			arrScheduler[1] = hours;
			arrScheduler[2] = "*";
			arrScheduler[3] = lsDate;
			arrScheduler[4] = "*";
		} else if (Schedulermode.equalsIgnoreCase("DAY_OF_MONTH")) {
			arrScheduler[0] = minis;
			arrScheduler[1] = hours;
			arrScheduler[2] = lsDate;
			arrScheduler[3] = "*";
			arrScheduler[4] = "*";
		}

		String strTimePartern = "";
		for (int i = 0; i < arrScheduler.length; i++) {
			strTimePartern += arrScheduler[i] + " ";
		}
		return strTimePartern.trim();
	}
}
