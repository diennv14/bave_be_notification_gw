package com.bave.be.utils;

import java.text.Normalizer;
import java.util.regex.Pattern;

/**
 * <p>
 * Title: StringUtil
 * </p>
 * <p>
 * Description: Utility for string processing
 * </p>
 * <p>
 * Copyright: Copyright (c) 2001
 * </p>
 * <p>
 * Company: FPT
 * </p>
 * 
 * @author Thai Hoang Hiep
 * @version 1.0
 */

public class StringUtil extends com.fss.util.StringUtil {
	public static boolean isEmpty(String str) {
		if (str == null)
			return true;
		else {
			return str.length() <= 0;
		}
	}

	public static boolean isEmpty(Object str) {
		if (str == null)
			return true;
		else {
			return str.toString().length() <= 0;
		}
	}

	public static String removeAccent(String s) {
		if (s != null) {
			String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
			Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
			return pattern.matcher(temp).replaceAll("").replace('đ', 'd').replace('Đ', 'D');
		}
		return null;
	}
}
