package com.bave.be.utils;

import java.math.BigDecimal;

public class AmountUtil {
	// public static final int VAT = 11;

	public static void main(String[] args) {

		float amountWVAT = 200000;
		float tax = 10;
		// System.out.println("amountSubtractVat:" +
		// amountSubtractVat(220000.345, tax));
		// System.out.println("amountPlusVat:" + amountPlusVat(10f, tax));
		// System.out.println("amountVat:" + amountVat(220000f, tax));
		// System.out.println(Math.round(amountSubtractVat(41000)));
		// System.out.println(amountVat(41000));
		//
		// System.out.println(amountSubtractVat(41000) + amountVat(41000));
		// System.out.println(Math.abs(11111));

		// System.out.println(subtractPlusVat(2250000));

		System.out.println(AmountUtil.amountPlusVat(new BigDecimal(2250000).doubleValue(), 10));

		System.out.println(BigDecimal.valueOf(
				Math.round(AmountUtil.round(AmountUtil.amountPlusVat(new BigDecimal(2250000).doubleValue(), 10)))));

		double pi = BigDecimal
				.valueOf(Math
						.round(AmountUtil.round(AmountUtil.amountPlusVat(new BigDecimal(2250000).doubleValue(), 10))))
				.doubleValue();
		System.out.println(pi);
		System.out.println("-----------------------------");
		System.out.println(Math.round(pi));
		System.out.println((double) Math.round(pi * 10) / 10);
		System.out.println((double) Math.round(pi * 100) / 100);
		System.out.println((double) Math.round(pi * 1000) / 1000);
		System.out.println((double) Math.round(pi * 10000) / 10000);
		System.out.println((double) Math.round(pi * 100000) / 100000);
		System.out.println((double) Math.round(pi * 1000000) / 1000000);
		System.out.println((double) Math.round(pi * 10000000) / 10000000);

		int backDate = 10;
		int minute = (AmountUtil.isPlusOrMinus(new Double(backDate)) == false) ? -backDate : backDate;
		System.out.print(minute);
	}

	// LamTron

	public static Double round(Double value) {
		if (value == null)
			return value;
		int decimalPlaces = 0;
		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP);
		value = bd.doubleValue();
		return value;
	}

	// Cong thuc tinh thue
	public static Float subtractPlusVat(Integer exportTax) {
		if (exportTax == null)
			return 0f;
		return new Float(1 + (exportTax / 100f));
	}

	// Tinh gia goc tu gia da co thue
	public static Double amountSubtractVat(Double amountWithVAT, Integer exportTax) {
		if (amountWithVAT == null || exportTax == null)
			return amountWithVAT;
		double val = amountWithVAT / subtractPlusVat(exportTax);
		return round(val);
	}

	// Tinh thue tu gia chua thue
	public static Double amountPlusVat(Double amountWithoutVAT, Integer exportTax) {
		if (amountWithoutVAT == null || exportTax == null)
			return amountWithoutVAT;
		double val = amountWithoutVAT * subtractPlusVat(exportTax);
		return round(val);
	}

	// Tien thue tu gia da co thue
	public static Double amountVat(Double amountWithVAT, Integer exportTax) {
		if (amountWithVAT == null || exportTax == null)
			return amountWithVAT;
		double val = amountWithVAT - amountSubtractVat(amountWithVAT, exportTax);
		return round(val);
	}

	// Tien la am hay duong
	public static Boolean isPlusOrMinus(Double amount) {
		if (amount == null)
			return true;
		return (amount > 0) ? true : false;
	}
}
