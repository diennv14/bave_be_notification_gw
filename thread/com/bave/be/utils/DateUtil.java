package com.bave.be.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2004
 * </p>
 * <p>
 * Company: FPT - FSS
 * </p>
 * 
 * @author not Thai Hoang Hiep
 * @version 1.0
 */

public class DateUtil extends com.fss.util.DateUtil {
	public static final String FORMAT_DATE2 = "dd/MM/yyyy";
	public static final String FORMAT_DATEHHMMSS2 = "dd/MM/yyyy HH:mm:ss";
	public static final String FORMAT_DMHHMMSS2 = "dd/MM HH:mm:ss";
	public static final String FORMAT_SQLDATE2 = "yyyy-MM-dd HH:mm:ss";

	public static Date nowUTC() {
		return DateUtil.convertToUTC(Calendar.getInstance().getTime());
	}

	public static Calendar calendarUTC() {
		ZoneId zoneId = ZoneId.of(TimeZone.getDefault().getID());
		LocalDateTime fromDateTime = new Date().toInstant().atZone(zoneId).toLocalDateTime();
		ZonedDateTime toDate = ZonedDateTime.of(fromDateTime, zoneId);
		Calendar cal = Calendar.getInstance();
		cal.set(toDate.getYear(), toDate.getMonthValue() - 1, toDate.getDayOfMonth(), toDate.getHour(),
				toDate.getMinute(), toDate.getSecond());
		return cal;
	}

	public static Date getOverDay(Integer day, Integer hour, Integer minute, Integer second) {
		return getOverDay(day, hour, minute, second, null);
	}

	public static Date getOverDayUTC(Integer day, Integer hour, Integer minute, Integer second) {
		return getOverDayUTC(day, hour, minute, second, null);
	}

	public static Date getOverDayUTC(Integer day, Integer hour, Integer minute, Integer second, Date forDate) {
		Calendar cal = calendarUTC();
		if (forDate != null)
			cal.setTime(forDate);
		if (day != null) {
			cal.add(Calendar.DATE, day);
		}
		cal.set(Calendar.HOUR_OF_DAY, (hour != null) ? hour : 23);
		cal.set(Calendar.MINUTE, (minute != null) ? minute : 59);
		cal.set(Calendar.SECOND, (second != null) ? second : 59);
		// cal.set(Calendar.MILLISECOND, (millisecond != null) ? millisecond :
		// 999);
		return cal.getTime();
	}

	public static Date getOverDay(Integer day, Integer hour, Integer minute, Integer second, Date forDate) {
		Calendar cal = Calendar.getInstance();
		if (forDate != null)
			cal.setTime(forDate);
		if (day != null) {
			cal.add(Calendar.DATE, day);
		}
		cal.set(Calendar.HOUR_OF_DAY, (hour != null) ? hour : 23);
		cal.set(Calendar.MINUTE, (minute != null) ? minute : 59);
		cal.set(Calendar.SECOND, (second != null) ? second : 59);
		// cal.set(Calendar.MILLISECOND, (millisecond != null) ? millisecond :
		// 999);
		return cal.getTime();
	}

	public static String dateToString(Date date, String strFormat) {
		if (date != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(strFormat);
			return simpleDateFormat.format(date);
		} else
			return null;
	}

	public static Date stringToDate(String dateString, String strFormat) {
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(strFormat);
			return simpleDateFormat.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Date sqlLongDateToUtilDate(java.sql.Timestamp sqlDate) {
		if (sqlDate != null) {
			Date utilDate = new Date(sqlDate.getTime());
			return utilDate;
		} else
			return null;
	}

	public static java.sql.Timestamp utilLongDateToSqlDate(Date utilDate) {
		if (utilDate != null) {
			java.sql.Timestamp sqlDate = new java.sql.Timestamp(utilDate.getTime());
			return sqlDate;
		} else
			return null;
	}

	public static Date convertTimeZone(Date date, String fromTimeZone, String toTimeZone) {
		ZoneId fromZoneId = ZoneId.of(fromTimeZone);
		ZoneId toZoneId = ZoneId.of(toTimeZone);
		LocalDateTime fromDateTime = date.toInstant().atZone(fromZoneId).toLocalDateTime();
		ZonedDateTime fromDate = ZonedDateTime.of(fromDateTime, fromZoneId);
		ZonedDateTime toDate = fromDate.withZoneSameInstant(toZoneId);
		Calendar cal = Calendar.getInstance();
		cal.set(toDate.getYear(), toDate.getMonthValue() - 1, toDate.getDayOfMonth(), toDate.getHour(),
				toDate.getMinute(), toDate.getSecond());
		return cal.getTime();
	}

	public static Date convertToUTC(Date date) {
		ZoneId fromZoneId = ZoneId.of(TimeZone.getDefault().getID());
		ZoneId toZoneId = ZoneId.of(TimeZone.getTimeZone("UTC").getID());
		LocalDateTime fromDateTime = date.toInstant().atZone(fromZoneId).toLocalDateTime();
		ZonedDateTime fromDate = ZonedDateTime.of(fromDateTime, fromZoneId);
		ZonedDateTime toDate = fromDate.withZoneSameInstant(toZoneId);
		Calendar cal = Calendar.getInstance();
		cal.set(toDate.getYear(), toDate.getMonthValue() - 1, toDate.getDayOfMonth(), toDate.getHour(),
				toDate.getMinute(), toDate.getSecond());
		return cal.getTime();
	}
}
