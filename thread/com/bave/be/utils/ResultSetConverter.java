package com.bave.be.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ResultSetConverter {

	public static <T> T convertToObject(ResultSet resultSet, Class<T> clazz) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		// if (mapModelInfo != null) {
		// map = updateValueByFieldType(map, mapModelInfo);
		// }
		// T t = (T) mapper.convertValue(map, clazz);

		T t = (T) clazz.newInstance();
		Set<String> setColumns = getColumnsName(resultSet);
		for (Field field : t.getClass().getDeclaredFields()) {
			boolean sccessible = field.isAccessible();
			field.setAccessible(true);
			String fieldName = field.getName();
			JsonProperty jsonAnnotation = field.getAnnotation(JsonProperty.class);
			String jsonPropertyName = fieldName;
			if (jsonAnnotation != null) {
				jsonPropertyName = jsonAnnotation.value();
			}
			try {

				if (setColumns.contains(jsonPropertyName)) {
					Object val = resultSet.getObject(jsonPropertyName);
					if (field.getType().equals(Date.class)) {
						if (val.toString().length() > 10) {
							field.set(t, DateUtil.sqlLongDateToUtilDate(resultSet.getTimestamp(jsonPropertyName)));
						}
					} else {
						// if (field.getType().equals(val.getClass())) {
						// T valT = (T) mapper.convertValue(val,
						// field.getType());
						// field.set(t, valT);
						// } else {
						T valT = (T) mapper.convertValue(val, field.getType());
						field.set(t, valT);
						// }
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			field.setAccessible(sccessible);
		}
		return t;
	}

	public static List<String> getAllColumnsName(Class clazz, Set<String> excludeColumns) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		List<String> lstColumn = new ArrayList<>();
		for (Field field : clazz.getDeclaredFields()) {
			JsonProperty jsonAnnotation = field.getAnnotation(JsonProperty.class);
			if (jsonAnnotation != null && !excludeColumns.contains(jsonAnnotation.value())) {
				lstColumn.add(jsonAnnotation.value());
			}
		}
		return lstColumn;
	}

	public static LinkedHashMap<Integer, String> getAllFieldTypeByLstName(List<String> lstColumn, Object obj)
			throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		LinkedHashMap<String, String> mapType = new LinkedHashMap<>();

		for (Field field : obj.getClass().getDeclaredFields()) {
			boolean sccessible = field.isAccessible();
			field.setAccessible(true);
			JsonProperty jsonAnnotation = field.getAnnotation(JsonProperty.class);
			if (jsonAnnotation != null) {
				String columnName = jsonAnnotation.value();
				if (lstColumn.contains(columnName)) {
					mapType.put(columnName, field.getType().getName());
				}
			}
			field.setAccessible(sccessible);
		}
		LinkedHashMap<Integer, String> mapRs = new LinkedHashMap<>();
		int index = 0;
		for (String columnName : lstColumn) {
			mapType.containsKey(columnName);
			mapRs.put(index, mapType.get(columnName));
			index++;
		}
		return mapRs;
	}

	public static List<Object> getAllValuesByLstName(List<String> lstColumn, Object obj, String ignorePk)
			throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		Map<String, Object> mValue = new HashMap<>();
		for (Field field : obj.getClass().getDeclaredFields()) {
			boolean sccessible = field.isAccessible();
			field.setAccessible(true);
			JsonProperty jsonAnnotation = field.getAnnotation(JsonProperty.class);
			if (jsonAnnotation != null) {
				String columnName = jsonAnnotation.value();
				if (ignorePk != null && ignorePk.equals(columnName)) {
					continue;
				}
				if (lstColumn.contains(columnName)) {
					Object val = field.get(obj);
					mValue.put(columnName, val);
				}
			}
			field.setAccessible(sccessible);
		}
		List<Object> lstValue = new ArrayList<>();
		for (String columnName : lstColumn) {
			if (ignorePk != null && ignorePk.equals(columnName)) {
				continue;
			}
			lstValue.add(mValue.get(columnName));
		}
		return lstValue;
	}

	public static List<Object> getAllValuesByLstNameMap(List<String> lstColumn, Map<String, String> obj,
			String ignorePk) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		List<Object> lstValue = new ArrayList<>();
		for (String key : lstColumn) {
			lstValue.add(obj.get(key));
		}
		return lstValue;
	}

	public static boolean hasColumn(ResultSet rs, String columnName) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		int columns = rsmd.getColumnCount();
		for (int x = 1; x <= columns; x++) {
			if (columnName.equals(rsmd.getColumnName(x))) {
				return true;
			}
		}
		return false;
	}

	public static Set<String> getColumnsName(ResultSet rs) throws SQLException {
		Set<String> setColumns = new HashSet<>();
		ResultSetMetaData rsmd = rs.getMetaData();
		int columns = rsmd.getColumnCount();
		for (int x = 1; x <= columns; x++) {
			setColumns.add(rsmd.getColumnName(x));
		}
		return setColumns;
	}

	public static <T> T callSetter(T t, String methodName, Object value) throws Exception {
		Method m = t.getClass().getDeclaredMethod(methodName, Map.class);
		if (m != null && value != null) {
			m.invoke(t, value);
		}
		return t;
	}

}
