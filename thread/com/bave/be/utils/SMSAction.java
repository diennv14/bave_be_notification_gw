package com.bave.be.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

//Visist http://http://esms.vn/SMSApi/ApiSendSMSNormal for more information about API
//Â© 2013 esms.vn
//Website: http://esms.vn/
//Hotline: 0902.435.340
//skype: giangsangdesign
//Chi tiet huong dan cach su dung: http://esms.vn/blog/3-buoc-de-co-the-gui-tin-nhan-tu-website-ung-dung-cua-ban-bang-sms-api-cua-esmsvn
public class SMSAction {

	private String message;
	private String phone;
	private int type = 3;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String execute() {

		return "SUCCESS";

	}

	public String sendGetXML(String APIKey, String SecretKey) throws IOException {

		String url = "http://rest.esms.vn/MainService.svc/xml/SendMultipleMessage_V4_get?ApiKey="
				+ URLEncoder.encode(APIKey, "UTF-8") + "&SecretKey=" + URLEncoder.encode(SecretKey, "UTF-8")
				+ "&SmsType=" + type + "&Phone=" + URLEncoder.encode(phone, "UTF-8") + "&Content="
				+ URLEncoder.encode(message, "UTF-8");

		URL obj;
		try {
			obj = new URL(url);

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			// you need to encode ONLY the values of the parameters

			con.setRequestMethod("GET");

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
			if (responseCode == 200)// Ä�Ã£ gá»�i URL thÃ nh cÃ´ng, tuy nhiÃªn
									// báº¡n pháº£i
									// tá»± kiá»ƒm tra CodeResult xem tin nháº¯n
									// cÃ³
									// gá»­i thÃ nh cÃ´ng khÃ´ng, vÃ¬ cÃ³ thá»ƒ
									// tÃ i khoáº£n
									// báº¡n khÃ´ng Ä‘á»§ tiá»�n thÃ¬ sáº½
									// tháº¥t báº¡i
			{
				// Check CodeResult from response
			}
			// Ä�á»�c Response
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			System.out.println(response.toString());
			Document document = loadXMLFromString(response.toString());
			document.getDocumentElement().normalize();
			System.out.println("Root element :" + document.getDocumentElement().getNodeName());
			Node node = document.getElementsByTagName("CodeResult").item(0);
			System.out.println("CodeResult: " + node.getTextContent());
			node = document.getElementsByTagName("SMSID").item(0);
			if (node != null) {
				System.out.println("SMSID: " + node.getTextContent());
			} else {
				node = document.getElementsByTagName("ErrorMessage").item(0);
				System.out.println("ErrorMessage: " + node.getTextContent());
			}
			// document.getElementsByTagName("CountRegenerate").item(0).va
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "SUCCESS";

	}

	public String sendGetJSON(String APIKey, String SecretKey) throws IOException {

		String url = "http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get?ApiKey="
				+ URLEncoder.encode(APIKey, "UTF-8") + "&SecretKey=" + URLEncoder.encode(SecretKey, "UTF-8")
				+ "&SmsType=" + type + "&Phone=" + URLEncoder.encode(phone, "UTF-8") + "&Content="
				+ URLEncoder.encode(message, "UTF-8");

		URL obj;
		String codeResult = "";
		String errorMessage = "";
		try {
			obj = new URL(url);

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			// you need to encode ONLY the values of the parameters

			con.setRequestMethod("GET");
			con.setRequestProperty("Accept", "application/json");

			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);
			if (responseCode == 200)// Ä�Ã£ gá»�i URL thÃ nh cÃ´ng, tuy nhiÃªn
									// báº¡n pháº£i
									// tá»± kiá»ƒm tra CodeResult xem tin nháº¯n
									// cÃ³
									// gá»­i thÃ nh cÃ´ng khÃ´ng, vÃ¬ cÃ³ thá»ƒ
									// tÃ i khoáº£n
									// báº¡n khÃ´ng Ä‘á»§ tiá»�n thÃ¬ sáº½
									// tháº¥t báº¡i
			{
				// Check CodeResult from response
			}
			// Ä�á»�c Response
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();

			// print result
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> json = mapper.readValue(response.toString(), new TypeReference<Map<String, String>>() {
			});
			if (json.get("CodeResult") != null) {
				codeResult = json.get("CodeResult").toString();
			}

			if (json.get("ErrorMessage") != null) {
				errorMessage = json.get("ErrorMessage").toString();
			}
			System.out.println("CodeResult=" + json.get("CodeResult"));
			System.out.println("SMSID=" + json.get("SMSID"));
			System.out.println("ErrorMessage=" + json.get("ErrorMessage"));
			// document.getElementsByTagName("CountRegenerate").item(0).va
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (codeResult != null && codeResult.equals("103")) {
			return errorMessage;
		} else {
			return "SUCCESS";
		}
	}

	public Document loadXMLFromString(String xml) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputSource is = new InputSource(new StringReader(xml));
		return builder.parse(is);
	}
}
