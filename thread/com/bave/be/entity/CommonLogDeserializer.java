package com.bave.be.entity;

import java.util.Map;

import org.apache.kafka.common.serialization.Deserializer;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CommonLogDeserializer implements Deserializer {

	@Override
	public void close() {
		// TODO Auto-generated method stub

	}

	@Override
	public void configure(Map arg0, boolean arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public Object deserialize(String arg0, byte[] arg1) {
		ObjectMapper mapper = new ObjectMapper();
		CommonLog notification = null;
		try {
			notification = mapper.readValue(arg1, CommonLog.class);
		} catch (Exception e) {

			e.printStackTrace();
		}
		return notification;
	}

}
