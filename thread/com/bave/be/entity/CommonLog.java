package com.bave.be.entity;

public class CommonLog {

	private String garaName;

	private String content;

	private String logType;

	public String getGaraName() {
		return garaName;
	}

	public void setGaraName(String garaName) {
		this.garaName = garaName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}
}
