package com.bave.be.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class NotificationTemplate {
	@JsonProperty("id")
	private int id;
	@JsonProperty("name")
	private String name;
	@JsonProperty("description")
	private String description;
	@JsonProperty("message")
	private String message;
	@JsonProperty("action")
	private String action;
	@JsonProperty("channel")
	private String channel;	

	public NotificationTemplate() {
		super();
	}

	public NotificationTemplate(int id, String name, String description, String message) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.message = message;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}
}
