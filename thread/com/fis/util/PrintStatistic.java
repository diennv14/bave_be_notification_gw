package com.fis.util;

public interface PrintStatistic
{
	String printStatistic();
	
	String printWarning();
}
