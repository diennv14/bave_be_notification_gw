package com.fis.util;

import java.sql.Connection;
import java.sql.SQLException;

import com.fss.sql.Database;

/**
 * <p>
 * Title: MCA GW
 * </p>
 * 
 * <p>
 * Description:
 * </p>
 * 
 * <p>
 * Copyright: Copyright (c) 2011
 * </p>
 * 
 * <p>
 * Company: FIS-SOT
 * </p>
 * 
 * @author LiemLT
 * @version 1.0
 */

public abstract class AbstractBean {
	public AbstractBean() {
	}

	protected Connection mcnMain = null;

	// set connection
	public void setConnection(Connection cn) {
		this.mcnMain = cn;
	}

	public Connection getConnection() {
		return mcnMain;
	}

	// init all db object
	public abstract void init() throws SQLException;

	// Close all db object
	public abstract void close();

	// close connection
	public void closeConnection() {
		try {
			if (mcnMain != null && !mcnMain.isClosed()) {
				Database.closeObject(mcnMain);
				mcnMain = null;
			}
		} catch (SQLException ex) {
		}
	}
}
