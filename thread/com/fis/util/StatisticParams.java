package com.fis.util;

public class StatisticParams
{
	public static int    DUR_LEVEL1_WARNING  = 300;		// 300s, 5 phut
	public static int    DUR_LEVEL2_ERROR    = 600;		// 600s, 10 phut
	public static int    DUR_LEVEL3_CRITICAL = 3600;	    // 3600s, 60 phut
												   
	public static String STR_LEVEL1_WARNING  = "L1 Warning"; // 300s, 5 phut
	public static String STR_LEVEL2_ERROR    = "L2 ERROR";   // 600s, 10 phut
	public static String STR_LEVEL3_CRITICAL = "L3 CRITICAL"; // 3600s, 60 phut
												   
	public static String STR_CHECK		 = "Check";	 // 3600s, 60 phut
	public static String STR_NORMAL		= "Normal";	// 3600s, 60 phut
												   
}
