package com.fis.thread;

import java.util.Vector;

import com.fss.thread.ManageableThread;
import com.fss.thread.ParameterType;
import com.fss.util.AppException;

/**
 * 
 * @author Sonnh17
 * 
 */

public abstract class ManageableThreadEx extends ManageableThread {
	private int miDebugLevel = 0;
	public String strVersion = "1.0";

	public ThreadManagerEx getThreadManagerEx() {
		return ((ThreadManagerEx) mmgrMain);
	}

	public void fillParameter() throws AppException {
		miDebugLevel = loadInteger("DebugLevel");

		super.fillParameter();
	}

	public Vector getParameterDefinition() {
		Vector vtReturn = super.getParameterDefinition();
		vtReturn.addElement(createParameterDefinition("DebugLevel", "", ParameterType.PARAM_TEXTBOX_MASK, "9", ""));

		return vtReturn;
	}

	public void debugMonitor(String strLogContent, int iDebugLevel) {
		if (iDebugLevel <= miDebugLevel) {
			logMonitor("(B" + String.valueOf(iDebugLevel) + ")" + strLogContent);
		}
	}

	public void logError(String strLogContent) {
		logMonitor("Error ocurred: " + strLogContent);
	}

	public int logProcessAlive(int processLive) {
		if (processLive == 60) {
			logMonitor("Process running :D:D");
			processLive = 0;
		}
		processLive = processLive + 1;
		return processLive;
	}
}
