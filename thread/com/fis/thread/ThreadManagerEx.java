package com.fis.thread;

import java.sql.Connection;

import com.fss.thread.ProcessorListener;
import com.fss.thread.ThreadManager;

public class ThreadManagerEx extends ThreadManager {

	// private LinkQueue<Account> AccountQueue = new
	// LinkQueue<Account>(999999,"Account");
	int ProcessId = 0;

	public int getProcessId() {
		return ProcessId;
	}

	public void setProcessId(int processId) {
		ProcessId = processId;
	}

	public ThreadManagerEx() throws Exception {
		super();
	}

	public ThreadManagerEx(int iPort, ProcessorListener lsn) throws Exception {
		super(iPort, lsn);
	}

	@Override
	public String getSelectLoadableThreadSQLCommand(Connection cn) {
		return "SELECT THREAD_ID,THREAD_NAME,CLASS_NAME,STARTUP_TYPE,STATUS,COMMAND FROM THREAD WHERE STARTUP_TYPE > 0 AND PROCESS_ID = '"
				+ ProcessId + "' ORDER BY THREAD_ID";
	}

	@Override
	public String getSelectDisbledThreadSQLCommand(Connection cn) {
		return "SELECT THREAD_ID FROM THREAD WHERE STARTUP_TYPE = 0 AND PROCESS_ID = '" + ProcessId + "'";
	}

}
