//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.fss.thread;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;
import org.jgroups.Address;
import org.jgroups.Message;

import com.fis.ha.cluster.ManageableThreadSate;
import com.fis.ha.jgroups.ClusterCommand;
import com.fis.log.Logable;
import com.fis.util.Tools;
import com.fss.ddtp.DDTP;
import com.fss.ddtp.SocketTransmitter;
import com.fss.dictionary.Dictionary;
import com.fss.dictionary.DictionaryNode;
import com.fss.sql.Database;
import com.fss.util.AppException;
import com.fss.util.DateUtil;
import com.fss.util.FileUtil;
import com.fss.util.LogInterface;
import com.fss.util.StringUtil;

public abstract class ManageableThread extends ParameterUtil implements Runnable, Logable {
	private static final int MAX_LOG_SIZE = 16384;
	private static final int MAX_LOG_DATE = 60;
	public static final int FILE_MANAGEMENT = 0;
	public static final int DB_MANAGEMENT = 1;
	public volatile int miThreadStatus = 2;
	public volatile int miThreadCommand = 0;
	public int miManageMethod = 1;
	protected Thread mthrMain;
	protected String mstrThreadID = "";
	protected String mstrLogID = "";
	protected volatile String mstrThreadName = "";
	protected volatile String mstrClassName = "";
	protected volatile String mstrStartupType = "";
	protected Vector mvtSchedule;
	private volatile AtomicBoolean mabRunning = new AtomicBoolean(false);
	protected Map mmapParam = new LinkedHashMap();
	protected Connection mcnMain = null;
	protected Dictionary mdicSchedule;
	protected ThreadManager mmgrMain;
	protected String mstrLogFileName;
	protected boolean mbAlertByMail = false;
	protected boolean mbAutoConnectDB = false;
	protected int miExecutionCount = 0;
	protected volatile int miDelayTime = 300;
	private static Dictionary mdicThreadList;
	private static long mlThreadFileLastModified;
	private static long mlThreadFileSize;
	public volatile int miLogLevel = 2;
	public String mstrGroupID = "";
	public String mstrGroupName = "";
	public boolean mblFirstStandbyMode = false;
	static Logger logger;

	static {
		logger = ThreadManager.clusterLogger;
	}

	public ManageableThread() {
	}

	public void setConnection(Connection cn) {
		this.mcnMain = cn;
	}

	public Connection getConnection() {
		return this.mcnMain;
	}

	public boolean shouldAlertByMail() {
		return this.mbAlertByMail;
	}

	public void setThreadID(String strThreadID) {
		this.mstrThreadID = strThreadID;
	}

	public String getThreadID() {
		return this.mstrThreadID;
	}

	public void setThreadName(String strThreadName) {
		this.mstrThreadName = strThreadName;
		if (this.mthrMain != null) {
			this.mthrMain.setName(this.mstrThreadName);
		}

	}

	public String getThreadName() {
		return this.mstrThreadName;
	}

	public void setClassName(String strClassName) {
		this.mstrClassName = strClassName;
	}

	public String getClassName() {
		return this.mstrClassName;
	}

	public int getThreadStatus() {
		return this.miThreadStatus;
	}

	public void removeAllParameter() {
		this.mmapParam.clear();
	}

	public Map getParameter() {
		return this.mmapParam;
	}

	public Object getParameter(Object parameterKey) {
		return this.mmapParam.get(parameterKey);
	}

	public void setParameter(String strParameterName, Object objParameterValue) {
		this.mmapParam.put(strParameterName, objParameterValue);
	}

	public void setParameter(Map mapParam) {
		this.mmapParam.clear();
		this.mmapParam.putAll(mapParam);
	}

	public String getStartupType() {
		return this.mstrStartupType;
	}

	public void setStartupType(String strStartupType) {
		this.mstrStartupType = strStartupType;
	}

	protected int exec(String strCommand, int iTimeout) throws Exception {
		Process process = Runtime.getRuntime().exec(strCommand);
		new ProcessCleaner(process);
		StringBuffer strLog = new StringBuffer();
		StringBuffer strErrLog = new StringBuffer();
		long lStartDate = System.currentTimeMillis();

		try {
			label57: do {
				int iAvailable = process.getInputStream().available();
				int iErrAvailable = process.getErrorStream().available();

				while (true) {
					while (true) {
						if (iAvailable <= 0 && iErrAvailable <= 0) {
							continue label57;
						}

						if (iAvailable > 0) {
							char cLog = (char) process.getInputStream().read();
							if (cLog != '\n' && cLog != '\r') {
								strLog.append(cLog);
							} else {
								if (strLog.length() > 0) {
									this.logMonitor("\t" + strLog.toString());
								}

								lStartDate = (new Date()).getTime();
								strLog = new StringBuffer();
							}

							--iAvailable;
						} else {
							char cErrLog = (char) process.getErrorStream().read();
							if (cErrLog != '\n' && cErrLog != '\r') {
								strErrLog.append(cErrLog);
							} else {
								if (strErrLog.length() > 0) {
									this.logMonitor("\t" + strErrLog.toString());
								}

								lStartDate = (new Date()).getTime();
								strErrLog = new StringBuffer();
							}

							--iAvailable;
						}
					}
				}
			} while (System.currentTimeMillis() - lStartDate <= (long) iTimeout);

			process.destroy();
			throw new AppException("Time out reached, command execution was destroyed.");
		} catch (AppException var12) {
			throw var12;
		} catch (Exception var13) {
			var13.printStackTrace();
			this.logError("Process terminated with code " + process.exitValue());
			return process.exitValue();
		}
	}

	protected void openConnection() throws Exception {
		this.closeConnection();
		this.mcnMain = this.mmgrMain.getConnection();
		if (this.mcnMain == null) {
			throw new Exception("Could not connect to database, check connection parameter");
		}
	}

	protected void closeConnection() {
		Database.closeObject(this.mcnMain);
		this.mcnMain = null;
	}

	protected void logStart(String strFileName) throws Exception {
		if (this.miManageMethod == 1 && this.mcnMain != null && !this.mcnMain.isClosed()) {
			this.mstrLogID = Database.getSequenceValue(this.mcnMain, "THREAD_LOG_SEQ", "THREAD_LOG", "LOG_ID");
			String strSQL = "INSERT INTO THREAD_LOG(LOG_ID,THREAD_ID,FILE_NAME,START_DATE) VALUES(?,?,?,SYSDATE)";
			PreparedStatement stmt = null;

			try {
				stmt = this.mcnMain.prepareStatement(strSQL);
				stmt.setString(1, this.mstrLogID);
				stmt.setString(2, this.mstrThreadID);
				stmt.setString(3, strFileName);
				stmt.executeUpdate();
			} finally {
				Database.closeObject(stmt);
			}
		}

	}

	protected void logDetail(String strDescription, String strContext, String strNote) throws Exception {
		if (this.miManageMethod == 1 && this.mcnMain != null && !this.mcnMain.isClosed()) {
			String strSQL = "INSERT INTO THREAD_LOG_DETAIL(LOG_ID,CONTEXT,DESCRIPTION,NOTE,LOG_DATE)";
			strSQL = strSQL + " VALUES(?,?,?,?,SYSDATE)";
			PreparedStatement stmt = null;

			try {
				stmt = this.mcnMain.prepareStatement(strSQL);
				stmt.setString(1, this.mstrLogID);
				stmt.setString(2, strContext);
				stmt.setString(3, strDescription);
				stmt.setString(4, strNote);
				stmt.executeUpdate();
			} finally {
				Database.closeObject(stmt);
			}
		}

	}

	protected void logComplete(String strTotalRecords, String strSuccessRecords, String strErrorRecords,
			String strStatus) throws Exception {
		if (this.miManageMethod == 1 && this.mcnMain != null && !this.mcnMain.isClosed()) {
			String strSQL = "UPDATE THREAD_LOG SET TOTAL_RECORDS=?,SUCCESS_RECORDS=?,";
			strSQL = strSQL + "ERROR_RECORDS=?,STATUS=?,END_DATE=SYSDATE WHERE LOG_ID=?";
			PreparedStatement stmt = null;

			try {
				stmt = this.mcnMain.prepareStatement(strSQL);
				stmt.setString(1, strTotalRecords);
				stmt.setString(2, strSuccessRecords);
				stmt.setString(3, strErrorRecords);
				stmt.setString(4, strStatus);
				stmt.setString(5, this.mstrLogID);
				stmt.executeUpdate();
			} finally {
				Database.closeObject(stmt);
			}
		}

	}

	public String addSchedule(String strSchedule) throws Exception {
		return this.addSchedule(strSchedule, (SocketTransmitter) null, (LogInterface) null, (Connection) null);
	}

	protected String addSchedule(String strSchedule, SocketTransmitter channel, LogInterface log, Connection cn)
			throws Exception {
		return this.addSchedule(strSchedule, channel, log, cn, true);
	}

	protected String addSchedule(String strSchedule, SocketTransmitter channel, LogInterface log, Connection cn,
			boolean mblSynCluster) throws Exception {
		this.loadSchedule();
		ByteArrayInputStream is = new ByteArrayInputStream(strSchedule.getBytes());
		Dictionary dic = new Dictionary(is);
		int iMaxID = 0;

		for (int iIndex = 0; iIndex < this.mvtSchedule.size(); ++iIndex) {
			int iScheduleID = Integer
					.parseInt(((Dictionary) this.mvtSchedule.elementAt(iIndex)).getString("ScheduleID"));
			if (iScheduleID > iMaxID) {
				iMaxID = iScheduleID;
			}
		}

		String strScheduleID = String.valueOf(iMaxID + 1);
		dic.mndRoot.setChildValue("ScheduleID", strScheduleID);
		this.mvtSchedule.addElement(dic);
		this.storeSchedule(cn, mblSynCluster);
		if (log != null) {
			String userChannel = channel == null ? "Cluster" : channel.getUserName();
			String strLogID = log.logHeader(ThreadConstant.SYSTEM_THREAD_MANAGER, userChannel, "I");
			String strChangeID = log.logTableChange(strLogID, this.getThreadTableName(), strScheduleID, "Insert");
			log.logColumnChange(strChangeID, "SCHEDULE", "", strSchedule);
		}

		return strScheduleID;
	}

	public void updateSchedule(String strScheduleID, String strSchedule) throws Exception {
		this.updateSchedule(strScheduleID, strSchedule, (SocketTransmitter) null, (LogInterface) null,
				(Connection) null);
	}

	protected void updateSchedule(String strScheduleID, String strSchedule, SocketTransmitter channel, LogInterface log,
			Connection cn) throws Exception {
		this.updateSchedule(strScheduleID, strSchedule, channel, log, cn, true);
	}

	protected void updateSchedule(String strScheduleID, String strSchedule, SocketTransmitter channel, LogInterface log,
			Connection cn, boolean mblSynCluster) throws Exception {
		Dictionary dicOld = this.loadSchedule();
		ByteArrayInputStream is = new ByteArrayInputStream(strSchedule.getBytes());
		Dictionary dic = new Dictionary(is);
		boolean bFound = false;
		int iIndex = 0;

		while (!bFound && iIndex < this.mvtSchedule.size()) {
			int iScheduleID = Integer
					.parseInt(((Dictionary) this.mvtSchedule.elementAt(iIndex)).getString("ScheduleID"));
			if (iScheduleID == Integer.parseInt(strScheduleID)) {
				bFound = true;
			} else {
				++iIndex;
			}
		}

		if (!bFound) {
			throw new AppException("FSS-00012", "ThreadProcessor.updateSchedule", "Schedule");
		} else {
			if (dic.getString("ScheduleID") == null || dic.getString("ScheduleID").length() == 0) {
				dic.mndRoot.setChildValue("ScheduleID", strScheduleID);
			}

			this.mvtSchedule.setElementAt(dic, iIndex);
			this.storeSchedule(cn, mblSynCluster);
			if (log != null) {
				String userChannel = channel == null ? "Cluster" : channel.getUserName();
				String strLogID = log.logHeader(ThreadConstant.SYSTEM_THREAD_MANAGER, userChannel, "U");
				String strChangeID = log.logTableChange(strLogID, this.getThreadTableName(), strScheduleID, "Update");
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				dicOld.store(os);
				os.close();
				log.logColumnChange(strChangeID, "SCHEDULE", new String(os.toByteArray()), strSchedule);
			}

		}
	}

	public void deleteSchedule(String strScheduleID) throws Exception {
		this.deleteSchedule(strScheduleID, (SocketTransmitter) null, (LogInterface) null, (Connection) null);
	}

	protected void deleteSchedule(String strScheduleID, SocketTransmitter channel, LogInterface log, Connection cn)
			throws Exception {
		this.deleteSchedule(strScheduleID, channel, log, cn, true);
	}

	protected void deleteSchedule(String strScheduleID, SocketTransmitter channel, LogInterface log, Connection cn,
			boolean mblSynCluster) throws Exception {
		Dictionary dicOld = this.loadSchedule();
		boolean bFound = false;
		int iIndex = 0;

		while (!bFound && iIndex < this.mvtSchedule.size()) {
			int iScheduleID = Integer
					.parseInt(((Dictionary) this.mvtSchedule.elementAt(iIndex)).getString("ScheduleID"));
			if (iScheduleID == Integer.parseInt(strScheduleID)) {
				bFound = true;
			} else {
				++iIndex;
			}
		}

		if (!bFound) {
			throw new AppException("FSS-00012", "ThreadProcessor.updateSchedule", "Schedule");
		} else {
			this.mvtSchedule.removeElementAt(iIndex);
			this.storeSchedule(cn, mblSynCluster);
			if (log != null && channel != null) {
				String userChannel = channel == null ? "Cluster" : channel.getUserName();
				String strLogID = log.logHeader(ThreadConstant.SYSTEM_THREAD_MANAGER, userChannel, "D");
				String strChangeID = log.logTableChange(strLogID, this.getThreadTableName(), strScheduleID, "Delete");
				ByteArrayOutputStream os = new ByteArrayOutputStream();
				dicOld.store(os);
				os.close();
				log.logColumnChange(strChangeID, "SCHEDULE", new String(os.toByteArray()), "");
			}

		}
	}

	public String querySchedule() throws Exception {
		this.loadSchedule();
		Dictionary dic = ScheduleUtil.scheduleToScript(this.mvtSchedule);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		dic.store(os);
		return new String(os.toByteArray());
	}

	public Dictionary loadSchedule() throws Exception {
		Dictionary dic = new Dictionary();
		if (this.miManageMethod == 1) {
			Connection cn = null;
			try {
				cn = this.mmgrMain.getConnection();
				String strSQL = this.getSelectScheduleSQLCommand();
				if (cn != null) {
					Statement stmt = cn.createStatement();
					ResultSet rs = stmt.executeQuery(strSQL);
					if (!rs.next()) {
						throw new AppException("FSS-00021", "ThreadProcessor.loadSchedule", this.mstrThreadID);
					}
					InputStream is = rs.getAsciiStream(1);
					if (is != null) {
						dic.load(is);
					}
					rs.close();
					stmt.close();
				}
			} finally {
				Database.closeObject(cn);
			}
		} else if (this.miManageMethod == 0) {
			Dictionary dicThreadList = loadThreadConfig();
			DictionaryNode ndThread = dicThreadList.mndRoot.setChildValue(this.mstrThreadID, (String) null);
			DictionaryNode ndSchedule = ndThread.setChildValue("Schedule", (String) null);
			dic = new Dictionary(ndSchedule);
		}

		this.mvtSchedule = ScheduleUtil.scriptToSchedule(dic);
		return dic;
	}

	protected String getSelectScheduleSQLCommand() {
		return "SELECT SCHEDULE FROM " + this.getThreadTableName() + " WHERE " + this.getThreadIDFieldName() + "="
				+ this.mstrThreadID;
	}

	protected String getUpdateScheduleSQLCommand() {
		return "UPDATE " + this.getThreadTableName() + " SET SCHEDULE=? WHERE " + this.getThreadIDFieldName() + "=?";
	}

	public void storeSchedule(Connection cn) throws Exception {
		this.storeSchedule(cn, true);
	}

	public void storeSchedule(Connection cn, boolean mblSynCluster) throws Exception {
		Dictionary dic = new Dictionary();

		DictionaryNode ndThread;
		for (int iIndex = 0; iIndex < this.mvtSchedule.size(); ++iIndex) {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			((Dictionary) this.mvtSchedule.elementAt(iIndex)).store(os);
			ndThread = new DictionaryNode(dic);
			ndThread.mstrName = "Schedule";
			ndThread.mstrValue = new String(os.toByteArray());
			dic.mndRoot.addChild(ndThread);
		}

		if (this.miManageMethod == 1) {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			dic.store(os);
			boolean bConnectionNotExist = cn == null;

			try {
				if (bConnectionNotExist) {
					cn = this.mmgrMain.getConnection();
				}

				String strSQL = this.getUpdateScheduleSQLCommand();
				PreparedStatement stmt = cn.prepareStatement(strSQL);
				ByteArrayInputStream is = new ByteArrayInputStream(os.toByteArray());
				stmt.setAsciiStream(1, is, os.size());
				stmt.setInt(2, Integer.parseInt(this.mstrThreadID));
				stmt.executeUpdate();
				stmt.close();
			} finally {
				if (bConnectionNotExist) {
					Database.closeObject(cn);
				}

			}
		} else if (this.miManageMethod == 0) {
			String var15 = ThreadConstant.THREAD_CONFIG_FILE;
			synchronized (ThreadConstant.THREAD_CONFIG_FILE) {
				Dictionary dicThreadList = loadThreadConfig();
				ndThread = dicThreadList.mndRoot.setChildValue(this.mstrThreadID, (String) null);
				DictionaryNode ndSchedule = ndThread.setChildValue("Schedule", (String) null);
				ndSchedule.setChildList(dic.mndRoot.getChildList());
				this.storeThreadConfig(dicThreadList, mblSynCluster);
			}
		}

	}

	public void start() {
		this.destroyNoSyn();
		this.mthrMain = new Thread(this);
		this.mthrMain.setName(this.mstrThreadName);
		this.mthrMain.start();
	}

	public void destroyNoSyn() {
		if (this.mthrMain != null) {
			try {
				this.mthrMain.stop();
				this.mthrMain = null;
			} catch (Exception var2) {
				var2.printStackTrace();
			}
		}

		this.miThreadCommand = 0;
		this.miThreadStatus = 2;
		this.mmgrMain.threadController.removeThread(this);
		this.mabRunning.set(false);
		this.updateStatus();
	}

	public void destroy() {
		this.destroyNoSyn();
		this.synThreadState();
	}

	public void synThreadState() {
		if (this.mmgrMain.getClusterType() != 0 && this.mmgrMain.getClusterStatus() == 1) {
			logger.debug("Syn thread state: ThreadID=" + this.getThreadID() + ", status=" + this.getThreadStatus());
			Iterator var2 = this.mmgrMain.listManageableThreadSate.iterator();

			ManageableThreadSate threadState;
			while (var2.hasNext()) {
				threadState = (ManageableThreadSate) var2.next();
				if (threadState.getThreadID().equals(this.getThreadID())) {
					threadState.setThreadState(this.getThreadStatus());
					break;
				}
			}

			threadState = new ManageableThreadSate(this.getThreadID(), this.getThreadStatus());
			ClusterCommand command = new ClusterCommand(10, threadState);

			try {
				ThreadManager.clusterChannel
						.send(new Message((Address) null, ThreadManager.clusterChannel.getAddress(), command));
			} catch (Exception var4) {
				var4.printStackTrace();
			}
		}

	}

	protected void loadParameter() throws Exception {
		this.loadConfig();
		this.fillParameter();
		this.validateParameter();
	}

	public void fillLogFile() throws AppException {
		this.mstrLogFileName = this.loadDirectory("LogDir", true, true);
		this.mstrLogFileName = this.mstrLogFileName + StringUtil.format(new Date(), "yyyyMMdd") + ".log";
	}

	public void fillParameter() throws AppException {
		this.fillLogFile();
		String strLogLevel = StringUtil.nvl(this.getParameter("LogLevel"), "INFO");
		this.miLogLevel = 2;

		for (int i = 0; i < ThreadConstant.LOG_LEVEL.length; ++i) {
			if (strLogLevel.equalsIgnoreCase(ThreadConstant.LOG_LEVEL[i])) {
				this.miLogLevel = i;
				break;
			}
		}

		String strAutoConnect = StringUtil.nvl(this.getParameter("ConnectDB"), "");
		if (strAutoConnect.equals("Manual")) {
			this.mbAutoConnectDB = false;
		} else if (strAutoConnect.equals("Automatic")) {
			this.mbAutoConnectDB = true;
		} else if (this.miManageMethod == 1) {
			this.mbAutoConnectDB = true;
		} else {
			this.mbAutoConnectDB = false;
		}

		this.mbAlertByMail = StringUtil.nvl(this.getParameter("AlertByMail"), "N").equals("Y");
		this.miDelayTime = this.loadUnsignedInteger("DelayTime");
	}

	public void validateParameter() throws Exception {
		if (this.mstrLogFileName.length() > 0) {
			File flTest = new File(this.mstrLogFileName);
			if (!flTest.exists()) {
				try {
					if (!flTest.createNewFile()) {
						throw new AppException("Could not create log file", "ManageableThread.validateParameter",
								"LogDir");
					}
				} catch (Exception var3) {
					throw new AppException("Could not create log file", "ManageableThread.validateParameter", "LogDir");
				}
			}
		}

	}

	public void logActionStart(String strLog) {
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM HH:mm:ss");
		strLog = fmt.format(new Date()) + " START " + strLog + "\r\n";
		this.log(strLog, 6);
	}

	public void logActionStop(String strLog) {
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM HH:mm:ss");
		strLog = fmt.format(new Date()) + " STOP " + strLog + "\r\n";
		this.log(strLog, 7);
	}

	public void logActionRunImmediate(String strLog) {
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM HH:mm:ss");
		strLog = fmt.format(new Date()) + " RUNI " + strLog + "\r\n";
		this.log(strLog, 8);
	}

	public void logActionDestroy(String strLog) {
		SimpleDateFormat fmt = new SimpleDateFormat("dd/MM HH:mm:ss");
		strLog = fmt.format(new Date()) + " DESTR " + strLog + "\r\n";
		this.log(strLog, 9);
	}

	public void logMonitor(String strLog) {
		this.logMonitor(strLog, false, 2);
	}

	public void logMonitor(String strLog, boolean bSendMail) {
		this.logMonitor(strLog, bSendMail, 2);
	}

	public void logMonitor(String strLog, boolean bSendMail, int iLogType) {
		if (iLogType >= this.miLogLevel) {
			if (bSendMail) {
				this.alertByMail(strLog, iLogType);
			}

			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM HH:mm:ss");
			strLog = fmt.format(new Date()) + " " + getLogType(iLogType) + " " + strLog + "\r\n";
			this.log(strLog, iLogType);
		}

	}

	public void logTrace(String strLog) {
		this.logMonitor(strLog, false, 1);
	}

	public void logTrace(String strLog, boolean bSendMail) {
		this.logMonitor(strLog, bSendMail, 1);
	}

	public void logDebug(String strLog) {
		this.logMonitor(strLog, false, 0);
	}

	public void logDebug(String strLog, boolean bSendMail) {
		this.logMonitor(strLog, bSendMail, 0);
	}

	public void logInfo(String strLog) {
		this.logMonitor(strLog, false, 2);
	}

	public void logInfo(String strLog, boolean bSendMail) {
		this.logMonitor(strLog, bSendMail, 2);
	}

	public void logWarn(String strLog) {
		this.logMonitor(strLog, false, 3);
	}

	public void logWarn(String strLog, boolean bSendMail) {
		this.logMonitor(strLog, bSendMail, 3);
	}

	public void logError(String strLog) {
		this.logMonitor(strLog, false, 4);
	}

	public void logError(String strLog, boolean bSendMail) {
		this.logMonitor(strLog, bSendMail, 4);
	}

	public void logFatal(String strLog) {
		this.logMonitor(strLog, false, 5);
	}

	public void logFatal(String strLog, boolean bSendMail) {
		this.logMonitor(strLog, bSendMail, 5);
	}

	public void alertByMail(String strLog, int iLevel) {
		this.mmgrMain.alertByMail(this.mstrThreadID, strLog, iLevel);
	}

	public void alertByMail(String strLog) {
		this.alertByMail(strLog, 4);
	}

	public void log(String strLog, int iLogType) {
		if (!strLog.endsWith("\n")) {
			strLog = strLog + "\n";
		}

		this.logToUser(strLog, iLogType);
		this.logToFile(strLog);
	}

	public void log(String strLog) {
		this.log(strLog, 2);
	}

	public void logToUser(String strLog, int iLogType) {
		if (this.mmgrMain != null && this.mmgrMain.isConnected()) {
			DDTP request = new DDTP();
			request.setString("ThreadID", this.mstrThreadID);
			request.setString("LogResult", strLog);
			request.setString("LogType", String.valueOf(iLogType));
			request.setString("ThreadStatus", String.valueOf(this.miThreadStatus));

			try {
				this.mmgrMain.sendRequestToAll(request, "logMonitor", "MonitorProcessor");
			} catch (Exception var5) {
				var5.printStackTrace();
			}
		}

	}

	public void logToFile(String strLog) {
		if (this.mstrLogFileName != null && this.mstrLogFileName.length() > 0) {
			FileOutputStream os = null;

			try {
				os = new FileOutputStream(this.mstrLogFileName, true);
				FileChannel outChannel = os.getChannel();
				ByteBuffer buffer = ByteBuffer.allocate(strLog.length());
				byte[] bytes = strLog.getBytes();
				buffer.put(bytes);
				buffer.flip();
				outChannel.write(buffer);
			} catch (Exception var9) {
				var9.printStackTrace();
			} finally {
				FileUtil.safeClose(os);
			}
		}

	}

	public String getLogContent() {
		try {
			String strLogDir = this.loadDirectory("LogDir", true, true);
			StringBuffer strLog = new StringBuffer();
			Date dt = new Date();
			int iByteRead = 0;
			int iDateCount = 0;

			for (byte[] btBuffer = new byte[16384]; iByteRead < 16384
					&& iDateCount < 60; dt = DateUtil.addDay(dt, -1)) {
				FileInputStream is = null;

				try {
					File flLogFile = new File(strLogDir + StringUtil.format(dt, "yyyyMMdd") + ".log");
					if (flLogFile.exists()) {
						is = new FileInputStream(flLogFile);
						int iByteSkip = (int) flLogFile.length() + iByteRead - 16384;
						if (iByteSkip > 0) {
							is.skip((long) iByteSkip);
						}

						int iBufferRead = is.read(btBuffer);
						if (iBufferRead > 0) {
							iByteRead += iBufferRead;
							strLog.insert(0, new String(btBuffer, 0, iBufferRead));
						}
					}
				} catch (Exception var15) {
					var15.printStackTrace();
				} finally {
					FileUtil.safeClose(is);
				}

				++iDateCount;
			}

			return strLog.toString();
		} catch (Exception var17) {
			var17.printStackTrace();
			return "";
		}
	}

	public Vector getParameterDefinition() {
		Vector vtReturn = new Vector();
		Vector vtValue = new Vector();
		vtValue.addElement("DEBUG");
		vtValue.addElement("TRACE");
		vtValue.addElement("INFO");
		vtValue.addElement("WARN");
		vtValue.addElement("ERROR");
		vtValue.addElement("FATAL");
		vtReturn.addElement(createParameterDefinition("LogLevel", "DEBUG", ParameterType.PARAM_COMBOBOX, vtValue,
				"DEBUG<TRACE<INFO<WARN<ERROR<FATAL"));
		vtReturn.addElement(createParameterDefinition("LogDir", "", ParameterType.PARAM_TEXTBOX_MAX, "256", ""));
		vtValue = new Vector();
		vtValue.addElement("Automatic");
		vtValue.addElement("Manual");
		vtReturn.addElement(createParameterDefinition("ConnectDB", "", ParameterType.PARAM_COMBOBOX, vtValue, ""));
		vtValue = new Vector();
		vtValue.addElement("Y");
		vtValue.addElement("N");
		vtReturn.addElement(createParameterDefinition("AlertByMail", "", ParameterType.PARAM_COMBOBOX, vtValue, ""));
		vtReturn.addElement(createParameterDefinition("DelayTime", "", ParameterType.PARAM_TEXTBOX_MASK, "99990",
				"Delay time between sessions (in second)"));
		return vtReturn;
	}

	public String loadMandatory(String strParameterName) throws AppException {
		String strParameterValue = StringUtil.nvl(this.getParameter(strParameterName), "");
		return loadMandatory(strParameterName, strParameterValue);
	}

	public String loadPassword(String strParameterName) throws AppException {
		String strParameterValue = StringUtil.nvl(this.getParameter(strParameterName), "");
		return loadPassword(strParameterName, strParameterValue);
	}

	public int loadInteger(String strParameterName) throws AppException {
		String strParameterValue = StringUtil.nvl(this.getParameter(strParameterName), "");
		return loadInteger(strParameterName, strParameterValue);
	}

	public long loadLong(String strParameterName) throws AppException {
		String strParameterValue = StringUtil.nvl(this.getParameter(strParameterName), "");
		return loadLong(strParameterName, strParameterValue);
	}

	public double loadDouble(String strParameterName) throws AppException {
		String strParameterValue = StringUtil.nvl(this.getParameter(strParameterName), "");
		return loadDouble(strParameterName, strParameterValue);
	}

	public int loadUnsignedInteger(String strParameterName) throws AppException {
		String strParameterValue = StringUtil.nvl(this.getParameter(strParameterName), "");
		return loadUnsignedInteger(strParameterName, strParameterValue);
	}

	public long loadUnsignedLong(String strParameterName) throws AppException {
		String strParameterValue = StringUtil.nvl(this.getParameter(strParameterName), "");
		return loadUnsignedLong(strParameterName, strParameterValue);
	}

	public double loadUnsignedDouble(String strParameterName) throws AppException {
		String strParameterValue = StringUtil.nvl(this.getParameter(strParameterName), "");
		return loadUnsignedDouble(strParameterName, strParameterValue);
	}

	public Date loadTime(String strParameterName) throws AppException {
		String strParameterValue = StringUtil.nvl(this.getParameter(strParameterName), "");
		return loadTime(strParameterName, strParameterValue);
	}

	public Date loadDate(String strParameterName) throws AppException {
		String strParameterValue = StringUtil.nvl(this.getParameter(strParameterName), "");
		return loadDate(strParameterName, strParameterValue);
	}

	public String loadCustomDate(String strParameterName, String strDateFormat) throws AppException {
		String strParameterValue = StringUtil.nvl(this.getParameter(strParameterName), "");
		return loadCustomDate(strParameterName, strDateFormat, strParameterValue);
	}

	public String loadDirectory(String strParameterName, boolean bAutoCreate, boolean bMandatory) throws AppException {
		String strParameterValue = StringUtil.nvl(this.getParameter(strParameterName), "");
		return loadDirectory(strParameterName, strParameterValue, bAutoCreate, bMandatory);
	}

	public Object loadClass(String strParameterName) throws AppException {
		String strParameterValue = StringUtil.nvl(this.getParameter(strParameterName), "");
		return loadClass(strParameterName, strParameterValue);
	}

	public String loadResource(String strParameterName) throws AppException {
		String strParameterValue = StringUtil.nvl(this.getParameter(strParameterName), "");
		return loadResource(strParameterName, strParameterValue);
	}

	public String loadYesNo(String strParameterName) throws AppException {
		String strParameterValue = StringUtil.nvl(this.getParameter(strParameterName), "");
		return loadYesNo(strParameterName, strParameterValue);
	}

	public void runImmediate() throws Exception {
		Thread thr = new Thread() {
			public void run() {
				ManageableThread.this.logActionRunImmediate("Start performing immediate execution");

				try {
					ManageableThread.this.session();
				} catch (Exception var2) {
					var2.printStackTrace();
					ManageableThread.this.logError("Error occurred: " + Tools.getExceptionMessageWithCause(var2),
							ManageableThread.this.shouldAlertByMail());
				}

				ManageableThread.this.logActionRunImmediate("Immediate execution completed");
			}
		};
		thr.setName("RunImmediate:" + this.mstrThreadName);
		thr.start();
	}

	public int getScheduleStatus() throws Exception {
		this.loadSchedule();
		int iScheduleStatus = 1;
		if (this.mvtSchedule != null && this.mvtSchedule.size() > 0) {
			int iScheduleIndex = 0;

			while (iScheduleStatus != 0 && iScheduleIndex < this.mvtSchedule.size()) {
				this.mdicSchedule = (Dictionary) this.mvtSchedule.elementAt(iScheduleIndex);
				iScheduleStatus = ScheduleUtil.getStatus(this.mdicSchedule);
				if (iScheduleStatus == 2) {
					ScheduleUtil.changeNextDate(this.mdicSchedule, true);
				} else if (iScheduleStatus != 0) {
					++iScheduleIndex;
				}
			}
		} else {
			iScheduleStatus = 0;
		}

		return iScheduleStatus;
	}

	private boolean getCurrentStandbyMode() {
		return true;
	}

	public boolean getFirstStandbyMode() {
		return this.mblFirstStandbyMode;
	}

	public void setFirstStandbyMode(boolean blMode) {
		this.mblFirstStandbyMode = blMode;
	}

	private boolean checkStandbyMode() {
		try {
			if (this.getFirstStandbyMode() != this.getCurrentStandbyMode()) {
				this.logActionStart(this.mstrThreadName + " changed standby mode is="
						+ (this.getCurrentStandbyMode() ? "on" : "off"));
				this.mmgrMain.logAction("<FONT color=\"#0033CC\"><B>" + this.mstrThreadName + " standby is ="
						+ (this.getCurrentStandbyMode() ? "on" : "off") + " </B></FONT>");
			}

			return this.getCurrentStandbyMode();
		} catch (Exception var2) {
			var2.printStackTrace();
			return false;
		}
	}

	public void run() {
		try {
			this.loadConfig();
			this.mstrLogFileName = this.loadDirectory("LogDir", true, true);
			this.mstrLogFileName = this.mstrLogFileName + StringUtil.format(new Date(), "yyyyMMdd") + ".log";
		} catch (Exception var12) {
			var12.printStackTrace();
		}

		if (this.miThreadCommand == 1) {
			this.miThreadCommand = 0;
		}

		this.miThreadStatus = 1;
		this.mblFirstStandbyMode = this.mmgrMain.isConfigActiveNode();
		this.synThreadState();
		this.logActionStart(this.mstrThreadName + " started");
		this.mmgrMain.logAction("<FONT color=\"#0033CC\"><B>" + this.mstrThreadName + " started</B></FONT>");
		this.updateStatus();

		try {
			this.onStartThread();

			while (this.miThreadCommand != 2) {
				try {
					if (!this.mmgrMain.isServerLocked()) {
						int iScheduleStatus = this.getScheduleStatus();
						if (iScheduleStatus == 0 && this.session() && this.mdicSchedule != null) {
							ScheduleUtil.increaseExecutionCount(this.mdicSchedule);
							this.storeSchedule((Connection) null);
						}
					}
				} catch (Exception var11) {
					var11.printStackTrace();
					this.logError("Error occured: " + Tools.getExceptionMessageWithCause(var11),
							this.shouldAlertByMail());
				} finally {
					try {
						for (int iIndex = 0; iIndex < this.miDelayTime && this.miThreadCommand != 2; ++iIndex) {
							Thread.sleep(1000L);
						}
					} catch (Exception var13) {
						var13.printStackTrace();
						this.logError("Error occured: " + Tools.getExceptionMessageWithCause(var13),
								this.shouldAlertByMail());
					}

				}
			}
		} catch (Exception var15) {
			var15.printStackTrace();
			this.logError("Error occured: " + Tools.getExceptionMessageWithCause(var15), this.shouldAlertByMail());
		}

		this.miThreadStatus = 2;
		if (this.miThreadCommand == 2) {
			this.miThreadCommand = 0;
		}

		this.logActionStop(this.mstrThreadName + " stopped");
		this.mmgrMain.logAction("<FONT color=\"#CC0033\"><B>" + this.mstrThreadName + " stopped</B></FONT>");
		this.onStopThread();
		this.updateStatus();
		this.synThreadState();
	}

	protected void onStartThread() throws Exception {
	}

	protected void onStopThread() {
	}

	public boolean session() throws Exception {
		if (this.mabRunning.get()) {
			throw new Exception("Thread is running");
		} else {
			this.mabRunning.set(true);

			try {
				this.loadParameter();
				this.beforeSession();
				this.processSession();
				return true;
			} catch (Exception var39) {
				var39.printStackTrace();
				this.logError("Error occured: " + Tools.getExceptionMessageWithCause(var39), this.shouldAlertByMail());
			} finally {
				try {
					this.afterSession();
				} catch (Exception var37) {
					var37.printStackTrace();
					this.logError("Error occured: " + Tools.getExceptionMessageWithCause(var37),
							this.shouldAlertByMail());
				} finally {
					this.mabRunning.set(false);
				}

			}

			return false;
		}
	}

	public void loadConfig() throws Exception {
		if (this.miManageMethod == 0) {
			String var1 = ThreadConstant.THREAD_CONFIG_FILE;
			synchronized (ThreadConstant.THREAD_CONFIG_FILE) {
				Dictionary dicThreadList = loadThreadConfig();
				DictionaryNode ndThread = dicThreadList.mndRoot.setChildValue(this.mstrThreadID, (String) null);
				DictionaryNode ndParameter = ndThread.setChildValue("Parameter", (String) null);
				this.mmapParam = (new Dictionary(ndParameter)).toMap();
			}
		} else {
			Connection cn = null;

			try {
				cn = this.mmgrMain.getConnection();
				String strSQL = this.getSelectParameterSQLCommand();
				if (cn != null) {
					Statement stmt = cn.createStatement();
					ResultSet rs = stmt.executeQuery(strSQL);

					while (rs.next()) {
						String strName = rs.getString(1);
						String strValue = StringUtil.nvl(rs.getString(2), "");
						if (strValue.startsWith("$Vector$")) {
							byte[] bt = rs.getString(2).getBytes();
							ByteArrayInputStream is = new ByteArrayInputStream(bt);
							Dictionary dic = new Dictionary(is);
							is.close();
							Vector vt = dic.mndRoot.getChild("$Vector$").getVector();
							this.setParameter(strName, vt);
						} else {
							this.setParameter(strName, StringUtil.nvl(strValue, ""));
						}
					}

					rs.close();
					stmt.close();
				}
			} finally {
				Database.closeObject(cn);
			}
		}

	}

	protected String getThreadTableName() {
		return "THREAD";
	}

	protected String getThreadParamTableName() {
		return "THREAD_PARAM";
	}

	protected String getThreadIDFieldName() {
		return "THREAD_ID";
	}

	protected String getThreadNameFieldName() {
		return "THREAD_NAME";
	}

	protected String getClassNameFieldName() {
		return "CLASS_NAME";
	}

	protected String getStartupTypeFieldName() {
		return "STARTUP_TYPE";
	}

	protected String getParameterNameFieldName() {
		return "PARAM_NAME";
	}

	protected String getParameterValueFieldName() {
		return "PARAM_VALUE";
	}

	protected String getSelectParameterSQLCommand() {
		return "SELECT " + this.getParameterNameFieldName() + "," + this.getParameterValueFieldName() + " FROM "
				+ this.getThreadParamTableName() + " WHERE " + this.getThreadIDFieldName() + "=" + this.mstrThreadID;
	}

	public void storeConfig() throws Exception {
		this.storeConfig((SocketTransmitter) null, (LogInterface) null, (Connection) null);
	}

	protected void storeConfig(SocketTransmitter channel, LogInterface log, Connection cn) throws Exception {
		this.storeConfig(channel, log, cn, true);
	}

	protected void storeConfig(SocketTransmitter channel, LogInterface log, Connection cn, boolean mblSynCluster)
			throws Exception {
		if (this.miManageMethod == 0) {
			String var5 = ThreadConstant.THREAD_CONFIG_FILE;
			synchronized (ThreadConstant.THREAD_CONFIG_FILE) {
				Dictionary dicThreadList = loadThreadConfig();
				DictionaryNode ndThread = dicThreadList.mndRoot.getChild(this.mstrThreadID, true);
				DictionaryNode ndParameter = ndThread.getChild("Parameter", true);
				Map mapOld = (new Dictionary(ndParameter)).toMap();
				ndThread.setChildValue("Parameter", (String) null,
						(new Dictionary(this.mmapParam)).mndRoot.getChildList());
				this.storeThreadConfig(dicThreadList, mblSynCluster);
				if (log != null && channel != null) {
					String strLogID = log.logHeader(ThreadConstant.SYSTEM_THREAD_MANAGER, channel.getUserName(), "U");
					String strChangeID = log.logTableChange(strLogID, this.getThreadParamTableName(),
							this.getThreadID(), "Update");
					Iterator iterator = this.mmapParam.entrySet().iterator();

					while (iterator.hasNext()) {
						Entry entry = (Entry) iterator.next();
						Object objKey = entry.getKey();
						log.logColumnChange(strChangeID, StringUtil.nvl(objKey, ""),
								StringUtil.nvl(mapOld.remove(objKey), ""), StringUtil.nvl(entry.getValue(), ""));
					}

					iterator = mapOld.keySet().iterator();

					while (iterator.hasNext()) {
						Object objKey = iterator.next();
						log.logColumnChange(strChangeID, StringUtil.nvl(objKey, ""),
								StringUtil.nvl(mapOld.get(objKey), ""), "");
					}
				}
			}
		} else {
			Object[] objKeyList = this.mmapParam.keySet().toArray();
			Object[] objValueList = this.mmapParam.values().toArray();
			boolean bConnectionNotExist = cn == null;

			try {
				if (bConnectionNotExist) {
					cn = this.mmgrMain.getConnection();
					cn.setAutoCommit(false);
				}

				this.storeConfigToDB(cn, objKeyList, objValueList, channel, log);
				if (bConnectionNotExist) {
					cn.commit();
				}
			} finally {
				if (bConnectionNotExist) {
					Database.closeObject(cn);
				}

			}
		}

	}

	protected String getUpdateParameterSQLCommand() {
		return "UPDATE " + this.getThreadParamTableName() + " SET " + this.getParameterValueFieldName() + "=? WHERE "
				+ this.getParameterNameFieldName() + "=? AND " + this.getThreadIDFieldName() + "=" + this.mstrThreadID;
	}

	protected String getInsertParameterSQLCommand() {
		return "INSERT INTO " + this.getThreadParamTableName() + "(" + this.getThreadIDFieldName() + ","
				+ this.getParameterNameFieldName() + "," + this.getParameterValueFieldName() + ") VALUES("
				+ this.mstrThreadID + ",?,?)";
	}

	public String storeConfigToDB(Connection cn, Object[] objKeyList, Object[] objValueList) throws Exception {
		return this.storeConfigToDB(cn, objKeyList, objValueList, (SocketTransmitter) null, (LogInterface) null);
	}

	protected String storeConfigToDB(Connection cn, Object[] objKeyList, Object[] objValueList,
			SocketTransmitter channel, LogInterface log) throws Exception {
		String strLogID = null;
		if (log != null && channel != null) {
			strLogID = log.logHeader(ThreadConstant.SYSTEM_THREAD_MANAGER, channel.getUserName(), "U");
			log.logBeforeDelete(strLogID, this.getThreadParamTableName(),
					this.getThreadIDFieldName() + "=" + this.getThreadID());
		}

		String strSQL = this.getUpdateParameterSQLCommand();
		PreparedStatement stmtUpdate = cn.prepareStatement(strSQL, 1003, 1007);
		strSQL = this.getInsertParameterSQLCommand();
		PreparedStatement stmtInsert = cn.prepareStatement(strSQL, 1003, 1007);
		String strInsertParam = "";
		int iParamCount = objKeyList.length;

		for (int iParamIndex = 0; iParamIndex < iParamCount; ++iParamIndex) {
			if (objValueList[iParamIndex] != null) {
				stmtUpdate.setString(2, (String) objKeyList[iParamIndex]);
				Dictionary dic;
				ByteArrayOutputStream os;
				if (objValueList[iParamIndex] instanceof Vector) {
					dic = new Dictionary();
					dic.mndRoot.setChildVector("$Vector$", (Vector) objValueList[iParamIndex]);
					os = new ByteArrayOutputStream();
					dic.store(os);
					os.flush();
					stmtUpdate.setString(1, new String(os.toByteArray()));
					os.close();
				} else {
					stmtUpdate.setString(1, (String) objValueList[iParamIndex]);
				}

				if (stmtUpdate.executeUpdate() <= 0) {
					strInsertParam = strInsertParam + "'" + objKeyList[iParamIndex] + "',";
					stmtInsert.setString(1, (String) objKeyList[iParamIndex]);
					if (objValueList[iParamIndex] instanceof Vector) {
						dic = new Dictionary();
						dic.mndRoot.setChildVector("$Vector$", (Vector) objValueList[iParamIndex]);
						os = new ByteArrayOutputStream();
						dic.store(os);
						os.flush();
						stmtInsert.setString(2, new String(os.toByteArray()));
						os.close();
					} else {
						stmtInsert.setString(2, (String) objValueList[iParamIndex]);
					}

					stmtInsert.executeUpdate();
				}
			}
		}

		stmtInsert.close();
		stmtUpdate.close();
		if (log != null && channel != null) {
			log.logAfterInsert(strLogID, this.getThreadParamTableName(),
					this.getThreadIDFieldName() + "=" + this.getThreadID());
		}

		if (strInsertParam.length() > 0) {
			strInsertParam = strInsertParam.substring(0, strInsertParam.length() - 1);
		}

		return strInsertParam;
	}

	public boolean updateDBThread(Connection cn, String strThreadName, String strThreadClass,
			String strThreadStartupType, String strThreadID) throws Exception {
		return this.updateDBThread(cn, strThreadName, strThreadClass, strThreadStartupType, strThreadID,
				(SocketTransmitter) null, (LogInterface) null);
	}

	protected boolean updateDBThread(Connection cn, String strThreadName, String strThreadClass,
			String strThreadStartupType, String strThreadID, SocketTransmitter channel, LogInterface log)
					throws Exception {
		Vector vtChangeID = null;
		String strSQL;
		if (log != null && channel != null) {
			strSQL = log.logHeader(ThreadConstant.SYSTEM_THREAD_MANAGER, channel.getUserName(), "U");
			vtChangeID = log.logBeforeUpdate(strSQL, this.getThreadTableName(),
					this.getThreadIDFieldName() + "=" + strThreadID);
		}

		strSQL = "UPDATE " + this.getThreadTableName() + " SET " + this.getThreadNameFieldName() + " = ?,"
				+ this.getClassNameFieldName() + "=?," + this.getStartupTypeFieldName() + "=?" + " WHERE "
				+ this.getThreadIDFieldName() + " = ?";
		PreparedStatement stmt = cn.prepareStatement(strSQL);
		stmt.setString(1, strThreadName);
		stmt.setString(2, strThreadClass);
		stmt.setInt(3, Integer.parseInt(strThreadStartupType));
		stmt.setInt(4, Integer.parseInt(strThreadID));
		int iCount = stmt.executeUpdate();
		stmt.close();
		if (log != null && channel != null && vtChangeID != null) {
			log.logAfterUpdate(vtChangeID);
		}

		return iCount > 0;
	}

	public boolean updateFileThread(String strThreadName, String strClassName, String strStartupType,
			String strThreadID) throws Exception {
		return this.updateFileThread(strThreadName, strClassName, strStartupType, strThreadID, (SocketTransmitter) null,
				(LogInterface) null);
	}

	protected boolean updateFileThread(String strThreadName, String strClassName, String strStartupType,
			String strThreadID, SocketTransmitter channel, LogInterface log) throws Exception {
		return this.updateFileThread(strThreadName, strClassName, strStartupType, strThreadID, channel, log, true);
	}

	protected boolean updateFileThread(String strThreadName, String strClassName, String strStartupType,
			String strThreadID, SocketTransmitter channel, LogInterface log, boolean blnSynCluster) throws Exception {
		String var8 = ThreadConstant.THREAD_CONFIG_FILE;
		synchronized (ThreadConstant.THREAD_CONFIG_FILE) {
			Dictionary dicThreadList = loadThreadConfig();
			DictionaryNode ndThread = dicThreadList.mndRoot.getChild(strThreadID);
			if (ndThread == null) {
				return false;
			} else {
				DictionaryNode nd = null;
				nd = ndThread.getChild("ThreadName", true);
				String strOldThreadName = StringUtil.nvl(nd.mstrValue, "");
				nd.mstrValue = strThreadName;
				nd = ndThread.getChild("ClassName", true);
				String strOldClassName = StringUtil.nvl(nd.mstrValue, "");
				nd.mstrValue = strClassName;
				nd = ndThread.getChild("StartupType", true);
				String strOldStartupType = StringUtil.nvl(nd.mstrValue, "");
				nd.mstrValue = strStartupType;
				this.storeThreadConfig(dicThreadList, blnSynCluster);
				if (log != null && channel != null) {
					String strLogID = log.logHeader(ThreadConstant.SYSTEM_THREAD_MANAGER, channel.getUserName(), "U");
					String strChangeID = log.logTableChange(strLogID, this.getThreadTableName(), strThreadID, "Update");
					log.logColumnChange(strChangeID, this.getThreadNameFieldName(), strOldThreadName, strThreadName);
					log.logColumnChange(strChangeID, this.getClassNameFieldName(), strOldClassName, strClassName);
					log.logColumnChange(strChangeID, this.getStartupTypeFieldName(), strOldStartupType, strStartupType);
				}

				return true;
			}
		}
	}

	public static synchronized Dictionary loadThreadConfig() throws Exception {
		return loadThreadConfig(ThreadConstant.THREAD_CONFIG_FILE);
	}

	public static synchronized Dictionary loadThreadConfig(String strFileName) throws Exception {
		File fl = new File(strFileName);
		if (!fl.exists()) {
			return new Dictionary();
		} else {
			if (mlThreadFileSize != fl.length() || mlThreadFileLastModified != fl.lastModified()) {
				FileInputStream is = null;

				try {
					is = new FileInputStream(fl);
					mdicThreadList = new Dictionary(is);
					mlThreadFileSize = fl.length();
					mlThreadFileLastModified = fl.lastModified();
				} finally {
					FileUtil.safeClose(is);
				}
			}

			return mdicThreadList;
		}
	}

	public synchronized void storeThreadConfig(Dictionary dicThreadList) throws Exception {
		this.storeThreadConfig(dicThreadList, ThreadConstant.THREAD_CONFIG_FILE, true);
	}

	public synchronized void storeThreadConfig(Dictionary dicThreadList, boolean synCluster) throws Exception {
		this.storeThreadConfig(dicThreadList, ThreadConstant.THREAD_CONFIG_FILE, synCluster);
	}

	public synchronized void storeThreadConfig(Dictionary dicThreadList, String strFileName, boolean synCluster)
			throws Exception {
		FileOutputStream os = null;

		try {
			os = new FileOutputStream(strFileName + ".TMP");
			dicThreadList.store(os);
		} finally {
			FileUtil.safeClose(os);
		}

		if (!FileUtil.renameFile(strFileName + ".TMP", strFileName)) {
			throw new Exception("Could not rename file " + strFileName + ".TMP to " + strFileName);
		} else {
			mdicThreadList = dicThreadList;
			File fl = new File(strFileName);
			mlThreadFileSize = fl.length();
			mlThreadFileLastModified = fl.lastModified();
			if (synCluster) {
				this.synThreadConfig();
			}

		}
	}

	public static synchronized void synThreadConfigFile(byte[] byteThreadConfig) throws IOException {
		String temp = System.getProperty("user.dir") + "/TEMP";
		File f = new File(temp);
		SimpleDateFormat sdf;
		if (!f.exists()) {
			f.mkdir();
		} else {
			try {
				File[] files = f.listFiles();
				Arrays.sort(files);
				sdf = new SimpleDateFormat("ddMMyyyy");
				File[] var8 = files;
				int var7 = files.length;

				for (int var6 = 0; var6 < var7; ++var6) {
					File file = var8[var6];

					try {
						String fileName = file.getName();
						int end = fileName.lastIndexOf("_");
						if (end <= 1) {
							file.delete();
						} else {
							int start = fileName.substring(0, end - 1).lastIndexOf("_", end);
							if (start >= 0 && end > 0) {
								String strDate = fileName.substring(start + 1, end);
								Date date = sdf.parse(strDate);
								Calendar c = Calendar.getInstance();
								c.setTime(date);
								c.add(5, 10);
								if (c.getTime().compareTo(new Date()) < 0) {
									file.delete();
								}
							}
						}
					} catch (Exception var15) {
						var15.printStackTrace();
						file.delete();
					}
				}
			} catch (Exception var16) {
				var16.printStackTrace();
			}
		}

		if (byteThreadConfig == null) {
			logger.warn("synThreadConfigFile: receive byteThreadConfig=null from cluster => don't update");
		} else {
			File file = new File(ThreadConstant.THREAD_CONFIG_FILE);
			if (file.exists()) {
				sdf = new SimpleDateFormat("ddMMyyyy");
				String tail = sdf.format(new Date()) + "_" + System.currentTimeMillis();
				FileUtil.renameFile(ThreadConstant.THREAD_CONFIG_FILE,
						temp + "/OLD_" + ThreadConstant.THREAD_CONFIG_FILE + ".BAK_" + tail);
			}

			FileOutputStream fos = new FileOutputStream("SYN_" + ThreadConstant.THREAD_CONFIG_FILE + ".TMP");
			fos.write(byteThreadConfig);
			fos.close();
			FileUtil.renameFile("SYN_" + ThreadConstant.THREAD_CONFIG_FILE + ".TMP", ThreadConstant.THREAD_CONFIG_FILE);
		}
	}

	public static synchronized byte[] loadByThreadConfig() throws Exception {
		File fl = new File(ThreadConstant.THREAD_CONFIG_FILE);
		if (!fl.exists()) {
			return new byte[0];
		} else {
			FileInputStream is = null;

			byte[] byteThreadConfig;
			try {
				is = new FileInputStream(fl);
				int length = is.available();
				byteThreadConfig = new byte[length];
				is.read(byteThreadConfig, 0, length);
			} finally {
				FileUtil.safeClose(is);
			}

			return byteThreadConfig;
		}
	}

	public void synThreadConfig() throws Exception {
		if (this.mmgrMain.getClusterType() != 0) {
			byte[] byteThreadConfig = loadByThreadConfig();
			ClusterCommand command = new ClusterCommand(9, byteThreadConfig);
			ThreadManager.clusterChannel
					.send(new Message((Address) null, ThreadManager.clusterChannel.getAddress(), command));
		}

	}

	public void updateStorage() throws Exception {
		this.updateStorage((SocketTransmitter) null, (LogInterface) null, (Connection) null);
	}

	protected void updateStorage(SocketTransmitter channel, LogInterface log, Connection cn) throws Exception {
		this.updateStorage(channel, log, cn, true);
	}

	protected void updateStorage(SocketTransmitter channel, LogInterface log, Connection cn, boolean mblSynCluster)
			throws Exception {
		if (this.miManageMethod == 1) {
			boolean bConnectionNotExist = cn == null;

			try {
				if (bConnectionNotExist) {
					cn = this.mmgrMain.getConnection();
				}

				this.updateDBThread(cn, this.mstrThreadName, this.mstrClassName, this.mstrStartupType,
						this.mstrThreadID, channel, log);
			} finally {
				if (bConnectionNotExist) {
					Database.closeObject(cn);
				}

			}
		} else {
			this.updateFileThread(this.mstrThreadName, this.mstrClassName, this.mstrStartupType, this.mstrThreadID,
					channel, log, mblSynCluster);
		}

	}

	public void updateStatus() {
		if (this.miManageMethod == 0) {
			try {
				String var1 = ThreadConstant.THREAD_CONFIG_FILE;
				synchronized (ThreadConstant.THREAD_CONFIG_FILE) {
					Dictionary dicThreadList = loadThreadConfig();
					DictionaryNode ndThread = dicThreadList.mndRoot.setChildValue(this.mstrThreadID, (String) null);
					ndThread.setChildValue("Status", String.valueOf(this.miThreadStatus));
					this.storeThreadConfig(dicThreadList);
				}
			} catch (Exception var13) {
				var13.printStackTrace();
			}
		} else {
			Connection cn = null;

			try {
				cn = this.mmgrMain.getConnection();
				String strStatus = String.valueOf(this.miThreadStatus);
				String strSQL = this.getUpdateThreadStatusSQLCommand(strStatus);
				Statement stmt = cn.createStatement();
				stmt.executeUpdate(strSQL);
				stmt.close();
			} catch (Exception var10) {
				var10.printStackTrace();
			} finally {
				Database.closeObject(cn);
			}
		}

	}

	protected String getUpdateThreadStatusSQLCommand(String strStatus) {
		return "UPDATE " + this.getThreadTableName() + " SET STATUS=" + strStatus + " WHERE "
				+ this.getThreadIDFieldName() + "=" + this.mstrThreadID;
	}

	protected void beforeSession() throws Exception {
		if (this.mbAutoConnectDB) {
			this.openConnection();
		}

	}

	protected void afterSession() throws Exception {
		this.closeConnection();
	}

	protected abstract void processSession() throws Exception;

	protected static void removeParameterDefinition(Vector vtParameterList, String strParameter) {
		for (int iIndex = vtParameterList.size() - 1; iIndex >= 0; --iIndex) {
			String strParameterName = (String) ((Vector) vtParameterList.elementAt(iIndex)).elementAt(0);
			if (strParameterName.equals(strParameter)) {
				vtParameterList.removeElementAt(iIndex);
			}
		}

	}

	public boolean isRunning() {
		return this.mabRunning.get();
	}

	public static String getLogType(int iLogType) {
		return iLogType >= 0 && iLogType <= ThreadConstant.LOG_LEVEL.length - 1 ? ThreadConstant.LOG_LEVEL[iLogType]
				: "INFO";
	}

	public static int decodeLogType(String strLogType) {
		for (int i = 0; i < ThreadConstant.LOG_LEVEL.length; ++i) {
			if (strLogType.trim().equalsIgnoreCase(ThreadConstant.LOG_LEVEL[i])) {
				return i;
			}
		}

		return 2;
	}

	public void setGroupName(String groupName) {
		this.mstrGroupName = groupName;
	}

	public String getGroupName() {
		return this.mstrGroupName;
	}

	public void setGroupID(String strGroupID) {
		this.mstrGroupID = strGroupID;
	}

	public String getGroupID() {
		return this.mstrGroupID;
	}

	protected boolean insertFileThread(String strThreadName, String strClassName, String strStartupType,
			String strThreadID, SocketTransmitter channel, LogInterface log) throws Exception {
		String var7 = ThreadConstant.THREAD_CONFIG_FILE;
		synchronized (ThreadConstant.THREAD_CONFIG_FILE) {
			Dictionary dicThreadList = loadThreadConfig();
			DictionaryNode ndThread = dicThreadList.mndRoot.getChild(strThreadID, true);
			DictionaryNode nd = null;
			nd = ndThread.getChild("ThreadName", true);
			String strOldThreadName = StringUtil.nvl(nd.mstrValue, "");
			nd.mstrValue = strThreadName;
			nd = ndThread.getChild("ClassName", true);
			String strOldClassName = StringUtil.nvl(nd.mstrValue, "");
			nd.mstrValue = strClassName;
			nd = ndThread.getChild("StartupType", true);
			String strOldStartupType = StringUtil.nvl(nd.mstrValue, "");
			nd.mstrValue = strStartupType;
			this.storeThreadConfig(dicThreadList);
			if (log != null && channel != null) {
				String strLogID = log.logHeader(ThreadConstant.SYSTEM_THREAD_MANAGER, channel.getUserName(), "I");
				String strChangeID = log.logTableChange(strLogID, this.getThreadTableName(), strThreadID, "Insert");
				log.logColumnChange(strChangeID, this.getThreadNameFieldName(), strOldThreadName, strThreadName);
				log.logColumnChange(strChangeID, this.getClassNameFieldName(), strOldClassName, strClassName);
				log.logColumnChange(strChangeID, this.getStartupTypeFieldName(), strOldStartupType, strStartupType);
			}

			return true;
		}
	}

	protected boolean insertDBThread(Connection cn, String strThreadName, String strThreadClass,
			String strThreadStartupType, String strThreadID, SocketTransmitter channel, LogInterface log)
					throws Exception {
		String strLogID = null;
		if (log != null && channel != null) {
			strLogID = log.logHeader(ThreadConstant.SYSTEM_THREAD_MANAGER, channel.getUserName(), "U");
		}

		String strSQL = "INSERT INTO thread (thread_id, thread_name, class_name, startup_type, status)  VALUES (?,?,?,?,?)";
		cn.prepareStatement(strSQL);
		PreparedStatement stmt = cn.prepareStatement(strSQL);
		stmt.setInt(1, Integer.parseInt(strThreadID));
		stmt.setString(2, strThreadName);
		stmt.setString(3, strThreadClass);
		stmt.setInt(4, Integer.parseInt(strThreadStartupType));
		stmt.setString(5, "2");
		int iCount = stmt.executeUpdate();
		stmt.close();
		if (log != null && channel != null) {
			log.logAfterInsert(strLogID, "THREAD", this.getThreadIDFieldName() + "=" + strThreadID);
		}

		return iCount > 0;
	}
}
